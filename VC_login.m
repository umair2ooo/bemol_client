#import "VC_login.h"

#import "Categories.h"

@interface VC_login ()
{
}

@property (weak, nonatomic) IBOutlet UITextField *text_UserName;
@property (weak, nonatomic) IBOutlet UITextField *text_Password;
@property (weak, nonatomic) IBOutlet UIButton *button_savePassword;

- (IBAction)action_login:(id)sender;
- (IBAction)action_goDirectToHome:(id)sender;
- (IBAction)action_forgotPassword:(id)sender;
- (IBAction)action_savePass:(id)sender;


@end

@implementation VC_login

- (void)viewDidLoad{
    [super viewDidLoad];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ApplicationDelegate.single setFontFamily:k_fontName forView:self.view andSubViews:YES];
    });
}

-(void)viewWillAppear:(BOOL)animated{
    
    //ApplicationDelegate.single.delegate_ = self;
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:k_password])
    {
        self.text_UserName.text = [[NSUserDefaults standardUserDefaults] valueForKey:k_userName];
        self.text_Password.text = [[NSUserDefaults standardUserDefaults] valueForKey:k_password];
        self.button_savePassword.selected = YES;
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated
{
}


#pragma mark - action_forgotPassword
- (IBAction)action_forgotPassword:(id)sender{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Não é possível acessar a sua conta?"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = NSLocalizedString(@"Email", @"Email");
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];

    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action)
                               {
                                   UITextField *textField_forgetPass = alertController.textFields.firstObject;
                                   
                                   if ([ApplicationDelegate.single method_NSStringIsValidEmail:textField_forgetPass.text]){
                                       NSLog(@"OK action");
                                       NSLog(@"Login value: %@",textField_forgetPass.text);
                                       
//                                       [ServiceModel sharedClient].delegate_ = self;
//                                       [[ServiceModel sharedClient] method_forgotPassword:textField_forgetPass.text];
                                       [[ServiceModel sharedClient]POST:k_forgotPassword
                                                            parameters:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                        textField_forgetPass.text,@"logonId",
                                                                        @"-",@"challengeAnswer", nil]
                                                                onView:self.view
                                                               success:^(AFHTTPRequestOperation *operation, id responseObject)
                                        {
                                            if (![responseObject valueForKey:@"errors"])
                                            {
                                                [UtilitiesHelper showAlert:[NSString stringWithFormat:@"que enviou um e-mail para %@",textField_forgetPass.text]];
                                            }
                                            else
                                            {
                                                [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                                                            valueForKey:@"errorMessage"]];
                                            }
                                            
                                        }
                                                               failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                        {
                                            DLog(@"%@",error.description);
                                            //[UtilitiesHelper showAlert:error.description];
                                            [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
                                        }];

                                       
                                   }
                                   else{
                                       [[[UIAlertView alloc] initWithTitle:nil
                                                                   message:@"Por favor insira o endereço de e-mail válido"
                                                                  delegate:nil
                                                         cancelButtonTitle:@"Ok"
                                                         otherButtonTitles:nil, nil]
                                        show];
                                   }
                               }];
    
    okAction.enabled = NO;
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark -
#pragma mark === UITextField - UIControlEventEditingChanged ===
#pragma mark -

- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UITextField *login = alertController.textFields.firstObject;
        UIAlertAction *okAction = alertController.actions.lastObject;
        okAction.enabled = login.text.length > 2;
    }
}


#pragma mark - action_login
- (IBAction)action_login:(id)sender
{
    if ([self.text_UserName.text length] && [self.text_Password.text length])
    {
        if ([ApplicationDelegate.single method_NSStringIsValidEmail:self.text_UserName.text])
        {
            NSDictionary *parameters;
            NSString *url;

            if ([ApplicationDelegate.single.string_orderId length])
            {
                url = k_loginPerson;
                DLog(@"%@", ApplicationDelegate.single.string_orderId);
                parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                              self.text_UserName.text,@"logonId",
                              self.text_Password.text,@"logonPassword",
                              @"-1,-5,-6",@"x_calculationUsage",
                              ApplicationDelegate.single.string_orderId,@"orderId",nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:k_getShopingCartNotification object:nil];
            }
            else
            {
                url = k_LoginIdentity;
                parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                              self.text_UserName.text,@"logonId",
                              self.text_Password.text,@"logonPassword", nil];
            }
            
            [[ServiceModel sharedClient] POST:url
                                   parameters:parameters
                                       onView:self.view
                                      success:^(AFHTTPRequestOperation *operation, id responseObject)
            {
                DLog(@"%@",responseObject);
                
                if ([responseObject valueForKey:@"WCToken"])
                {
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_WCToken];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_WCTrustedToken];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_personalizationID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_userId];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_WCToken] forKey:k_WCToken];
                    [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_WCTrustedToken] forKey:k_WCTrustedToken];
                    [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_personalizationID
                                                                      ] forKey:k_personalizationID];
                    [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:k_isLogin];

                    
                    
                    DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_userId]);
                    DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken]);
                    DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken]);
                    DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_personalizationID]);
                    
                    DLog(@"%@", [[responseObject valueForKey:k_userId] class]);
                    
                    if ([[responseObject valueForKey:k_userId] isKindOfClass:[NSNumber class]])
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[responseObject valueForKey:k_userId]] forKey:k_userId];
                        if ([responseObject valueForKey:k_userId] != [[NSUserDefaults standardUserDefaults] valueForKey:k_userId])
                        {
                            [[NSUserDefaults standardUserDefaults] setValue:nil forKey:k_count];
                        }
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_userId] forKey:k_userId];
                        if (![[responseObject valueForKey:k_userId] isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:k_userId]])
                        {
                            [[NSUserDefaults standardUserDefaults] setValue:nil forKey:k_count];
                        }
                    }
                    
                     DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_userId]);
                    
                    if (self.button_savePassword.selected)
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:self.text_UserName.text forKey:k_userName];
                        [[NSUserDefaults standardUserDefaults] setObject:self.text_Password.text forKey:k_password];
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:k_userName];
                        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:k_password];
                    }
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    //[self performSegueWithIdentifier:@"segue_home" sender:nil];
                    
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                else
                {
                    if ([responseObject valueForKey:@"errors"])
                    {
                        [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
                    }
                    else
                    {
                        [UtilitiesHelper showAlert:@"A autenticação falhou"];
                    }
                }

                                          
            }
                                      failure:^(AFHTTPRequestOperation *operation, NSError *error)
            {
                DLog(@"%@",error);
                //[UtilitiesHelper showAlert:error.description];
                [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
            }];
            
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:nil
                                        message:@"Por favor insira o endereço de e-mail válido"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
    else
    {
        if ([self.text_UserName.text length] == 0)
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Por favor, insira o nome do usuário"
                                       delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Por favor, insira a senha"
                                       delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
}

#pragma mark - action_savePass
- (IBAction)action_savePass:(id)sender{
    
    if (self.button_savePassword.selected){
        self.button_savePassword.selected = NO;
    }
    else{
        self.button_savePassword.selected = YES;
    }
}


#pragma mark - action_goDirectToHome
- (IBAction)action_goDirectToHome:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self performSegueWithIdentifier:@"segue_home" sender:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - web service delegate
-(void)delegate_responseData:(id)data error:(NSError *)error servicename:(NSString *)servicename
{
        if (data)
        {
            if ([servicename isEqualToString:k_forgetPassword])
            {
                [[[UIAlertView alloc] initWithTitle:nil
                                            message:data
                                           delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil, nil]
                 show];
            }
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:nil
                                        message:error.description
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil] show];
        }
        
}


@end