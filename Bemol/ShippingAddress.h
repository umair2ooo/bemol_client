#import <Foundation/Foundation.h>

@interface ShippingAddress : NSObject {

    NSString *addressId;
    NSString *bairo;
    NSString *cep;
    NSString *cidade;
    NSString *endereco;
    NSString *firstName;
    NSString *nickName;
    NSString *numero;
    NSString *uf;
    NSString *complemento;
}

@property (nonatomic, copy) NSString *addressId;
@property (nonatomic, copy) NSString *bairo;
@property (nonatomic, copy) NSString *cep;
@property (nonatomic, copy) NSString *cidade;
@property (nonatomic, copy) NSString *endereco;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *numero;
@property (nonatomic, copy) NSString *uf;
@property (nonatomic, copy) NSString *complemento;

+ (ShippingAddress *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
