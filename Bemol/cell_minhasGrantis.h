//
//  cell_minhasGrantis.h
//  Bemol
//
//  Created by Fahim Bilwani on 19/11/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cell_minhasGrantis : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label_description;
@property (weak, nonatomic) IBOutlet UILabel *label_SKU;
@property (weak, nonatomic) IBOutlet UILabel *label_date;
@property (weak, nonatomic) IBOutlet UILabel *label_status;

@end
