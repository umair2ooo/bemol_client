#import <Foundation/Foundation.h>

@interface ProductListPrice : NSObject {

    NSString *priceDescription;
    NSString *priceUsage;
    NSString *priceValue;

}

@property (nonatomic, copy) NSString *priceDescription;
@property (nonatomic, copy) NSString *priceUsage;
@property (nonatomic, copy) NSString *priceValue;

+ (ProductListPrice *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
