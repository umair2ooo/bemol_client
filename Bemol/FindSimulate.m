#import "FindSimulate.h"

#import "findSimulateSimulateMap.h"

@implementation FindSimulate

@synthesize simulateMap;

+ (FindSimulate *)instanceFromDictionary:(NSDictionary *)aDictionary {

    FindSimulate *instance = [[FindSimulate alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.simulateMap = [findSimulateSimulateMap instanceFromDictionary:[aDictionary objectForKey:@"SimulateMap"]];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.simulateMap) {
        [dictionary setObject:self.simulateMap forKey:@"simulateMap"];
    }

    return dictionary;

}


@end
