//
//  VC_address.m
//  Bemol
//
//  Created by Fahim Bilwani on 16/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_address.h"
#import "cell_address.h"
#import "ShippingAddress.h"
#import "CheckoutShippingAddress.h"
#import "VC_addNewAddress.h"

@interface VC_address ()
{
    NSMutableArray *array_addresses;
    NSInteger selectedIndex;
}
@property (weak, nonatomic) IBOutlet UITableView *tableview_;

@property(nonatomic, strong) ShippingAddress *obj_shippingAddress;
@property(nonatomic, strong) CheckoutShippingAddress *obj_checkoutShippingAddress;



- (IBAction)action_addnewAddress:(id)sender;
- (IBAction)action_edit:(id)sender;

@end

@implementation VC_address

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Carrinho"];
    
    
    [self getAllShippingAddress];
    
//    array_addresses = [[NSMutableArray alloc] initWithObjects:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Minha casa",@"nome",@"ashwini",@"Destinatario",@"69099-254",@"CEP",@"Rua santa Luzia Numero: 1055",@"Endereco",@"Cidade de Deus",@"Bairro",@"Manaus, AM",@"Cidad/UF", nil], nil];
//    [array_addresses addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Casa da vovo",@"nome",@"Vovo Maria",@"Destinatario",@"69099-099",@"CEP",@"Rua Maria Luzia Numero: 10",@"Endereco",@"Cidade de Deus",@"Bairro",@"Manaus, AM",@"Cidad/UF", nil]];
//    [self.tableview_ reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    array_addresses = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getAllShippingAddress
-(void)getAllShippingAddress
{
    [[ServiceModel sharedClient] GET:k_getAllShippingAddress
                          parameters:nil
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             DLog(@"%@",responseObject);
             self.obj_checkoutShippingAddress = [CheckoutShippingAddress instanceFromDictionary:responseObject];
             [self.tableview_ reloadData];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}

- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - tableview dataSource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.obj_checkoutShippingAddress.shippingAddresses count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell_address";
    
    cell_address *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[cell_address alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    self.obj_shippingAddress = [self.obj_checkoutShippingAddress.shippingAddresses objectAtIndex:indexPath.row];
    
    cell.button_edit.tag = indexPath.row;
    cell.label_address.text = self.obj_shippingAddress.firstName;
    cell.label_cep.text = self.obj_shippingAddress.cep;
    cell.label_endereco.text = self.obj_shippingAddress.endereco;
    cell.label_bairro.text = self.obj_shippingAddress.bairo;
    cell.label_cidad.text = [NSString stringWithFormat:@"%@, %@",self.obj_shippingAddress.cidade,self.obj_shippingAddress.uf];
    cell.label_nome.text = self.obj_shippingAddress.numero;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //segue_exitToCheckout
    self.obj_shippingAddress = [self.obj_checkoutShippingAddress.shippingAddresses objectAtIndex:indexPath.row];
    
    if (![self.obj_shippingAddress.complemento isKindOfClass:[NSNull class]])
    {
         self.dic_selectedAddress = [NSMutableDictionary dictionaryWithDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:self.obj_shippingAddress.numero,@"nome",self.obj_shippingAddress.firstName,@"Destinatario",self.obj_shippingAddress.cep,@"CEP",self.obj_shippingAddress.endereco,@"Endereco",self.obj_shippingAddress.bairo,@"Bairro",[NSString stringWithFormat:@"%@, %@",self.obj_shippingAddress.cidade,self.obj_shippingAddress.uf],@"Cidad/UF",self.obj_shippingAddress.addressId,@"addressId",self.obj_shippingAddress.complemento,@"complemento", nil]];
    }
    else
    {
         self.dic_selectedAddress = [NSMutableDictionary dictionaryWithDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:self.obj_shippingAddress.numero,@"nome",self.obj_shippingAddress.firstName,@"Destinatario",self.obj_shippingAddress.cep,@"CEP",self.obj_shippingAddress.endereco,@"Endereco",self.obj_shippingAddress.bairo,@"Bairro",[NSString stringWithFormat:@"%@, %@",self.obj_shippingAddress.cidade,self.obj_shippingAddress.uf],@"Cidad/UF",self.obj_shippingAddress.addressId,@"addressId",self.obj_shippingAddress.complemento,@"", nil]];
    }
   
    [self performSegueWithIdentifier:@"segue_exitToCheckout" sender:nil];
}

#pragma mark - action_addnewAddress
- (IBAction)action_addnewAddress:(id)sender
{
    [self performSegueWithIdentifier:@"segue_addressform" sender:[NSNumber numberWithBool:NO]];
}

#pragma mark - action_edit
- (IBAction)action_edit:(id)sender
{
    selectedIndex = [sender tag];
   [self performSegueWithIdentifier:@"segue_addressform" sender:[NSNumber numberWithBool:YES]];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_addressform"])
    {
        VC_addNewAddress *obj = segue.destinationViewController;
        obj.bool_comingfromedit = [sender boolValue];
        if (obj.bool_comingfromedit)
        {
            self.obj_shippingAddress = [self.obj_checkoutShippingAddress.shippingAddresses objectAtIndex:selectedIndex];
            DLog(@"%@",self.obj_shippingAddress.firstName);
            obj.obj_shippingAddress = self.obj_shippingAddress;
        }
        else
            obj.obj_shippingAddress = nil;
        
    }
}
@end
