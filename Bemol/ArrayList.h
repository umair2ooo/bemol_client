#import <Foundation/Foundation.h>

@interface ArrayList : NSObject {

    NSNumber *comEntrada;
    NSString *dataPedido;
    NSNumber *indexPlana;
    NSString *sapCode;

}

@property (nonatomic, copy) NSNumber *comEntrada;
@property (nonatomic, copy) NSString *dataPedido;
@property (nonatomic, copy) NSNumber *indexPlana;
@property (nonatomic, copy) NSString *sapCode;

+ (ArrayList *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
