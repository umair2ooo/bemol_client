#import "CatalogEntryView.h"

#import "Attribute.h"
#import "Price.h"
#import "XproductReviewList.h"

@implementation CatalogEntryView

@synthesize attributes;
@synthesize buyable;
@synthesize fullImage;
@synthesize fullImageAltDescription;
@synthesize keyword;
@synthesize metaDescription;
@synthesize metaKeyword;
@synthesize name;
@synthesize parentCategoryID;
@synthesize partNumber;
@synthesize price;
@synthesize productType;
@synthesize resourceId;
@synthesize shortDescription;
@synthesize storeID;
@synthesize thumbnail;
@synthesize title;
@synthesize uniqueID;
@synthesize xproductReviewList;
@synthesize longDescription;

+ (CatalogEntryView *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CatalogEntryView *instance = [[CatalogEntryView alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }


    NSArray *receivedAttributes = [aDictionary objectForKey:@"Attributes"];
    if ([receivedAttributes isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedAttributes = [NSMutableArray arrayWithCapacity:[receivedAttributes count]];
        for (NSDictionary *item in receivedAttributes) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedAttributes addObject:[Attribute instanceFromDictionary:item]];
            }
        }

        self.attributes = populatedAttributes;

    }
    self.buyable = [aDictionary objectForKey:@"buyable"];
    self.fullImage = [aDictionary objectForKey:@"fullImage"];
    self.fullImageAltDescription = [aDictionary objectForKey:@"fullImageAltDescription"];
    self.keyword = [aDictionary objectForKey:@"keyword"];
    self.metaDescription = [aDictionary objectForKey:@"metaDescription"];
    self.metaKeyword = [aDictionary objectForKey:@"metaKeyword"];
    self.name = [aDictionary objectForKey:@"name"];
    self.parentCategoryID = [aDictionary objectForKey:@"parentCategoryID"];
    self.partNumber = [aDictionary objectForKey:@"partNumber"];

    NSArray *receivedPrice = [aDictionary objectForKey:@"Price"];
    if ([receivedPrice isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedPrice = [NSMutableArray arrayWithCapacity:[receivedPrice count]];
        for (NSDictionary *item in receivedPrice) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedPrice addObject:[Price instanceFromDictionary:item]];
            }
        }

        self.price = populatedPrice;

    }
    self.productType = [aDictionary objectForKey:@"productType"];
    self.resourceId = [aDictionary objectForKey:@"resourceId"];
    self.shortDescription = [aDictionary objectForKey:@"shortDescription"];
    self.longDescription = [aDictionary objectForKey:@"longDescription"];
    self.storeID = [aDictionary objectForKey:@"storeID"];
    self.thumbnail = [aDictionary objectForKey:@"thumbnail"];
    self.title = [aDictionary objectForKey:@"title"];
    self.uniqueID = [aDictionary objectForKey:@"uniqueID"];

    NSArray *receivedXproductReviewList = [aDictionary objectForKey:@"xproductReviewList"];
    if ([receivedXproductReviewList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedXproductReviewList = [NSMutableArray arrayWithCapacity:[receivedXproductReviewList count]];
        for (NSDictionary *item in receivedXproductReviewList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedXproductReviewList addObject:[XproductReviewList instanceFromDictionary:item]];
            }
        }

        self.xproductReviewList = populatedXproductReviewList;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.attributes) {
        [dictionary setObject:self.attributes forKey:@"attributes"];
    }

    if (self.buyable) {
        [dictionary setObject:self.buyable forKey:@"buyable"];
    }

    if (self.fullImage) {
        [dictionary setObject:self.fullImage forKey:@"fullImage"];
    }

    if (self.fullImageAltDescription) {
        [dictionary setObject:self.fullImageAltDescription forKey:@"fullImageAltDescription"];
    }

    if (self.keyword) {
        [dictionary setObject:self.keyword forKey:@"keyword"];
    }

    if (self.metaDescription) {
        [dictionary setObject:self.metaDescription forKey:@"metaDescription"];
    }

    if (self.metaKeyword) {
        [dictionary setObject:self.metaKeyword forKey:@"metaKeyword"];
    }

    if (self.name) {
        [dictionary setObject:self.name forKey:@"name"];
    }

    if (self.parentCategoryID) {
        [dictionary setObject:self.parentCategoryID forKey:@"parentCategoryID"];
    }

    if (self.partNumber) {
        [dictionary setObject:self.partNumber forKey:@"partNumber"];
    }

    if (self.price) {
        [dictionary setObject:self.price forKey:@"price"];
    }

    if (self.productType) {
        [dictionary setObject:self.productType forKey:@"productType"];
    }

    if (self.resourceId) {
        [dictionary setObject:self.resourceId forKey:@"resourceId"];
    }

    if (self.shortDescription) {
        [dictionary setObject:self.shortDescription forKey:@"shortDescription"];
    }
    
    if (self.longDescription) {
        [dictionary setObject:self.longDescription forKey:@"longDescription"];
    }

    if (self.storeID) {
        [dictionary setObject:self.storeID forKey:@"storeID"];
    }

    if (self.thumbnail) {
        [dictionary setObject:self.thumbnail forKey:@"thumbnail"];
    }

    if (self.title) {
        [dictionary setObject:self.title forKey:@"title"];
    }

    if (self.uniqueID) {
        [dictionary setObject:self.uniqueID forKey:@"uniqueID"];
    }

    if (self.xproductReviewList) {
        [dictionary setObject:self.xproductReviewList forKey:@"xproductReviewList"];
    }

    return dictionary;

}


@end
