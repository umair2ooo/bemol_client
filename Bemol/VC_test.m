#import "VC_test.h"

@interface VC_test ()

@property (weak, nonatomic) IBOutlet UIWebView *webview_;

- (IBAction)action_cancel:(id)sender;

@end

@implementation VC_test

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //    NSString *urlString = [NSString stringWithFormat:@"%@webapp/wcs/stores/servlet/m20UserRegistrationAddForm?storeId=10001&langId=-6&catalogId=10001&register_button=Criar+novo+cadastro", k_url ];
    
    NSString *urlString = [NSString stringWithFormat:@"https://www.bemol.com.br/webapp/wcs/stores/servlet/m20UserRegistrationAddForm?storeId=10001&langId=-6&catalogId=10001&register_button=Criar+novo+cadastro"];

    DLog(@"%@", urlString);
    
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [self.webview_ loadRequest:req];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


//#pragma mark - delegates
//- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
//    return YES;
//}
//
//
//- (void)webViewDidStartLoad:(UIWebView *)webView{
//}
//
//
//- (void)webViewDidFinishLoad:(UIWebView *)webView{
//}
//
//
//
//- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
//}
//
//
//- (void)URLProtocol:(NSURLProtocol *)protocol didReceiveResponse:(NSURLResponse *)response cacheStoragePolicy:(NSURLCacheStoragePolicy)policy{
//}

@end