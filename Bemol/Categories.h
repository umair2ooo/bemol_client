#import <Foundation/Foundation.h>

@interface Categories : NSObject

@property(nonatomic, copy)NSString *identifier;
@property(nonatomic, copy)NSString *name;
@property(nonatomic, copy)NSString *productsURL;
@property(nonatomic, copy)NSString *resourceId;
@property(nonatomic, copy)NSString *thumbnail;
@property(nonatomic, copy)NSString *uniqueID;
@property(nonatomic, copy)NSString *shortDescription;


@end