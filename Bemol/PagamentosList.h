#import <Foundation/Foundation.h>

@interface PagamentosList : NSObject {

    NSString *contractNumber;
    NSString *datePagamento;
    NSString *dateVencimiento;
    NSString *parcela;
    NSString *qtyParcela;
    NSString *status;
    NSString *valor;
    NSString *valorDesconto;

}

@property (nonatomic, copy) NSString *contractNumber;
@property (nonatomic, copy) NSString *datePagamento;
@property (nonatomic, copy) NSString *dateVencimiento;
@property (nonatomic, copy) NSString *parcela;
@property (nonatomic, copy) NSString *qtyParcela;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *valor;
@property (nonatomic, copy) NSString *valorDesconto;

+ (PagamentosList *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
