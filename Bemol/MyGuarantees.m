#import "MyGuarantees.h"

#import "GarantiaList.h"

@implementation MyGuarantees

@synthesize countTotal;
@synthesize garantiaList;

+ (MyGuarantees *)instanceFromDictionary:(NSDictionary *)aDictionary {

    MyGuarantees *instance = [[MyGuarantees alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.countTotal = [aDictionary objectForKey:@"countTotal"];

    NSArray *receivedGarantiaList = [aDictionary objectForKey:@"garantiaList"];
    if ([receivedGarantiaList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedGarantiaList = [NSMutableArray arrayWithCapacity:[receivedGarantiaList count]];
        for (NSDictionary *item in receivedGarantiaList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedGarantiaList addObject:[GarantiaList instanceFromDictionary:item]];
            }
        }

        self.garantiaList = populatedGarantiaList;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.countTotal) {
        [dictionary setObject:self.countTotal forKey:@"countTotal"];
    }

    if (self.garantiaList) {
        [dictionary setObject:self.garantiaList forKey:@"garantiaList"];
    }

    return dictionary;

}


@end
