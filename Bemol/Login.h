#import <Foundation/Foundation.h>

@interface Login : NSObject
@property(nonatomic, copy)NSString *userId;
@property(nonatomic, copy)NSString *WCToken;
@property(nonatomic, copy)NSString *WCTrustedToken;
@property(nonatomic, copy)NSString *personalizationID;
@end