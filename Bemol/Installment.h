#import <Foundation/Foundation.h>

@class InstallmentCredFlexMap;
@class InstallmentInstallmentMap;

@interface Installment : NSObject {

    InstallmentCredFlexMap *credFlexMap;
    InstallmentInstallmentMap *installmentMap;

}

@property (nonatomic, strong) InstallmentCredFlexMap *credFlexMap;
@property (nonatomic, strong) InstallmentInstallmentMap *installmentMap;

+ (Installment *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
