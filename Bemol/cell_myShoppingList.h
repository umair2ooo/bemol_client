//
//  cell_myShoppingList.h
//  Bemol
//
//  Created by Fahim Bilwani on 01/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cell_myShoppingList : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label_description;
@property (weak, nonatomic) IBOutlet UILabel *label_SKU;
@property (weak, nonatomic) IBOutlet UILabel *label_price;
@property (weak, nonatomic) IBOutlet UILabel *label_status;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_circle;
@end
