#import "ProductDetail.h"

#import "CatalogEntryView.h"
#import "MetaDatum.h"

@implementation ProductDetail

@synthesize catalogEntryView;
@synthesize metaData;
@synthesize recordSetComplete;
@synthesize recordSetCount;
@synthesize recordSetStartNumber;
@synthesize recordSetTotal;
@synthesize resourceId;
@synthesize resourceName;

+ (ProductDetail *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ProductDetail *instance = [[ProductDetail alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }


    NSArray *receivedCatalogEntryView = [aDictionary objectForKey:@"CatalogEntryView"];
    if ([receivedCatalogEntryView isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedCatalogEntryView = [NSMutableArray arrayWithCapacity:[receivedCatalogEntryView count]];
        for (NSDictionary *item in receivedCatalogEntryView) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedCatalogEntryView addObject:[CatalogEntryView instanceFromDictionary:item]];
            }
        }

        self.catalogEntryView = populatedCatalogEntryView;

    }

    NSArray *receivedMetaData = [aDictionary objectForKey:@"MetaData"];
    if ([receivedMetaData isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedMetaData = [NSMutableArray arrayWithCapacity:[receivedMetaData count]];
        for (NSDictionary *item in receivedMetaData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedMetaData addObject:[MetaDatum instanceFromDictionary:item]];
            }
        }

        self.metaData = populatedMetaData;

    }
    self.recordSetComplete = [aDictionary objectForKey:@"recordSetComplete"];
    self.recordSetCount = [aDictionary objectForKey:@"recordSetCount"];
    self.recordSetStartNumber = [aDictionary objectForKey:@"recordSetStartNumber"];
    self.recordSetTotal = [aDictionary objectForKey:@"recordSetTotal"];
    self.resourceId = [aDictionary objectForKey:@"resourceId"];
    self.resourceName = [aDictionary objectForKey:@"resourceName"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.catalogEntryView) {
        [dictionary setObject:self.catalogEntryView forKey:@"catalogEntryView"];
    }

    if (self.metaData) {
        [dictionary setObject:self.metaData forKey:@"metaData"];
    }

    if (self.recordSetComplete) {
        [dictionary setObject:self.recordSetComplete forKey:@"recordSetComplete"];
    }

    if (self.recordSetCount) {
        [dictionary setObject:self.recordSetCount forKey:@"recordSetCount"];
    }

    if (self.recordSetStartNumber) {
        [dictionary setObject:self.recordSetStartNumber forKey:@"recordSetStartNumber"];
    }

    if (self.recordSetTotal) {
        [dictionary setObject:self.recordSetTotal forKey:@"recordSetTotal"];
    }

    if (self.resourceId) {
        [dictionary setObject:self.resourceId forKey:@"resourceId"];
    }

    if (self.resourceName) {
        [dictionary setObject:self.resourceName forKey:@"resourceName"];
    }

    return dictionary;

}


@end
