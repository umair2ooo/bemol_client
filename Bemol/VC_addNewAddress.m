//
//  VC_addNewAddress.m
//  Bemol
//
//  Created by Fahim Bilwani on 16/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_addNewAddress.h"
#import "VC_checkout.h"


@interface VC_addNewAddress ()

@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UIButton *button_summit;
@property (weak, nonatomic) IBOutlet UITextField *textfield_nome;
@property (weak, nonatomic) IBOutlet UITextField *textfield_Destinatario;
@property (weak, nonatomic) IBOutlet UITextField *textfield_cep;
@property (weak, nonatomic) IBOutlet UITextField *textfield_endereco;
@property (weak, nonatomic) IBOutlet UITextField *textfield_numero;
@property (weak, nonatomic) IBOutlet UITextField *textfield_barrio;
@property (weak, nonatomic) IBOutlet UITextField *textfield_complemento;
@property (weak, nonatomic) IBOutlet UITextField *textfield_estado;
@property (weak, nonatomic) IBOutlet UITextField *textfield_pais;

@end

@implementation VC_addNewAddress

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.scroller setContentSize:CGSizeMake(0,self.button_summit.frame.size.height+self.button_summit.frame.origin.y)];
    [self.navigationItem setTitle:@"Carrinho"];
    
    if (self.bool_comingfromedit)
    {
        [self method_setContents];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)method_setContents
{
    self.textfield_Destinatario.text = self.obj_shippingAddress.firstName;
    self.textfield_cep.text = self.obj_shippingAddress.cep;
    self.textfield_endereco.text = self.obj_shippingAddress.endereco;
    self.textfield_numero.text = self.obj_shippingAddress.numero;
    self.textfield_barrio.text = self.obj_shippingAddress.bairo;
}

- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_Submit:(id)sender
{
    if (![self.textfield_nome.text length])
    {
        [UtilitiesHelper showAlert:@"entrar Nome do"];
    }
    else if (![self.textfield_Destinatario.text length])
    {
        [UtilitiesHelper showAlert:@"entrar Destinatario"];
    }
    else if (![self.textfield_cep.text length])
    {
        [UtilitiesHelper showAlert:@"entrar CEP"];
    }
    else if (![self.textfield_endereco.text length])
    {
        [UtilitiesHelper showAlert:@"entrar Endereco"];
    }
    else if (![self.textfield_numero.text length])
    {
        [UtilitiesHelper showAlert:@"entrar Numero"];
    }
    else if (![self.textfield_barrio.text length])
    {
        [UtilitiesHelper showAlert:@"entrar Barrio"];
    }
    else if (![self.textfield_complemento.text length])
    {
        [UtilitiesHelper showAlert:@"entrar Complemento"];
    }
    else if (![self.textfield_estado.text length])
    {
        [UtilitiesHelper showAlert:@"entrar Estada"];
    }
    else if (![self.textfield_pais.text length])
    {
        [UtilitiesHelper showAlert:@"entrar Pais/regiao"];
    }
    else
    {
        if (self.bool_comingfromedit)
        {
            [[ServiceModel sharedClient] PUT:[NSString stringWithFormat:@"%@%@",k_addressUpdate,self.obj_shippingAddress.addressId]
                                  parameters:[NSDictionary dictionaryWithObjectsAndKeys:
                                              self.obj_shippingAddress.nickName,@"nickName",
                                              self.textfield_Destinatario.text,@"firstName",
                                              self.textfield_endereco.text,@"endereco",
                                              self.textfield_complemento.text,@"complemento",
                                              self.obj_shippingAddress.cidade,@"city",
                                              @"AM",@"state",
                                              @"BR",@"country",
                                              self.textfield_cep.text,@"zipCode",
                                              @"12",@"numero",
                                              self.textfield_barrio.text,@"bairo",
                                              @"Ponto",@"pointo",
                                              @"S",@"addressType",nil]
                                      onView:self.view
                                     success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 if (![responseObject valueForKey:@"errors"])
                 {
                     DLog(@"%@",responseObject);
                     //VC_checkout *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_checkout"];
                     [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
                     [UtilitiesHelper showAlert:@"endereço atualizado com sucesso"];
                     
                 }
                 else
                 {
                     [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                                 valueForKey:@"errorMessage"]];
                 }
                 
             }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
             }];
        }
        else
        {
            [[ServiceModel sharedClient] POST:k_addressUpdate
                                  parameters:[NSDictionary dictionaryWithObjectsAndKeys:
                                              self.textfield_Destinatario.text,@"nickName",
                                              self.textfield_Destinatario.text,@"firstName",
                                              self.textfield_endereco.text,@"endereco",
                                              self.textfield_complemento.text,@"complemento",
                                              @"Manaus",@"city",
                                              @"AM",@"state",
                                              @"BR",@"country",
                                              self.textfield_cep.text,@"zipCode",
                                              @"12",@"numero",
                                              self.textfield_barrio.text,@"bairo",
                                              @"Ponto",@"pointo",
                                              @"S",@"addressType",nil]
                                      onView:self.view
                                     success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 if (![responseObject valueForKey:@"errors"])
                 {
                     DLog(@"%@",responseObject);
                     self.dic_selectedAddress = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.textfield_nome.text,@"nome",self.textfield_Destinatario.text,@"Destinatario",self.textfield_cep.text,@"CEP",self.textfield_endereco.text,@"Endereco",self.textfield_barrio.text,@"Bairro",self.textfield_pais.text,@"Cidad/UF", nil];
                     [self performSegueWithIdentifier:@"segue_exitToCheckoutViaddNewaddress" sender:nil];
                     [UtilitiesHelper showAlert:@"novo endereço adicionado com sucesso"];
                 }
                 else
                 {
                     [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                                 valueForKey:@"errorMessage"]];
                 }
                 
             }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
             }];
            
            
        }
    }
}


#pragma mark - textField delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField isEqual:self.textfield_cep])
    {
        NSString *text = textField.text;
        if (range.location == 9)
        {
            return NO;
        }
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSMutableString *mutableText = [text mutableCopy];
        if (mutableText.length > 5)
        {
            [mutableText insertString:@"-" atIndex:5];
        }
        text = mutableText;
        textField.text = text;
        return NO;
    }
    else if ([textField isEqual:self.textfield_numero])
    {
        if (range.location == 4)
        {
            return NO;
        }
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:self.textfield_cep])
    {
        DLog(@"hit CEP validation service here");
        if ([self.textfield_cep.text length])
        {
            [self method_checkCEP];
        }
        else
        {
            [UtilitiesHelper showAlert:@"Por favor, indique CEP"];
        }
    }
}

#pragma mark - method_checkCEP
-(void)method_checkCEP
{
    DLog(@"%@", [NSString stringWithFormat:@"%@%@%@", k_url, k_CEP, self.textfield_cep.text]);
    
    [[ServiceModel sharedClient] GET:[NSString stringWithFormat:@"%@%@",k_CEP,self.textfield_cep.text] parameters:nil onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         if (responseObject && ![responseObject valueForKey:@"errors"])
         {
             self.textfield_endereco.text  = [responseObject valueForKey:@"endereco"];
             self.textfield_barrio.text = [responseObject valueForKey:@"bairro"];;
             self.textfield_estado.text = [responseObject valueForKey:@"estado"];
         }
         else
         {
             if ([responseObject valueForKey:@"errors"])
             {
                 [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
             }
             else
             {
                 [UtilitiesHelper showAlert:@"CEP inválido"];
             }
         }
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}




@end
