#import "Entry.h"

@implementation Entry

@synthesize count;
@synthesize entryValue;
@synthesize label;

+ (Entry *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Entry *instance = [[Entry alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.count = [aDictionary objectForKey:@"count"];
    self.entryValue = [aDictionary objectForKey:@"entryValue"];
    self.label = [aDictionary objectForKey:@"label"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.count) {
        [dictionary setObject:self.count forKey:@"count"];
    }

    if (self.entryValue) {
        [dictionary setObject:self.entryValue forKey:@"entryValue"];
    }

    if (self.label) {
        [dictionary setObject:self.label forKey:@"label"];
    }

    return dictionary;

}


@end
