#import <Foundation/Foundation.h>

@interface ContractsContractsMap : NSObject {

    NSMutableArray *list;

}

@property (nonatomic, copy) NSMutableArray *list;

+ (ContractsContractsMap *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
