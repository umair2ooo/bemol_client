//
//  VC_SubCategory.h
//  CaptureLife
//
//  Created by Fahim Bilwani on 4/27/15.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VC_SubCategory : BaseViewController
@property(nonatomic, strong)NSMutableArray *array_categories;
@property(nonatomic, strong)NSString *string_categoryName;
@end
