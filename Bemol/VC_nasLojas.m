#import "VC_nasLojas.h"
#import "Additionalstores.h"
#import "AdditionalstoresAdditionalStoreMap.h"
#import "Cell_nasLojas.h"
#import "NSString+HTML.h"
#import "PhysicalStoreList.h"

@interface VC_nasLojas ()<CLLocationManagerDelegate>
{
    NSString *currentLat;
    NSString *currentLong;
}

@property (nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) Additionalstores *obj_additionalstores;
@property(nonatomic, strong) AdditionalstoresAdditionalStoreMap *obj_additionalStoreMap;
@property(nonatomic, strong) PhysicalStoreList *obj_physicalStoreList;

@property (weak, nonatomic) IBOutlet UITableView *tableview_;
@end

@implementation VC_nasLojas

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.title = self.string_title;
    
    if (![CLLocationManager locationServicesEnabled])
    {
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:@"Ligue Location Service para permitir Bemol para determinar sua localização"
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil]
         show];
    }
    else
    {
        [self method_currentLocation];
    }
    
    [[ServiceModel sharedClient] POST:k_additionalstores
                           parameters:[NSDictionary dictionaryWithObjectsAndKeys:@"false",@"displayAdditionalStores",@"",@"preferredStoreList",self.string_productId,@"itemId", nil]
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             DLog(@"%@",responseObject);
             self.obj_additionalstores = [Additionalstores instanceFromDictionary:responseObject];
             self.obj_additionalStoreMap = self.obj_additionalstores.additionalStoreMap;
             if (![self.obj_additionalStoreMap.physicalStoreList count])
             {
                 [UtilitiesHelper showAlert:@"lojas não disponíveis"];
             }
             [self.tableview_ reloadData];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
     }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];

}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = nil;
    self.locationManager = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)method_currentLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        //for background and foreground
        //[self.locationManager requestAlwaysAuthorization]
        //foreground
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
}


#pragma mark - tableview delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.obj_additionalStoreMap.physicalStoreList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Cell_nasLojas *cell = (Cell_nasLojas *)[tableView dequeueReusableCellWithIdentifier:@"Cell_nasLojas"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Cell_nasLojas" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain)ge
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    self.obj_physicalStoreList = [self.obj_additionalStoreMap.physicalStoreList objectAtIndex:indexPath.row];
    cell.label_name.text = self.obj_physicalStoreList.name;
    cell.label_address.text = self.obj_physicalStoreList.addressLine;
    cell.label_displayvalue.text = [self.obj_physicalStoreList.displayValue kv_stringByStrippingHTML:self.obj_physicalStoreList.displayValue];
    cell.label_number.text = self.obj_physicalStoreList.telephone1;
    cell.label_distance.text = [NSString stringWithFormat:@"%.0f m",[self method_calculateDistance:[NSString stringWithFormat:@"%@",self.obj_physicalStoreList.geoCodeLatitude] lon:[NSString stringWithFormat:@"%@",self.obj_physicalStoreList.geoCodeLongitude]]];
    
    return cell;
}

- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_cancel:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - didUpdateToLocation
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *currentLocation = [locations lastObject];
    if (currentLocation != nil)
    {
        currentLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        currentLong = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    }
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"" message:@"Ligue Location Service para permitir Bemol para determinar sua localização" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
    
}

#pragma mark - method_calculateDistance
-(float)method_calculateDistance:(NSString *)lat lon:(NSString *)lon
{
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[currentLat floatValue] longitude:[currentLong floatValue]];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[lat floatValue] longitude:[lon floatValue]];
    // now calculating the distance using inbuild method distanceFromLocation:
    float distInMeter = [location1 distanceFromLocation:location2]; // which returns in meters
//    //converting meters to mile
//    float distInMile = 0.000621371192 * distInMeter;
    DLog(@"Actual Distance : %0f",distInMeter);
    return distInMeter;
}

@end