//
//  ServiceModel.m
//
//  Created by Waqas Khalid on 10/19/13.
//  Copyright (c) 2013 . All rights reserved.


#import "ServiceModel.h"
#import "UtilitiesHelper.h"


@implementation ServiceModel


+ (instancetype)sharedClient {
    static ServiceModel *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[ServiceModel alloc] initWithBaseURL:[NSURL URLWithString:k_url]];
    });
    
    
    
    return _sharedClient;
}



- (AFHTTPRequestOperation *)POST:(NSString *)URLString
                      parameters:(NSDictionary *)parameters
                          onView:(UIView *)loaderOnView
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure   {
 
    DLog(@"%@",parameters);
    DLog(@"%@",URLString);
    if ([UtilitiesHelper isReachable])
    {
        [UtilitiesHelper showLoader:@"Carregando.." forView:loaderOnView setMode:MBProgressHUDModeIndeterminate delegate:nil];
        
        
        DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_userId]);
        DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken]);
        DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken]);
        DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_personalizationID]);
        
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken] forHTTPHeaderField:k_WCToken];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken] forHTTPHeaderField:k_WCTrustedToken];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_personalizationID] forHTTPHeaderField:k_personalizationID];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_userId] forHTTPHeaderField:k_userId];

        [self.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        
        AFHTTPRequestOperation *operation =  [self POST:URLString parameters:parameters success:success failure:failure];
    
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"response object ====> %@",responseObject);

        [UtilitiesHelper hideLoader:loaderOnView];
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [UtilitiesHelper hideLoader:loaderOnView];
        DLog(@"error ====> %@",error.description);
        if ([error.userInfo valueForKey:@"com.alamofire.serialization.response.error.data"])
        {
            NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:[error.userInfo valueForKey:@"com.alamofire.serialization.response.error.data"] options:
                                  NSJSONReadingMutableContainers error:nil];
            
            DLog(@"%@",JSON);
            
            
            if ([[[[JSON valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorKey"] isEqualToString:@"_ERR_INVALID_COOKIE"])
            {
                //                    [[NSNotificationCenter defaultCenter]
                //                     postNotificationName:k_showLoginNotification
                //                     object:self];
                
                
                
                
                
                [self method_login:^(id data, NSError *error)
                 {
                     if (data)
                     {
                         [self POST:URLString
                         parameters:parameters
                             onView:nil
                            success:^(AFHTTPRequestOperation *operationTwo, id responseObjectTwo)
                          {
                              success(operationTwo,responseObjectTwo);
                          }
                          
                            failure:^(AFHTTPRequestOperation *operationTwo, NSError *error)
                         {
                             failure (operationTwo,error);
                         }];
                     }
                     else
                     {
                         [self method_guestLogin:^(id data, NSError *error) {
                             
                             if (data)
                             {
                                 [self POST:URLString
                                 parameters:parameters
                                     onView:nil
                                    success:^(AFHTTPRequestOperation *operationTwo, id responseObjectTwo)
                                  {
                                      success(operationTwo,responseObjectTwo);
                                  }
                                  
                                    failure:^(AFHTTPRequestOperation *operationTwo, NSError *error)
                                  {
                                      failure (operationTwo,error);
                                  }];
                             }
                             else
                             {
                                 failure (operation,error);
                             }
                         }];
                     }
                 }];
            }
        }
        else
        {
            failure (operation,error);
        }
    }];

        return operation;

    }
    else
        return nil;

}




- (AFHTTPRequestOperation *)GET:(NSString *)URLString
                     parameters:(NSDictionary *)parameters
                         onView:(UIView *)loaderOnView
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    
    if ([UtilitiesHelper isReachable])
    {
        DLog(@"%@",URLString);
        DLog(@"parameters:%@",parameters);

        
        DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_userId]);
        DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken]);
        DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken]);
        DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_personalizationID]);
        
        if (loaderOnView)
        {
            [UtilitiesHelper showLoader:@"Carregando.." forView:loaderOnView setMode:MBProgressHUDModeIndeterminate delegate:nil];
        }

        //[self.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken] forHTTPHeaderField:k_WCToken];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken] forHTTPHeaderField:k_WCTrustedToken];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_personalizationID] forHTTPHeaderField:k_personalizationID];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_userId] forHTTPHeaderField:k_userId];
        
        
        [self.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];

        AFHTTPRequestOperation *operation =  [self GET:URLString parameters:parameters success:success failure:failure];

        self.responseSerializer = [AFJSONResponseSerializer serializer];
    
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
        {
        
            DLog(@"response object ====> %@",responseObject);
        
            [UtilitiesHelper hideLoader:loaderOnView];
            
                                        success(operation,responseObject);

        }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error)
        {
            [UtilitiesHelper hideLoader:loaderOnView];
            if ([error.userInfo valueForKey:@"com.alamofire.serialization.response.error.data"])
            {
                NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:[error.userInfo valueForKey:@"com.alamofire.serialization.response.error.data"] options:
                                      NSJSONReadingMutableContainers error:nil];
                
                DLog(@"%@",JSON);
                
                
                
                if ([[[[JSON valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorKey"] isEqualToString:@"_ERR_INVALID_COOKIE"])
                {
//                    [[NSNotificationCenter defaultCenter]
//                     postNotificationName:k_showLoginNotification
//                     object:self];
                    
                    
                    
                    
                    
                    [self method_login:^(id data, NSError *error)
                    {
                        if (data)
                        {
                            [self GET:URLString parameters:parameters
                               onView:nil
                              success:^(AFHTTPRequestOperation *operationTwo, id responseObjectTwo)
                            {
                                success(operationTwo,responseObjectTwo);
                            }
                              failure:^(AFHTTPRequestOperation *operationTwo, NSError *error)
                            {
                                failure (operationTwo,error);
                            }];
                        }
                        else
                        {
                            [self method_guestLogin:^(id data, NSError *error) {
                                if (data)
                                {
                                    [self GET:URLString parameters:parameters
                                       onView:nil
                                      success:^(AFHTTPRequestOperation *operationTwo, id responseObjectTwo)
                                     {
                                         success(operationTwo,responseObjectTwo);
                                     }
                                      failure:^(AFHTTPRequestOperation *operationTwo, NSError *error)
                                     {
                                         failure (operationTwo,error);
                                     }];
                                }
                                else
                                {
                                    failure (operation,error);
                                }
                            }];
                        }
                    }];
                }
            }
            else
            {
                failure (operation,error);
            }
        }];


        [operation start];
        return operation;
    }
    else
        return nil;
    
}

- (AFHTTPRequestOperation *)PUT:(NSString *)URLString
                     parameters:(NSDictionary *)parameters
                         onView:(UIView *)loaderOnView
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    DLog(@"%@",parameters);
    if ([UtilitiesHelper isReachable])
    {
        
        if (loaderOnView)
        {
            [UtilitiesHelper showLoader:@"Carregando.." forView:loaderOnView setMode:MBProgressHUDModeIndeterminate delegate:nil];
        }
        
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken] forHTTPHeaderField:k_WCToken];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken] forHTTPHeaderField:k_WCTrustedToken];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_personalizationID] forHTTPHeaderField:k_personalizationID];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_userId] forHTTPHeaderField:k_userId];
        [self.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        AFHTTPRequestOperation *operation =  [self PUT:URLString parameters:parameters success:success failure:failure];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            DLog(@"response object ====> %@",responseObject);
            
            [UtilitiesHelper hideLoader:loaderOnView];
            success(operation,responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [UtilitiesHelper hideLoader:loaderOnView];
            DLog(@"error ====> %@",error.description);
            if ([error.userInfo valueForKey:@"com.alamofire.serialization.response.error.data"])
            {
                NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:[error.userInfo valueForKey:@"com.alamofire.serialization.response.error.data"] options:
                                      NSJSONReadingMutableContainers error:nil];
                
                DLog(@"%@",JSON);
                
                if ([[[[JSON valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorKey"] isEqualToString:@"_ERR_INVALID_COOKIE"])
                {
                    //                    [[NSNotificationCenter defaultCenter]
                    //                     postNotificationName:k_showLoginNotification
                    //                     object:self];
                    
                    
                    
                    
                    
                    [self method_login:^(id data, NSError *error)
                     {
                         if (data)
                         {
                             [self PUT:URLString parameters:parameters
                                onView:nil
                               success:^(AFHTTPRequestOperation *operationTwo, id responseObjectTwo)
                             {
                                 success(operationTwo,responseObjectTwo);
                             }
                              
                               failure:^(AFHTTPRequestOperation *operationTwo, NSError *error)
                             {
                                 failure (operationTwo,error);
                             }];
                         }
                         else
                         {
                             [self method_guestLogin:^(id data, NSError *error) {
                                 if (data)
                                 {
                                     [self PUT:URLString parameters:parameters
                                        onView:nil
                                       success:^(AFHTTPRequestOperation *operationTwo, id responseObjectTwo)
                                      {
                                          success(operationTwo,responseObjectTwo);
                                      }
                                      
                                       failure:^(AFHTTPRequestOperation *operationTwo, NSError *error)
                                      {
                                          failure (operationTwo,error);
                                      }];
                                 }
                                 else
                                 {
                                     failure (operation,error);
                                 }
                             }];
                         }
                     }];
                }
            }
            else
            {
                failure (operation,error);
            }
        }];
        return operation;
    }
    else
        return nil;
    
}




- (AFHTTPRequestOperation *)DELETE:(NSString *)URLString
                      parameters:(NSDictionary *)parameters
                          onView:(UIView *)loaderOnView
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    
    DLog(@"%@",parameters);
    DLog(@"%@",URLString);
    if ([UtilitiesHelper isReachable])
    {
        [UtilitiesHelper showLoader:@"Carregando.." forView:loaderOnView setMode:MBProgressHUDModeIndeterminate delegate:nil];
        
        
        DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_userId]);
        DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken]);
        DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken]);
        DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_personalizationID]);
        
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken] forHTTPHeaderField:k_WCToken];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken] forHTTPHeaderField:k_WCTrustedToken];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_personalizationID] forHTTPHeaderField:k_personalizationID];
        [self.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:k_userId] forHTTPHeaderField:k_userId];
        
        [self.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        
        AFHTTPRequestOperation *operation =  [self DELETE:URLString parameters:parameters success:success failure:failure];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
        {
            DLog(@"response object ====> %@",responseObject);
            
            [UtilitiesHelper hideLoader:loaderOnView];
            success(operation,responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [UtilitiesHelper hideLoader:loaderOnView];
            DLog(@"error ====> %@",error.description);
            if ([error.userInfo valueForKey:@"com.alamofire.serialization.response.error.data"])
            {
                NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:[error.userInfo valueForKey:@"com.alamofire.serialization.response.error.data"] options:
                                      NSJSONReadingMutableContainers error:nil];
                
                DLog(@"%@",JSON);
                
                if ([[[[JSON valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorKey"] isEqualToString:@"_ERR_INVALID_COOKIE"])
                {
                    //                    [[NSNotificationCenter defaultCenter]
                    //                     postNotificationName:k_showLoginNotification
                    //                     object:self];
                    
                    
                    
                    
                    
                    [self method_login:^(id data, NSError *error)
                     {
                         if (data)
                         {
                             [self DELETE:URLString parameters:parameters
                                onView:nil
                               success:^(AFHTTPRequestOperation *operationTwo, id responseObjectTwo)
                              {
                                  success(operationTwo,responseObjectTwo);
                              }
                              
                               failure:^(AFHTTPRequestOperation *operationTwo, NSError *error)
                              {
                                  failure (operationTwo,error);
                              }];
                         }
                         else
                         {
                             [self method_guestLogin:^(id data, NSError *error)
                              {
                                  if (data)
                                  {
                                      [self DELETE:URLString parameters:parameters
                                            onView:nil
                                           success:^(AFHTTPRequestOperation *operationTwo, id responseObjectTwo)
                                       {
                                           success(operationTwo,responseObjectTwo);
                                       }
                                       
                                           failure:^(AFHTTPRequestOperation *operationTwo, NSError *error)
                                       {
                                           failure (operationTwo,error);
                                       }];
                                  }
                                  else
                                  {
                                      failure (operation,error);
                                  }
                                 
                             }];
                         }
                     }];
                }
            }
            else
            {
                failure (operation,error);
            }
        }];
        
        return operation;
        
    }
    else
        return nil;
}




- (void) cancelOperation:(UIView *)view
{
    [self.operationQueue cancelAllOperations];
    [UtilitiesHelper hideLoader:view];
}




#pragma mark - action_login
- (void)method_login:(void(^)(id data,NSError* error))block
{
        NSDictionary *parameters;
        NSString *url;
    
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_WCToken];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_WCTrustedToken];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_personalizationID];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_userId];
    
    
    
    DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_userName]);
    DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_password]);
        
        if (ApplicationDelegate.single.string_orderId)
        {
            url = k_loginPerson;
            DLog(@"%@", ApplicationDelegate.single.string_orderId);
            parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                          [[NSUserDefaults standardUserDefaults] objectForKey:k_userName],@"logonId",
                          [[NSUserDefaults standardUserDefaults] objectForKey:k_password],@"logonPassword",
                          @"-1,-5,-6",@"x_calculationUsage",
                          ApplicationDelegate.single.string_orderId,@"orderId",nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:k_getShopingCartNotification object:nil];
        }
        else
        {
            url = k_LoginIdentity;
            parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                          [[NSUserDefaults standardUserDefaults] objectForKey:k_userName],@"logonId",
                          [[NSUserDefaults standardUserDefaults] objectForKey:k_password],@"logonPassword", nil];
        }
        
        [[ServiceModel sharedClient] POST:url
                               parameters:parameters
                                   onView:nil
                                  success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             DLog(@"%@",responseObject);
             
             if ([responseObject valueForKey:@"WCToken"])
             {
                 [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_WCToken] forKey:k_WCToken];
                 [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_WCTrustedToken] forKey:k_WCTrustedToken];
                 [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_personalizationID
                                                                   ] forKey:k_personalizationID];
                 [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:k_isLogin];
                 
                 
                 if ([[responseObject valueForKey:k_userId] isKindOfClass:[NSNumber class]])
                 {
                     [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@", [responseObject valueForKey:k_userId]]
                                                               forKey:k_userId];
                     
                     
                     if ([responseObject valueForKey:k_userId] != [[NSUserDefaults standardUserDefaults] valueForKey:k_userId])
                     {
                         [[NSUserDefaults standardUserDefaults] setValue:nil forKey:k_count];
                     }
                 }
                 else
                 {
                     [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_userId] forKey:k_userId];
                     if (![[responseObject valueForKey:k_userId] isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:k_userId]])
                     {
                         [[NSUserDefaults standardUserDefaults] setValue:nil forKey:k_count];
                     }
                 }
                 
                 block(responseObject,nil);
                 
                 DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_userId]);
                 
                 [[NSUserDefaults standardUserDefaults] synchronize];
             }
             else
             {
                 block(nil,nil);
                 
                 if ([responseObject valueForKey:@"errors"])
                 {
                     [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
                 }
                 else
                 {
                     [UtilitiesHelper showAlert:@"A autenticação falhou"];
                 }
             }
             
             
         }
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             block(nil,error);
             
             DLog(@"%@",error);
             //[UtilitiesHelper showAlert:error.description];
             [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
         }];
}





#pragma mark - guestLogin
-(void)method_guestLogin:(void (^)(id data, NSError * error))block
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_WCToken];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_WCTrustedToken];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_personalizationID];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_userId];
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:k_isLogin];
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:k_count];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_points];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_limit];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ApplicationDelegate.single.dic_userDetails = nil;
    
    
    
    [[ServiceModel sharedClient] POST:k_guestLogin
                           parameters:nil
                               onView:nil
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (responseObject && ![responseObject valueForKey:@"errors"])
         {
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_WCToken] forKey:k_WCToken];
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_WCTrustedToken] forKey:k_WCTrustedToken];
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_personalizationID] forKey:k_personalizationID];
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_userId] forKey:k_userId];
             
             
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_userId]);
             DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken]);
             DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken]);
             DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_personalizationID]);
             
             if (![[responseObject valueForKey:k_userId] isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:k_userId]])
             {
                 [[NSUserDefaults standardUserDefaults] setValue:nil forKey:k_count];
             }
             
             block(responseObject, nil);
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
             
             block(nil, nil);
         }
     }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         block(nil, error);
     }];
}




//#pragma mark - SOAP
//- (void)method_getProductListByCategory:(NSString *)uniqueid
//{
//    self.string_serviceName = k_productList;
//    _soapMessage = [NSString stringWithFormat:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
//                   "<soapenv:Header xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
//                   "<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" soapenv:mustUnderstand=\"1\">"
//                   "<wsse:UsernameToken>"
//                   "<wsse:Username>wcsadmin</wsse:Username>"
//                   "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">passw0rd</wsse:Password>"
//                   "</wsse:UsernameToken>"
//                   "</wsse:Security>"
//                   "</soapenv:Header>"
//                   "<soapenv:Body>"
//                   "<_cat:GetCatalogEntry xmlns:_cat=\"http://www.ibm.com/xmlns/prod/commerce/9/catalog\" xmlns:_wcf=\"http://www.ibm.com/xmlns/prod/commerce/9/foundation\" xmlns:oa=\"http://www.openapplications.org/oagis/9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" versionID=\"6.0.0.6\">"
//                   "<oa:ApplicationArea xsi:type=\"_wcf:ApplicationAreaType\">"
//                   "<oa:CreationDateTime>2008-12-26T12:51:33.515Z</oa:CreationDateTime>"
//                   "<oa:BODID>d280ed80-d31d-11dd-b980-81f9495485fd</oa:BODID>"
//                   "<_wcf:BusinessContext>"
//                   "<_wcf:ContextData name=\"storeId\">10001</_wcf:ContextData>"
//                   "<_wcf:ContextData name=\"catalogId\">10001</_wcf:ContextData>"
//                   "<_wcf:ContextData name=\"langId\">-6</_wcf:ContextData>"
//                   "</_wcf:BusinessContext>"
//                   "</oa:ApplicationArea>"
//                   "<_cat:DataArea>"
//                   "<oa:Get>"
//                   "<oa:Expression expressionLanguage=\"_wcf:XPath\">\{_wcf.ap=IBM_Store_Summary}/CatalogEntry[Description[Attributes[published=1]] and ParentCatalogGroupIdentifier[(UniqueID='%@')] ]"
//                   "</oa:Expression>"
//                   "</oa:Get>"
//                   "</_cat:DataArea>"
//                   "</_cat:GetCatalogEntry>"
//                   "</soapenv:Body>"
//                   "</soapenv:Envelope>", uniqueid];
//    NSLog(@"%@", _soapMessage);
//
//    //Now create a request to the URL
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@webapp/wcs/component/catalog/services/CatalogServices",k_url]];
//    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
//    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[_soapMessage length]];
//    [theRequest addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
//    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
//    [theRequest addValue:[NSString stringWithFormat:@"%@services/CatalogServices.wsdl", k_url] forHTTPHeaderField:@"SOAPAction"];
//    [theRequest setHTTPMethod:@"POST"];
//    [theRequest setHTTPBody: [_soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
//
//    //initiate the request
//    NSURLConnection *connection =
//    [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
//
//    if(connection)
//    {
//        _webResponseData = [NSMutableData data] ;
//    }
//    else
//    {
//        NSLog(@"Connection is NULL");
//    }
//}
//
//- (void)method_forgotPassword:(NSString *)email_
//{
//    self.string_serviceName = k_forgetPassword;
//
//    _soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
//                    "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
//                    "<soapenv:Header>"
//                    "</soapenv:Header>"
//                    "<soapenv:Body>"
//                    "<_mbr:ProcessPerson xmlns:_mbr=\"http://www.ibm.com/xmlns/prod/commerce/9/member\" xmlns:_wcf=\"http://www.ibm.com/xmlns/prod/commerce/9/foundation\" xmlns:oa=\"http://www.openapplications.org/oagis/9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" versionID=\"6.0.0.4\">"
//                    "<oa:ApplicationArea xsi:type=\"_wcf:ApplicationAreaType\">"
//                    "<oa:CreationDateTime>2007-10-29T11:10:07.219Z</oa:CreationDateTime>"
//                    "<oa:BODID>6a31f8f0-8639-11dc-b1fe-832f472605d7</oa:BODID>"
//                    "<_wcf:BusinessContext>"
//                    "<_wcf:ContextData name=\"storeId\">10001</_wcf:ContextData>"
//                    "</_wcf:BusinessContext>"
//                    "</oa:ApplicationArea>"
//                    "<_mbr:DataArea>"
//                    "<oa:Process>"
//                    "<oa:ActionCriteria>"
//                    "<oa:ActionExpression actionCode=\"ResetPassword\" expressionLanguage=\"_wcf:XPath\"/>"
//                    "</oa:ActionCriteria>"
//                    "</oa:Process>"
//                    "<_mbr:Person>"
//                    "<_mbr:Credential>"
//                    "<_mbr:LogonID>%@</_mbr:LogonID>"
//                    "<_mbr:SecurityHint>"
//                    "<_mbr:Answer>-</_mbr:Answer>"
//                    "</_mbr:SecurityHint>"
//                    "</_mbr:Credential>"
//                    "<_mbr:PersonalProfile/>"
//                    "<_mbr:ContactInfo>"
//                    "<_wcf:ContactInfoIdentifier>"
//                    "<_wcf:ExternalIdentifier/>"
//                    "</_wcf:ContactInfoIdentifier>"
//                    "<_wcf:Address/>"
//                    "</_mbr:ContactInfo>"
//                    "</_mbr:Person>"
//                    "</_mbr:DataArea>"
//                    "</_mbr:ProcessPerson>"
//                    "</soapenv:Body>"
//                    "</soapenv:Envelope>", email_];
//    NSLog(@"%@", _soapMessage);
//
//    //Now create a request to the URL
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@webapp/wcs/component/member/services/MemberServices",k_url]];
//    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
//    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[_soapMessage length]];
//    [theRequest addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
//    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
//    [theRequest addValue:[NSString stringWithFormat:@"%@services/MemberServices.wsdl", k_url] forHTTPHeaderField:@"SOAPAction"];
//    [theRequest setHTTPMethod:@"POST"];
//    [theRequest setHTTPBody: [_soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
//
//    //initiate the request
//    NSURLConnection *connection =
//    [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
//
//    if(connection)
//    {
//        _webResponseData = [NSMutableData data] ;
//    }
//    else
//    {
//        NSLog(@"Connection is NULL");
//    }
//}
//
//
//
//#pragma mark - nsurlconnection delegate methods for SOAP
////Implement the connection delegate methods.
//-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
//    [self.webResponseData  setLength:0];
//}
//
//-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
//    [self.webResponseData  appendData:data];
//}
//
//-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
//    NSLog(@"Some error in your Connection. Please try again.");
//
//    if ([self.delegate_ respondsToSelector:@selector(delegate_responseData:error:servicename:)])
//    {
//        [self.delegate_ delegate_responseData:error error:error servicename:nil];
//    }
//}
//
//-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
//    NSLog(@"Received %lu Bytes", (unsigned long)[_webResponseData length]);
//    NSString *theXML = [[NSString alloc] initWithBytes:
//                        [_webResponseData mutableBytes] length:[_webResponseData length] encoding:NSUTF8StringEncoding];
//
//    NSLog(@"%@",theXML);
//
//    //now parsing the xml
//    NSData *myData = [theXML dataUsingEncoding:NSUTF8StringEncoding];
//
//    [XMLConverter convertXMLData:myData completion:^(BOOL success, NSMutableDictionary *dictionary, NSError *error)
//    {
//        if ([self.string_serviceName isEqualToString:k_productList])
//        {
//            NSArray *array_productListDetail = [NSArray arrayWithArray:[[[[[dictionary valueForKey:@"soapenv:Envelope"] valueForKey:@"soapenv:Body"] valueForKey:@"_cat:ShowCatalogEntry"] valueForKey:@"_cat:DataArea"] valueForKey:@"_cat:CatalogEntry"]];
//
//
//            NSMutableArray *array_productList = [[NSMutableArray alloc] init];
//            [array_productListDetail enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
//             {
//                 [array_productList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                               [[obj valueForKey:@"_cat:CatalogEntryIdentifier"] valueForKey:@"_wcf:UniqueID"],k_uniqueID,
//                                               [[obj valueForKey:@"_cat:Description"] valueForKey:@"_cat:Name"],@"name",
//                                               [[obj valueForKey:@"_cat:Description"] valueForKey:@"_cat:ShortDescription"],@"ShortDescription",
//                                               [[obj valueForKey:@"_cat:Description"] valueForKey:@"_cat:Thumbnail"],@"thumbnail",
//                                               [[[[[obj valueForKey:@"_cat:Price"] valueForKey:@"_wcf:ContractPrice"] valueForKey:@"_wcf:Price"] valueForKey:@"_wcf:Price"] valueForKey:@"#text"],@"Price",nil]];
//             }];
//            DLog(@"%@",array_productList);
//            if ([self.delegate_ respondsToSelector:@selector(delegate_responseData:error:servicename:)])
//            {
//                [self.delegate_ delegate_responseData:array_productList error:nil servicename:k_productList];
//            }
//            array_productList = nil;
//        }
//        else if ([self.string_serviceName isEqualToString:k_forgetPassword])
//        {
//
//            if ([self.delegate_ respondsToSelector:@selector(delegate_responseData:error:servicename:)])
//            {
//                [self.delegate_ delegate_responseData:[[[[[[[[dictionary valueForKey:@"soapenv:Envelope"] valueForKey:@"soapenv:Body"] valueForKey:@"_mbr:AcknowledgePerson"] valueForKey:@"_mbr:DataArea"] valueForKey:@"oa:Acknowledge"] valueForKey:@"oa:ResponseCriteria"] valueForKey:@"oa:ChangeStatus"] valueForKey:@"oa:Description"]
//                                                error:nil
//                                          servicename:k_forgetPassword];
//            }
//        }
//
//    }];
//}


@end
