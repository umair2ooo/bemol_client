#import "customNavBar.h"

@implementation customNavBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self == [super initWithCoder:aDecoder]){
        [self setNavigationBar];
    }
    return self;
}

#pragma mark- setNavigationBar
-(void)setNavigationBar
{
    self.translucent = YES;
    //set bar image
    UIImage *barImage = [[UIImage imageNamed:@"bemol-hdr"]
     resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 20, 20) resizingMode:UIImageResizingModeStretch];
    [self setBackgroundImage:barImage forBarMetrics:UIBarMetricsDefault];
    
//    [[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"tophdrbgstrip"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forBarMetrics:UIBarMetricsDefault];

    //set title color
    [self setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    //set back button text color
    //        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    //        //set back button arrow color
    //        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
}

//- (void)drawRect:(CGRect)rect
//{
//    UIImage *image = [UIImage imageNamed: @"tophdrbgstrip"];
//    
//    [image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//    //[self setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
//}
@end