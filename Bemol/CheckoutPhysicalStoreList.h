#import <Foundation/Foundation.h>

@interface CheckoutPhysicalStoreList : NSObject {

    NSString *addressLine;
    NSString *city;
    NSString *country;
    NSString *displayName;
    NSString *displayValue;
    NSString *fax1;
    NSNumber *geoCodeLatitude;
    NSNumber *geoCodeLongitude;
    NSString *name;
    NSString *physicalStoreIdentifier;
    NSString *postalcode;
    NSString *telephone1;
    NSString *uniqueID;
    NSString *value;
    float distance;

}

@property (nonatomic, copy) NSString *addressLine;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *displayName;
@property (nonatomic, copy) NSString *displayValue;
@property (nonatomic, copy) NSString *fax1;
@property (nonatomic, copy) NSNumber *geoCodeLatitude;
@property (nonatomic, copy) NSNumber *geoCodeLongitude;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *physicalStoreIdentifier;
@property (nonatomic, copy) NSString *postalcode;
@property (nonatomic, copy) NSString *telephone1;
@property (nonatomic, copy) NSString *uniqueID;
@property (nonatomic, copy) NSString *value;
@property (nonatomic) float distance;

+ (CheckoutPhysicalStoreList *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
