//
//  Property.m
//  
//
//  Created by fahim on 07/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "Property.h"

@implementation Property

@synthesize baseMarketingKey;
@synthesize baseMarketingValue;

+ (Property *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Property *instance = [[Property alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.baseMarketingKey = [aDictionary objectForKey:@"baseMarketingKey"];
    self.baseMarketingValue = [aDictionary objectForKey:@"baseMarketingValue"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.baseMarketingKey) {
        [dictionary setObject:self.baseMarketingKey forKey:@"baseMarketingKey"];
    }

    if (self.baseMarketingValue) {
        [dictionary setObject:self.baseMarketingValue forKey:@"baseMarketingValue"];
    }

    return dictionary;

}

@end
