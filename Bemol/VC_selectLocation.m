//
//  VC_selectLocation.m
//  Bemol
//
//  Created by Fahim Bilwani on 17/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_selectLocation.h"
#import "Cell_nasLojas.h"
#import "StoreSelection.h"
#import "CheckoutPhysicalStoreList.h"
#import "NSString+HTML.h"

@interface VC_selectLocation ()<CLLocationManagerDelegate>
{
    NSString *currentLat;
    NSString *currentLong;
}

@property (nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) StoreSelection *obj_storeSelection;
@property(nonatomic, strong) CheckoutPhysicalStoreList *obj_checkoutPhysicalStoreList;
@property(nonatomic, strong)NSMutableArray *array_stores;

@property (weak, nonatomic) IBOutlet UITableView *tableview_;
@property (weak, nonatomic) IBOutlet UILabel *label_mainHeading;
@property (weak, nonatomic) IBOutlet UILabel *label_subHeading;

- (IBAction)action_back:(id)sender;
@end

@implementation VC_selectLocation

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (![CLLocationManager locationServicesEnabled])
    {
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:@"Ligue Location Service para permitir Bemol para determinar sua localização"
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil]
         show];
    }
    else
    {
        [self method_currentLocation];
    }
    
    if (self.bool_comingFromCheckinnaLoja)
    {
        [self.navigationItem setTitle:@"Check in na loja"];
        [self stores];
        self.label_mainHeading.text = @"Nossas lojas";
        self.label_subHeading.text = @"Faça check-in na loja mais próxima";
    }
    else
    {
        if (!self.bool_comingFromCartList)
        {
            [self.navigationItem setTitle:@"Escolher loja"];
            self.label_mainHeading.text = @"Quero buscar na loja";
            self.label_subHeading.text = @"Selecione uma das lojas abaixo:";
        }
        else
        {
            [self.navigationItem setTitle:@"Carrinho"];
            self.label_mainHeading.text = @"Disponibilidade nas lojas";
            self.label_subHeading.text = @"Os produtos do seu carrinho estão disponíves nas seguintes lojas:";
        }
        [self getStoreList];
    }
    
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = nil;
    self.locationManager = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - getStoreList
-(void)getStoreList
{
    DLog(@"%@",self.string_orderId);
    [[ServiceModel sharedClient] POST:k_getStoreList
                          parameters:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"preferredStoreList",self.string_orderId,@"orderId", nil]
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             DLog(@"%@",responseObject);
             self.obj_storeSelection = [StoreSelection instanceFromDictionary:responseObject];
             
             [self.obj_storeSelection.physicalStoreList enumerateObjectsUsingBlock:^(CheckoutPhysicalStoreList *obj, NSUInteger idx, BOOL *stop)
              {
                  obj.distance = [self method_calculateDistance:
                                                                [NSString stringWithFormat:@"%@",obj.geoCodeLatitude]
                                                                lon:[NSString stringWithFormat:@"%@",obj.geoCodeLongitude]];
                  
              }];
             
             self.array_stores = [[NSMutableArray alloc] initWithArray:[self.obj_storeSelection.physicalStoreList sortedArrayUsingComparator:^NSComparisonResult(CheckoutPhysicalStoreList *s1, CheckoutPhysicalStoreList *s2)
                                                                          {
                                                                              NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                                                                              [f setNumberStyle:NSNumberFormatterNoStyle];
                                                                              NSNumber * distancex = [NSNumber numberWithFloat: s1.distance];
                                                                              NSNumber * distancey = [NSNumber numberWithFloat: s2.distance];
                                                                              
                                                                              if (!distancey || !distancex)
                                                                              {
                                                                                  return true;
                                                                              }
                                                                              
                                                                              return [distancex compare:distancey];
                                                                          }]
                                    ];

             [self.tableview_ reloadData];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}

#pragma mark - stores
-(void)stores
{
    [[ServiceModel sharedClient] GET:k_stores
                           parameters:nil
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             DLog(@"%@",responseObject);
             self.obj_storeSelection = [StoreSelection instanceFromDictionary:responseObject];
             
             [self.obj_storeSelection.physicalStoreList enumerateObjectsUsingBlock:^(CheckoutPhysicalStoreList *obj, NSUInteger idx, BOOL *stop)
              {
                  obj.distance = [self method_calculateDistance:
                                  [NSString stringWithFormat:@"%@",obj.geoCodeLatitude]
                                                            lon:[NSString stringWithFormat:@"%@",obj.geoCodeLongitude]];
                  
              }];
             
             self.array_stores = [[NSMutableArray alloc] initWithArray:[self.obj_storeSelection.physicalStoreList sortedArrayUsingComparator:^NSComparisonResult(CheckoutPhysicalStoreList *s1, CheckoutPhysicalStoreList *s2)
                                                                        {
                                                                            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                                                                            [f setNumberStyle:NSNumberFormatterNoStyle];
                                                                            NSNumber * distancex = [NSNumber numberWithFloat: s1.distance];
                                                                            NSNumber * distancey = [NSNumber numberWithFloat: s2.distance];
                                                                            
                                                                            if (!distancey || !distancex)
                                                                            {
                                                                                return true;
                                                                            }
                                                                            
                                                                            return [distancex compare:distancey];
                                                                        }]
                                  ];

             [self.tableview_ reloadData];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}


#pragma mark - tableview delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.obj_storeSelection.physicalStoreList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Cell_nasLojas *cell = (Cell_nasLojas *)[tableView dequeueReusableCellWithIdentifier:@"Cell_nasLojas"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Cell_nasLojas" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain)ge
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    self.obj_checkoutPhysicalStoreList = [self.array_stores objectAtIndex:indexPath.row];
    cell.label_name.text = self.obj_checkoutPhysicalStoreList.name;
    cell.label_address.text = self.obj_checkoutPhysicalStoreList.addressLine;
    cell.label_displayvalue.text = [self.obj_checkoutPhysicalStoreList.displayValue kv_stringByStrippingHTML:self.obj_checkoutPhysicalStoreList.displayValue];
    cell.label_number.text = self.obj_checkoutPhysicalStoreList.telephone1;
    cell.label_distance.text = [NSString stringWithFormat:@"%.1f m",self.obj_checkoutPhysicalStoreList.distance];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.bool_comingFromCheckinnaLoja && !self.bool_comingFromCartList)
    {
        //segue_exitToCheckout
        self.obj_checkoutPhysicalStoreList = [self.obj_storeSelection.physicalStoreList objectAtIndex:indexPath.row];
        
        self.dic_selectedstoreSelection = [NSMutableDictionary dictionaryWithDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:self.obj_checkoutPhysicalStoreList.name,@"name",self.obj_checkoutPhysicalStoreList.addressLine,@"address",self.obj_checkoutPhysicalStoreList.displayValue,@"displayValue",self.obj_checkoutPhysicalStoreList.telephone1,@"telephone1",[NSString stringWithFormat:@"%.0f m",[self method_calculateDistance:[NSString stringWithFormat:@"%@",self.obj_checkoutPhysicalStoreList.geoCodeLatitude] lon:[NSString stringWithFormat:@"%@",self.obj_checkoutPhysicalStoreList.geoCodeLongitude]]],@"distance",self.obj_checkoutPhysicalStoreList.uniqueID,@"uniqueID", nil]];
        
        [self performSegueWithIdentifier:@"segue_exitToCheckoutViaLocation" sender:nil];
    }
}

- (void)method_currentLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        //for background and foreground
        //[self.locationManager requestAlwaysAuthorization]
        //foreground
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
}

- (IBAction)action_cancel:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (IBAction)action_back:(id)sender
    {
        [self.navigationController popViewControllerAnimated:YES];
    }

#pragma mark - didUpdateToLocation
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *currentLocation = [locations lastObject];
    if (currentLocation != nil)
    {
        currentLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        currentLong = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    }
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"" message:@"Ligue Location Service para permitir Bemol para determinar sua localização" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
    
}

#pragma mark - method_calculateDistance
-(float)method_calculateDistance:(NSString *)lat lon:(NSString *)lon
{
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[currentLat floatValue] longitude:[currentLong floatValue]];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[lat floatValue] longitude:[lon floatValue]];
    // now calculating the distance using inbuild method distanceFromLocation:
    float distInMeter = [location1 distanceFromLocation:location2]; // which returns in meters
    //    //converting meters to mile
    //    float distInMile = 0.000621371192 * distInMeter;
    //DLog(@"Actual Distance : %0f",distInMeter);
    return distInMeter;
}



@end
