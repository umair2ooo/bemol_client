#import "ProductListMetaDatum.h"

@implementation ProductListMetaDatum

@synthesize metaData;
@synthesize metaKey;

+ (ProductListMetaDatum *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ProductListMetaDatum *instance = [[ProductListMetaDatum alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.metaData = [aDictionary objectForKey:@"metaData"];
    self.metaKey = [aDictionary objectForKey:@"metaKey"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.metaData) {
        [dictionary setObject:self.metaData forKey:@"metaData"];
    }

    if (self.metaKey) {
        [dictionary setObject:self.metaKey forKey:@"metaKey"];
    }

    return dictionary;

}


@end
