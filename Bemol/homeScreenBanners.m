#import "HomeScreenBanners.h"

#import "MarketingSpotDatum.h"

@implementation HomeScreenBanners

@synthesize marketingSpotData;
@synthesize resourceId;
@synthesize resourceName;

+ (HomeScreenBanners *)instanceFromDictionary:(NSDictionary *)aDictionary {

    HomeScreenBanners *instance = [[HomeScreenBanners alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }


    NSArray *receivedMarketingSpotData = [aDictionary objectForKey:@"MarketingSpotData"];
    if ([receivedMarketingSpotData isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedMarketingSpotData = [NSMutableArray arrayWithCapacity:[receivedMarketingSpotData count]];
        for (NSDictionary *item in receivedMarketingSpotData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedMarketingSpotData addObject:[MarketingSpotDatum instanceFromDictionary:item]];
            }
        }

        self.marketingSpotData = populatedMarketingSpotData;

    }
    self.resourceId = [aDictionary objectForKey:@"resourceId"];
    self.resourceName = [aDictionary objectForKey:@"resourceName"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.marketingSpotData) {
        [dictionary setObject:self.marketingSpotData forKey:@"marketingSpotData"];
    }

    if (self.resourceId) {
        [dictionary setObject:self.resourceId forKey:@"resourceId"];
    }

    if (self.resourceName) {
        [dictionary setObject:self.resourceName forKey:@"resourceName"];
    }

    return dictionary;

}

@end
