//
//  VC_cancelPedido.h
//  Bemol
//
//  Created by Fahim Bilwani on 27/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "List.h"
#import "OrderDetails.h"

@interface VC_cancelPedido : BaseViewController
@property(nonatomic, strong)OrderDetails *obj_orderDetails;
@property(nonatomic) BOOL bool_ispedido;

@end
