#import "Value.h"

@implementation Value

@synthesize identifier;
@synthesize uniqueID;
@synthesize values;

+ (Value *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Value *instance = [[Value alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.identifier = [aDictionary objectForKey:@"identifier"];
    self.uniqueID = [aDictionary objectForKey:@"uniqueID"];
    self.values = [aDictionary objectForKey:@"values"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.identifier) {
        [dictionary setObject:self.identifier forKey:@"identifier"];
    }

    if (self.uniqueID) {
        [dictionary setObject:self.uniqueID forKey:@"uniqueID"];
    }

    if (self.values) {
        [dictionary setObject:self.values forKey:@"values"];
    }

    return dictionary;

}


@end
