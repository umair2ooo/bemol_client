//
//  VC_pedidoMessage.m
//  Bemol
//
//  Created by Fahim Bilwani on 29/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "VC_pedidoMessage.h"

@interface VC_pedidoMessage ()

- (IBAction)action_back:(id)sender;
- (IBAction)action_done:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *label_message;
@property (weak, nonatomic) IBOutlet UILabel *label_title;

@end

@implementation VC_pedidoMessage

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Histórico de compras"];
    
    DLog(@"%d",self.bool_ispedido);
    
    
    if (self.bool_ispedido)
    {
        self.label_title.text = @"Pedido cancelado";
        self.label_message.text = @"Seu pedido foi canceledo com sucesso!";
    }
    else
    {
        self.label_title.text = @"Produto cancelado";
        self.label_message.text = @"Seu produto foi canceledo com sucesso!";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_done:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end


