#import "VC_Menu.h"
#import "VC_selectLocation.h"

@interface VC_Menu ()

@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UILabel *label_points;
@property (weak, nonatomic) IBOutlet UILabel *label_limit;


- (IBAction)action_hide:(id)sender;
- (IBAction)action_signout:(id)sender;

@end

@implementation VC_Menu

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self.scroller setContentSize:CGSizeMake(0, 550)];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:k_points])
    {
        self.label_points.text = [NSString stringWithFormat:@"%@ pts",[[NSUserDefaults standardUserDefaults] objectForKey:k_points]];
    }
    else
    {
        self.label_points.text = @"0 pts";
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:k_limit])
    {
        self.label_limit.text = [UtilitiesHelper method_currencyFormatter:[[NSUserDefaults standardUserDefaults] objectForKey:k_limit]];
    }
    else
    {
        self.label_limit.text = @"R$ 0";
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_checkinnaLoja"])
    {
        VC_selectLocation *obj = segue.destinationViewController;
        obj.bool_comingFromCheckinnaLoja = YES;
        obj.bool_comingFromCartList = NO;
    }
}

#pragma mark - action_hide
- (IBAction)action_hide:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:k_menuNotification object:self];
}

- (IBAction)action_signout:(id)sender
{
    [[ServiceModel sharedClient] DELETE:k_Logut
                           parameters:nil
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         DLog(@"%@",responseObject);
         
         if (![responseObject valueForKey:@"errors"])
         {
             [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_WCToken];
             [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_WCTrustedToken];
             [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_personalizationID];
             [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_userId];
             [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:k_isLogin];
             [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:k_count];
             [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_points];
             [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_limit];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             ApplicationDelegate.single.dic_userDetails = nil;
             ApplicationDelegate.single.string_orderId = nil;
//             [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_orderid];
//             [[NSUserDefaults standardUserDefaults] synchronize];
             
             [[NSNotificationCenter defaultCenter] postNotificationName:k_logoutNotification object:self];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
         }
     }
            failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error);
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
    //[self dismissViewControllerAnimated:YES completion:nil];
}
@end