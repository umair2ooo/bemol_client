#import "StoreSelection.h"

#import "CheckoutPhysicalStoreList.h"

@implementation StoreSelection

@synthesize physicalStoreCount;
@synthesize physicalStoreList;

+ (StoreSelection *)instanceFromDictionary:(NSDictionary *)aDictionary {

    StoreSelection *instance = [[StoreSelection alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.physicalStoreCount = [aDictionary objectForKey:@"physicalStoreCount"];

    NSArray *receivedPhysicalStoreList = [aDictionary objectForKey:@"physicalStoreList"];
    if ([receivedPhysicalStoreList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedPhysicalStoreList = [NSMutableArray arrayWithCapacity:[receivedPhysicalStoreList count]];
        for (NSDictionary *item in receivedPhysicalStoreList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedPhysicalStoreList addObject:[CheckoutPhysicalStoreList instanceFromDictionary:item]];
            }
        }

        self.physicalStoreList = populatedPhysicalStoreList;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.physicalStoreCount) {
        [dictionary setObject:self.physicalStoreCount forKey:@"physicalStoreCount"];
    }

    if (self.physicalStoreList) {
        [dictionary setObject:self.physicalStoreList forKey:@"physicalStoreList"];
    }

    return dictionary;

}


@end
