//
//  cell_Cep.h
//  Bemol
//
//  Created by Fahim Bilwani on 30/09/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cell_Cep : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label_name;
@property (weak, nonatomic) IBOutlet UILabel *label_frete;
@property (weak, nonatomic) IBOutlet UILabel *label_prazo;
@end
