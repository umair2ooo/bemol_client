#import "CartaoBemolCreditoMap.h"

@implementation CartaoBemolCreditoMap

@synthesize disponivel;
@synthesize limiteDeCredito;
@synthesize saldoDisponivel;
@synthesize saldoUtilizado;

+ (CartaoBemolCreditoMap *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CartaoBemolCreditoMap *instance = [[CartaoBemolCreditoMap alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.disponivel = [aDictionary objectForKey:@"Disponivel"];
    self.limiteDeCredito = [aDictionary objectForKey:@"Limite_De_Credito"];
    self.saldoDisponivel = [aDictionary objectForKey:@"Saldo_Disponivel"];
    self.saldoUtilizado = [aDictionary objectForKey:@"Saldo_Utilizado"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.disponivel) {
        [dictionary setObject:self.disponivel forKey:@"disponivel"];
    }

    if (self.limiteDeCredito) {
        [dictionary setObject:self.limiteDeCredito forKey:@"limiteDeCredito"];
    }

    if (self.saldoDisponivel) {
        [dictionary setObject:self.saldoDisponivel forKey:@"saldoDisponivel"];
    }

    if (self.saldoUtilizado) {
        [dictionary setObject:self.saldoUtilizado forKey:@"saldoUtilizado"];
    }

    return dictionary;

}


@end
