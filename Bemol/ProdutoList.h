#import <Foundation/Foundation.h>

@interface ProdutoList : NSObject {

    NSNumber *countTotal;
    NSMutableArray *list;

}

@property (nonatomic, copy) NSNumber *countTotal;
@property (nonatomic, copy) NSMutableArray *list;

+ (ProdutoList *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
