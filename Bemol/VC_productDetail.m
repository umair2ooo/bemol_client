#import "VC_productDetail.h"
#import "YCameraViewController.h"
#import "ProductDetail.h"
#import "CatalogEntryView.h"
#import "Cell_productdetailCharacterstics.h"
#import "Attribute.h"
#import "Value.h"
#import "XproductReviewList.h"
#import "VC_comments.h"
#import "VC_simulacao de.h"
#import "cell_Cep.h"
#import "VC_nasLojas.h"
#import <Pinterest/Pinterest.h>
#import <GooglePlus/GooglePlus.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <Social/Social.h>
#import "VC_home.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "GPPShareActivity.h"
#import <TYMProgressBarView/TYMProgressBarView.h>

@interface VC_productDetail ()<MFMailComposeViewControllerDelegate,GPPSignInDelegate>
{
    NSDictionary *dic_shipping;
    Pinterest*  _pinterest;
}

@property (weak, nonatomic) IBOutlet UILabel *label_name;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_;
@property (weak, nonatomic) IBOutlet UILabel *label_price;
@property (weak, nonatomic) IBOutlet UILabel *label_numberOfPieces;
@property (weak, nonatomic) IBOutlet UIView *view_options;
@property (weak, nonatomic) IBOutlet UIView *view_freteAndPrazo;
@property (weak, nonatomic) IBOutlet UITableView *tableview_characterstics;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_main;
@property (weak, nonatomic) IBOutlet UILabel *label_description;
@property (weak, nonatomic) IBOutlet UILabel *label_estrelas1;
@property (weak, nonatomic) IBOutlet UILabel *label_estrelas2;
@property (weak, nonatomic) IBOutlet UILabel *label_estrelas3;
@property (weak, nonatomic) IBOutlet UILabel *label_estrelas4;
@property (weak, nonatomic) IBOutlet UILabel *label_estrelas5;
@property (weak, nonatomic) IBOutlet UILabel *label_avgStar;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_star1;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_star2;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_star3;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_star4;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_star5;
@property (weak, nonatomic) IBOutlet UITextField *textfield_CEP;
@property (weak, nonatomic) IBOutlet UITableView *tableview_cep;
@property (weak, nonatomic) IBOutlet UIView *view_hideRating;
@property (weak, nonatomic) IBOutlet UIView *view_comments;
@property (weak, nonatomic) IBOutlet UIView *view_description;
@property (weak, nonatomic) IBOutlet UIView *view_rating;
@property (weak, nonatomic) IBOutlet UIView *view_lojas;
@property (weak, nonatomic) IBOutlet UIView *view_simulate;
@property (weak, nonatomic) IBOutlet UIView *view_share;
@property (weak, nonatomic) IBOutlet UIView *view_progressbar1;
@property (weak, nonatomic) IBOutlet UIView *view_progressbar2;
@property (weak, nonatomic) IBOutlet UIView *view_progressbar3;
@property (weak, nonatomic) IBOutlet UIView *view_progressbar4;
@property (weak, nonatomic) IBOutlet UIView *view_progressbar5;



@property(nonatomic, strong)ProductDetail *obj_productDetail;
@property(nonatomic, strong)CatalogEntryView *obj_catalogEntryView;
@property(nonatomic, strong)Attribute *obj_attribute;
@property(nonatomic, strong)Value *obj_value;
@property(nonatomic, strong)XproductReviewList *obj_xproductReviewList;

- (IBAction)action_addToCart:(id)sender;
- (IBAction)action_back:(id)sender;
- (IBAction)action_AR:(id)sender;
- (IBAction)action_CEP:(id)sender;
- (IBAction)action_share:(id)sender;

@end

@implementation VC_productDetail

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Detalhes do Produto";

    
    [self.scroller_main setContentSize:CGSizeMake(0,
                                                  self.view_options.frame.size.height+self.view_options.frame.origin.y+10)];
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ApplicationDelegate.single setFontFamily:k_fontName forView:self.view andSubViews:YES];
    });
    
    
    
    DLog(@"uniqueID: %@",self.uniqueID);
    
    [[ServiceModel sharedClient]GET:[NSString stringWithFormat:@"%@%@",k_productDetail,self.uniqueID]
                         parameters:nil onView:self.view
                            success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             
             self.obj_productDetail = [ProductDetail instanceFromDictionary:responseObject];
             
             [self setUIContents];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                        valueForKey:@"errorMessage"]];
         }
    }
        failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error.description);
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];

    [self method_setupGooglePlus];
}

#pragma mark - method_setupGooglePlus
-(void)method_setupGooglePlus
{
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    //signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    signIn.clientID = @"589541872820-v8esfpae0go53eem2chjmud24qq5lj88.apps.googleusercontent.com";
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    //signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
}

-(void)setUIContents
{
    self.obj_catalogEntryView = [self.obj_productDetail.catalogEntryView objectAtIndex:0];
    
    self.label_name.text = self.obj_catalogEntryView.name;
    if ([[self.obj_catalogEntryView.price objectAtIndex:1] valueForKey:@"priceValue"])
    {
        self.label_price.text =[NSString stringWithFormat:@"%@ a vista*",[UtilitiesHelper method_currencyFormatter:[[self.obj_catalogEntryView.price objectAtIndex:1] valueForKey:@"priceValue"]]];
        
        float installmentPrice = ([[[self.obj_catalogEntryView.price objectAtIndex:1] valueForKey:@"priceValue"] floatValue] / 5);
        self.label_numberOfPieces.text = [NSString stringWithFormat:@"5x de %@ sem juros no cartão de crédito",[UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%f",installmentPrice]]];
    }
    
    
    //long description
    [self.label_description sizeToFit];
    
    if (self.obj_catalogEntryView.longDescription)
        self.label_description.text = self.obj_catalogEntryView.longDescription;
    else
        self.label_description.text = self.obj_catalogEntryView.shortDescription;
    
    //set image
    [self.imageView_ sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", k_imageurl,self.obj_catalogEntryView.fullImage]]
                       placeholderImage:[UIImage imageNamed:k_placeHolder]];
    
    [self.tableview_characterstics reloadData];
    
    if (self.obj_catalogEntryView.xproductReviewList)
    {
        [self.view_rating setHidden:NO];
        [self.view_comments setFrame:CGRectMake(0, self.view_rating.frame.size.height + self.view_rating.frame.origin.y+2, self.view.frame.size.width, self.view_comments.frame.size.height)];
        [self.view_lojas setFrame:CGRectMake(0, self.view_comments.frame.size.height + self.view_comments.frame.origin.y+2, self.view.frame.size.width, self.view_lojas.frame.size.height)];
        [self.view_simulate setFrame:CGRectMake(0, self.view_lojas.frame.size.height + self.view_lojas.frame.origin.y+2, self.view.frame.size.width, self.view_simulate.frame.size.height)];
        [self.view_share setFrame:CGRectMake(0, self.view_simulate.frame.size.height + self.view_simulate.frame.origin.y+2, self.view.frame.size.width, self.view_share.frame.size.height)];
        [self showRatings];
    }
    else
    {
        [self.view_rating setHidden:YES];
        [self.view_comments setFrame:CGRectMake(0, self.view_description.frame.size.height + self.view_description.frame.origin.y+2, self.view.frame.size.width, self.view_comments.frame.size.height)];
        [self.view_lojas setFrame:CGRectMake(0, self.view_comments.frame.size.height + self.view_comments.frame.origin.y+2, self.view.frame.size.width, self.view_lojas.frame.size.height)];
        [self.view_simulate setFrame:CGRectMake(0, self.view_lojas.frame.size.height + self.view_lojas.frame.origin.y+2, self.view.frame.size.width, self.view_simulate.frame.size.height)];
        [self.view_share setFrame:CGRectMake(0, self.view_simulate.frame.size.height + self.view_simulate.frame.origin.y+2, self.view.frame.size.width, self.view_share.frame.size.height)];
        
        
        [self.scroller_main setContentSize:CGSizeMake(0,
                                                      self.view_options.frame.size.height+self.view_options.frame.origin.y+10 - self.view_rating.frame.size.height)];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    //ApplicationDelegate.single.delegate_ = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    

//    DLog(@"%lu", self.navigationController.viewControllers.count);
//    DLog(@"%@", self.navigationController.viewControllers);
//    NSLog(@"%@",[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-1]);
    
//    if ([[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-1] isKindOfClass:[VC_home class]])
//    {
//        [[NSNotificationCenter defaultCenter] postNotificationName:k_getShopingCartNotification object:nil];
//    }
    
    
   // ApplicationDelegate.single.delegate_ = nil;
}


#pragma mark - showRatings
-(void)showRatings
{
    if (!self.obj_catalogEntryView.xproductReviewList)
    {
        [self.view_hideRating setHidden:NO];
        return;
    }
    
    //ratings
    self.obj_xproductReviewList = [self.obj_catalogEntryView.xproductReviewList objectAtIndex:0];
    
    // Create a progress bar view and set its appearance
    TYMProgressBarView *progressBarView = [[TYMProgressBarView alloc] initWithFrame:self.view_progressbar1.frame];
    progressBarView.barBorderWidth = 1.0;
    progressBarView.barBackgroundColor = [UtilitiesHelper colorFromHexString:@"C9C9C9"];
    progressBarView.barFillColor = [UIColor colorWithRed:255 green:198 blue:0 alpha:1];
    DLog(@"%f",[self.obj_xproductReviewList.Estrelas5 floatValue] / 10);
    progressBarView.progress = ([self.obj_xproductReviewList.Estrelas5 floatValue] / 10);
    
    TYMProgressBarView *progressBarView1 = [[TYMProgressBarView alloc] initWithFrame:self.view_progressbar2.frame];
    progressBarView1.barBorderWidth = 1.0;
    progressBarView1.barBackgroundColor = [UtilitiesHelper colorFromHexString:@"C9C9C9"];
    progressBarView1.barFillColor = [UIColor colorWithRed:255 green:198 blue:0 alpha:1];
    progressBarView1.progress = ([self.obj_xproductReviewList.Estrelas4 floatValue] / 10);
    
    TYMProgressBarView *progressBarView2 = [[TYMProgressBarView alloc] initWithFrame:self.view_progressbar3.frame];
    progressBarView2.barBorderWidth = 1.0;
    progressBarView2.barBackgroundColor = [UtilitiesHelper colorFromHexString:@"C9C9C9"];
    progressBarView2.barFillColor = [UIColor colorWithRed:255 green:198 blue:0 alpha:1];
    progressBarView2.progress = ([self.obj_xproductReviewList.Estrelas3 floatValue] / 10);
    
    TYMProgressBarView *progressBarView3 = [[TYMProgressBarView alloc] initWithFrame:self.view_progressbar4.frame];
    progressBarView3.barBorderWidth = 1.0;
    progressBarView3.barBackgroundColor = [UtilitiesHelper colorFromHexString:@"C9C9C9"];
    progressBarView3.barFillColor = [UIColor colorWithRed:255 green:198 blue:0 alpha:1];
    progressBarView3.progress = ([self.obj_xproductReviewList.Estrelas2 floatValue] / 10);
    
    TYMProgressBarView *progressBarView4 = [[TYMProgressBarView alloc] initWithFrame:self.view_progressbar5.frame];
    progressBarView4.barBorderWidth = 1.0;
    progressBarView4.barBackgroundColor = [UtilitiesHelper colorFromHexString:@"C9C9C9"];
    progressBarView4.barFillColor = [UIColor colorWithRed:255 green:198 blue:0 alpha:1];
    progressBarView4.progress = ([self.obj_xproductReviewList.Estrelas1 floatValue] / 10);
    
    // Add it to your view
    [self.view_rating addSubview:progressBarView];
    [self.view_rating addSubview:progressBarView1];
    [self.view_rating addSubview:progressBarView2];
    [self.view_rating addSubview:progressBarView3];
    [self.view_rating addSubview:progressBarView4];
    
    self.label_estrelas1.text = [NSString stringWithFormat:@"%@",self.obj_xproductReviewList.Estrelas1];
    self.label_estrelas2.text = [NSString stringWithFormat:@"%@",self.obj_xproductReviewList.Estrelas2];
    self.label_estrelas3.text = [NSString stringWithFormat:@"%@",self.obj_xproductReviewList.Estrelas3];
    self.label_estrelas4.text = [NSString stringWithFormat:@"%@",self.obj_xproductReviewList.Estrelas4];
    self.label_estrelas5.text = [NSString stringWithFormat:@"%@",self.obj_xproductReviewList.Estrelas5];
    self.label_avgStar.text = [NSString stringWithFormat:@"%@ de 5 estrelas",self.obj_xproductReviewList.avgRate];
    
    if ([self.obj_xproductReviewList.avgRate isEqual:@1])
    {
        [self.imageview_star1 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star2 setImage:[UIImage imageNamed:@"stargray"]];
        [self.imageview_star3 setImage:[UIImage imageNamed:@"stargray"]];
        [self.imageview_star4 setImage:[UIImage imageNamed:@"stargray"]];
        [self.imageview_star5 setImage:[UIImage imageNamed:@"stargray"]];
    }
    else if ([self.obj_xproductReviewList.avgRate isEqual:@2])
    {
        [self.imageview_star1 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star2 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star3 setImage:[UIImage imageNamed:@"stargray"]];
        [self.imageview_star4 setImage:[UIImage imageNamed:@"stargray"]];
        [self.imageview_star5 setImage:[UIImage imageNamed:@"stargray"]];
    }
    else if ([self.obj_xproductReviewList.avgRate isEqual:@3])
    {
        [self.imageview_star1 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star2 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star3 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star4 setImage:[UIImage imageNamed:@"stargray"]];
        [self.imageview_star5 setImage:[UIImage imageNamed:@"stargray"]];
    }
    else if ([self.obj_xproductReviewList.avgRate isEqual:@4])
    {
        [self.imageview_star1 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star2 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star3 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star4 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star5 setImage:[UIImage imageNamed:@"stargray"]];
    }
    else
    {
        [self.imageview_star1 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star2 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star3 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star4 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star5 setImage:[UIImage imageNamed:@"starfull"]];
    }
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_productComments"])
    {
        VC_comments *obj = segue.destinationViewController;
        obj.string_title = self.obj_catalogEntryView.name;
        obj.array_reviewList = (NSMutableArray *)sender;
    }
    else if ([segue.identifier isEqualToString:@"segue_productSimulate"])
    {
        VC_simulacao_de *obj_simulacao = segue.destinationViewController;
        obj_simulacao.string_title = self.obj_catalogEntryView.name;
        obj_simulacao.dic_productDetail = (NSDictionary *)sender;
    }
    else if ([segue.identifier isEqualToString:@"segue_productnasLojas"])
    {
        VC_nasLojas *obj = segue.destinationViewController;
        obj.string_title = self.obj_catalogEntryView.name;
        obj.string_productId = sender;
    }
}

#pragma mark - action_addToCart
- (IBAction)action_addToCart:(id)sender
{
        if (self.obj_catalogEntryView.uniqueID)
        {
            DLog(@"%@",self.obj_catalogEntryView.uniqueID);
            //no combination
            NSArray *array_temp = [NSArray arrayWithObjects:
                                   [NSDictionary dictionaryWithObjectsAndKeys:
                                    self.obj_catalogEntryView.uniqueID,@"productId",
                                   @"1",@"quantity", nil], nil];

            [[ServiceModel sharedClient]POST:k_addToCartService parameters:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                           array_temp, @"orderItem",
                                                                           nil]
                                      onView:self.view success:^(AFHTTPRequestOperation *operation, id responseObject)
                                        {
                                            DLog(@"%@",responseObject);
                                            if ([responseObject valueForKey:@"orderId"])
                                            {
//                                                [defaults setObject:[responseObject valueForKey:@"orderId"] forKey:k_orderid];
//                                                [defaults synchronize];
                                                if (!ApplicationDelegate.single.string_orderId)
                                                {
                                                    ApplicationDelegate.single.string_orderId = [responseObject valueForKey:@"orderId"];
                                                }
                                                [UtilitiesHelper showAlert:@"O produto foi adicionado ao seu carrinho de compras"];
                                                
                                                // set cart counter
                                                NSUserDefaults * defaults =  [NSUserDefaults standardUserDefaults];
                                                int cartCounter = [[defaults stringForKey:k_count] intValue];
                                                
                                                [self method_shoppingCartCounter:[NSString stringWithFormat:@"%d",++cartCounter]];
                                            }
                                            else if ([responseObject valueForKey:@"errors"])
                                            {
                                                [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
                                            }
                                            else
                                            {
                                                [UtilitiesHelper showAlert:@"O item não adicionou, por favor tente mais tarde"];
                                            }

                                        }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                        {
                                            DLog(@"%@",error.description);
                                            //[UtilitiesHelper showAlert:error.description];
                                            [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
                                        }];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:nil
                                        message:@"ID de produto não encontrado"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
        }
}

#pragma mark - showNHidefreteAndPrazoView
-(void)showNHidefreteAndPrazoView:(BOOL)hide
{
    if (!hide)
    {
        self.view_freteAndPrazo.hidden = false;
        self.view_options.frame = CGRectMake(self.view_options.frame.origin.x,
                                             self.view_options.frame.origin.y + self.view_freteAndPrazo.frame.size.height + 8,
                                             self.view_options.frame.size.width,
                                             self.view_options.frame.size.height);
    }
    else
    {
        self.view_freteAndPrazo.hidden = true;
        self.view_options.frame = CGRectMake(self.view_options.frame.origin.x,
                                             self.view_options.frame.origin.y - self.view_freteAndPrazo.frame.size.height - 8,
                                             self.view_options.frame.size.width,
                                             self.view_options.frame.size.height);
    }
    
    [self.scroller_main setContentSize:CGSizeMake(0,
                                                  self.view_options.frame.size.height+self.view_options.frame.origin.y)];
}

#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - action_AR
- (IBAction)action_AR:(id)sender
{
    YCameraViewController *obj =[[YCameraViewController alloc] initWithNibName:@"YCameraViewController" bundle:nil];
    obj.image_productimage = self.imageView_.image;
    obj.delegate = self;
    
//    [self method_isHideHeader_BOOL:YES];
//    
//    DLog(@"%@", self.dic_productsList);
//    obj.dic_productsList = self.dic_productsList;
    
    [self.navigationController pushViewController:obj animated:YES];
}

#pragma mark - action_CEP
- (IBAction)action_CEP:(id)sender
{
    if ([self.textfield_CEP.text length])
    {
        [[ServiceModel sharedClient] POST:k_shipping
        parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.textfield_CEP.text,@"cep",@"",@"arraySKU",self.obj_catalogEntryView.uniqueID,@"arrayCatentry",@"1",@"arrayQty",[[self.obj_catalogEntryView.price objectAtIndex:1] valueForKey:@"priceValue"],@"arrayPrice", nil]
        onView:self.view
        success:^(AFHTTPRequestOperation *operation, id responseObject)
        {
            if (![responseObject valueForKey:@"errors"])
            {
                dic_shipping = [NSDictionary dictionaryWithDictionary:responseObject];
                if ([[dic_shipping valueForKey:@"ShippingMap"] valueForKey:@"NORMAL"])
                {
                    [self.tableview_cep reloadData];
                    [self.textfield_CEP resignFirstResponder];
                    [self showNHidefreteAndPrazoView:NO];

                }
                else
                {
                    [UtilitiesHelper showAlert:@"O CEP informado não é válido!"];
                    //[self showNHidefreteAndPrazoView:YES];
                }
            }
            else
            {
                [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                            valueForKey:@"errorMessage"]];
//                [self showNHidefreteAndPrazoView:YES];
                
            }
        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error)
        {
            [UtilitiesHelper showAlert:@"Há algo de errado a partir do lado do servidor, por favor, tente mais tarde"];
//            [self showNHidefreteAndPrazoView:YES];
        }];
    }
}

#pragma mark - action_share
- (IBAction)action_share:(id)sender
{
    //facebook
    if ([sender tag] == 1)
    {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [controller setInitialText:self.obj_catalogEntryView.name];
            DLog(@"%@",self.obj_catalogEntryView.resourceId);
            //[controller addURL:[NSURL URLWithString:self.obj_catalogEntryView.resourceId]];
            [controller addImage:self.imageView_.image];
            [self presentViewController:controller animated:YES completion:Nil];
        }
        else
        {
            
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Facebook account not found. Go to the Settings application to add your Facebook account to this device."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
    //Twitter
    else if ([sender tag] == 2)
    {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet setInitialText:self.obj_catalogEntryView.name];
            //[tweetSheet addURL:[NSURL URLWithString:self.obj_catalogEntryView.resourceId]];
            [tweetSheet addImage:self.imageView_.image];
            [self presentViewController:tweetSheet animated:YES completion:nil];
        }
        else
        {
            
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Twitter account not found. Go to the Settings application to add your Twitter account to this device."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
    //pinterest
    else if ([sender tag] == 3)
    {
        // Initialize a Pinterest instance with our client_id
        _pinterest = [[Pinterest alloc] initWithClientId:@"1443279" urlSchemeSuffix:@"prod"];
        [_pinterest createPinWithImageURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", k_imageurl,self.obj_catalogEntryView.fullImage]] sourceURL:[NSURL URLWithString:self.obj_catalogEntryView.resourceId] description:self.obj_catalogEntryView.shortDescription];
    }
    //google plus
    else if ([sender tag] == 4)
    {
        [[GPPSignIn sharedInstance] authenticate];
        [self reportAuthStatus];
    }
    //email
    else
    {
        if([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *emailDialog = [[MFMailComposeViewController alloc] init];
            emailDialog.mailComposeDelegate = self;
            [emailDialog setSubject:self.obj_catalogEntryView.name];
            [emailDialog setMessageBody:self.obj_catalogEntryView.shortDescription isHTML:NO];
            [emailDialog addAttachmentData:UIImageJPEGRepresentation(self.imageView_.image, 1) mimeType:@"image/jpeg" fileName:@"product.jpeg"];
            
            [self presentViewController:emailDialog animated:YES completion:^(void)
             {
                 NSLog(@"composer open");
             }];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Alert"
                                        message:@"Your Email account is not configured on device. Please configure your email settings in device settings."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
}

#pragma mark - googleplus delegate
- (void)reportAuthStatus
{
    if ([GPPSignIn sharedInstance].authentication)
    {
        NSLog(@"Status: Authenticated");
        
        id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
        
        [shareBuilder setContentDeepLinkID:@"589541872820-v8esfpae0go53eem2chjmud24qq5lj88.apps.googleusercontent.com"];
        [shareBuilder setTitle:self.obj_catalogEntryView.name description:self.obj_catalogEntryView.shortDescription thumbnailURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", k_imageurl,self.obj_catalogEntryView.fullImage]]];
        
        
        
        [shareBuilder open];
    }
    else
    {
        // To authenticate, use Google+ sign-in button.
        NSLog(@"Status: Not authenticated");
    }
    [self refreshUserInfo];
}



- (void)refreshUserInfo
{
    if ([GPPSignIn sharedInstance].authentication == nil)
    {
//        self.label_userName.text = @"<name>";
//        self.label_email.text = @"<email address>";
//        self.imageView_.image = nil;
        return;
    }
    
    DLog(@"%@",[GPPSignIn sharedInstance].userEmail);
    //self.label_email.text = [GPPSignIn sharedInstance].userEmail;
    
    // The googlePlusUser member will be populated only if the appropriate
    // scope is set when signing in.
    GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
    if (person == nil) {
        return;
    }
    
    DLog(@"%@",person.displayName);
    //self.label_userName.text = person.displayName;
    
    // Load avatar image asynchronously, in background
    dispatch_queue_t backgroundQueue =
    dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(backgroundQueue, ^{
        NSData *avatarData = nil;
        NSString *imageURLString = person.image.url;
        if (imageURLString) {
            NSURL *imageURL = [NSURL URLWithString:imageURLString];
            avatarData = [NSData dataWithContentsOfURL:imageURL];
        }
        
        if (avatarData) {
            // Update UI from the main thread when available
            dispatch_async(dispatch_get_main_queue(), ^{
                self.imageView_.image = [UIImage imageWithData:avatarData];
            });
        }
    });
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth error: (NSError *) error
{
    NSLog(@"Received error %@ and auth object %@",error, auth);
    
    [self reportAuthStatus];
}



#pragma mark - tableview delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.tableview_cep])
    {
        return [[[dic_shipping valueForKey:@"ShippingMap"] allKeys] count];
    }
    else
    {
        if ([self.obj_catalogEntryView.attributes count])
        {
            return [self.obj_catalogEntryView.attributes count];
        }
        else
        {
            return 1;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.tableview_cep])
    {
        cell_Cep *cell = (cell_Cep *)[tableView dequeueReusableCellWithIdentifier:@"cell_Cep"];
        
        if (cell == nil)
        {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_Cep" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = [topLevelObjects objectAtIndex:0];
        }
        
        cell.label_name.text = [[[dic_shipping valueForKey:@"ShippingMap"] valueForKey:[[[dic_shipping valueForKey:@"ShippingMap"] allKeys] objectAtIndex:indexPath.row]] objectAtIndex:0];
        cell.label_frete.text = [NSString stringWithFormat:@"R$ %@",[[[dic_shipping valueForKey:@"ShippingMap"] valueForKey:[[[dic_shipping valueForKey:@"ShippingMap"] allKeys] objectAtIndex:indexPath.row]] objectAtIndex:1]] ;
        cell.label_prazo.text = [[[dic_shipping valueForKey:@"ShippingMap"] valueForKey:[[[dic_shipping valueForKey:@"ShippingMap"] allKeys] objectAtIndex:indexPath.row]] objectAtIndex:2];
        
        return cell;
    }
    else
    {
        Cell_productdetailCharacterstics *cell = (Cell_productdetailCharacterstics *)[tableView dequeueReusableCellWithIdentifier:@"Cell_productdetailCharacterstics"];
        
        if (cell == nil)
        {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Cell_productdetailCharacterstics" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = [topLevelObjects objectAtIndex:0];
        }
        
        if ([self.obj_catalogEntryView.attributes count])
        {
            cell.textLabel.text = @"";
            self.obj_attribute = [self.obj_catalogEntryView.attributes objectAtIndex:indexPath.row];
            cell.label_charName.text = self.obj_attribute.name;
            
            if ([self.obj_attribute.values count])
            {
                self.obj_value = [self.obj_attribute.values objectAtIndex:0];
                cell.label_charDetail.text = self.obj_value.values;
            }
        }
        else
        {
            cell.textLabel.text = @"Não Characterstics disponíveis";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font=[UIFont fontWithName:@"Arial" size:18];
            cell.label_charDetail.text = @"";
            cell.label_charName.text = @"";
        }
         return cell;
    }
}

- (IBAction)action_cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - action_comments
- (IBAction)action_comments:(id)sender
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:k_isLogin] isEqualToString:@"yes"])
    {
        if (self.obj_xproductReviewList.reviewList)
        {
            [self performSegueWithIdentifier:@"segue_productComments" sender:self.obj_xproductReviewList.reviewList];
        }
    }
    else
    {
        [UtilitiesHelper showAlert:@"Você precisa estar logado para classificar a avalição"];
    }
}
#pragma mark - action_simulate
- (IBAction)action_simulate:(id)sender
{
        [self performSegueWithIdentifier:@"segue_productSimulate" sender:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_catalogEntryView.uniqueID,@"catalogEntryId",[[self.obj_catalogEntryView.price objectAtIndex:1] valueForKey:@"priceValue"],@"price", nil]];
}
#pragma mark - action_nasLojas
- (IBAction)action_nasLojas:(id)sender
{
    [self performSegueWithIdentifier:@"segue_productnasLojas" sender:self.obj_catalogEntryView.uniqueID];
}

#pragma mark - textField delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField isEqual:self.textfield_CEP])
    {
        NSString *text = textField.text;
        if (range.location == 9)
        {
            return NO;
        }
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSMutableString *mutableText = [text mutableCopy];
        if (mutableText.length > 5)
        {
            [mutableText insertString:@"-" atIndex:5];
        }
        text = mutableText;
        textField.text = text;
        return NO;
    }
    return YES;
}

#pragma mark - method_shopping Cart Counter
-(void)method_shoppingCartCounter:(NSString *)counter
{
    NSUserDefaults *obj = [NSUserDefaults standardUserDefaults];
    [obj setObject:[obj objectForKey:k_userId] forKey:k_userId];
    [obj setValue:counter forKey:k_count];
    
    [obj synchronize];
}

#pragma mark- mailComposer Delegates
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
            
        case MFMailComposeResultCancelled:
            NSLog(@"%@",@"Result: canceled") ;
            break;
        case MFMailComposeResultSaved:
            NSLog(@"%@",@"Result: saved");
            break;
        case MFMailComposeResultSent:
        {
            NSLog(@"%@",@"Result: Sent");
            break;
        }
        case MFMailComposeResultFailed:
            NSLog(@"%@",@"Result: Failed");
            break;
        default:
            NSLog(@"%@",@"Result: not sent");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end