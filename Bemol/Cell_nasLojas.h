//
//  Cell_nasLojas.h
//  Bemol
//
//  Created by Fahim Bilwani on 17/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cell_nasLojas : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label_name;
@property (weak, nonatomic) IBOutlet UILabel *label_address;
@property (weak, nonatomic) IBOutlet UILabel *label_displayvalue;
@property (weak, nonatomic) IBOutlet UILabel *label_number;
@property (weak, nonatomic) IBOutlet UILabel *label_distance;
@end
