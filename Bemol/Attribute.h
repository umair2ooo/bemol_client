#import <Foundation/Foundation.h>

@interface Attribute : NSObject {

    NSString *comparable;
    NSString *dataType;
    NSString *displayable;
    NSString *identifier;
    NSString *name;
    NSString *searchable;
    NSString *uniqueID;
    NSString *usage;
    NSMutableArray *values;

}

@property (nonatomic, copy) NSString *comparable;
@property (nonatomic, copy) NSString *dataType;
@property (nonatomic, copy) NSString *displayable;
@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *searchable;
@property (nonatomic, copy) NSString *uniqueID;
@property (nonatomic, copy) NSString *usage;
@property (nonatomic, copy) NSMutableArray *values;

+ (Attribute *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
