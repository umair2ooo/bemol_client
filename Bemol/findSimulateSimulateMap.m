#import "findSimulateSimulateMap.h"

@implementation findSimulateSimulateMap

@synthesize dataPedido;
@synthesize dataVencimento;
@synthesize entrada;
@synthesize financiamento;
@synthesize jurosAno;
@synthesize jurosMes;
@synthesize prazo;
@synthesize prestacao;
@synthesize sapCode;

+ (findSimulateSimulateMap *)instanceFromDictionary:(NSDictionary *)aDictionary {

    findSimulateSimulateMap *instance = [[findSimulateSimulateMap alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.dataPedido = [aDictionary objectForKey:@"DataPedido"];
    self.dataVencimento = [aDictionary objectForKey:@"dataVencimento"];
    self.entrada = [aDictionary objectForKey:@"entrada"];
    self.financiamento = [aDictionary objectForKey:@"financiamento"];
    self.jurosAno = [aDictionary objectForKey:@"jurosAno"];
    self.jurosMes = [aDictionary objectForKey:@"jurosMes"];
    self.prazo = [aDictionary objectForKey:@"prazo"];
    self.prestacao = [aDictionary objectForKey:@"prestacao"];
    self.sapCode = [aDictionary objectForKey:@"sapCode"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.dataPedido) {
        [dictionary setObject:self.dataPedido forKey:@"dataPedido"];
    }

    if (self.dataVencimento) {
        [dictionary setObject:self.dataVencimento forKey:@"dataVencimento"];
    }

    if (self.entrada) {
        [dictionary setObject:self.entrada forKey:@"entrada"];
    }

    if (self.financiamento) {
        [dictionary setObject:self.financiamento forKey:@"financiamento"];
    }

    if (self.jurosAno) {
        [dictionary setObject:self.jurosAno forKey:@"jurosAno"];
    }

    if (self.jurosMes) {
        [dictionary setObject:self.jurosMes forKey:@"jurosMes"];
    }

    if (self.prazo) {
        [dictionary setObject:self.prazo forKey:@"prazo"];
    }

    if (self.prestacao) {
        [dictionary setObject:self.prestacao forKey:@"prestacao"];
    }

    if (self.sapCode) {
        [dictionary setObject:self.sapCode forKey:@"sapCode"];
    }

    return dictionary;

}


@end
