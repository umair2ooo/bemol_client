#import <UIKit/UIKit.h>

@interface cell_checkOut : UITableViewCell
{
    
}
@property (weak, nonatomic) IBOutlet UILabel *label_description;
@property (weak, nonatomic) IBOutlet UILabel *label_price;
@property (weak, nonatomic) IBOutlet UILabel *label_quantity;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_product;

@end