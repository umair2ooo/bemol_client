#import "ProdutoList.h"

#import "List.h"

@implementation ProdutoList

@synthesize countTotal;
@synthesize list;

+ (ProdutoList *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ProdutoList *instance = [[ProdutoList alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.countTotal = [aDictionary objectForKey:@"countTotal"];

    NSArray *receivedList = [aDictionary objectForKey:@"list"];
    if ([receivedList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedList = [NSMutableArray arrayWithCapacity:[receivedList count]];
        for (NSDictionary *item in receivedList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedList addObject:[List instanceFromDictionary:item]];
            }
        }

        self.list = populatedList;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.countTotal) {
        [dictionary setObject:self.countTotal forKey:@"countTotal"];
    }

    if (self.list) {
        [dictionary setObject:self.list forKey:@"list"];
    }

    return dictionary;

}


@end
