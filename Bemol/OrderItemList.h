#import <Foundation/Foundation.h>

@interface OrderItemList : NSObject {

    BOOL cancelableProduct;
    NSString *itemStatus;
    NSString *productName;
    NSString *productURL;
    NSNumber *quantidade;
    NSString *sku;
    NSString *strSKUsEntregaId;
    NSString *strSKUsNotaFiscaisId;
    NSString *strSKUsStatus;
    NSNumber *totalPrice;
    NSNumber *unitPrice;

}

@property (nonatomic, assign) BOOL cancelableProduct;
@property (nonatomic, copy) NSString *itemStatus;
@property (nonatomic, copy) NSString *productName;
@property (nonatomic, copy) NSString *productURL;
@property (nonatomic, copy) NSNumber *quantidade;
@property (nonatomic, copy) NSString *sku;
@property (nonatomic, copy) NSString *strSKUsEntregaId;
@property (nonatomic, copy) NSString *strSKUsNotaFiscaisId;
@property (nonatomic, copy) NSString *strSKUsStatus;
@property (nonatomic, copy) NSNumber *totalPrice;
@property (nonatomic, copy) NSNumber *unitPrice;

+ (OrderItemList *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
