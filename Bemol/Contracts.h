#import <Foundation/Foundation.h>

@class ContractsContractsMap;

@interface Contracts : NSObject {

    ContractsContractsMap *contractsMap;

}

@property (nonatomic, strong) ContractsContractsMap *contractsMap;

+ (Contracts *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
