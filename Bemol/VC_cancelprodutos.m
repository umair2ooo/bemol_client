//
//  VC_cancelprodutos.m
//  Bemol
//
//  Created by Fahim Bilwani on 29/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "VC_cancelprodutos.h"
#import "NotaFiscal.h"
#import "OrderItemList.h"
#import "VC_pedidoMessage.h"

@interface VC_cancelprodutos ()

@property (weak, nonatomic) IBOutlet UITextView *textview_justification;
@property (weak, nonatomic) IBOutlet UILabel *label_productName;
@property (weak, nonatomic) IBOutlet UITextField *textfield_quantity;
@property (weak, nonatomic) IBOutlet UILabel *label_price;
@property (weak, nonatomic) IBOutlet UIButton *button_radiobutton1;
@property (weak, nonatomic) IBOutlet UIButton *button_radiobutton2;
@property (weak, nonatomic) IBOutlet UIButton *button_checkbox;

- (IBAction)action_back:(id)sender;
- (IBAction)action_cancel:(id)sender;
- (IBAction)action_radiobutton:(id)sender;

@property(nonatomic, strong)NotaFiscal *obj_notaFiscal;
@property(nonatomic, strong)OrderItemList *obj_orderItemList;


@end

@implementation VC_cancelprodutos

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Historico de compras"];
    
    self.obj_notaFiscal = [self.obj_orderDetails.notaFiscal objectAtIndex:0];
    self.obj_orderItemList = [self.obj_notaFiscal.itemList objectAtIndex:0];
    self.label_productName.text = self.obj_orderItemList.productName;
    self.textfield_quantity.text = [NSString stringWithFormat:@"%@",self.obj_orderItemList.quantidade];
    self.label_price.text = [NSString stringWithFormat:@"R$ %@",self.obj_orderItemList.unitPrice];
    [self.button_radiobutton1 setSelected:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segue_message"])
    {
        VC_pedidoMessage *obj = segue.destinationViewController;
        obj.bool_ispedido = self.bool_ispedido;
    }
}


- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_cancel:(id)sender
{
    if (self.button_checkbox.selected)
    {
        [[ServiceModel sharedClient] POST:k_cancelPedido
                               parameters:[NSDictionary dictionaryWithObjectsAndKeys:@"0",@"isPedido",
                                           self.obj_orderDetails.orderId,@"orderId",
                                           self.obj_orderDetails.lojaId,@"lojaId",
                                           self.textview_justification.text,@"cancelJustification",
                                           self.obj_orderItemList.strSKUsStatus,@"cancelStatus",
                                           self.obj_orderItemList.sku,@"cancelSKUs",
                                           self.button_radiobutton1.selected?@"S":@"N",@"cancelAction",
                                           @"",@"strSKUsEntregaId",
                                           @"",@"strSKUsNotaFiscaisId",
                                           self.textfield_quantity.text,@"quantidade",nil]
                                   onView:self.view
                                  success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             if (![responseObject valueForKey:@"errors"])
             {
                 
                 if ([[responseObject valueForKey:@"valor"] boolValue] == TRUE)
                 {
                     [self performSegueWithIdentifier:@"segue_Produtomessage" sender:nil];
                 }
             }
             else
             {
                 [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                             valueForKey:@"errorMessage"]];
             }
         }
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             DLog(@"%@",error.description);
             [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
         }];

    }
    else
    {
        [UtilitiesHelper showAlert:@"Por favor, aceite os termos devoluções e trocas"];
    }
}

- (IBAction)action_radiobutton:(id)sender
{
    if ([sender tag] == 1)
    {
        if (self.button_radiobutton1.selected)
        {
            [self.button_radiobutton2 setSelected:YES];
            [self.button_radiobutton1 setSelected:NO];
        }
        else
        {
            [self.button_radiobutton1 setSelected:YES];
            [self.button_radiobutton2 setSelected:NO];
        }
    }
    else if ([sender tag] == 2)
    {
        if (self.button_radiobutton2.selected)
        {
            [self.button_radiobutton1 setSelected:YES];
            [self.button_radiobutton2 setSelected:NO];
        }
        else
        {
            [self.button_radiobutton2 setSelected:YES];
            [self.button_radiobutton1 setSelected:NO];
        }
    }
    else
    {
        if (self.button_checkbox.selected)
            [self.button_checkbox setSelected:NO];
        else
            [self.button_checkbox setSelected:YES];
    }
}

@end
