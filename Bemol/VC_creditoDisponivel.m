#import "VC_creditoDisponivel.h"
#import "AvailableCredits.h"
#import "MACircleProgressIndicator.h"
#import "CartaoBemol.h"
#import "CartaoBemolCreditoMap.h"

@interface VC_creditoDisponivel ()
{
    NSTimer *timer_Points;
}

@property (weak, nonatomic) IBOutlet UILabel *label_limit;
@property (weak, nonatomic) IBOutlet UILabel *label_utilizado;
@property (weak, nonatomic) IBOutlet UILabel *label_disponivel;
@property (weak, nonatomic) IBOutlet UILabel *label_Name;

@property (weak, nonatomic) IBOutlet MACircleProgressIndicator *smallProgressIndicator;
@property(nonatomic, strong)AvailableCredits *obj_availableCredits;
@property(nonatomic, strong) CartaoBemol *obj_CartaoBemol;
@property(nonatomic, strong) CartaoBemolCreditoMap *obj_CartaoBemolCreditoMap;

@end

@implementation VC_creditoDisponivel

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Crédito Disponível"];
    
    self.label_Name.text = [[[ApplicationDelegate.single.dic_userDetails valueForKey:@"firstName"] componentsSeparatedByString:@" "] objectAtIndex:0];
    
    MACircleProgressIndicator *appearance = [MACircleProgressIndicator appearance];
    
    // The color property sets the actual color of the procress circle (how
    // suprising ;) )
    appearance.color = [UIColor blueColor];
    appearance.strokeWidth = 20.0;
    timer_Points = [NSTimer scheduledTimerWithTimeInterval:0.2f target:self selector:@selector(method_createCircle) userInfo:nil repeats:YES];
    

    if (ApplicationDelegate.single.obj_CartaoBemol)
    {
        self.obj_CartaoBemolCreditoMap = ApplicationDelegate.single.obj_CartaoBemol.creditoMap;
        self.label_limit.text = [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_CartaoBemolCreditoMap.limiteDeCredito]] ;
        self.label_utilizado.text =  [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_CartaoBemolCreditoMap.saldoUtilizado]];
        self.label_disponivel.text =  [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_CartaoBemolCreditoMap.saldoDisponivel]];
        
        [[NSUserDefaults standardUserDefaults] setObject:self.obj_CartaoBemolCreditoMap.limiteDeCredito forKey:k_limit];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        [[ServiceModel sharedClient] GET:k_cartaoBemol
                              parameters:nil
                                  onView:self.view
                                 success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             if (![responseObject valueForKey:@"errors"])
             {
                 ApplicationDelegate.single.obj_CartaoBemol = [CartaoBemol instanceFromDictionary:responseObject];
                 self.obj_CartaoBemolCreditoMap = ApplicationDelegate.single.obj_CartaoBemol.creditoMap;
                 self.label_limit.text = [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_CartaoBemolCreditoMap.limiteDeCredito]] ;
                 self.label_utilizado.text =  [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_CartaoBemolCreditoMap.saldoUtilizado]];
                 self.label_disponivel.text =  [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_CartaoBemolCreditoMap.saldoDisponivel]];
                 
                 [[NSUserDefaults standardUserDefaults] setObject:self.obj_CartaoBemolCreditoMap.limiteDeCredito forKey:k_limit];
                 [[NSUserDefaults standardUserDefaults] synchronize];
             }
             else
             {
                 [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                             valueForKey:@"errorMessage"]];
             }
         }
                                 failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             DLog(@"%@",error.description);
             //[UtilitiesHelper showAlert:error.description];
             [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
         }];
    }
    
    
//    [[ServiceModel sharedClient] GET:k_AvailableCredit
//                          parameters:nil
//                              onView:self.view
//                             success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         DLog(@"%@",responseObject);
//         if (![responseObject valueForKey:@"errors"])
//         {
//             self.obj_availableCredits = [AvailableCredits instanceFromDictionary:responseObject];
//             self.label_limit.text = [NSString stringWithFormat:@"R$ %@",self.obj_availableCredits.limiteTotal];
//             self.label_utilizado.text = [NSString stringWithFormat:@"R$ %@",self.obj_availableCredits.limite];
//             self.label_disponivel.text = [NSString stringWithFormat:@"R$ %@",self.obj_availableCredits.limiteDiff];
//             
//             //utilizido balance used
//             //disponivel available balance
//         }
//         else
//         {
//             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
//                                         valueForKey:@"errorMessage"]];
//         }
//     }
//                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
//     {
//         DLog(@"%@",error.description);
//         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
//     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)method_createCircle
{
    if (self.smallProgressIndicator.value <= 0.600000)
    {
        float newValue = self.smallProgressIndicator.value + 0.1;
        self.smallProgressIndicator.value = newValue;
    }
    else
    {
        [timer_Points invalidate];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end