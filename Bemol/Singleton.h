#import <Foundation/Foundation.h>
#import "XMLConverter.h"
#import "CartaoBemol.h"

//@protocol WebServiceResponseDelegate <NSObject>
//
//-(void)delegate_responseData:(id)data error:(NSError *)error servicename:(NSString *)servicename;
//
//@end

@interface Singleton : NSObject<NSURLConnectionDelegate,NSXMLParserDelegate>

@property(nonatomic, strong)NSString *string_controllerName;
@property(nonatomic, strong)NSDictionary *dic_productDetail;
@property(nonatomic, strong)NSMutableDictionary *dic_userDetails;
@property(nonatomic, strong)NSString *string_orderId;
@property(nonatomic) BOOL isGuestCheckOut;
@property(nonatomic) BOOL bool_isComingfromCart;
@property NSString *soapMessage;
@property NSMutableData *webResponseData;
@property(nonatomic, strong)NSString *string_serviceName;
@property(nonatomic, strong) CartaoBemol *obj_CartaoBemol;
//@property(nonatomic, strong)id <WebServiceResponseDelegate>delegate_;


+(Singleton *)retriveSingleton;

-(BOOL) method_connectToInternet;
-(BOOL) method_NSStringIsValidEmail:(NSString *)checkString;
-(BOOL) method_checkOnlySpaces:(NSString *)str;
-(BOOL)method_phoneNumberValidation:(NSString *)phoneNumber;

-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width;

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews;



@end