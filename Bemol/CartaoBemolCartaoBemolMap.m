#import "CartaoBemolCartaoBemolMap.h"

@implementation CartaoBemolCartaoBemolMap

@synthesize string_9;
@synthesize address;
@synthesize bairro;
@synthesize birthDay;
@synthesize birthMonth;
@synthesize birthYear;
@synthesize cEP;
@synthesize cLI;
@synthesize cPF;
@synthesize gender;
@synthesize image;
@synthesize mobile;
@synthesize name;
@synthesize number;
@synthesize state;
@synthesize t03;
@synthesize telephone;
@synthesize vALIDOATE;
@synthesize viaCartao;

+ (CartaoBemolCartaoBemolMap *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CartaoBemolCartaoBemolMap *instance = [[CartaoBemolCartaoBemolMap alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.string_9 = [aDictionary objectForKey:@"90"];
    self.address = [aDictionary objectForKey:@"Address"];
    self.bairro = [aDictionary objectForKey:@"Bairro"];
    self.birthDay = [aDictionary objectForKey:@"Birth_Day"];
    self.birthMonth = [aDictionary objectForKey:@"Birth_Month"];
    self.birthYear = [aDictionary objectForKey:@"Birth_Year"];
    self.cEP = [aDictionary objectForKey:@"CEP"];
    self.cLI = [aDictionary objectForKey:@"CLI"];
    self.cPF = [aDictionary objectForKey:@"CPF"];
    self.gender = [aDictionary objectForKey:@"Gender"];
    self.image = [aDictionary objectForKey:@"Image"];
    self.mobile = [aDictionary objectForKey:@"Mobile"];
    self.name = [aDictionary objectForKey:@"Name"];
    self.number = [aDictionary objectForKey:@"Number"];
    self.state = [aDictionary objectForKey:@"State"];
    self.t03 = [aDictionary objectForKey:@"T03"];
    self.telephone = [aDictionary objectForKey:@"Telephone"];
    self.vALIDOATE = [aDictionary objectForKey:@"validade"];
    self.viaCartao = [aDictionary objectForKey:@"viaCartao"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.string_9) {
        [dictionary setObject:self.string_9 forKey:@"90"];
    }

    if (self.address) {
        [dictionary setObject:self.address forKey:@"address"];
    }

    if (self.bairro) {
        [dictionary setObject:self.bairro forKey:@"bairro"];
    }

    if (self.birthDay) {
        [dictionary setObject:self.birthDay forKey:@"birthDay"];
    }

    if (self.birthMonth) {
        [dictionary setObject:self.birthMonth forKey:@"birthMonth"];
    }

    if (self.birthYear) {
        [dictionary setObject:self.birthYear forKey:@"birthYear"];
    }

    if (self.cEP) {
        [dictionary setObject:self.cEP forKey:@"cEP"];
    }

    if (self.cLI) {
        [dictionary setObject:self.cLI forKey:@"cLI"];
    }

    if (self.cPF) {
        [dictionary setObject:self.cPF forKey:@"cPF"];
    }

    if (self.gender) {
        [dictionary setObject:self.gender forKey:@"gender"];
    }

    if (self.image) {
        [dictionary setObject:self.image forKey:@"image"];
    }

    if (self.mobile) {
        [dictionary setObject:self.mobile forKey:@"mobile"];
    }

    if (self.name) {
        [dictionary setObject:self.name forKey:@"name"];
    }

    if (self.number) {
        [dictionary setObject:self.number forKey:@"number"];
    }

    if (self.state) {
        [dictionary setObject:self.state forKey:@"state"];
    }

    if (self.t03) {
        [dictionary setObject:self.t03 forKey:@"t03"];
    }

    if (self.telephone) {
        [dictionary setObject:self.telephone forKey:@"telephone"];
    }

    if (self.vALIDOATE) {
        [dictionary setObject:self.vALIDOATE forKey:@"validade"];
    }
    
    if (self.viaCartao) {
        [dictionary setObject:self.viaCartao forKey:@"viaCartao"];
    }

    return dictionary;

}


@end
