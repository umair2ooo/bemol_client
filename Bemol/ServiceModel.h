//
//  ServiceModel.h
//

#import "XMLConverter.h"
#import "AFHTTPRequestOperationManager.h"

//@protocol WebServiceResponseDelegate <NSObject>
//
//-(void)delegate_responseData:(id)data error:(NSError *)error servicename:(NSString *)servicename;
//
//@end

@interface ServiceModel : AFHTTPRequestOperationManager
{
    
    
}

@property (nonatomic, assign) BOOL  isSessionValid;
@property(nonatomic, strong) NSString *soapMessage;
@property NSMutableData *webResponseData;
@property(nonatomic, strong) NSString *string_serviceName;
//@property(nonatomic, strong)id <WebServiceResponseDelegate>delegate_;

+ (instancetype)sharedClient;


- (AFHTTPRequestOperation *)GET:(NSString *)URLString
                      parameters:(NSDictionary *)parameters
                          onView:(UIView *)loaderOnView
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;


//                     controller:(UIViewController *)controller_ selector_:(SEL)selctor_


- (AFHTTPRequestOperation *)POST:(NSString *)URLString
                      parameters:(NSDictionary *)parameters
                          onView:(UIView *)loaderOnView
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;


- (AFHTTPRequestOperation *)PUT:(NSString *)URLString
                     parameters:(NSDictionary *)parameters
                         onView:(UIView *)loaderOnView
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;


- (AFHTTPRequestOperation *)DELETE:(NSString *)URLString
                        parameters:(NSDictionary *)parameters
                            onView:(UIView *)loaderOnView
                           success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;


- (void) cancelOperation:(UIView *)view;


//- (void)method_getProductListByCategory:(NSString *)uniqueid;
//
//- (void)method_forgotPassword:(NSString *)email_;
@end
