#import "XCredFlexMap.h"

@implementation XCredFlexMap

@synthesize comEntrada;
@synthesize dataPedido;
@synthesize indexPlana;
@synthesize sapCode;

+ (XCredFlexMap *)instanceFromDictionary:(NSDictionary *)aDictionary {

    XCredFlexMap *instance = [[XCredFlexMap alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.comEntrada = [aDictionary objectForKey:@"com_entrada"];
    self.dataPedido = [aDictionary objectForKey:@"DataPedido"];
    self.indexPlana = [aDictionary objectForKey:@"index_plana"];
    self.sapCode = [aDictionary objectForKey:@"SapCode"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.comEntrada) {
        [dictionary setObject:self.comEntrada forKey:@"comEntrada"];
    }

    if (self.dataPedido) {
        [dictionary setObject:self.dataPedido forKey:@"dataPedido"];
    }

    if (self.indexPlana) {
        [dictionary setObject:self.indexPlana forKey:@"indexPlana"];
    }

    if (self.sapCode) {
        [dictionary setObject:self.sapCode forKey:@"sapCode"];
    }

    return dictionary;

}


@end
