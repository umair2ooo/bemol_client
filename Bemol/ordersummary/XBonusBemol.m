#import "XBonusBemol.h"

@implementation XBonusBemol

@synthesize bonusBemol;
@synthesize listaBonusBemol;
@synthesize valeCredito;

+ (XBonusBemol *)instanceFromDictionary:(NSDictionary *)aDictionary {

    XBonusBemol *instance = [[XBonusBemol alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.bonusBemol = [aDictionary objectForKey:@"bonusBemol"];

    NSArray *receivedListaBonusBemol = [aDictionary objectForKey:@"listaBonusBemol"];
    if ([receivedListaBonusBemol isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedListaBonusBemol = [NSMutableArray arrayWithCapacity:[receivedListaBonusBemol count]];
        for (NSDictionary *item in receivedListaBonusBemol) {
            [populatedListaBonusBemol addObject:item];
        }

        self.listaBonusBemol = populatedListaBonusBemol;

    }
    self.valeCredito = [aDictionary objectForKey:@"valeCredito"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.bonusBemol) {
        [dictionary setObject:self.bonusBemol forKey:@"bonusBemol"];
    }

    if (self.listaBonusBemol) {
        [dictionary setObject:self.listaBonusBemol forKey:@"listaBonusBemol"];
    }

    if (self.valeCredito) {
        [dictionary setObject:self.valeCredito forKey:@"valeCredito"];
    }

    return dictionary;

}


@end
