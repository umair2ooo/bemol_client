#import "UsableBillingAddres.h"

@implementation UsableBillingAddres

@synthesize addressId;
@synthesize nickName;

+ (UsableBillingAddres *)instanceFromDictionary:(NSDictionary *)aDictionary {

    UsableBillingAddres *instance = [[UsableBillingAddres alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.addressId = [aDictionary objectForKey:@"addressId"];
    self.nickName = [aDictionary objectForKey:@"nickName"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.addressId) {
        [dictionary setObject:self.addressId forKey:@"addressId"];
    }

    if (self.nickName) {
        [dictionary setObject:self.nickName forKey:@"nickName"];
    }

    return dictionary;

}


@end
