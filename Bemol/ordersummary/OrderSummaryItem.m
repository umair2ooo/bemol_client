#import "OrderSummaryItem.h"

#import "UsableShippingChargePolicy.h"

@implementation OrderSummaryItem

@synthesize addressId;
@synthesize addressLine;
@synthesize bairo;
@synthesize carrier;
@synthesize city;
@synthesize contractId;
@synthesize correlationGroup;
@synthesize country;
@synthesize createDate;
@synthesize currency;
@synthesize descriptionText;
@synthesize email1;
@synthesize email2;
@synthesize fax1;
@synthesize fax2;
@synthesize firstName;
@synthesize freeGift;
@synthesize fulfillmentCenterId;
@synthesize fulfillmentCenterName;
@synthesize isExpedited;
@synthesize lastName;
@synthesize lastUpdateDate;
@synthesize middleName;
@synthesize nickName;
@synthesize numero;
@synthesize offerID;
@synthesize orderItemFulfillmentStatus;
@synthesize orderItemId;
@synthesize orderItemInventoryStatus;
@synthesize orderItemPrice;
@synthesize orderItemStatus;
@synthesize partNumber;
@synthesize personTitle;
@synthesize phone1;
@synthesize phone1Publish;
@synthesize phone1Type;
@synthesize phone2;
@synthesize phone2Publish;
@synthesize phone2Type;
@synthesize postalCode;
@synthesize productId;
@synthesize productShortDescription;
@synthesize productThumbnail;
@synthesize productUrl;
@synthesize quantity;
@synthesize salesTax;
@synthesize salesTaxCurrency;
@synthesize shipModeCode;
@synthesize shipModeDescription;
@synthesize shipModeId;
@synthesize shippingCharge;
@synthesize shippingChargeCurrency;
@synthesize shippingTax;
@synthesize shippingTaxCurrency;
@synthesize stateOrProvinceName;
@synthesize unitPrice;
@synthesize unitQuantity;
@synthesize unitUom;
@synthesize uOM;
@synthesize usableShippingChargePolicy;

+ (OrderSummaryItem *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OrderSummaryItem *instance = [[OrderSummaryItem alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.addressId = [aDictionary objectForKey:@"addressId"];

    NSArray *receivedAddressLine = [aDictionary objectForKey:@"addressLine"];
    if ([receivedAddressLine isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedAddressLine = [NSMutableArray arrayWithCapacity:[receivedAddressLine count]];
        for (NSDictionary *item in receivedAddressLine) {
            [populatedAddressLine addObject:item];
        }

        self.addressLine = populatedAddressLine;

    }
    self.bairo = [aDictionary objectForKey:@"bairo"];
    self.carrier = [aDictionary objectForKey:@"carrier"];
    self.city = [aDictionary objectForKey:@"city"];
    self.contractId = [aDictionary objectForKey:@"contractId"];
    self.correlationGroup = [aDictionary objectForKey:@"correlationGroup"];
    self.country = [aDictionary objectForKey:@"country"];
    self.createDate = [aDictionary objectForKey:@"createDate"];
    self.currency = [aDictionary objectForKey:@"currency"];
    self.descriptionText = [aDictionary objectForKey:@"description"];
    self.email1 = [aDictionary objectForKey:@"email1"];
    self.email2 = [aDictionary objectForKey:@"email2"];
    self.fax1 = [aDictionary objectForKey:@"fax1"];
    self.fax2 = [aDictionary objectForKey:@"fax2"];
    self.firstName = [aDictionary objectForKey:@"firstName"];
    self.freeGift = [aDictionary objectForKey:@"freeGift"];
    self.fulfillmentCenterId = [aDictionary objectForKey:@"fulfillmentCenterId"];
    self.fulfillmentCenterName = [aDictionary objectForKey:@"fulfillmentCenterName"];
    self.isExpedited = [aDictionary objectForKey:@"isExpedited"];
    self.lastName = [aDictionary objectForKey:@"lastName"];
    self.lastUpdateDate = [aDictionary objectForKey:@"lastUpdateDate"];
    self.middleName = [aDictionary objectForKey:@"middleName"];
    self.nickName = [aDictionary objectForKey:@"nickName"];
    self.numero = [aDictionary objectForKey:@"numero"];
    self.offerID = [aDictionary objectForKey:@"offerID"];
    self.orderItemFulfillmentStatus = [aDictionary objectForKey:@"orderItemFulfillmentStatus"];
    self.orderItemId = [aDictionary objectForKey:@"orderItemId"];
    self.orderItemInventoryStatus = [aDictionary objectForKey:@"orderItemInventoryStatus"];
    self.orderItemPrice = [aDictionary objectForKey:@"orderItemPrice"];
    self.orderItemStatus = [aDictionary objectForKey:@"orderItemStatus"];
    self.partNumber = [aDictionary objectForKey:@"partNumber"];
    self.personTitle = [aDictionary objectForKey:@"personTitle"];
    self.phone1 = [aDictionary objectForKey:@"phone1"];
    self.phone1Publish = [aDictionary objectForKey:@"phone1Publish"];
    self.phone1Type = [aDictionary objectForKey:@"phone1Type"];
    self.phone2 = [aDictionary objectForKey:@"phone2"];
    self.phone2Publish = [aDictionary objectForKey:@"phone2Publish"];
    self.phone2Type = [aDictionary objectForKey:@"phone2Type"];
    self.postalCode = [aDictionary objectForKey:@"postalCode"];
    self.productId = [aDictionary objectForKey:@"productId"];
    self.productShortDescription = [aDictionary objectForKey:@"productShortDescription"];
    self.productThumbnail = [aDictionary objectForKey:@"productThumbnail"];
    self.productUrl = [aDictionary objectForKey:@"productUrl"];
    self.quantity = [aDictionary objectForKey:@"quantity"];
    self.salesTax = [aDictionary objectForKey:@"salesTax"];
    self.salesTaxCurrency = [aDictionary objectForKey:@"salesTaxCurrency"];
    self.shipModeCode = [aDictionary objectForKey:@"shipModeCode"];
    self.shipModeDescription = [aDictionary objectForKey:@"shipModeDescription"];
    self.shipModeId = [aDictionary objectForKey:@"shipModeId"];
    self.shippingCharge = [aDictionary objectForKey:@"shippingCharge"];
    self.shippingChargeCurrency = [aDictionary objectForKey:@"shippingChargeCurrency"];
    self.shippingTax = [aDictionary objectForKey:@"shippingTax"];
    self.shippingTaxCurrency = [aDictionary objectForKey:@"shippingTaxCurrency"];
    self.stateOrProvinceName = [aDictionary objectForKey:@"stateOrProvinceName"];
    self.unitPrice = [aDictionary objectForKey:@"unitPrice"];
    self.unitQuantity = [aDictionary objectForKey:@"unitQuantity"];
    self.unitUom = [aDictionary objectForKey:@"unitUom"];
    self.uOM = [aDictionary objectForKey:@"UOM"];

    NSArray *receivedUsableShippingChargePolicy = [aDictionary objectForKey:@"usableShippingChargePolicy"];
    if ([receivedUsableShippingChargePolicy isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedUsableShippingChargePolicy = [NSMutableArray arrayWithCapacity:[receivedUsableShippingChargePolicy count]];
        for (NSDictionary *item in receivedUsableShippingChargePolicy) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedUsableShippingChargePolicy addObject:[UsableShippingChargePolicy instanceFromDictionary:item]];
            }
        }

        self.usableShippingChargePolicy = populatedUsableShippingChargePolicy;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.addressId) {
        [dictionary setObject:self.addressId forKey:@"addressId"];
    }

    if (self.addressLine) {
        [dictionary setObject:self.addressLine forKey:@"addressLine"];
    }

    if (self.bairo) {
        [dictionary setObject:self.bairo forKey:@"bairo"];
    }

    if (self.carrier) {
        [dictionary setObject:self.carrier forKey:@"carrier"];
    }

    if (self.city) {
        [dictionary setObject:self.city forKey:@"city"];
    }

    if (self.contractId) {
        [dictionary setObject:self.contractId forKey:@"contractId"];
    }

    if (self.correlationGroup) {
        [dictionary setObject:self.correlationGroup forKey:@"correlationGroup"];
    }

    if (self.country) {
        [dictionary setObject:self.country forKey:@"country"];
    }

    if (self.createDate) {
        [dictionary setObject:self.createDate forKey:@"createDate"];
    }

    if (self.currency) {
        [dictionary setObject:self.currency forKey:@"currency"];
    }

    if (self.descriptionText) {
        [dictionary setObject:self.descriptionText forKey:@"descriptionText"];
    }

    if (self.email1) {
        [dictionary setObject:self.email1 forKey:@"email1"];
    }

    if (self.email2) {
        [dictionary setObject:self.email2 forKey:@"email2"];
    }

    if (self.fax1) {
        [dictionary setObject:self.fax1 forKey:@"fax1"];
    }

    if (self.fax2) {
        [dictionary setObject:self.fax2 forKey:@"fax2"];
    }

    if (self.firstName) {
        [dictionary setObject:self.firstName forKey:@"firstName"];
    }

    if (self.freeGift) {
        [dictionary setObject:self.freeGift forKey:@"freeGift"];
    }

    if (self.fulfillmentCenterId) {
        [dictionary setObject:self.fulfillmentCenterId forKey:@"fulfillmentCenterId"];
    }

    if (self.fulfillmentCenterName) {
        [dictionary setObject:self.fulfillmentCenterName forKey:@"fulfillmentCenterName"];
    }

    if (self.isExpedited) {
        [dictionary setObject:self.isExpedited forKey:@"isExpedited"];
    }

    if (self.lastName) {
        [dictionary setObject:self.lastName forKey:@"lastName"];
    }

    if (self.lastUpdateDate) {
        [dictionary setObject:self.lastUpdateDate forKey:@"lastUpdateDate"];
    }

    if (self.middleName) {
        [dictionary setObject:self.middleName forKey:@"middleName"];
    }

    if (self.nickName) {
        [dictionary setObject:self.nickName forKey:@"nickName"];
    }

    if (self.numero) {
        [dictionary setObject:self.numero forKey:@"numero"];
    }

    if (self.offerID) {
        [dictionary setObject:self.offerID forKey:@"offerID"];
    }

    if (self.orderItemFulfillmentStatus) {
        [dictionary setObject:self.orderItemFulfillmentStatus forKey:@"orderItemFulfillmentStatus"];
    }

    if (self.orderItemId) {
        [dictionary setObject:self.orderItemId forKey:@"orderItemId"];
    }

    if (self.orderItemInventoryStatus) {
        [dictionary setObject:self.orderItemInventoryStatus forKey:@"orderItemInventoryStatus"];
    }

    if (self.orderItemPrice) {
        [dictionary setObject:self.orderItemPrice forKey:@"orderItemPrice"];
    }

    if (self.orderItemStatus) {
        [dictionary setObject:self.orderItemStatus forKey:@"orderItemStatus"];
    }

    if (self.partNumber) {
        [dictionary setObject:self.partNumber forKey:@"partNumber"];
    }

    if (self.personTitle) {
        [dictionary setObject:self.personTitle forKey:@"personTitle"];
    }

    if (self.phone1) {
        [dictionary setObject:self.phone1 forKey:@"phone1"];
    }

    if (self.phone1Publish) {
        [dictionary setObject:self.phone1Publish forKey:@"phone1Publish"];
    }

    if (self.phone1Type) {
        [dictionary setObject:self.phone1Type forKey:@"phone1Type"];
    }

    if (self.phone2) {
        [dictionary setObject:self.phone2 forKey:@"phone2"];
    }

    if (self.phone2Publish) {
        [dictionary setObject:self.phone2Publish forKey:@"phone2Publish"];
    }

    if (self.phone2Type) {
        [dictionary setObject:self.phone2Type forKey:@"phone2Type"];
    }

    if (self.postalCode) {
        [dictionary setObject:self.postalCode forKey:@"postalCode"];
    }

    if (self.productId) {
        [dictionary setObject:self.productId forKey:@"productId"];
    }

    if (self.productShortDescription) {
        [dictionary setObject:self.productShortDescription forKey:@"productShortDescription"];
    }

    if (self.productThumbnail) {
        [dictionary setObject:self.productThumbnail forKey:@"productThumbnail"];
    }

    if (self.productUrl) {
        [dictionary setObject:self.productUrl forKey:@"productUrl"];
    }

    if (self.quantity) {
        [dictionary setObject:self.quantity forKey:@"quantity"];
    }

    if (self.salesTax) {
        [dictionary setObject:self.salesTax forKey:@"salesTax"];
    }

    if (self.salesTaxCurrency) {
        [dictionary setObject:self.salesTaxCurrency forKey:@"salesTaxCurrency"];
    }

    if (self.shipModeCode) {
        [dictionary setObject:self.shipModeCode forKey:@"shipModeCode"];
    }

    if (self.shipModeDescription) {
        [dictionary setObject:self.shipModeDescription forKey:@"shipModeDescription"];
    }

    if (self.shipModeId) {
        [dictionary setObject:self.shipModeId forKey:@"shipModeId"];
    }

    if (self.shippingCharge) {
        [dictionary setObject:self.shippingCharge forKey:@"shippingCharge"];
    }

    if (self.shippingChargeCurrency) {
        [dictionary setObject:self.shippingChargeCurrency forKey:@"shippingChargeCurrency"];
    }

    if (self.shippingTax) {
        [dictionary setObject:self.shippingTax forKey:@"shippingTax"];
    }

    if (self.shippingTaxCurrency) {
        [dictionary setObject:self.shippingTaxCurrency forKey:@"shippingTaxCurrency"];
    }

    if (self.stateOrProvinceName) {
        [dictionary setObject:self.stateOrProvinceName forKey:@"stateOrProvinceName"];
    }

    if (self.unitPrice) {
        [dictionary setObject:self.unitPrice forKey:@"unitPrice"];
    }

    if (self.unitQuantity) {
        [dictionary setObject:self.unitQuantity forKey:@"unitQuantity"];
    }

    if (self.unitUom) {
        [dictionary setObject:self.unitUom forKey:@"unitUom"];
    }

    if (self.uOM) {
        [dictionary setObject:self.uOM forKey:@"uOM"];
    }

    if (self.usableShippingChargePolicy) {
        [dictionary setObject:self.usableShippingChargePolicy forKey:@"usableShippingChargePolicy"];
    }

    return dictionary;

}


@end
