#import "XShippingMode.h"

@implementation XShippingMode

@synthesize shipModeCode;
@synthesize shipModeId;
@synthesize shipModeName;
@synthesize shipModePrazo;
@synthesize shipModePrice;

+ (XShippingMode *)instanceFromDictionary:(NSDictionary *)aDictionary {

    XShippingMode *instance = [[XShippingMode alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.shipModeCode = [aDictionary objectForKey:@"shipModeCode"];
    self.shipModeId = [aDictionary objectForKey:@"shipModeId"];
    self.shipModeName = [aDictionary objectForKey:@"shipModeName"];
    self.shipModePrazo = [aDictionary objectForKey:@"shipModePrazo"];
    self.shipModePrice = [aDictionary objectForKey:@"shipModePrice"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.shipModeCode) {
        [dictionary setObject:self.shipModeCode forKey:@"shipModeCode"];
    }

    if (self.shipModeId) {
        [dictionary setObject:self.shipModeId forKey:@"shipModeId"];
    }

    if (self.shipModeName) {
        [dictionary setObject:self.shipModeName forKey:@"shipModeName"];
    }

    if (self.shipModePrazo) {
        [dictionary setObject:self.shipModePrazo forKey:@"shipModePrazo"];
    }

    if (self.shipModePrice) {
        [dictionary setObject:self.shipModePrice forKey:@"shipModePrice"];
    }

    return dictionary;

}


@end
