#import <Foundation/Foundation.h>

@interface UsablePaymentInformation : NSObject {

    NSString *descriptionText;
    NSString *language;
    NSString *paymentMethodName;
    NSString *paymentTermConditionId;
    NSMutableArray *usableBillingAddress;
    NSString *xumetAttrPageName;
    NSString *xumetPolicyId;

}

@property (nonatomic, copy) NSString *descriptionText;
@property (nonatomic, copy) NSString *language;
@property (nonatomic, copy) NSString *paymentMethodName;
@property (nonatomic, copy) NSString *paymentTermConditionId;
@property (nonatomic, copy) NSMutableArray *usableBillingAddress;
@property (nonatomic, copy) NSString *xumetAttrPageName;
@property (nonatomic, copy) NSString *xumetPolicyId;

+ (UsablePaymentInformation *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
