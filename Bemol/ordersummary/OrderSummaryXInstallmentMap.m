#import "OrderSummaryXInstallmentMap.h"

#import "AMEXRegistradoCheckout.h"
#import "DiscoverRegistradoCheckout.h"
#import "MasterCardRegistradoCheckout.h"
#import "VISARegistradoCheckout.h"

@implementation OrderSummaryXInstallmentMap

@synthesize aMEXRegistradoCheckout;
@synthesize discoverRegistradoCheckout;
@synthesize garantiaItemStatus;
@synthesize masterCardRegistradoCheckout;
@synthesize vISARegistradoCheckout;

+ (OrderSummaryXInstallmentMap *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OrderSummaryXInstallmentMap *instance = [[OrderSummaryXInstallmentMap alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }


    NSArray *receivedAMEXRegistradoCheckout = [aDictionary objectForKey:@"AMEXRegistradoCheckout"];
    if ([receivedAMEXRegistradoCheckout isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedAMEXRegistradoCheckout = [NSMutableArray arrayWithCapacity:[receivedAMEXRegistradoCheckout count]];
        for (NSDictionary *item in receivedAMEXRegistradoCheckout) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedAMEXRegistradoCheckout addObject:[AMEXRegistradoCheckout instanceFromDictionary:item]];
            }
        }

        self.aMEXRegistradoCheckout = populatedAMEXRegistradoCheckout;

    }

    NSArray *receivedDiscoverRegistradoCheckout = [aDictionary objectForKey:@"DiscoverRegistradoCheckout"];
    if ([receivedDiscoverRegistradoCheckout isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedDiscoverRegistradoCheckout = [NSMutableArray arrayWithCapacity:[receivedDiscoverRegistradoCheckout count]];
        for (NSDictionary *item in receivedDiscoverRegistradoCheckout) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedDiscoverRegistradoCheckout addObject:[DiscoverRegistradoCheckout instanceFromDictionary:item]];
            }
        }

        self.discoverRegistradoCheckout = populatedDiscoverRegistradoCheckout;

    }
    self.garantiaItemStatus = [(NSNumber *)[aDictionary objectForKey:@"garantiaItemStatus"] boolValue];

    NSArray *receivedMasterCardRegistradoCheckout = [aDictionary objectForKey:@"Master CardRegistradoCheckout"];
    if ([receivedMasterCardRegistradoCheckout isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedMasterCardRegistradoCheckout = [NSMutableArray arrayWithCapacity:[receivedMasterCardRegistradoCheckout count]];
        for (NSDictionary *item in receivedMasterCardRegistradoCheckout) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedMasterCardRegistradoCheckout addObject:[MasterCardRegistradoCheckout instanceFromDictionary:item]];
            }
        }

        self.masterCardRegistradoCheckout = populatedMasterCardRegistradoCheckout;

    }

    NSArray *receivedVISARegistradoCheckout = [aDictionary objectForKey:@"VISARegistradoCheckout"];
    if ([receivedVISARegistradoCheckout isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedVISARegistradoCheckout = [NSMutableArray arrayWithCapacity:[receivedVISARegistradoCheckout count]];
        for (NSDictionary *item in receivedVISARegistradoCheckout) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedVISARegistradoCheckout addObject:[VISARegistradoCheckout instanceFromDictionary:item]];
            }
        }

        self.vISARegistradoCheckout = populatedVISARegistradoCheckout;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.aMEXRegistradoCheckout) {
        [dictionary setObject:self.aMEXRegistradoCheckout forKey:@"aMEXRegistradoCheckout"];
    }

    if (self.discoverRegistradoCheckout) {
        [dictionary setObject:self.discoverRegistradoCheckout forKey:@"discoverRegistradoCheckout"];
    }

    [dictionary setObject:[NSNumber numberWithBool:self.garantiaItemStatus] forKey:@"garantiaItemStatus"];

    if (self.masterCardRegistradoCheckout) {
        [dictionary setObject:self.masterCardRegistradoCheckout forKey:@"masterCardRegistradoCheckout"];
    }

    if (self.vISARegistradoCheckout) {
        [dictionary setObject:self.vISARegistradoCheckout forKey:@"vISARegistradoCheckout"];
    }

    return dictionary;

}


@end
