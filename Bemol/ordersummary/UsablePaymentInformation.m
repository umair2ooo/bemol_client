#import "UsablePaymentInformation.h"

#import "UsableBillingAddres.h"

@implementation UsablePaymentInformation

@synthesize descriptionText;
@synthesize language;
@synthesize paymentMethodName;
@synthesize paymentTermConditionId;
@synthesize usableBillingAddress;
@synthesize xumetAttrPageName;
@synthesize xumetPolicyId;

+ (UsablePaymentInformation *)instanceFromDictionary:(NSDictionary *)aDictionary {

    UsablePaymentInformation *instance = [[UsablePaymentInformation alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.descriptionText = [aDictionary objectForKey:@"description"];
    self.language = [aDictionary objectForKey:@"language"];
    self.paymentMethodName = [aDictionary objectForKey:@"paymentMethodName"];
    self.paymentTermConditionId = [aDictionary objectForKey:@"paymentTermConditionId"];

    NSArray *receivedUsableBillingAddress = [aDictionary objectForKey:@"usableBillingAddress"];
    if ([receivedUsableBillingAddress isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedUsableBillingAddress = [NSMutableArray arrayWithCapacity:[receivedUsableBillingAddress count]];
        for (NSDictionary *item in receivedUsableBillingAddress) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedUsableBillingAddress addObject:[UsableBillingAddres instanceFromDictionary:item]];
            }
        }

        self.usableBillingAddress = populatedUsableBillingAddress;

    }
    self.xumetAttrPageName = [aDictionary objectForKey:@"xumet_attrPageName"];
    self.xumetPolicyId = [aDictionary objectForKey:@"xumet_policyId"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.descriptionText) {
        [dictionary setObject:self.descriptionText forKey:@"descriptionText"];
    }

    if (self.language) {
        [dictionary setObject:self.language forKey:@"language"];
    }

    if (self.paymentMethodName) {
        [dictionary setObject:self.paymentMethodName forKey:@"paymentMethodName"];
    }

    if (self.paymentTermConditionId) {
        [dictionary setObject:self.paymentTermConditionId forKey:@"paymentTermConditionId"];
    }

    if (self.usableBillingAddress) {
        [dictionary setObject:self.usableBillingAddress forKey:@"usableBillingAddress"];
    }

    if (self.xumetAttrPageName) {
        [dictionary setObject:self.xumetAttrPageName forKey:@"xumetAttrPageName"];
    }

    if (self.xumetPolicyId) {
        [dictionary setObject:self.xumetPolicyId forKey:@"xumetPolicyId"];
    }

    return dictionary;

}


@end
