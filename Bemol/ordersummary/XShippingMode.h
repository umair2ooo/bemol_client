#import <Foundation/Foundation.h>

@interface XShippingMode : NSObject {

    NSString *shipModeCode;
    NSNumber *shipModeId;
    NSString *shipModeName;
    NSNumber *shipModePrazo;
    NSNumber *shipModePrice;

}

@property (nonatomic, copy) NSString *shipModeCode;
@property (nonatomic, copy) NSNumber *shipModeId;
@property (nonatomic, copy) NSString *shipModeName;
@property (nonatomic, copy) NSNumber *shipModePrazo;
@property (nonatomic, copy) NSNumber *shipModePrice;

+ (XShippingMode *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
