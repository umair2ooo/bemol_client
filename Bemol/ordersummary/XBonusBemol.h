#import <Foundation/Foundation.h>

@interface XBonusBemol : NSObject {

    NSString *bonusBemol;
    NSMutableArray *listaBonusBemol;
    NSString *valeCredito;

}

@property (nonatomic, copy) NSString *bonusBemol;
@property (nonatomic, copy) NSMutableArray *listaBonusBemol;
@property (nonatomic, copy) NSString *valeCredito;

+ (XBonusBemol *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
