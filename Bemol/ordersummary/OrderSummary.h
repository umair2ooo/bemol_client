#import <Foundation/Foundation.h>

@class OrderSummaryXInstallmentMap;

@interface OrderSummary : NSObject {

    NSString *buyerDistinguishedName;
    NSString *buyerId;
    NSString *checkoutUrl;
    NSString *grandTotal;
    NSString *grandTotalCurrency;
    NSString *lastUpdateDate;
    NSString *orderId;
    NSMutableArray *orderItem;
    NSString *orderStatus;
    NSString *paymentInstructionUrl;
    NSString *precheckoutUrl;
    NSString *prepareIndicator;
    NSString *recordSetComplete;
    NSString *recordSetCount;
    NSString *recordSetStartNumber;
    NSString *recordSetTotal;
    NSString *resourceId;
    NSString *resourceName;
    NSString *shipAsComplete;
    NSString *shippingInfoUrl;
    NSString *storeNameIdentifier;
    NSString *storeUniqueID;
    NSString *totalAdjustment;
    NSString *totalAdjustmentCurrency;
    NSString *totalProductPrice;
    NSString *totalProductPriceCurrency;
    NSString *totalSalesTax;
    NSString *totalSalesTaxCurrency;
    NSString *totalShippingCharge;
    NSString *totalShippingChargeCurrency;
    NSString *totalShippingTax;
    NSString *totalShippingTaxCurrency;
    NSMutableArray *usablePaymentInformation;
    NSString *usablePaymentInfoUrl;
    NSString *usableShippingInfoUrl;
    NSMutableArray *xBonusBemol;
    NSMutableArray *xCredFlexMap;
    OrderSummaryXInstallmentMap *xInstallmentMap;
    NSMutableArray *xShippingModes;

}

@property (nonatomic, copy) NSString *buyerDistinguishedName;
@property (nonatomic, copy) NSString *buyerId;
@property (nonatomic, copy) NSString *checkoutUrl;
@property (nonatomic, copy) NSString *grandTotal;
@property (nonatomic, copy) NSString *grandTotalCurrency;
@property (nonatomic, copy) NSString *lastUpdateDate;
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSMutableArray *orderItem;
@property (nonatomic, copy) NSString *orderStatus;
@property (nonatomic, copy) NSString *paymentInstructionUrl;
@property (nonatomic, copy) NSString *precheckoutUrl;
@property (nonatomic, copy) NSString *prepareIndicator;
@property (nonatomic, copy) NSString *recordSetComplete;
@property (nonatomic, copy) NSString *recordSetCount;
@property (nonatomic, copy) NSString *recordSetStartNumber;
@property (nonatomic, copy) NSString *recordSetTotal;
@property (nonatomic, copy) NSString *resourceId;
@property (nonatomic, copy) NSString *resourceName;
@property (nonatomic, copy) NSString *shipAsComplete;
@property (nonatomic, copy) NSString *shippingInfoUrl;
@property (nonatomic, copy) NSString *storeNameIdentifier;
@property (nonatomic, copy) NSString *storeUniqueID;
@property (nonatomic, copy) NSString *totalAdjustment;
@property (nonatomic, copy) NSString *totalAdjustmentCurrency;
@property (nonatomic, copy) NSString *totalProductPrice;
@property (nonatomic, copy) NSString *totalProductPriceCurrency;
@property (nonatomic, copy) NSString *totalSalesTax;
@property (nonatomic, copy) NSString *totalSalesTaxCurrency;
@property (nonatomic, copy) NSString *totalShippingCharge;
@property (nonatomic, copy) NSString *totalShippingChargeCurrency;
@property (nonatomic, copy) NSString *totalShippingTax;
@property (nonatomic, copy) NSString *totalShippingTaxCurrency;
@property (nonatomic, copy) NSMutableArray *usablePaymentInformation;
@property (nonatomic, copy) NSString *usablePaymentInfoUrl;
@property (nonatomic, copy) NSString *usableShippingInfoUrl;
@property (nonatomic, copy) NSMutableArray *xBonusBemol;
@property (nonatomic, copy) NSMutableArray *xCredFlexMap;
@property (nonatomic, strong) OrderSummaryXInstallmentMap *xInstallmentMap;
@property (nonatomic, copy) NSMutableArray *xShippingModes;

+ (OrderSummary *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
