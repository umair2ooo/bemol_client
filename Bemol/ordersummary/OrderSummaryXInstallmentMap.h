#import <Foundation/Foundation.h>

@interface OrderSummaryXInstallmentMap : NSObject {

    NSMutableArray *aMEXRegistradoCheckout;
    NSMutableArray *discoverRegistradoCheckout;
    BOOL garantiaItemStatus;
    NSMutableArray *masterCardRegistradoCheckout;
    NSMutableArray *vISARegistradoCheckout;

}

@property (nonatomic, copy) NSMutableArray *aMEXRegistradoCheckout;
@property (nonatomic, copy) NSMutableArray *discoverRegistradoCheckout;
@property (nonatomic, assign) BOOL garantiaItemStatus;
@property (nonatomic, copy) NSMutableArray *masterCardRegistradoCheckout;
@property (nonatomic, copy) NSMutableArray *vISARegistradoCheckout;

+ (OrderSummaryXInstallmentMap *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
