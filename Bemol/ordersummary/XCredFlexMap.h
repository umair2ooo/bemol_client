#import <Foundation/Foundation.h>

@interface XCredFlexMap : NSObject {

    NSNumber *comEntrada;
    NSString *dataPedido;
    NSNumber *indexPlana;
    NSString *sapCode;

}

@property (nonatomic, copy) NSNumber *comEntrada;
@property (nonatomic, copy) NSString *dataPedido;
@property (nonatomic, copy) NSNumber *indexPlana;
@property (nonatomic, copy) NSString *sapCode;

+ (XCredFlexMap *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
