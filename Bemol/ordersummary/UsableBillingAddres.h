#import <Foundation/Foundation.h>

@interface UsableBillingAddres : NSObject {

    NSString *addressId;
    NSString *nickName;

}

@property (nonatomic, copy) NSString *addressId;
@property (nonatomic, copy) NSString *nickName;

+ (UsableBillingAddres *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
