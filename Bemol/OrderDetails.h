#import <Foundation/Foundation.h>

@interface OrderDetails : NSObject {

    BOOL cancelableOrder;
    NSString *compra;
    id entrega;
    NSString *loja;
    NSString *lojaId;
    id montagem;
    NSMutableArray *notaFiscal;
    NSString *numeroDoPedido;
    NSString *orderId;
    NSMutableArray *pagamento;
    NSString *status;
    NSString *statusDesc;
    NSNumber *valor;

}

@property (nonatomic, assign) BOOL cancelableOrder;
@property (nonatomic, copy) NSString *compra;
@property (nonatomic, strong) id entrega;
@property (nonatomic, copy) NSString *loja;
@property (nonatomic, copy) NSString *lojaId;
@property (nonatomic, strong) id montagem;
@property (nonatomic, copy) NSMutableArray *notaFiscal;
@property (nonatomic, copy) NSString *numeroDoPedido;
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSMutableArray *pagamento;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *statusDesc;
@property (nonatomic, copy) NSNumber *valor;

+ (OrderDetails *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
