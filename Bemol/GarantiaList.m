#import "GarantiaList.h"

@implementation GarantiaList

@synthesize dataPedido;
@synthesize lojaId;
@synthesize nomeSku;
@synthesize notaFiscal;
@synthesize precoUnitario;
@synthesize sku;
@synthesize tempGar;
@synthesize urlSku;
@synthesize vendaPosterior;

+ (GarantiaList *)instanceFromDictionary:(NSDictionary *)aDictionary {

    GarantiaList *instance = [[GarantiaList alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.dataPedido = [aDictionary objectForKey:@"DataPedido"];
    self.lojaId = [aDictionary objectForKey:@"LojaId"];
    self.nomeSku = [aDictionary objectForKey:@"NomeSku"];
    self.notaFiscal = [aDictionary objectForKey:@"NotaFiscal"];
    self.precoUnitario = [aDictionary objectForKey:@"PrecoUnitario"];
    self.sku = [aDictionary objectForKey:@"sku"];
    self.tempGar = [aDictionary objectForKey:@"TempGar"];
    self.urlSku = [aDictionary objectForKey:@"UrlSku"];
    self.vendaPosterior = [aDictionary objectForKey:@"VendaPosterior"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.dataPedido) {
        [dictionary setObject:self.dataPedido forKey:@"dataPedido"];
    }

    if (self.lojaId) {
        [dictionary setObject:self.lojaId forKey:@"lojaId"];
    }

    if (self.nomeSku) {
        [dictionary setObject:self.nomeSku forKey:@"nomeSku"];
    }

    if (self.notaFiscal) {
        [dictionary setObject:self.notaFiscal forKey:@"notaFiscal"];
    }

    if (self.precoUnitario) {
        [dictionary setObject:self.precoUnitario forKey:@"precoUnitario"];
    }

    if (self.sku) {
        [dictionary setObject:self.sku forKey:@"sku"];
    }

    if (self.tempGar) {
        [dictionary setObject:self.tempGar forKey:@"tempGar"];
    }

    if (self.urlSku) {
        [dictionary setObject:self.urlSku forKey:@"urlSku"];
    }

    if (self.vendaPosterior) {
        [dictionary setObject:self.vendaPosterior forKey:@"vendaPosterior"];
    }

    return dictionary;

}


@end
