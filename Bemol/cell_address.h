//
//  cell_address.h
//  Bemol
//
//  Created by Fahim Bilwani on 16/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cell_address : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label_address;
@property (weak, nonatomic) IBOutlet UILabel *label_cep;
@property (weak, nonatomic) IBOutlet UILabel *label_endereco;
@property (weak, nonatomic) IBOutlet UILabel *label_bairro;
@property (weak, nonatomic) IBOutlet UILabel *label_cidad;
@property (weak, nonatomic) IBOutlet UILabel *label_nome;
@property (weak, nonatomic) IBOutlet UIButton *button_edit;
@end
