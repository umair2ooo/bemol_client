#import "Price.h"

@implementation Price

@synthesize contractId;
@synthesize isLowestContractPrice;
@synthesize priceDescription;
@synthesize priceUsage;
@synthesize priceValue;

+ (Price *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Price *instance = [[Price alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.contractId = [aDictionary objectForKey:@"contractId"];
    self.isLowestContractPrice = [aDictionary objectForKey:@"isLowestContractPrice"];
    self.priceDescription = [aDictionary objectForKey:@"priceDescription"];
    self.priceUsage = [aDictionary objectForKey:@"priceUsage"];
    self.priceValue = [aDictionary objectForKey:@"priceValue"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.contractId) {
        [dictionary setObject:self.contractId forKey:@"contractId"];
    }

    if (self.isLowestContractPrice) {
        [dictionary setObject:self.isLowestContractPrice forKey:@"isLowestContractPrice"];
    }

    if (self.priceDescription) {
        [dictionary setObject:self.priceDescription forKey:@"priceDescription"];
    }

    if (self.priceUsage) {
        [dictionary setObject:self.priceUsage forKey:@"priceUsage"];
    }

    if (self.priceValue) {
        [dictionary setObject:self.priceValue forKey:@"priceValue"];
    }

    return dictionary;

}


@end
