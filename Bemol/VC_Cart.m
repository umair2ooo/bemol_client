#import "VC_Cart.h"
#import "cell_Cart.h"
#import "VC_checkout.h"
#import "OrderItem.h"
#import "VC_selectLocation.h"
#import "VC_productDetail.h"

//#import "VC_loginIn.h"

@interface VC_Cart ()<DetailObject,NSURLSessionDelegate,UITextFieldDelegate>
{
    int int_indexOfRemovingObject;
    int int_indexOfEditingObject;
    NSMutableDictionary *dicPricedetails;
//    NSMutableArray *array_cartList;
}

//@property(nonatomic, strong) webService_Post_Put_Delete *nsurl_obj;
@property(nonatomic, strong)NSString *string_quantity;
//@property (strong, nonatomic) MKNetworkOperation *operation;
//@property (strong, nonatomic) WebServiceEngine *webServiceEngine;

@property(nonatomic, readwrite)BOOL bool_goingToLogInScree;

@property(nonatomic, strong)ShoppingCart *obj_shoppingCart;
@property(nonatomic, strong)OrderItem *obj_OrderItem;

@property (weak, nonatomic) IBOutlet UITableView *tableview_;
@property (weak, nonatomic) IBOutlet UILabel *label_totalPrice;
@property (weak, nonatomic) IBOutlet UIButton *button_Checkout;

- (IBAction)action_cancel:(id)sender;
- (IBAction)action_promotion:(id)sender;
- (IBAction)action_hide:(id)sender;
- (IBAction)action_checkOut:(id)sender;
- (IBAction)action_stores:(id)sender;

@end

@implementation VC_Cart




#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.label_totalPrice.text = @"Subtotal: R$ 00";
    
    [self.tableview_ reloadData];
//    [self method_calculateTotal:@""];
    
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
//    [self method_setAddToCartCounterLabel];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(getCartsFromServer:)
                                                     name:k_getShopingCartNotification
                                                   object:nil];
    if (ApplicationDelegate.single.bool_isComingfromCart)
    {
        ApplicationDelegate.single.bool_isComingfromCart = NO;
        [self getCartsFromServer:nil];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    //ApplicationDelegate.single.delegate_ = nil;
    
    
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - getCartsFromServer
-(void)getCartsFromServer:(NSNotification *)notifiaction
{
    [[ServiceModel sharedClient] GET:k_CartService
                          parameters:nil
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         DLog(@"%@",responseObject);
         if (![responseObject valueForKey:@"errors"] && [responseObject isKindOfClass:[NSDictionary class]])
         {
             if (self.obj_shoppingCart)
                 self.obj_shoppingCart = nil;
             
             self.obj_shoppingCart = [ShoppingCart instanceFromDictionary:responseObject];
             
             
             if (!self.obj_shoppingCart.orderItem.count || self.obj_shoppingCart.orderItem.count == 0)
             {
                 [self method_shoppingCartCounter:@"0"];
             }
             else
             {
                 [self method_shoppingCartCounter:[NSString stringWithFormat:@"%lu", (unsigned long)self.obj_shoppingCart.orderItem.count]];
             }
             
             if ([self.obj_shoppingCart.recordSetCount isEqualToString:@"0"])
             {
                 [UtilitiesHelper showAlert:@"Seu carrinho de compras está vazio. Se encontrar algum item que deseja incluir em seu carrinho de compras durante a compra, clique em Adicionar ao carrinho de compras."];
                 [self action_hide:nil];
             }
             else
             {
                 self.label_totalPrice.text = [NSString stringWithFormat:@"Subtotal: %@",[UtilitiesHelper method_currencyFormatter:self.obj_shoppingCart.grandTotal]];
                 
                 [self.tableview_ reloadData];
             }
         }
         else
         {
             if ([responseObject valueForKey:@"errors"])
             {
                 [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                             valueForKey:@"errorMessage"]];
             }
             else
             {
                  [UtilitiesHelper showAlert:@"Seu carrinho de compras está vazio. Se encontrar algum item que deseja incluir em seu carrinho de compras durante a compra, clique em Adicionar ao carrinho de compras."];
             }
             
             [self action_hide:nil];
         }
     }
        failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error.description);
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
         [self action_hide:nil];
     }];
    
}



#pragma mark - Navigation
 //In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_checkout"])
    {
        VC_checkout *obj = segue.destinationViewController;
        obj.orderId = sender;
    }
//    else if ([segue.identifier isEqualToString:@"segue_productDetaiviaList"])
//    {
//        VC_productDetail *obj = segue.destinationViewController;
//        
//        OrderItem *obj_orderItem = sender;
//        
//        
//        obj.string_title = obj_orderItem.productShortDescription;
//        obj.uniqueID = obj_orderItem.productId;
//    }
    else
    {
        VC_selectLocation *obj = segue.destinationViewController;
        obj.string_orderId = sender;
        obj.bool_comingFromCheckinnaLoja = NO;
        obj.bool_comingFromCartList = YES;
    }
}

#pragma mark - action_cancel
- (IBAction)action_cancel:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark - action_promotion
- (IBAction)action_promotion:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Promotion Code"
                              message:@"Please enter promotion code:"
                              delegate:self
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:@"Cancel", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    alertView.tag = 100;
    /* Display a numerical keypad for this text field */
    UITextField *textField = [alertView textFieldAtIndex:0];
    textField.keyboardType = UIKeyboardTypeNumberPad;
    [alertView show];
}


#pragma mark - method_details
-(void)method_details:(int)tag
{
    DLog(@"%d",tag);
}
#pragma mark - method_deleteObject
-(void)method_deleteObject:(int)tag
{
//    int_indexOfRemovingObject = tag;
//
//    NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:
//                              [(OrderItem *)[self.obj_shoppingCart.orderItem objectAtIndex:tag] orderItemId], @"orderItemId", nil];
//    
//    
//    NSString *strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/cart/@self/delete_order_item",k_url];
//    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//
//    
//        [ApplicationDelegate.single method_hitWebService:strUrl
//                                                    data:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_shoppingCart.orderId,@"orderId",
//                                                          [NSArray arrayWithObject:dic_temp], @"orderItem", nil]
//                                              seriveType:@"PUT"
//                                              seriveName:@"deleteCart"];
    
    
    int_indexOfRemovingObject = tag;
    DLog(@"%d",int_indexOfRemovingObject);
    NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:
                              [(OrderItem *)[self.obj_shoppingCart.orderItem objectAtIndex:int_indexOfRemovingObject] orderItemId], @"orderItemId",
                              @"0", @"quantity",  nil];
    
    NSArray *array_temp = [NSArray arrayWithObjects:dic_temp, nil];
    
    NSDictionary *dic_tempTwo = [NSDictionary dictionaryWithObjectsAndKeys:
                                 array_temp, @"orderItem",nil];
    DLog(@"%@", dic_tempTwo);
    
    
    
    NSString *strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/cart/@self",k_url];
    
    DLog(@"strUrl: %@",strUrl);
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[ServiceModel sharedClient] PUT:k_CartService parameters:dic_tempTwo onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([responseObject valueForKey:@"orderId"])
         {
             [UtilitiesHelper showAlert:@"Item removido com sucesso"];
             
             [self.obj_shoppingCart.orderItem removeObjectAtIndex:int_indexOfRemovingObject];
             [self method_calculateTotal];
             //                [self method_setAddToCartCounterLabel];
             [self.tableview_ reloadData];
             
             if (!self.obj_shoppingCart.orderItem.count || self.obj_shoppingCart.orderItem.count == 0)
             {
                 [self method_shoppingCartCounter:@"0"];
             }
             else
             {
                 [self method_shoppingCartCounter:[NSString stringWithFormat:@"%lu", (unsigned long)self.obj_shoppingCart.orderItem.count]];
             }
             
             if (![self.obj_shoppingCart.orderItem count])
             {
                 self.obj_shoppingCart.orderItem = nil;
                 ApplicationDelegate.single.string_orderId = nil;
                 [self action_hide:nil];
             }
         }
         else
         {
             //errors
             if ([responseObject valueForKey:@"errors"])
             {
                 [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                             valueForKey:@"errorMessage"]];
             }
             else
             {
                 [UtilitiesHelper showAlert:@"Há algo de errado, por favor tente novamente"];
             }
         }
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}

-(void)method_editObject:(int)tag
{
    
}

#pragma mark - textField delegates
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.tableview_ indexPathForCell:(cell_Cart*)[[textField superview] superview]];
    DLog(@"%ld",(long)indexPath.row);
    
    self.string_quantity = textField.text;
    
    NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:
                              [(OrderItem *)[self.obj_shoppingCart.orderItem objectAtIndex:int_indexOfEditingObject] orderItemId], @"orderItemId",
                              textField.text, @"quantity",  nil];
    
    NSArray *array_temp = [NSArray arrayWithObjects:dic_temp, nil];
    
    NSDictionary *dic_tempTwo = [NSDictionary dictionaryWithObjectsAndKeys:
                                 array_temp, @"orderItem",nil];
    DLog(@"%@", dic_tempTwo);
    
    
    [[ServiceModel sharedClient] PUT:k_CartService parameters:dic_tempTwo onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //errors
         if (![responseObject valueForKey:@"errors"] && ![responseObject isKindOfClass:[NSNull class]])
         {
             [(OrderItem *)[self.obj_shoppingCart.orderItem objectAtIndex:int_indexOfEditingObject] setQuantity:self.string_quantity];
             
             [self method_calculateTotal];
             [self.tableview_ reloadData];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
         }
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}




#pragma mark- uialert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 101)
    {
    }
    else
    {
        // promotion code
        if (buttonIndex==0)
        {
            UITextField *textField = [alertView textFieldAtIndex:0];
            
            if ([textField.text length])
            {
                if ([textField.text isEqualToString:@"1111"] || [textField.text isEqualToString:@"2222"])
                {
                    [self method_calculateTotal];
                }
                else
                {
                    [[[UIAlertView alloc] initWithTitle:nil message:@"Invalid promotion code" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                }
            }
        }

    }
}
#pragma mark - tableview dataSource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.obj_shoppingCart.orderItem count]?[self.obj_shoppingCart.orderItem count]:0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_Cart *cell = (cell_Cart *)[tableView dequeueReusableCellWithIdentifier:@"cell_Cart"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_Cart" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    cell.textfield_edit.tag = indexPath.row;
    cell.button_details.tag = indexPath.row;
    cell.button_Remove.tag = indexPath.row;
    cell.delegate = self;
    
    [cell method_setValues:[self.obj_shoppingCart.orderItem objectAtIndex:indexPath.row]];

    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    OrderItem *obj_orderItem = [self.obj_shoppingCart.orderItem objectAtIndex:indexPath.row];

    VC_productDetail *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_productDetail"];
    
    
    obj.string_title = obj_orderItem.productShortDescription;
    obj.uniqueID = obj_orderItem.productId;
    
    [self.navigationController pushViewController:obj animated:YES];
}

#pragma mark - calculate total price and number of items
-(void)method_calculateTotal
{
    if ([self.obj_shoppingCart.orderItem count])
    {
        __block int int_totalItems = 0;
        __block float float_totalPrice = 0.0;
        
        
        [self.obj_shoppingCart.orderItem enumerateObjectsUsingBlock:^(OrderItem *obj, NSUInteger idx, BOOL *stop)
         {
             float_totalPrice = float_totalPrice + ([obj.unitPrice floatValue] * [obj.quantity intValue]);
             int_totalItems = int_totalItems + [[obj valueForKey:@"quantity"] intValue];
             if (idx == [self.obj_shoppingCart.orderItem count]-1)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     DLog(@"%f",float_totalPrice);
                     self.label_totalPrice.text = [NSString stringWithFormat:@"Subtotal: %@",[UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%.2f",float_totalPrice]]]  ;
                 });
             }
         }];
    }
    else
    {
        self.label_totalPrice.text = @"Subtotal: R$ 0.00";
    }
}

#pragma mark - action_checkOut
- (IBAction)action_checkOut:(id)sender
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:k_isLogin] isEqualToString:@"no"])
    {
        UINavigationController *obj_login = [self.storyboard instantiateViewControllerWithIdentifier:@"nav_logIn"];
        ApplicationDelegate.single.bool_isComingfromCart = YES;
        [self presentViewController:obj_login animated:YES completion:nil];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:k_hideShopingCartNotification object:nil];
        [self performSegueWithIdentifier:@"segue_checkout" sender:self.obj_shoppingCart.orderId];
    }
}

- (IBAction)action_stores:(id)sender
{
    if ([self.obj_shoppingCart.orderItem count ])
    {
        [self performSegueWithIdentifier:@"segue_stores" sender:self.obj_shoppingCart.orderId];
    }
}


#pragma mark - action_hide
- (IBAction)action_hide:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:k_hideShopingCartNotification object:nil];
}


#pragma mark - method_shopping Cart Counter
-(void)method_shoppingCartCounter:(NSString *)counter
{
    NSUserDefaults *obj = [NSUserDefaults standardUserDefaults];
    [obj setObject:[obj objectForKey:k_userId] forKey:k_userId];
    [obj setValue:counter forKey:k_count];
    [obj synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:k_showShopingCartCounter object:nil];
}



@end