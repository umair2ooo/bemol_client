//
//  UtilitiesHelper.h
//
//  Created by Muhammad Waqas Khalid on 8/29/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>
#import <MapKit/MKDirectionsResponse.h>

#import "MBProgressHUD.h"
//#import "BlocksKit.h"




//#ifndef IS_IPHONE_5
//#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
//#endif
//
//#ifndef IS_IPAD
//#define IS_IPAD    (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
//#endif

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


@interface UtilitiesHelper : NSObject
{
    
    

    
}


+ (id)shareUtitlities;
+ (id)getUserDefaultForKey:(NSString*)key;
+ (void)setUserDefaultForKey:(NSString*)key  value:(NSString*)value;
+(void)showLoader:(NSString *)title forView:(UIView *)view  setMode:(MBProgressHUDMode)mode delegate:(id)vwDelegate;
+(void)hideLoader:(UIView*)forView;
+(void)showPromptAlertforTitle:(NSString *)title withMessage:(NSString *)message forDelegate:(id)deleg;
//+(void)showYesNoPromptAlertforTitle:(NSString *)title withMessage:(NSString *)message block:(void(^)(BOOL))block;
+(BOOL) validateEmail:(NSString *)checkEmail;
//+(NSURL *)imageURLMaker :(NSString *)imgUrl;
+(BOOL)isReachable;
+(BOOL) checkForEmptySpaces:(UITextField *)textField;
+(void)writeJsonToFile:(id)responseString withFileName:(NSString*)fileName;
//+(id)writeJsonToFile:(NSString*)fileName;
+(id)readJsonFromFile:(NSString*)fileName;
+(BOOL) validateFullName:(NSString *)checkFullName;
+(BOOL) checkForEmptySpacesInTextView : (UITextView *)textView;
+(BOOL) checkAlphabets:(NSString *)text;
+(BOOL) checkForEmptySpacesInString : (NSString *)rawString;
+(BOOL) checkPassword:(NSString *)text;
+(BOOL) checkPhoneNumber:(NSString *)text;
+(BOOL) checkFaxNumber:(NSString *)text;
+(BOOL) validateUsername:(NSString*)checkUsername;
+(BOOL)checkLocationCoordinate:(CLLocationCoordinate2D)coordinate;
+ (id)loadNibNamed:(NSString *)nibName ofClass:(Class)objClass;
+(NSString *) addSuffixToNumber:(NSInteger) number;
+ (void)getAddressFromLocation:(CLLocation *)location completionHandler:(void(^)(NSString *locationAddress))completionHandler;
+ (void)getRouteFrom:(CLLocationCoordinate2D)source To:(CLLocationCoordinate2D)destination CompletionHandler:(void(^)(MKDirectionsResponse *response, NSError *error))completionHandler;
+(void) makeViewWithRoundedCornersModified:(UIView *) view radius:(double)newSize;
+ (UIImage *)scaleAndRotateImage:(UIImage *)image;

+ (UIImage*) maskImage:(UIImage *)image withOverlayMask:(UIImage *) maskImage;
+(void)showAlert:(NSString *)message;
+(NSString *)method_currencyFormatter:(NSString *)amount;
+ (NSString *)getIPAddress;
+ (void)singleTap:(UITapGestureRecognizer*)sender;
+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (void)setRoundedView:(UIImageView *)roundedView toDiameter:(float)newSize;
+ (NSString *)method_RemoveLeadingZeros:(NSString *)stringValues;
@end

