#import "ContractsList.h"

@implementation ContractsList

@synthesize debitoAutomatico;
@synthesize desconto;
@synthesize juros;
@synthesize multa;
@synthesize pagarEm;
@synthesize parcelaDataPagamento;
@synthesize parcelaDataVencim;
@synthesize parcelaNumber;
@synthesize parcelaStatus;
@synthesize parcelaValue;
@synthesize qtParcela;
@synthesize valor;
@synthesize valorDesconto;

+ (ContractsList *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ContractsList *instance = [[ContractsList alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.debitoAutomatico = [(NSNumber *)[aDictionary objectForKey:@"DebitoAutomatico"] boolValue];
    self.desconto = [aDictionary objectForKey:@"Desconto"];
    self.juros = [aDictionary objectForKey:@"Juros"];
    self.multa = [aDictionary objectForKey:@"Multa"];

    NSArray *receivedPagarEm = [aDictionary objectForKey:@"Pagar em"];
    if ([receivedPagarEm isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedPagarEm = [NSMutableArray arrayWithCapacity:[receivedPagarEm count]];
        for (NSDictionary *item in receivedPagarEm) {
            [populatedPagarEm addObject:item];
        }

        self.pagarEm = populatedPagarEm;

    }
    self.parcelaDataPagamento = [aDictionary objectForKey:@"ParcelaDataPagamento"];
    self.parcelaDataVencim = [aDictionary objectForKey:@"ParcelaDataVencim"];
    self.parcelaNumber = [aDictionary objectForKey:@"ParcelaNumber"];
    self.parcelaStatus = [aDictionary objectForKey:@"ParcelaStatus"];
    self.parcelaValue = [aDictionary objectForKey:@"ParcelaValue"];
    self.qtParcela = [aDictionary objectForKey:@"QtParcela"];
    self.valor = [aDictionary objectForKey:@"Valor"];
    self.valorDesconto = [aDictionary objectForKey:@"ValorDesconto"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    [dictionary setObject:[NSNumber numberWithBool:self.debitoAutomatico] forKey:@"debitoAutomatico"];

    if (self.desconto) {
        [dictionary setObject:self.desconto forKey:@"desconto"];
    }

    if (self.juros) {
        [dictionary setObject:self.juros forKey:@"juros"];
    }

    if (self.multa) {
        [dictionary setObject:self.multa forKey:@"multa"];
    }

    if (self.pagarEm) {
        [dictionary setObject:self.pagarEm forKey:@"pagarEm"];
    }

    if (self.parcelaDataPagamento) {
        [dictionary setObject:self.parcelaDataPagamento forKey:@"parcelaDataPagamento"];
    }

    if (self.parcelaDataVencim) {
        [dictionary setObject:self.parcelaDataVencim forKey:@"parcelaDataVencim"];
    }

    if (self.parcelaNumber) {
        [dictionary setObject:self.parcelaNumber forKey:@"parcelaNumber"];
    }

    if (self.parcelaStatus) {
        [dictionary setObject:self.parcelaStatus forKey:@"parcelaStatus"];
    }

    if (self.parcelaValue) {
        [dictionary setObject:self.parcelaValue forKey:@"parcelaValue"];
    }

    if (self.qtParcela) {
        [dictionary setObject:self.qtParcela forKey:@"qtParcela"];
    }

    if (self.valor) {
        [dictionary setObject:self.valor forKey:@"valor"];
    }

    if (self.valorDesconto) {
        [dictionary setObject:self.valorDesconto forKey:@"valorDesconto"];
    }

    return dictionary;

}


@end
