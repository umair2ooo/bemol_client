#import <Foundation/Foundation.h>

@class AdditionalstoresAdditionalStoreMap;

@interface Additionalstores : NSObject {

    AdditionalstoresAdditionalStoreMap *additionalStoreMap;

}

@property (nonatomic, strong) AdditionalstoresAdditionalStoreMap *additionalStoreMap;

+ (Additionalstores *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
