#import "VC_simulacao de.h"
#import "Installment.h"
#import "ArrayList.h"
#import "Cell_outros.h"
#import "FindSimulate.h"
#import "findSimulateSimulateMap.h"
#import "InstallmentCredFlexMap.h"
#import "InstallmentInstallmentMap.h"
#import "AMEXRegistradoCheckout.h"
#import "DiscoverRegistradoCheckout.h"
#import "MasterCardRegistradoCheckout.h"
#import "VISARegistradoCheckout.h"


@interface VC_simulacao_de ()<UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSArray *array_credito;
    UIPickerView *picker_;
    NSInteger integer_creditoSelectedIndex;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UITableView *tableview_;
@property (weak, nonatomic) IBOutlet UITextField *textField_date;
@property (weak, nonatomic) IBOutlet UITextField *textfield_entrada;
@property (weak, nonatomic) IBOutlet UITextField *textfield_credito;
@property (weak, nonatomic) IBOutlet UILabel *label_entrada;
@property (weak, nonatomic) IBOutlet UILabel *label_parcela;
@property (weak, nonatomic) IBOutlet UILabel *label_total;
@property (weak, nonatomic) IBOutlet UILabel *label_juros;
@property (weak, nonatomic) IBOutlet UILabel *label_jurosDetail;

@property(nonatomic, strong)Installment *obj_installment;
@property(nonatomic, strong)ArrayList *obj_arrayList;
@property(nonatomic, strong)FindSimulate *obj_simulate;
@property(nonatomic, strong)findSimulateSimulateMap *obj_findSimulateSimulateMap;
@property(nonatomic, strong)AMEXRegistradoCheckout *obj_registradoCheckout;
@property(nonatomic, strong)DiscoverRegistradoCheckout *obj_discoverRegistradoCheckout;
@property(nonatomic, strong)MasterCardRegistradoCheckout *obj_masterCardRegistradoCheckout;
@property(nonatomic, strong)VISARegistradoCheckout *obj_vISARegistradoCheckout;


@end

@implementation VC_simulacao_de

#pragma mark - show current date in text fld
-(void)method_showCurrentDate
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    self.textField_date.text = [dateFormat stringFromDate:today];
//    NSLog(@"date: %@", dateString);
}


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DLog(@"%@",self.dic_productDetail);
    
    self.title = self.string_title;
    
    [self.scroller setContentSize:CGSizeMake(0,
                                             self.tableview_.frame.origin.y + self.tableview_.frame.size.height)];
    
    
    [self method_showCurrentDate];
    
    [[ServiceModel sharedClient] POST:k_installment
                          parameters:[NSDictionary dictionaryWithObjectsAndKeys:[self.dic_productDetail valueForKey:@"price"],@"price",[self.dic_productDetail valueForKey:@"catalogEntryId"],@"catalogEntryId", nil]
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        if (![responseObject valueForKey:@"errors"])
        {
            self.obj_installment = [Installment instanceFromDictionary:responseObject];
            
            if ([self.obj_installment.credFlexMap.arrayList count])
            {
                self.obj_arrayList = [self.obj_installment.credFlexMap.arrayList objectAtIndex:0];
//                self.textfield_entrada.text = self.obj_arrayList.dataPedido;
                [self method_addentradaPickers];
            }
            if ([self.obj_installment.installmentMap.aMEXRegistradoCheckout count])
            {
                [self method_addcreditoPickers];
                [self.tableview_ reloadData];
                array_credito = [[NSArray alloc] initWithObjects:@"AMEXRegistradoCheckout",@"DiscoverRegistradoCheckout",@"Master CardRegistradoCheckout",@"VISARegistradoCheckout", nil];
            }
        }
        else
        {
            [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                        valueForKey:@"errorMessage"]];
        }
    }
            failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
    }];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setMinimumDate: [NSDate date]];
    [datePicker addTarget:self action:@selector(updatedateTextField:)
         forControlEvents:UIControlEventValueChanged];
    [self.textField_date setInputView:datePicker];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - add picker in textfields
-(void)method_addentradaPickers
{
    picker_ = [[UIPickerView alloc] init];
    [picker_ setDelegate:self];
    [picker_ setDataSource:self];
    self.textfield_entrada.inputView = picker_;
}
#pragma mark - add picker in textfields
-(void)method_addcreditoPickers
{
    picker_ = [[UIPickerView alloc] init];
    [picker_ setDelegate:self];
    [picker_ setDataSource:self];
    self.textfield_credito.inputView = picker_;
}

#pragma mark - updatedateTextField
-(void)updatedateTextField:(UIDatePicker *)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    //get tommorow date
    NSDateComponents *deltaComps = [[NSDateComponents alloc] init];
    [deltaComps setDay:1];
    NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
    DLog(@"%@",[dateFormat stringFromDate:tomorrow]);
    
    if ([[dateFormat stringFromDate:sender.date] isEqualToString:[dateFormat stringFromDate:[NSDate date]]])
    {
        [UtilitiesHelper showAlert:@"Informe uma data futura"];
    }
    else if ([[dateFormat stringFromDate:sender.date] isEqualToString:[dateFormat stringFromDate:tomorrow]])
    {
         [UtilitiesHelper showAlert:@"Informe uma data futura"];
    }
    else
    {
        self.textField_date.text = [dateFormat stringFromDate:sender.date];
        UIPickerView *picker_temp = (UIPickerView *)self.textfield_entrada.inputView;
        NSUInteger selectedRowOfPlanoDePagemento = [picker_temp selectedRowInComponent:0];
        
        self.obj_arrayList = [self.obj_installment.credFlexMap.arrayList objectAtIndex:selectedRowOfPlanoDePagemento];
        
        if ([self.textfield_entrada.text length])
        {
            [self findSimulate:[NSString stringWithFormat:@"%@",self.obj_arrayList.comEntrada]
                       SapCode:self.obj_arrayList.sapCode
                   Index_plana:[NSString stringWithFormat:@"%@",self.obj_arrayList.indexPlana]];
        }
        else
        {
            [UtilitiesHelper showAlert:@"Selecionar entrada primeiro"];
        }
 
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - tableview delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (integer_creditoSelectedIndex == 0)
    {
        return [self.obj_installment.installmentMap.aMEXRegistradoCheckout count];
    }
    else if (integer_creditoSelectedIndex == 1)
    {
        return [self.obj_installment.installmentMap.discoverRegistradoCheckout count];
    }
    else if (integer_creditoSelectedIndex == 2)
    {
        return [self.obj_installment.installmentMap.masterCardRegistradoCheckout count];
    }
    else
    {
        return [self.obj_installment.installmentMap.vISARegistradoCheckout count];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Cell_outros *cell = (Cell_outros *)[tableView dequeueReusableCellWithIdentifier:@"Cell_outros"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Cell_outros" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    if (integer_creditoSelectedIndex == 0)
    {
        self.obj_registradoCheckout = [self.obj_installment.installmentMap.aMEXRegistradoCheckout objectAtIndex:indexPath.row];
        cell.label_numberOfInstallment.text = [NSString stringWithFormat:@"0 + %@",self.obj_registradoCheckout.numberOfInstallment];
        cell.label_installmentAmount.text = [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_registradoCheckout.installmentAmount]];
        cell.label_totalAmount.text = [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_registradoCheckout.totalAmout]];
    }
    else if (integer_creditoSelectedIndex == 1)
    {
        self.obj_discoverRegistradoCheckout = [self.obj_installment.installmentMap.discoverRegistradoCheckout objectAtIndex:indexPath.row];
        cell.label_numberOfInstallment.text = [NSString stringWithFormat:@"0 + %@",self.obj_discoverRegistradoCheckout.numberOfInstallment];
        cell.label_installmentAmount.text = [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_discoverRegistradoCheckout.installmentAmount]];
        cell.label_totalAmount.text = [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_discoverRegistradoCheckout.totalAmout]];
    }
    else if (integer_creditoSelectedIndex == 2)
    {
        self.obj_masterCardRegistradoCheckout = [self.obj_installment.installmentMap.masterCardRegistradoCheckout objectAtIndex:indexPath.row];
        cell.label_numberOfInstallment.text = [NSString stringWithFormat:@"0 + %@",self.obj_masterCardRegistradoCheckout.numberOfInstallment];
        cell.label_installmentAmount.text = [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_masterCardRegistradoCheckout.installmentAmount]];
        cell.label_totalAmount.text = [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_masterCardRegistradoCheckout.totalAmout]];
    }
    else
    {
        self.obj_vISARegistradoCheckout = [self.obj_installment.installmentMap.vISARegistradoCheckout objectAtIndex:indexPath.row];
        cell.label_numberOfInstallment.text = [NSString stringWithFormat:@"0 + %@",self.obj_vISARegistradoCheckout.numberOfInstallment];
        cell.label_installmentAmount.text = [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_vISARegistradoCheckout.installmentAmount]];
        cell.label_totalAmount.text = [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_vISARegistradoCheckout.totalAmout]];
    }
    return cell;
}

- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:self.textfield_credito] || [textField isEqual:self.textfield_entrada])
    {
        [picker_ reloadAllComponents];
    }
    return YES;
}


#pragma mark - picker delegate
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.textfield_entrada.isFirstResponder)
    {
        return [self.obj_installment.credFlexMap.arrayList count];
    }
    else if (self.textfield_credito.isFirstResponder)
    {
        return [array_credito count];
    }
    return 0;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return  1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (self.textfield_entrada.isFirstResponder)
    {
        self.obj_arrayList = [self.obj_installment.credFlexMap.arrayList objectAtIndex:row];
        return self.obj_arrayList.dataPedido;
    }
    else
    {
        return [array_credito objectAtIndex:row];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (self.textfield_entrada.isFirstResponder)
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/yyyy"];
        //get tommorow date
        NSDateComponents *deltaComps = [[NSDateComponents alloc] init];
        [deltaComps setDay:1];
        NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
        DLog(@"%@",self.textField_date.text);
        DLog(@"%@",[dateFormat stringFromDate:tomorrow]);
        self.textfield_entrada.text = self.obj_arrayList.dataPedido;
        if ([self.textField_date.text isEqualToString:[dateFormat stringFromDate:[NSDate date]]])
        {
            [UtilitiesHelper showAlert:@"Informe uma data futura"];
        }
        else if ([self.textField_date.text isEqualToString:[dateFormat stringFromDate:tomorrow]])
        {
            [UtilitiesHelper showAlert:@"Informe uma data futura"];
        }
        else
        {
            self.obj_arrayList = [self.obj_installment.credFlexMap.arrayList objectAtIndex:row];
            if ([self.textField_date.text length])
            {
                [self findSimulate:[NSString stringWithFormat:@"%@",self.obj_arrayList.comEntrada]
                           SapCode:self.obj_arrayList.sapCode
                       Index_plana:[NSString stringWithFormat:@"%@",self.obj_arrayList.indexPlana]];
            }
        }
    }
    else
    {
        self.textfield_credito.text = [array_credito objectAtIndex:row];
        integer_creditoSelectedIndex = row;
        [self.tableview_ reloadData];
    }
}

#pragma mark - findSimulate
-(void)findSimulate:(NSString *)com_entrada SapCode:(NSString *)sapcode Index_plana:(NSString *)index_plana
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSDate *date_ = [[NSDate alloc] init];
    date_ = [dateFormat dateFromString:self.textField_date.text];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    [[ServiceModel sharedClient]
     POST:k_findSimulate
     parameters:[NSDictionary dictionaryWithObjectsAndKeys:[self.dic_productDetail valueForKey:@"price"],@"total",
                 com_entrada,@"com_entrada",
                 index_plana,@"index_plana",
                 sapcode,@"sapCode",
                 [dateFormat stringFromDate:date_],@"dataVencimento", nil]
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             DLog(@"%@",responseObject);
             if (![responseObject valueForKey:@"errors"])
             {
                 self.obj_simulate = [FindSimulate instanceFromDictionary:responseObject];
                 self.obj_findSimulateSimulateMap = self.obj_simulate.simulateMap;
                 self.label_entrada.text = [UtilitiesHelper method_currencyFormatter:self.obj_findSimulateSimulateMap.entrada];
                 self.label_parcela.text = [UtilitiesHelper method_currencyFormatter:self.obj_findSimulateSimulateMap.prestacao];
                 self.label_juros.text = [UtilitiesHelper method_currencyFormatter:self.obj_findSimulateSimulateMap.financiamento];
                 self.label_total.text = [UtilitiesHelper method_currencyFormatter:self.obj_findSimulateSimulateMap.prazo];
                 self.label_jurosDetail.text = [NSString stringWithFormat:@"Juros + DOC = %@%% por mês (%@ por ano)",self.obj_findSimulateSimulateMap.jurosMes,self.obj_findSimulateSimulateMap.jurosAno];
             }
             else
             {
                 [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                             valueForKey:@"errorMessage"]];
             }
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
     }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:error.description];
         //         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}
@end