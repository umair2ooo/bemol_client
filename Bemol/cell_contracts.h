//
//  cell_contracts.h
//  Bemol
//
//  Created by Fahim Bilwani on 13/11/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cell_contracts : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label_parcelaNumber;
@property (weak, nonatomic) IBOutlet UILabel *label_parcelavalue;
@property (weak, nonatomic) IBOutlet UILabel *label_vencimento;
@property (weak, nonatomic) IBOutlet UILabel *label_status;
@property (weak, nonatomic) IBOutlet UIButton *button_checkBox;

-(void)method_setValues:(NSDictionary *)dic;

@end
