//
//  VC_selectLocation.h
//  Bemol
//
//  Created by Fahim Bilwani on 17/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VC_selectLocation : BaseViewController
@property(nonatomic, strong)NSMutableDictionary *dic_selectedstoreSelection;
@property(nonatomic, strong)NSString *string_orderId;
@property(nonatomic) BOOL bool_comingFromCheckinnaLoja;
@property(nonatomic) BOOL bool_comingFromCartList;
@end
