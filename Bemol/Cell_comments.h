#import <UIKit/UIKit.h>

@interface Cell_comments : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageview_star1;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_star2;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_star3;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_star4;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_star5;
@property (weak, nonatomic) IBOutlet UILabel *label_date;
@property (weak, nonatomic) IBOutlet UILabel *label_name;
@property (weak, nonatomic) IBOutlet UILabel *label_comment;
@property (weak, nonatomic) IBOutlet UILabel *label_useful;
@property (weak, nonatomic) IBOutlet UILabel *label_notuseful;

-(void)method_setValues:(NSDictionary *)dic;

@end