#import "OrderDetails.h"

#import "NotaFiscal.h"
#import "Pagamento.h"

@implementation OrderDetails

@synthesize cancelableOrder;
@synthesize compra;
@synthesize entrega;
@synthesize loja;
@synthesize lojaId;
@synthesize montagem;
@synthesize notaFiscal;
@synthesize numeroDoPedido;
@synthesize orderId;
@synthesize pagamento;
@synthesize status;
@synthesize statusDesc;
@synthesize valor;

+ (OrderDetails *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OrderDetails *instance = [[OrderDetails alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.cancelableOrder = [(NSNumber *)[aDictionary objectForKey:@"cancelableOrder"] boolValue];
    self.compra = [aDictionary objectForKey:@"compra"];
    self.entrega = [aDictionary objectForKey:@"entrega"];
    self.loja = [aDictionary objectForKey:@"loja"];
    self.lojaId = [aDictionary objectForKey:@"lojaId"];
    self.montagem = [aDictionary objectForKey:@"montagem"];

    NSArray *receivedNotaFiscal = [aDictionary objectForKey:@"notaFiscal"];
    if ([receivedNotaFiscal isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedNotaFiscal = [NSMutableArray arrayWithCapacity:[receivedNotaFiscal count]];
        for (NSDictionary *item in receivedNotaFiscal) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedNotaFiscal addObject:[NotaFiscal instanceFromDictionary:item]];
            }
        }

        self.notaFiscal = populatedNotaFiscal;

    }
    self.numeroDoPedido = [aDictionary objectForKey:@"numeroDoPedido"];
    self.orderId = [aDictionary objectForKey:@"orderId"];

    NSArray *receivedPagamento = [aDictionary objectForKey:@"pagamento"];
    if ([receivedPagamento isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedPagamento = [NSMutableArray arrayWithCapacity:[receivedPagamento count]];
        for (NSDictionary *item in receivedPagamento) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedPagamento addObject:[Pagamento instanceFromDictionary:item]];
            }
        }

        self.pagamento = populatedPagamento;

    }
    self.status = [aDictionary objectForKey:@"status"];
    self.statusDesc = [aDictionary objectForKey:@"statusDesc"];
    self.valor = [aDictionary objectForKey:@"valor"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    [dictionary setObject:[NSNumber numberWithBool:self.cancelableOrder] forKey:@"cancelableOrder"];

    if (self.compra) {
        [dictionary setObject:self.compra forKey:@"compra"];
    }

    if (self.entrega) {
        [dictionary setObject:self.entrega forKey:@"entrega"];
    }

    if (self.loja) {
        [dictionary setObject:self.loja forKey:@"loja"];
    }

    if (self.lojaId) {
        [dictionary setObject:self.lojaId forKey:@"lojaId"];
    }

    if (self.montagem) {
        [dictionary setObject:self.montagem forKey:@"montagem"];
    }

    if (self.notaFiscal) {
        [dictionary setObject:self.notaFiscal forKey:@"notaFiscal"];
    }

    if (self.numeroDoPedido) {
        [dictionary setObject:self.numeroDoPedido forKey:@"numeroDoPedido"];
    }

    if (self.orderId) {
        [dictionary setObject:self.orderId forKey:@"orderId"];
    }

    if (self.pagamento) {
        [dictionary setObject:self.pagamento forKey:@"pagamento"];
    }

    if (self.status) {
        [dictionary setObject:self.status forKey:@"status"];
    }

    if (self.statusDesc) {
        [dictionary setObject:self.statusDesc forKey:@"statusDesc"];
    }

    if (self.valor) {
        [dictionary setObject:self.valor forKey:@"valor"];
    }

    return dictionary;

}


@end
