//
//  CustomNavigationController.h
//  Bemol
//
//  Created by Fahim Bilwani on 02/11/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationController : UINavigationController

@end
