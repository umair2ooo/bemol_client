#import "FormTextField.h"

@implementation FormTextField

#define k_padding 10.0

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, k_padding, 0);
}


// text position
- (CGRect)editingRectForBounds:(CGRect)bounds
{
    self.textAlignment = NSTextAlignmentLeft;
    
    return CGRectInset(bounds, k_padding, 0);
}

@end