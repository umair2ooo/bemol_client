#import <Foundation/Foundation.h>

@interface GarantiaList : NSObject {

    NSString *dataPedido;
    NSString *lojaId;
    NSString *nomeSku;
    NSString *notaFiscal;
    NSString *precoUnitario;
    NSString *sku;
    NSNumber *tempGar;
    NSString *urlSku;
    NSString *vendaPosterior;

}

@property (nonatomic, copy) NSString *dataPedido;
@property (nonatomic, copy) NSString *lojaId;
@property (nonatomic, copy) NSString *nomeSku;
@property (nonatomic, copy) NSString *notaFiscal;
@property (nonatomic, copy) NSString *precoUnitario;
@property (nonatomic, copy) NSString *sku;
@property (nonatomic, copy) NSNumber *tempGar;
@property (nonatomic, copy) NSString *urlSku;
@property (nonatomic, copy) NSString *vendaPosterior;

+ (GarantiaList *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
