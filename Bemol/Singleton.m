#import "Singleton.h"
#import "Reachability.h"
#import <AFNetworking.h>

#import <CoreText/CTFont.h>
//#import "AFHTTPClient.h"
#import "AFHTTPRequestOperationManager.h"



@implementation Singleton

@synthesize string_controllerName = _string_controllerName;
@synthesize dic_productDetail = _dic_productDetail;
@synthesize string_orderId = _string_orderId;
//@synthesize array_cartObjects =_array_cartObjects;
@synthesize isGuestCheckOut = _isGuestCheckOut;
@synthesize soapMessage = _soapMessage;
@synthesize webResponseData = _webResponseData;
@synthesize obj_CartaoBemol = _obj_CartaoBemol;
@synthesize dic_userDetails = _dic_userDetails;
@synthesize bool_isComingfromCart = _bool_isComingfromCart;


static Singleton *sharedSingleton = nil;

+(Singleton *)retriveSingleton
{
	@synchronized(self)
	{
		if(sharedSingleton ==nil)
		{
			sharedSingleton =[[Singleton alloc]init];
            
            DLog(@"Singleton created");
		}
	}
	return sharedSingleton;
}

+(id)allocWithZone:(NSZone *)zone
{
	@synchronized(self)
	{
		if(sharedSingleton ==nil)
		{
			sharedSingleton = [super allocWithZone:zone];
			return sharedSingleton;
		}
	}
	return nil;
}

-(BOOL)method_connectToInternet
{
    Reachability *r = [Reachability reachabilityWithHostname:@"www.google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];    
    BOOL result = NO;
    if (internetStatus == ReachableViaWiFi )
    {
        result = YES;    
    } 
    else if(internetStatus == ReachableViaWWAN)
    {
        result = YES;
    }
    return result;
}

#pragma mark - is email address validity?
-(BOOL)method_NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - are all spaces?
-(BOOL)method_checkOnlySpaces:(NSString *)str
{
    BOOL isValid = false;
    
    NSString *rawString = str;
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    
    if ([trimmed length] > 0)
    {
        isValid = true;
    }
    
    return isValid;
}
#pragma mark - phone number validation
-(BOOL)method_phoneNumberValidation:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}


#pragma mark - UILable variable height
-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width
{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    }
    else{
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 40.0f);
    return height;
}

#pragma mark - custom font for entire App
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        
        if ([self method_isBold:lbl.font])
        {
            [lbl setFont:[UIFont fontWithName:k_fontNameBold size:[[lbl font] pointSize]]];
        }
        else
        {
            [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
        }
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtfld = (UITextField *)view;

        if ([self method_isBold:txtfld.font])
        {
            [txtfld setFont:[UIFont fontWithName:k_fontNameBold size:[[txtfld font] pointSize]]];
        }
        else
        {
            [txtfld setFont:[UIFont fontWithName:fontFamily size:[[txtfld font] pointSize]]];
        }
    }


    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        if ([self method_isBold:btn.titleLabel.font])
        {
            [btn.titleLabel setFont:[UIFont fontWithName:k_fontNameBold size:[[btn.titleLabel font] pointSize]]];
        }
        else
        {
            [btn.titleLabel setFont:[UIFont fontWithName:fontFamily size:[[btn.titleLabel font] pointSize]]];
        }
    }


    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtView = (UITextView *)view;
        if ([self method_isBold:txtView.font])
        {
            [txtView setFont:[UIFont fontWithName:k_fontNameBold size:[[txtView font] pointSize]]];
        }
        else
        {
            [txtView setFont:[UIFont fontWithName:fontFamily size:[[txtView font] pointSize]]];
        }
    }
    
    
    if (isSubViews)
    {
        [view.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [self setFontFamily:fontFamily forView:obj andSubViews:YES];
        }];
    }
}


-(BOOL)method_isBold:(UIFont *)font_
{
    CTFontRef font = CTFontCreateWithName((CFStringRef)font_.fontName, font_.pointSize, NULL);
    CTFontSymbolicTraits traits = CTFontGetSymbolicTraits(font);
//    BOOL isItalic = ((traits & kCTFontItalicTrait) == kCTFontItalicTrait);
    BOOL isBold = ((traits & kCTFontBoldTrait) == kCTFontBoldTrait);
//    NSLog(@"Italic: %i Bold: %i", isItalic, isBold);
    CFRelease(font);
    
    return isBold;
}


//-(NSString *)method_convertCurrency:(NSString *)string_
//{
////    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:string_];
////    NSLocale *priceLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"]; // get the locale from your SKProduct
////    
////    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
////    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
////    [currencyFormatter setLocale:priceLocale];
////    NSString *currencyString = [currencyFormatter internationalCurrencySymbol]; // EUR, GBP, USD...
////    NSString *format = [currencyFormatter positiveFormat];
////    format = [format stringByReplacingOccurrencesOfString:@"¤" withString:currencyString];
////    // ¤ is a placeholder for the currency symbol
////    [currencyFormatter setPositiveFormat:format];
////    
////    return [currencyFormatter stringFromNumber:price];
//
//
//    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
//    NSLocale *localizableIdentifier = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
//    [formatter setLocale:localizableIdentifier];
//    float money = [string_ floatValue];
//    NSNumber *meuDinheiro = [NSNumber numberWithFloat:money];
//    
//    return [formatter stringFromNumber:meuDinheiro];
//}

@end