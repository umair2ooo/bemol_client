#import <Foundation/Foundation.h>

@interface CartaoBemolBounusMap : NSObject {

    NSString *historico;
    NSString *pedra;
    NSString *pontos;
    NSString *R;

}

@property (nonatomic, copy) NSString *historico;
@property (nonatomic, copy) NSString *pedra;
@property (nonatomic, copy) NSString *pontos;
@property (nonatomic, copy) NSString *R;

+ (CartaoBemolBounusMap *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
