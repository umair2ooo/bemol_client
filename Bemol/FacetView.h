#import <Foundation/Foundation.h>

@interface FacetView : NSObject {

    NSMutableArray *entry;
    NSString *name;
    NSString *value;

}

@property (nonatomic, copy) NSMutableArray *entry;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *value;

+ (FacetView *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
