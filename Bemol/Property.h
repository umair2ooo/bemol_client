//
//  Property.h
//  
//
//  Created by fahim on 07/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Property : NSObject {
    NSString *baseMarketingKey;
    NSString *baseMarketingValue;
}

@property (nonatomic, copy) NSString *baseMarketingKey;
@property (nonatomic, copy) NSString *baseMarketingValue;

+ (Property *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
