#import <UIKit/UIKit.h>

#import "homeScreenBanners.h"

#import "MarketingSpotDatum.h"
#import "BaseMarketingSpotActivityDatum.h"
#import "MarketingContentDescription.h"

@protocol homeScrollerProtocolDelegate <NSObject>

-(void)method_catalog;
-(void)method_barcode;
-(void)method_locateStore;
-(void)method_loyalityPoints;

@end

@interface scroller_home : UIScrollView<UIScrollViewDelegate>
{
    NSTimer *timer_;
    NSMutableArray *array_scrollerOneBanners;
}

@property(nonatomic, strong)HomeScreenBanners *obj_homeScreenBannersOne;


@property(nonatomic, strong)MarketingSpotDatum *obj_marketingSpotDatum;
@property(nonatomic, strong)BaseMarketingSpotActivityDatum *obj_baseMarketingSpotActivityDatum;
@property(nonatomic, strong)MarketingContentDescription *obj_marketingContentDescription;

                            
                            

@property(nonatomic, strong)id <homeScrollerProtocolDelegate> delegate_;

-(void)method_startScroller;
-(void)method_stopScroller;

-(void)method_getDataFromServer;

@end