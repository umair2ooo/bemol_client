//
//  cell_pagamentos.h
//  Bemol
//
//  Created by Fahim Bilwani on 27/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cell_pagamentos : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label_date;
@property (weak, nonatomic) IBOutlet UILabel *label_amount;
@property (weak, nonatomic) IBOutlet UIImageView *imagview_circle;

-(void)method_setValues:(NSDictionary *)dic;
@end
