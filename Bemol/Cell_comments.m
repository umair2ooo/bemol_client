#import "Cell_comments.h"
#import "NSString+HTML.h"

@implementation Cell_comments

- (void)awakeFromNib
{
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)method_setValues:(NSDictionary *)dic
{
    DLog(@"dic: %@", dic);
    
    self.label_name.text = [dic valueForKey:@"name"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *loc = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    [dateFormatter setLocale:loc];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.S"];
    NSDate *orignalDate   =  [dateFormatter dateFromString:[dic valueForKey:@"date"]];
    [dateFormatter setDateFormat: @"dd' de 'MMMM' de 'yyyy"];
    
    self.label_date.text = [dateFormatter stringFromDate:orignalDate];
    
   // self.label_date.text = [dic valueForKey:@"date"];
    self.label_comment.text = [[dic valueForKey:@"comments"] kv_stringByStrippingHTML:[dic valueForKey:@"comments"]];
    
    self.label_useful.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"useful"]];
    self.label_notuseful.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"notuseful"]];
    
    if ([[dic valueForKey:@"rating"] isEqual:@1])
    {
        [self.imageview_star1 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star2 setImage:[UIImage imageNamed:@"stargray"]];
        [self.imageview_star3 setImage:[UIImage imageNamed:@"stargray"]];
        [self.imageview_star4 setImage:[UIImage imageNamed:@"stargray"]];
        [self.imageview_star5 setImage:[UIImage imageNamed:@"stargray"]];
    }
    else if ([[dic valueForKey:@"rating"] isEqual:@2])
    {
        [self.imageview_star1 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star2 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star3 setImage:[UIImage imageNamed:@"stargray"]];
        [self.imageview_star4 setImage:[UIImage imageNamed:@"stargray"]];
        [self.imageview_star5 setImage:[UIImage imageNamed:@"stargray"]];
    }
    else if ([[dic valueForKey:@"rating"] isEqual:@3])
    {
        [self.imageview_star1 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star2 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star3 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star4 setImage:[UIImage imageNamed:@"stargray"]];
        [self.imageview_star5 setImage:[UIImage imageNamed:@"stargray"]];
    }
    else if ([[dic valueForKey:@"rating"] isEqual:@4])
    {
        [self.imageview_star1 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star2 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star3 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star4 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star5 setImage:[UIImage imageNamed:@"stargray"]];
    }
    else
    {
        [self.imageview_star1 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star2 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star3 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star4 setImage:[UIImage imageNamed:@"starfull"]];
        [self.imageview_star5 setImage:[UIImage imageNamed:@"starfull"]];
    }
}

@end