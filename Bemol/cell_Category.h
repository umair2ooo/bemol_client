//
//  cell_Category.h
//  CaptureLife
//
//  Created by Fahim Bilwani on 4/27/15.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cell_Category : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label_categoryName;
@property (weak, nonatomic) IBOutlet UIView *view_back;
@property (weak, nonatomic) IBOutlet UIImageView *image_category;

-(void)method_setValues:(NSDictionary *)dic;
@end
