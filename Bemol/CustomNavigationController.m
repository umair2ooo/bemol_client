//
//  CustomNavigationController.m
//  Bemol
//
//  Created by Fahim Bilwani on 02/11/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "CustomNavigationController.h"
#import "VC_home.h"
#import "VC_Category.h"
#import "VC_login.h"

@interface CustomNavigationController ()<UINavigationControllerDelegate>

@property (strong, nonatomic) UIBarButtonItem *rightBarButton;
@property (strong, nonatomic) UIBarButtonItem *backBarButtonItem;
@property (strong, nonatomic) UIBarButtonItem *Button_Username;
@end

@implementation CustomNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark- setNavigationBar
-(void)setNavigationBar
{
    self.navigationBar.translucent = YES;
    //set tint color
    [self.navigationBar setTintColor:[UIColor whiteColor]];
    
    //set title color
    [self.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    //set back button text color
    //        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    //        //set back button arrow color
    //        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    // BackButton
    self.backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-back"] style:UIBarButtonItemStylePlain target:self action:@selector(backTapped:)];
    
    //CrossButton
    self.rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-close"] style:UIBarButtonItemStylePlain target:self action:@selector(dismissTapped:)];
    
    self.Button_Username = [[UIBarButtonItem alloc] initWithTitle:@"Faça seu login aqui"
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:self
                                                                       action:@selector(logOut:)];
//    self.Button_Username.titleLabel.adjustsFontSizeToFitWidth = TRUE;
//    self.Button_Username.titleLabel.minimumFontSize = 40;
    
    
    self.navigationItem.leftBarButtonItem = self.backBarButtonItem;
//    self.navigationItem.rightBarButtonItem = self.rightBarButton;
    [self.navigationBar setItems:@[self.navigationItem]];
    
    self.delegate = self;
}

#pragma mark- backTapped
- (void)backTapped:(id)sender
{
    NSInteger numberOfViewControllers = self.viewControllers.count;
    
    if (numberOfViewControllers == 1)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self popViewControllerAnimated:YES];
    }
}

#pragma mark- dismissTapped
- (void)dismissTapped:(id)sender
{
    if ([[self.viewControllers objectAtIndex:0] isKindOfClass:[VC_home class]])
    {
        [self popToRootViewControllerAnimated:YES];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark- logOut
- (void)logOut:(id)sender
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:k_isLogin] isEqualToString:@"no"])
    {
        UINavigationController *obj_login = [self.storyboard instantiateViewControllerWithIdentifier:@"nav_logIn"];
        ApplicationDelegate.single.bool_isComingfromCart = NO;
        [self presentViewController:obj_login animated:YES completion:nil];
    }
   //[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark- setNavigationBarForHome
-(void)setNavigationBarForHome:(UIViewController *)controller_
{
    [self.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    if (!self.navigationItem.rightBarButtonItem)
    {
        UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0,0,100,52)];
        UIImageView *imageview_logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-bemol"]];
        [backButtonView addSubview:imageview_logo];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButtonView]];
        self.navigationItem.rightBarButtonItem = self.Button_Username;
    }
    else
    {
        self.topViewController.navigationItem.rightBarButtonItem = self.Button_Username;
    }
}

#pragma mark- setNavigationBarForInnerScreens
-(void)setNavigationBarForInnerScreens:(UIViewController *)controller_
{
    //set bar image
    UIImage *barImage = [[UIImage imageNamed:@"bemol-hdr"]
                         resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 20, 20) resizingMode:UIImageResizingModeStretch];
    [self.navigationBar setBackgroundImage:barImage forBarMetrics:UIBarMetricsDefault];
    
//    NSInteger numberOfViewControllers = self.viewControllers.count;

    if (self.viewControllers.count == 1)
    {
        [self.navigationItem setRightBarButtonItem:nil];
    }
    else
    {
        if (![[self.viewControllers objectAtIndex:self.viewControllers.count-2] isKindOfClass:[VC_home class]])
        {
            controller_.navigationItem.rightBarButtonItem = self.rightBarButton;
            controller_.navigationItem.leftBarButtonItem = self.backBarButtonItem;
        }
        else
        {
            self.topViewController.navigationItem.leftBarButtonItem = self.backBarButtonItem;
        }
    }
}

#pragma mark - UINavigation Controller delegates
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    DLog(@"%@", [viewController class]);
    //NSArray *controllerArray = self.viewControllers;
    
    if ([viewController isKindOfClass:[VC_home class]])
    {
        [self setNavigationBarForHome:viewController];
    }
    else
    {
        [self setNavigationBarForInnerScreens:viewController];
    }
}



- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    
}
@end