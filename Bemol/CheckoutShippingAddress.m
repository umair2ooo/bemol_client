#import "CheckoutShippingAddress.h"

#import "ShippingAddress.h"

@implementation CheckoutShippingAddress

@synthesize shippingAddresses;

+ (CheckoutShippingAddress *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CheckoutShippingAddress *instance = [[CheckoutShippingAddress alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }


    NSArray *receivedShippingAddresses = [aDictionary objectForKey:@"ShippingAddresses"];
    if ([receivedShippingAddresses isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedShippingAddresses = [NSMutableArray arrayWithCapacity:[receivedShippingAddresses count]];
        for (NSDictionary *item in receivedShippingAddresses) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedShippingAddresses addObject:[ShippingAddress instanceFromDictionary:item]];
            }
        }

        self.shippingAddresses = populatedShippingAddresses;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.shippingAddresses) {
        [dictionary setObject:self.shippingAddresses forKey:@"shippingAddresses"];
    }

    return dictionary;

}


@end
