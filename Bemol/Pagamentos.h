#import <Foundation/Foundation.h>

@interface Pagamentos : NSObject {

    NSNumber *countTotal;
    NSMutableArray *list;

}

@property (nonatomic, copy) NSNumber *countTotal;
@property (nonatomic, copy) NSMutableArray *list;

+ (Pagamentos *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
