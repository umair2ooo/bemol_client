#import <Foundation/Foundation.h>

@interface Price : NSObject {

    NSString *contractId;
    NSString *isLowestContractPrice;
    NSString *priceDescription;
    NSString *priceUsage;
    NSString *priceValue;

}

@property (nonatomic, copy) NSString *contractId;
@property (nonatomic, copy) NSString *isLowestContractPrice;
@property (nonatomic, copy) NSString *priceDescription;
@property (nonatomic, copy) NSString *priceUsage;
@property (nonatomic, copy) NSString *priceValue;

+ (Price *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
