//
//  cell_meusDados.h
//  Bemol
//
//  Created by Fahim Bilwani on 16/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cell_meusDados : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label_key;
@property (weak, nonatomic) IBOutlet UILabel *label_value;
@end
