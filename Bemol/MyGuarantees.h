#import <Foundation/Foundation.h>

@interface MyGuarantees : NSObject {

    NSNumber *countTotal;
    NSMutableArray *garantiaList;

}

@property (nonatomic, copy) NSNumber *countTotal;
@property (nonatomic, copy) NSMutableArray *garantiaList;

+ (MyGuarantees *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
