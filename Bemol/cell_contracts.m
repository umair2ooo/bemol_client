//
//  cell_contracts.m
//  Bemol
//
//  Created by Fahim Bilwani on 13/11/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "cell_contracts.h"

@implementation cell_contracts

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)method_setValues:(NSDictionary *)dic
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date_ = [[NSDate alloc] init];
    date_ = [dateFormat dateFromString:[dic valueForKey:@"date"]];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    self.label_vencimento.text = [dateFormat stringFromDate:date_];
    dateFormat = nil;
    date_ = nil;
    
    self.label_parcelavalue.text = [UtilitiesHelper method_currencyFormatter:[dic valueForKey:@"ParcelaValue"]] ;
    self.label_parcelaNumber.text = [NSString stringWithFormat:@"Parcela %@",[dic valueForKey:@"ParcelaNumber"]];
    
    if ([[dic valueForKey:@"Status"] isEqualToString:@"P"])
    {
        [self.label_status setTextColor:[UtilitiesHelper colorFromHexString:@"#319C00"]];
        [self.label_parcelavalue setTextColor:[UtilitiesHelper colorFromHexString:@"#319C00"]];
        [self.label_vencimento setTextColor:[UtilitiesHelper colorFromHexString:@"#319C00"]];
        self.label_status.text = [NSString stringWithFormat:@"Pago em %@",[dic valueForKey:@"date1"]];
    }
    else if ([[dic valueForKey:@"Status"] isEqualToString:@"A"])
    {
        [self.label_status setTextColor:[UtilitiesHelper colorFromHexString:@"#000000"]];
        [self.label_parcelavalue setTextColor:[UtilitiesHelper colorFromHexString:@"#000000"]];
        [self.label_vencimento setTextColor:[UtilitiesHelper colorFromHexString:@"#000000"]];
        self.label_status.text = [NSString stringWithFormat:@"A Pagar"];
    }
    else if ([[dic valueForKey:@"Status"] isEqualToString:@"E"])
    {
        [self.label_status setTextColor:[UtilitiesHelper colorFromHexString:@"#c3003f"]];
        [self.label_parcelavalue setTextColor:[UtilitiesHelper colorFromHexString:@"#c3003f"]];
        [self.label_vencimento setTextColor:[UtilitiesHelper colorFromHexString:@"#c3003f"]];
        self.label_status.text = [NSString stringWithFormat:@" Em Atraso"];
    }
    else if ([[dic valueForKey:@"Status"] isEqualToString:@"R"])
    {
        [self.label_status setTextColor:[UtilitiesHelper colorFromHexString:@"#6503b0"]];
        [self.label_parcelavalue setTextColor:[UtilitiesHelper colorFromHexString:@"#6503b0"]];
        [self.label_vencimento setTextColor:[UtilitiesHelper colorFromHexString:@"#6503b0"]];
        self.label_status.text = [NSString stringWithFormat:@"renegociar"];
    }
    else if ([[dic valueForKey:@"Status"] isEqualToString:@"H"])
    {
        [self.label_status setTextColor:[UtilitiesHelper colorFromHexString:@"#0864b7"]];
        [self.label_parcelavalue setTextColor:[UtilitiesHelper colorFromHexString:@"#0864b7"]];
        [self.label_vencimento setTextColor:[UtilitiesHelper colorFromHexString:@"#0864b7"]];
        self.label_status.text = [NSString stringWithFormat:@"A Pagar"];
    }
    else if ([[dic valueForKey:@"Status"] isEqualToString:@"W"])
    {
        [self.label_status setTextColor:[UtilitiesHelper colorFromHexString:@"#FF3300"]];
        [self.label_parcelavalue setTextColor:[UtilitiesHelper colorFromHexString:@"#FF3300"]];
        [self.label_vencimento setTextColor:[UtilitiesHelper colorFromHexString:@"#FF3300"]];
        self.label_status.text = [NSString stringWithFormat:@"aguardando"];
    }
}


@end
