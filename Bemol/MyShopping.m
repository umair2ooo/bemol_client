#import "MyShopping.h"

#import "PedidoList.h"
#import "ProdutoList.h"

@implementation MyShopping

@synthesize pedidoList;
@synthesize produtoList;

+ (MyShopping *)instanceFromDictionary:(NSDictionary *)aDictionary {

    MyShopping *instance = [[MyShopping alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }


    NSArray *receivedPedidoList = [aDictionary objectForKey:@"PedidoList"];
    if ([receivedPedidoList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedPedidoList = [NSMutableArray arrayWithCapacity:[receivedPedidoList count]];
        for (NSDictionary *item in receivedPedidoList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedPedidoList addObject:[PedidoList instanceFromDictionary:item]];
            }
        }

        self.pedidoList = populatedPedidoList;

    }

    NSArray *receivedProdutoList = [aDictionary objectForKey:@"ProdutoList"];
    if ([receivedProdutoList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedProdutoList = [NSMutableArray arrayWithCapacity:[receivedProdutoList count]];
        for (NSDictionary *item in receivedProdutoList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedProdutoList addObject:[ProdutoList instanceFromDictionary:item]];
            }
        }

        self.produtoList = populatedProdutoList;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.pedidoList) {
        [dictionary setObject:self.pedidoList forKey:@"pedidoList"];
    }

    if (self.produtoList) {
        [dictionary setObject:self.produtoList forKey:@"produtoList"];
    }

    return dictionary;

}


@end
