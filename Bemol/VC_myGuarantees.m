//
//  VC_myGuarantees.m
//  Bemol
//
//  Created by Fahim Bilwani on 23/09/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "VC_myGuarantees.h"
#import "cell_minhasGrantis.h"
#import "MyGuarantees.h"
#import "GarantiaList.h"

@interface VC_myGuarantees ()

@property (weak, nonatomic) IBOutlet UITableView *tableview_;

@property (nonatomic,strong) MyGuarantees *obj_myGuarantees;
@property (nonatomic,strong) GarantiaList *obj_garantiaList;
@end

@implementation VC_myGuarantees

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Minhas Garantias"];
    
    [[ServiceModel sharedClient] GET:k_MyGuarantees
                          parameters:nil
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        if (![responseObject valueForKey:@"errors"])
        {
            self.obj_myGuarantees = [MyGuarantees instanceFromDictionary:responseObject];
            [self.tableview_ reloadData];
        }
        else
        {
            [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                        valueForKey:@"errorMessage"]];
        }
    }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        DLog(@"%@",error.description);
        [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
    }];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - tableview delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.obj_myGuarantees.garantiaList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_minhasGrantis *cell = (cell_minhasGrantis *)[tableView dequeueReusableCellWithIdentifier:@"cell_minhasGrantis"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_minhasGrantis" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    self.obj_garantiaList = [self.obj_myGuarantees.garantiaList objectAtIndex:indexPath.row];
    cell.label_description.text = self.obj_garantiaList.nomeSku;
    cell.label_SKU.text = [NSString stringWithFormat:@"SKU: %@",[UtilitiesHelper method_RemoveLeadingZeros:self.obj_garantiaList.sku]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date_ = [[NSDate alloc] init];
    date_ = [dateFormat dateFromString:self.obj_garantiaList.dataPedido];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    cell.label_date.text = [NSString stringWithFormat:@"até %@", [dateFormat stringFromDate:date_]];
    dateFormat = nil;
    date_ = nil;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
