//
//  VC_addNewAddress.h
//  Bemol
//
//  Created by Fahim Bilwani on 16/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShippingAddress.h"

@interface VC_addNewAddress : BaseViewController
@property(nonatomic, strong)NSMutableDictionary *dic_selectedAddress;
@property(nonatomic) BOOL bool_comingfromedit;
@property(nonatomic, strong) ShippingAddress *obj_shippingAddress;
@end
