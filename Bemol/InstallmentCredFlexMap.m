#import "InstallmentCredFlexMap.h"

#import "ArrayList.h"

@implementation InstallmentCredFlexMap

@synthesize arrayList;

+ (InstallmentCredFlexMap *)instanceFromDictionary:(NSDictionary *)aDictionary {

    InstallmentCredFlexMap *instance = [[InstallmentCredFlexMap alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }


    NSArray *receivedArrayList = [aDictionary objectForKey:@"arrayList"];
    if ([receivedArrayList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedArrayList = [NSMutableArray arrayWithCapacity:[receivedArrayList count]];
        for (NSDictionary *item in receivedArrayList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedArrayList addObject:[ArrayList instanceFromDictionary:item]];
            }
        }

        self.arrayList = populatedArrayList;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.arrayList) {
        [dictionary setObject:self.arrayList forKey:@"arrayList"];
    }

    return dictionary;

}


@end
