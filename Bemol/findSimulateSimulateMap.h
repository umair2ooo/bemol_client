#import <Foundation/Foundation.h>

@interface findSimulateSimulateMap : NSObject {

    NSString *dataPedido;
    NSString *dataVencimento;
    NSString *entrada;
    NSString *financiamento;
    NSString *jurosAno;
    NSString *jurosMes;
    NSString *prazo;
    NSString *prestacao;
    NSString *sapCode;

}

@property (nonatomic, copy) NSString *dataPedido;
@property (nonatomic, copy) NSString *dataVencimento;
@property (nonatomic, copy) NSString *entrada;
@property (nonatomic, copy) NSString *financiamento;
@property (nonatomic, copy) NSString *jurosAno;
@property (nonatomic, copy) NSString *jurosMes;
@property (nonatomic, copy) NSString *prazo;
@property (nonatomic, copy) NSString *prestacao;
@property (nonatomic, copy) NSString *sapCode;

+ (findSimulateSimulateMap *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
