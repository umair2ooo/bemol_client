#import <Foundation/Foundation.h>

@interface Value : NSObject {

    NSString *identifier;
    NSString *uniqueID;
    NSString *values;

}

@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSString *uniqueID;
@property (nonatomic, copy) NSString *values;

+ (Value *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
