//
//  VC_SubofSubCategory.m
//  CaptureLife
//
//  Created by Fahim Bilwani on 4/27/15.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_SubofSubCategory.h"
#import "cell_Category.h"
#import "VC_ProductList.h"
#import "ProductList.h"


@interface VC_SubofSubCategory ()

@property(nonatomic, strong)Categories *category;
@property(nonatomic, strong)ProductList *obj_productList;


- (IBAction)action_back:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableview_;
@end

@implementation VC_SubofSubCategory


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:self.string_categoryName];
    [self.tableview_ reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //ApplicationDelegate.single.delegate_ = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //ApplicationDelegate.single.delegate_ = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - tableview dataSource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_Category *cell = (cell_Category *)[tableView dequeueReusableCellWithIdentifier:@"cell_Category"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_Category" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    self.category = [self.array_categories objectAtIndex:indexPath.row];
    [cell.label_categoryName setFrame:CGRectMake(25, 16, 300, 21)];
    [cell method_setValues:[NSDictionary dictionaryWithObjectsAndKeys:self.category.name,@"name",
                            self.category.thumbnail,@"thumbnail",nil]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.category = [self.array_categories objectAtIndex:indexPath.row];
    [UtilitiesHelper showLoader:@"Loading.."
                        forView:self.view
                        setMode:MBProgressHUDModeIndeterminate
                       delegate:nil];
    //[ServiceModel sharedClient].delegate_ = self;
    //[[ServiceModel sharedClient] method_getProductListByCategory:self.category.uniqueID];

    
    [[ServiceModel sharedClient]GET:[NSString stringWithFormat:@"%@%@?pageSize=%@&pageNumber=1&orderBy=5", k_productListService,self.category.uniqueID, k_pageSize]
                         parameters:nil
                             onView:self.view
                            success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             self.obj_productList = [ProductList instanceFromDictionary:responseObject];
//             VC_ProductList *objProductList = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_ProductList"];
//             objProductList.array_products = [[NSMutableArray alloc] initWithArray:self.obj_productList.catalogEntryView];
             [self performSegueWithIdentifier:@"segue_SubCatSubProductList" sender:self.obj_productList.catalogEntryView];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                            failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error.description);
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];

    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_SubCatSubProductList"])
    {
        VC_ProductList *obj = segue.destinationViewController;
        obj.dic_data = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                        (NSMutableArray *)sender, @"array",
                        self.category.uniqueID, @"id",
                        self.obj_productList.recordSetTotal, @"total", nil];
//        obj.array_products = [NSMutableArray arrayWithArray:(NSMutableArray *)sender];
    }
}

#pragma mark - delegate_responseData
-(void)delegate_responseData:(id)data error:(NSError *)error servicename:(NSString *)servicename
{
    DLog(@"%@", data);
    [UtilitiesHelper hideLoader:self.view];
    if (data)
    {
        if ([servicename isEqualToString:@"productList"])
        {
//            VC_ProductList *objProductList = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_ProductList"];
//            objProductList.array_products = [[NSMutableArray alloc] initWithArray:data];
            [self performSegueWithIdentifier:@"segue_SubCatSubProductList" sender:data];
        }
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:error.description
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil] show];
    }
    
}

- (IBAction)action_cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
