#import "DiscoverRegistradoCheckout.h"

@implementation DiscoverRegistradoCheckout

@synthesize featured;
@synthesize installmentAmount;
@synthesize interestRate;
@synthesize numberOfInstallment;
@synthesize totalAmout;

+ (DiscoverRegistradoCheckout *)instanceFromDictionary:(NSDictionary *)aDictionary {

    DiscoverRegistradoCheckout *instance = [[DiscoverRegistradoCheckout alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.featured = [(NSNumber *)[aDictionary objectForKey:@"Featured"] boolValue];
    self.installmentAmount = [aDictionary objectForKey:@"InstallmentAmount"];
    self.interestRate = [aDictionary objectForKey:@"InterestRate"];
    self.numberOfInstallment = [aDictionary objectForKey:@"Number_of_Installment"];
    self.totalAmout = [aDictionary objectForKey:@"TotalAmout"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    [dictionary setObject:[NSNumber numberWithBool:self.featured] forKey:@"featured"];

    if (self.installmentAmount) {
        [dictionary setObject:self.installmentAmount forKey:@"installmentAmount"];
    }

    if (self.interestRate) {
        [dictionary setObject:self.interestRate forKey:@"interestRate"];
    }

    if (self.numberOfInstallment) {
        [dictionary setObject:self.numberOfInstallment forKey:@"numberOfInstallment"];
    }

    if (self.totalAmout) {
        [dictionary setObject:self.totalAmout forKey:@"totalAmout"];
    }

    return dictionary;

}


@end
