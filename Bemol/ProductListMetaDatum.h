#import <Foundation/Foundation.h>

@interface ProductListMetaDatum : NSObject {

    NSString *metaData;
    NSString *metaKey;

}

@property (nonatomic, copy) NSString *metaData;
@property (nonatomic, copy) NSString *metaKey;

+ (ProductListMetaDatum *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
