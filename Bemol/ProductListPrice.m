#import "ProductListPrice.h"

@implementation ProductListPrice

@synthesize priceDescription;
@synthesize priceUsage;
@synthesize priceValue;

+ (ProductListPrice *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ProductListPrice *instance = [[ProductListPrice alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.priceDescription = [aDictionary objectForKey:@"priceDescription"];
    self.priceUsage = [aDictionary objectForKey:@"priceUsage"];
    self.priceValue = [aDictionary objectForKey:@"priceValue"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.priceDescription) {
        [dictionary setObject:self.priceDescription forKey:@"priceDescription"];
    }

    if (self.priceUsage) {
        [dictionary setObject:self.priceUsage forKey:@"priceUsage"];
    }

    if (self.priceValue) {
        [dictionary setObject:self.priceValue forKey:@"priceValue"];
    }

    return dictionary;

}


@end
