//
//  BaseMarketingSpotActivityDatum.m
//  
//
//  Created by fahim on 07/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "BaseMarketingSpotActivityDatum.h"

#import "MarketingContentDescription.h"
#import "Property.h"

@implementation BaseMarketingSpotActivityDatum

@synthesize activityFormat;
@synthesize activityIdentifierID;
@synthesize activityIdentifierName;
@synthesize activityPriority;
@synthesize baseMarketingSpotActivityID;
@synthesize baseMarketingSpotActivityName;
@synthesize baseMarketingSpotDataType;
@synthesize contentFormatId;
@synthesize contentFormatName;
@synthesize contentId;
@synthesize contentName;
@synthesize contentStoreId;
@synthesize contentUrl;
@synthesize marketingContentDescription;
@synthesize properties;

+ (BaseMarketingSpotActivityDatum *)instanceFromDictionary:(NSDictionary *)aDictionary {

    BaseMarketingSpotActivityDatum *instance = [[BaseMarketingSpotActivityDatum alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.activityFormat = [aDictionary objectForKey:@"activityFormat"];
    self.activityIdentifierID = [aDictionary objectForKey:@"activityIdentifierID"];
    self.activityIdentifierName = [aDictionary objectForKey:@"activityIdentifierName"];
    self.activityPriority = [aDictionary objectForKey:@"activityPriority"];
    self.baseMarketingSpotActivityID = [aDictionary objectForKey:@"baseMarketingSpotActivityID"];
    self.baseMarketingSpotActivityName = [aDictionary objectForKey:@"baseMarketingSpotActivityName"];
    self.baseMarketingSpotDataType = [aDictionary objectForKey:@"baseMarketingSpotDataType"];
    self.contentFormatId = [aDictionary objectForKey:@"contentFormatId"];
    self.contentFormatName = [aDictionary objectForKey:@"contentFormatName"];
    self.contentId = [aDictionary objectForKey:@"contentId"];
    self.contentName = [aDictionary objectForKey:@"contentName"];
    self.contentStoreId = [aDictionary objectForKey:@"contentStoreId"];
    self.contentUrl = [aDictionary objectForKey:@"contentUrl"];

    NSArray *receivedMarketingContentDescription = [aDictionary objectForKey:@"marketingContentDescription"];
    if ([receivedMarketingContentDescription isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedMarketingContentDescription = [NSMutableArray arrayWithCapacity:[receivedMarketingContentDescription count]];
        for (NSDictionary *item in receivedMarketingContentDescription) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedMarketingContentDescription addObject:[MarketingContentDescription instanceFromDictionary:item]];
            }
        }

        self.marketingContentDescription = populatedMarketingContentDescription;

    }

    NSArray *receivedProperties = [aDictionary objectForKey:@"properties"];
    if ([receivedProperties isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedProperties = [NSMutableArray arrayWithCapacity:[receivedProperties count]];
        for (NSDictionary *item in receivedProperties) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedProperties addObject:[Property instanceFromDictionary:item]];
            }
        }

        self.properties = populatedProperties;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.activityFormat) {
        [dictionary setObject:self.activityFormat forKey:@"activityFormat"];
    }

    if (self.activityIdentifierID) {
        [dictionary setObject:self.activityIdentifierID forKey:@"activityIdentifierID"];
    }

    if (self.activityIdentifierName) {
        [dictionary setObject:self.activityIdentifierName forKey:@"activityIdentifierName"];
    }

    if (self.activityPriority) {
        [dictionary setObject:self.activityPriority forKey:@"activityPriority"];
    }

    if (self.baseMarketingSpotActivityID) {
        [dictionary setObject:self.baseMarketingSpotActivityID forKey:@"baseMarketingSpotActivityID"];
    }

    if (self.baseMarketingSpotActivityName) {
        [dictionary setObject:self.baseMarketingSpotActivityName forKey:@"baseMarketingSpotActivityName"];
    }

    if (self.baseMarketingSpotDataType) {
        [dictionary setObject:self.baseMarketingSpotDataType forKey:@"baseMarketingSpotDataType"];
    }

    if (self.contentFormatId) {
        [dictionary setObject:self.contentFormatId forKey:@"contentFormatId"];
    }

    if (self.contentFormatName) {
        [dictionary setObject:self.contentFormatName forKey:@"contentFormatName"];
    }

    if (self.contentId) {
        [dictionary setObject:self.contentId forKey:@"contentId"];
    }

    if (self.contentName) {
        [dictionary setObject:self.contentName forKey:@"contentName"];
    }

    if (self.contentStoreId) {
        [dictionary setObject:self.contentStoreId forKey:@"contentStoreId"];
    }

    if (self.contentUrl) {
        [dictionary setObject:self.contentUrl forKey:@"contentUrl"];
    }

    if (self.marketingContentDescription) {
        [dictionary setObject:self.marketingContentDescription forKey:@"marketingContentDescription"];
    }

    if (self.properties) {
        [dictionary setObject:self.properties forKey:@"properties"];
    }

    return dictionary;

}

@end
