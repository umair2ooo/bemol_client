#import "NotaFiscal.h"

#import "OrderItemList.h"

@implementation NotaFiscal

@synthesize bairro;
@synthesize cep;
@synthesize cidade;
@synthesize endereco;
@synthesize itemList;
@synthesize notaFiscalId;
@synthesize sendDanfeEmail;
@synthesize valorDaNotaFiscal;

+ (NotaFiscal *)instanceFromDictionary:(NSDictionary *)aDictionary {

    NotaFiscal *instance = [[NotaFiscal alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.bairro = [aDictionary objectForKey:@"bairro"];
    self.cep = [aDictionary objectForKey:@"cep"];
    self.cidade = [aDictionary objectForKey:@"cidade"];
    self.endereco = [aDictionary objectForKey:@"endereco"];

    NSArray *receivedItemList = [aDictionary objectForKey:@"itemList"];
    if ([receivedItemList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedItemList = [NSMutableArray arrayWithCapacity:[receivedItemList count]];
        for (NSDictionary *item in receivedItemList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedItemList addObject:[OrderItemList instanceFromDictionary:item]];
            }
        }

        self.itemList = populatedItemList;

    }
    self.notaFiscalId = [aDictionary objectForKey:@"notaFiscalId"];
    self.sendDanfeEmail = [aDictionary objectForKey:@"sendDanfeEmail"];
    self.valorDaNotaFiscal = [aDictionary objectForKey:@"valorDaNotaFiscal"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.bairro) {
        [dictionary setObject:self.bairro forKey:@"bairro"];
    }

    if (self.cep) {
        [dictionary setObject:self.cep forKey:@"cep"];
    }

    if (self.cidade) {
        [dictionary setObject:self.cidade forKey:@"cidade"];
    }

    if (self.endereco) {
        [dictionary setObject:self.endereco forKey:@"endereco"];
    }

    if (self.itemList) {
        [dictionary setObject:self.itemList forKey:@"itemList"];
    }

    if (self.notaFiscalId) {
        [dictionary setObject:self.notaFiscalId forKey:@"notaFiscalId"];
    }

    if (self.sendDanfeEmail) {
        [dictionary setObject:self.sendDanfeEmail forKey:@"sendDanfeEmail"];
    }

    if (self.valorDaNotaFiscal) {
        [dictionary setObject:self.valorDaNotaFiscal forKey:@"valorDaNotaFiscal"];
    }

    return dictionary;

}


@end
