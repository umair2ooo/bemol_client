#import <UIKit/UIKit.h>

@interface Cell_outros : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label_totalAmount;
@property(weak,nonatomic) IBOutlet UILabel *label_installmentAmount;
@property(weak,nonatomic) IBOutlet UILabel *label_numberOfInstallment;
@end