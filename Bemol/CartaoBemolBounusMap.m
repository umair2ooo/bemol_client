#import "CartaoBemolBounusMap.h"

@implementation CartaoBemolBounusMap

@synthesize historico;
@synthesize pedra;
@synthesize pontos;
@synthesize R;

+ (CartaoBemolBounusMap *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CartaoBemolBounusMap *instance = [[CartaoBemolBounusMap alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.historico = [aDictionary objectForKey:@"Historico"];
    self.pedra = [aDictionary objectForKey:@"Pedra"];
    self.pontos = [aDictionary objectForKey:@"pontos"];
    self.R = [aDictionary objectForKey:@"R$"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.historico) {
        [dictionary setObject:self.historico forKey:@"historico"];
    }

    if (self.pedra) {
        [dictionary setObject:self.pedra forKey:@"pedra"];
    }

    if (self.pontos) {
        [dictionary setObject:self.pontos forKey:@"pontos"];
    }

    if (self.R) {
        [dictionary setObject:self.R forKey:@"R"];
    }

    return dictionary;

}


@end
