//
//  VC_bonusBemol.m
//  Bemol
//
//  Created by Fahim Bilwani on 14/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_bonusBemol.h"
#import "CartaoBemol.h"
#import "CartaoBemolBounusMap.h"

@interface VC_bonusBemol ()
@property (weak, nonatomic) IBOutlet UILabel *label_points;
@property (weak, nonatomic) IBOutlet UILabel *label_faltam;
@property (weak, nonatomic) IBOutlet UILabel *label_bounusatal;
@property (weak, nonatomic) IBOutlet UILabel *label_emReais;

@property(nonatomic, strong) CartaoBemol *obj_CartaoBemol;
@property(nonatomic, strong) CartaoBemolBounusMap *obj_CartaoBemolBounusMap;

@end

@implementation VC_bonusBemol

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Bônus Bemol"];
    
    if (ApplicationDelegate.single.obj_CartaoBemol)
    {
        self.obj_CartaoBemolBounusMap = ApplicationDelegate.single.obj_CartaoBemol.bounusMap;
        self.label_points.text = [NSString stringWithFormat:@"Bônus Bemol: %@ ponotos",self.obj_CartaoBemolBounusMap.historico];
        self.label_faltam.text = [NSString stringWithFormat:@"Faltam %@ pontos para o próximo nível",self.obj_CartaoBemolBounusMap.pedra];
        self.label_bounusatal.text = [NSString stringWithFormat:@"%@ pontos",self.obj_CartaoBemolBounusMap.pontos];
        self.label_emReais.text = [NSString stringWithFormat:@"R$ %@",self.obj_CartaoBemolBounusMap.R];
        
        [[NSUserDefaults standardUserDefaults] setObject:self.obj_CartaoBemolBounusMap.pontos forKey:k_points];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        [[ServiceModel sharedClient] GET:k_cartaoBemol
                              parameters:nil
                                  onView:self.view
                                 success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             if (![responseObject valueForKey:@"errors"])
             {
                 ApplicationDelegate.single.obj_CartaoBemol = [CartaoBemol instanceFromDictionary:responseObject];
                 self.obj_CartaoBemolBounusMap = ApplicationDelegate.single.obj_CartaoBemol.bounusMap;
                 self.label_points.text = [NSString stringWithFormat:@"Bônus Bemol: %@ ponotos",self.obj_CartaoBemolBounusMap.historico];
                 self.label_faltam.text = [NSString stringWithFormat:@"Faltam %@ pontos para o proximo nivel",self.obj_CartaoBemolBounusMap.pedra];
                 self.label_bounusatal.text = [NSString stringWithFormat:@"%@ pontos",self.obj_CartaoBemolBounusMap.pontos];
                 self.label_emReais.text = [NSString stringWithFormat:@"R$ %@",self.obj_CartaoBemolBounusMap.R];
                 
                 [[NSUserDefaults standardUserDefaults] setObject:self.obj_CartaoBemolBounusMap.pontos forKey:k_points];
                 [[NSUserDefaults standardUserDefaults] synchronize];
             }
             else
             {
                 [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                             valueForKey:@"errorMessage"]];
             }
         }
                                 failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             DLog(@"%@",error.description);
             //[UtilitiesHelper showAlert:error.description];
             [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
         }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
