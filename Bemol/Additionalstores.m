#import "Additionalstores.h"

#import "AdditionalstoresAdditionalStoreMap.h"

@implementation Additionalstores

@synthesize additionalStoreMap;

+ (Additionalstores *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Additionalstores *instance = [[Additionalstores alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.additionalStoreMap = [AdditionalstoresAdditionalStoreMap instanceFromDictionary:[aDictionary objectForKey:@"additionalStoreMap"]];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.additionalStoreMap) {
        [dictionary setObject:self.additionalStoreMap forKey:@"additionalStoreMap"];
    }

    return dictionary;

}


@end
