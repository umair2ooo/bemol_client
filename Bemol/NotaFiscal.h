#import <Foundation/Foundation.h>

@interface NotaFiscal : NSObject {

    NSString *bairro;
    NSString *cep;
    NSString *cidade;
    NSString *endereco;
    NSMutableArray *itemList;
    NSString *notaFiscalId;
    NSString *sendDanfeEmail;
    NSNumber *valorDaNotaFiscal;

}

@property (nonatomic, copy) NSString *bairro;
@property (nonatomic, copy) NSString *cep;
@property (nonatomic, copy) NSString *cidade;
@property (nonatomic, copy) NSString *endereco;
@property (nonatomic, copy) NSMutableArray *itemList;
@property (nonatomic, copy) NSString *notaFiscalId;
@property (nonatomic, copy) NSString *sendDanfeEmail;
@property (nonatomic, copy) NSNumber *valorDaNotaFiscal;

+ (NotaFiscal *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
