#import "VC_Category.h"
#import "cell_Category.h"
#import "VC_SubCategory.h"

@interface VC_Category ()
{
    NSMutableArray *array_categories;
}

@property(nonatomic, strong)Categories *category;

@property (weak, nonatomic) IBOutlet UITableView *tableview_;

- (IBAction)action_back:(id)sender;

@end

@implementation VC_Category

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:@"Departamentos"];
    
    array_categories = [[NSMutableArray alloc] init];
    
    [[ServiceModel sharedClient] GET:k_topCategory
                          parameters:nil
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
                {
                    [[responseObject valueForKey:@"CatalogGroupView"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
                    {
                        Categories *obj_ = [[Categories alloc] init];
                        obj_.identifier = [obj valueForKey:@"identifier"];
                        obj_.name = [obj valueForKey:@"name"];
                        obj_.productsURL = [obj valueForKey:@"productsURL"];
                        obj_.resourceId = [obj valueForKey:@"resourceId"];
                        obj_.uniqueID = [obj valueForKey:@"uniqueID"];
                        obj_.shortDescription = [obj valueForKey:@"shortDescription"];
                        
                        [array_categories addObject:obj_];
                    }];
//                    if ([array_categories count])
//                    {
//                        [array_categories removeObjectAtIndex:1];
//                    }
                    [self.tableview_ reloadData];
                }
                            failure:^(AFHTTPRequestOperation *operation, NSError *error)
                {
                    DLog(@"%@",error.description);
                    //[UtilitiesHelper showAlert:error.description];
                    [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
                }];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //ApplicationDelegate.single.delegate_ = self;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //ApplicationDelegate.single.delegate_ = nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - showLoader
-(void)showLoader
{
    //[SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
}
#pragma mark - tableview dataSource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array_categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_Category *cell = (cell_Category *)[tableView dequeueReusableCellWithIdentifier:@"cell_Category"];
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_Category" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    self.category = [array_categories objectAtIndex:indexPath.row];
    [cell method_setValues:[NSDictionary dictionaryWithObjectsAndKeys:self.category.name,@"name",
                            [NSString stringWithFormat:@"%ld",(long)indexPath.row],@"imageIndex",self.category.thumbnail,@"thumbnail",nil]];
    
    DLog(@"%ld", (long)indexPath.row);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.category = [array_categories objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    VC_SubCategory *objSubCategory = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_SubCategory"];
    objSubCategory.array_categories = [[NSMutableArray alloc] init];
    
    [[ServiceModel sharedClient]GET:[NSString stringWithFormat:@"%@%@",k_subCategory,self.category.uniqueID] parameters:nil onView:self.view success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [[responseObject valueForKey:@"CatalogGroupView"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
          {
              Categories *obj_ = [[Categories alloc] init];
              obj_.identifier = [obj valueForKey:@"identifier"];
              obj_.name = [obj valueForKey:@"name"];
              obj_.productsURL = [obj valueForKey:@"productsURL"];
              obj_.resourceId = [obj valueForKey:@"resourceId"];
              obj_.uniqueID = [obj valueForKey:@"uniqueID"];
              obj_.shortDescription = [obj valueForKey:@"shortDescription"];
        
              [objSubCategory.array_categories addObject:obj_];
          }];
         [self performSegueWithIdentifier:@"segue_SubCategory" sender:objSubCategory.array_categories];
     }
                            failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error.description);
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];

}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_SubCategory"])
    {
        VC_SubCategory *obj = segue.destinationViewController;
        obj.array_categories = [NSMutableArray arrayWithArray:(NSMutableArray *)sender];
        obj.string_categoryName = self.category.name;
    }
}

#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
