#import "OrderItem.h"

#import "UsableShippingChargePolicy.h"

@implementation OrderItem

@synthesize contractId;
@synthesize correlationGroup;
@synthesize createDate;
@synthesize currency;
@synthesize freeGift;
@synthesize fulfillmentCenterId;
@synthesize fulfillmentCenterName;
@synthesize lastUpdateDate;
@synthesize offerID;
@synthesize orderItemFulfillmentStatus;
@synthesize orderItemId;
@synthesize orderItemInventoryStatus;
@synthesize orderItemPrice;
@synthesize orderItemStatus;
@synthesize partNumber;
@synthesize productId;
@synthesize productUrl;
@synthesize quantity;
@synthesize salesTax;
@synthesize salesTaxCurrency;
@synthesize shippingCharge;
@synthesize shippingChargeCurrency;
@synthesize shippingTax;
@synthesize shippingTaxCurrency;
@synthesize unitPrice;
@synthesize unitQuantity;
@synthesize unitUom;
@synthesize uOM;
@synthesize usableShippingChargePolicy;
@synthesize productShortDescription;
@synthesize productThumbnail;

+ (OrderItem *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OrderItem *instance = [[OrderItem alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.contractId = [aDictionary objectForKey:@"contractId"];
    self.correlationGroup = [aDictionary objectForKey:@"correlationGroup"];
    self.createDate = [aDictionary objectForKey:@"createDate"];
    self.currency = [aDictionary objectForKey:@"currency"];
    self.freeGift = [aDictionary objectForKey:@"freeGift"];
    self.fulfillmentCenterId = [aDictionary objectForKey:@"fulfillmentCenterId"];
    self.fulfillmentCenterName = [aDictionary objectForKey:@"fulfillmentCenterName"];
    self.lastUpdateDate = [aDictionary objectForKey:@"lastUpdateDate"];
    self.offerID = [aDictionary objectForKey:@"offerID"];
    self.orderItemFulfillmentStatus = [aDictionary objectForKey:@"orderItemFulfillmentStatus"];
    self.orderItemId = [aDictionary objectForKey:@"orderItemId"];
    self.orderItemInventoryStatus = [aDictionary objectForKey:@"orderItemInventoryStatus"];
    self.orderItemPrice = [aDictionary objectForKey:@"orderItemPrice"];
    self.orderItemStatus = [aDictionary objectForKey:@"orderItemStatus"];
    self.partNumber = [aDictionary objectForKey:@"partNumber"];
    self.productId = [aDictionary objectForKey:@"productId"];
    self.productUrl = [aDictionary objectForKey:@"productUrl"];
    self.quantity = [aDictionary objectForKey:@"quantity"];
    self.salesTax = [aDictionary objectForKey:@"salesTax"];
    self.salesTaxCurrency = [aDictionary objectForKey:@"salesTaxCurrency"];
    self.shippingCharge = [aDictionary objectForKey:@"shippingCharge"];
    self.shippingChargeCurrency = [aDictionary objectForKey:@"shippingChargeCurrency"];
    self.shippingTax = [aDictionary objectForKey:@"shippingTax"];
    self.shippingTaxCurrency = [aDictionary objectForKey:@"shippingTaxCurrency"];
    self.unitPrice = [aDictionary objectForKey:@"unitPrice"];
    self.productThumbnail = [aDictionary objectForKey:@"productThumbnail"];
    self.productShortDescription = [aDictionary objectForKey:@"productShortDescription"];
    self.unitQuantity = [aDictionary objectForKey:@"unitQuantity"];
    self.unitUom = [aDictionary objectForKey:@"unitUom"];
    self.uOM = [aDictionary objectForKey:@"UOM"];

    NSArray *receivedUsableShippingChargePolicy = [aDictionary objectForKey:@"usableShippingChargePolicy"];
    if ([receivedUsableShippingChargePolicy isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedUsableShippingChargePolicy = [NSMutableArray arrayWithCapacity:[receivedUsableShippingChargePolicy count]];
        for (NSDictionary *item in receivedUsableShippingChargePolicy) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedUsableShippingChargePolicy addObject:[UsableShippingChargePolicy instanceFromDictionary:item]];
            }
        }

        self.usableShippingChargePolicy = populatedUsableShippingChargePolicy;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.contractId) {
        [dictionary setObject:self.contractId forKey:@"contractId"];
    }

    if (self.correlationGroup) {
        [dictionary setObject:self.correlationGroup forKey:@"correlationGroup"];
    }

    if (self.createDate) {
        [dictionary setObject:self.createDate forKey:@"createDate"];
    }

    if (self.currency) {
        [dictionary setObject:self.currency forKey:@"currency"];
    }

    if (self.freeGift) {
        [dictionary setObject:self.freeGift forKey:@"freeGift"];
    }

    if (self.fulfillmentCenterId) {
        [dictionary setObject:self.fulfillmentCenterId forKey:@"fulfillmentCenterId"];
    }

    if (self.fulfillmentCenterName) {
        [dictionary setObject:self.fulfillmentCenterName forKey:@"fulfillmentCenterName"];
    }

    if (self.lastUpdateDate) {
        [dictionary setObject:self.lastUpdateDate forKey:@"lastUpdateDate"];
    }

    if (self.offerID) {
        [dictionary setObject:self.offerID forKey:@"offerID"];
    }

    if (self.orderItemFulfillmentStatus) {
        [dictionary setObject:self.orderItemFulfillmentStatus forKey:@"orderItemFulfillmentStatus"];
    }

    if (self.orderItemId) {
        [dictionary setObject:self.orderItemId forKey:@"orderItemId"];
    }

    if (self.orderItemInventoryStatus) {
        [dictionary setObject:self.orderItemInventoryStatus forKey:@"orderItemInventoryStatus"];
    }

    if (self.orderItemPrice) {
        [dictionary setObject:self.orderItemPrice forKey:@"orderItemPrice"];
    }

    if (self.orderItemStatus) {
        [dictionary setObject:self.orderItemStatus forKey:@"orderItemStatus"];
    }

    if (self.partNumber) {
        [dictionary setObject:self.partNumber forKey:@"partNumber"];
    }

    if (self.productId) {
        [dictionary setObject:self.productId forKey:@"productId"];
    }

    if (self.productUrl) {
        [dictionary setObject:self.productUrl forKey:@"productUrl"];
    }

    if (self.quantity) {
        [dictionary setObject:self.quantity forKey:@"quantity"];
    }

    if (self.salesTax) {
        [dictionary setObject:self.salesTax forKey:@"salesTax"];
    }

    if (self.salesTaxCurrency) {
        [dictionary setObject:self.salesTaxCurrency forKey:@"salesTaxCurrency"];
    }

    if (self.shippingCharge) {
        [dictionary setObject:self.shippingCharge forKey:@"shippingCharge"];
    }

    if (self.shippingChargeCurrency) {
        [dictionary setObject:self.shippingChargeCurrency forKey:@"shippingChargeCurrency"];
    }

    if (self.shippingTax) {
        [dictionary setObject:self.shippingTax forKey:@"shippingTax"];
    }

    if (self.shippingTaxCurrency) {
        [dictionary setObject:self.shippingTaxCurrency forKey:@"shippingTaxCurrency"];
    }

    if (self.unitPrice) {
        [dictionary setObject:self.unitPrice forKey:@"unitPrice"];
    }
    
    if (self.productThumbnail) {
        [dictionary setObject:self.productThumbnail forKey:@"productThumbnail"];
    }
    
    if (self.productShortDescription) {
        [dictionary setObject:self.productShortDescription forKey:@"productShortDescription"];
    }

    if (self.unitQuantity) {
        [dictionary setObject:self.unitQuantity forKey:@"unitQuantity"];
    }

    if (self.unitUom) {
        [dictionary setObject:self.unitUom forKey:@"unitUom"];
    }

    if (self.uOM) {
        [dictionary setObject:self.uOM forKey:@"uOM"];
    }

    if (self.usableShippingChargePolicy) {
        [dictionary setObject:self.usableShippingChargePolicy forKey:@"usableShippingChargePolicy"];
    }

    return dictionary;

}

@end
