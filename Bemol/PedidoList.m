#import "PedidoList.h"

#import "List.h"

@implementation PedidoList

@synthesize countTotal;
@synthesize list;

+ (PedidoList *)instanceFromDictionary:(NSDictionary *)aDictionary {

    PedidoList *instance = [[PedidoList alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.countTotal = [aDictionary objectForKey:@"countTotal"];

    NSArray *receivedList = [aDictionary objectForKey:@"list"];
    if ([receivedList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedList = [NSMutableArray arrayWithCapacity:[receivedList count]];
        for (NSDictionary *item in receivedList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedList addObject:[List instanceFromDictionary:item]];
            }
        }

        self.list = populatedList;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.countTotal) {
        [dictionary setObject:self.countTotal forKey:@"countTotal"];
    }

    if (self.list) {
        [dictionary setObject:self.list forKey:@"list"];
    }

    return dictionary;

}


@end
