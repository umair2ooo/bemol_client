#import "scroller_home.h"


#define k_time 5.0
#define k_imageName @"categorypopupicon1"
//#define k_numberOfPage 5



@implementation scroller_home

@synthesize delegate_ = _delegate_;

-(void)method_getDataFromServer
{
    [[ServiceModel sharedClient]GET:k_homeBannerOne parameters:nil onView:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         if (array_scrollerOneBanners) {
             [array_scrollerOneBanners removeAllObjects];
         }
         else{
             array_scrollerOneBanners = [NSMutableArray new];
         }
         
         
         self.obj_homeScreenBannersOne = [HomeScreenBanners instanceFromDictionary:responseObject];
         
         DLog(@"%@", self.obj_homeScreenBannersOne);
         
         self.obj_marketingSpotDatum = [self.obj_homeScreenBannersOne.marketingSpotData objectAtIndex:0];
         
         
         [self.obj_marketingSpotDatum.baseMarketingSpotActivityData enumerateObjectsUsingBlock:^(BaseMarketingSpotActivityDatum *obj, NSUInteger idx, BOOL *stop)
          {
              self.obj_marketingContentDescription = [obj.marketingContentDescription objectAtIndex:0];
              
              
              NSString *string_temp = (NSString *)self.obj_marketingContentDescription.maketingText;
              string_temp = [[string_temp componentsSeparatedByString:@" alt"] objectAtIndex:0];
              if ([string_temp containsString:@"src=/"])
              {
                  string_temp = [[string_temp componentsSeparatedByString:@"src="] objectAtIndex:1];
              }
              else
              {
                  string_temp = [[string_temp componentsSeparatedByString:@"src=\""] objectAtIndex:1];
              }
              
              
              self.obj_marketingContentDescription.maketingText = string_temp;
              
              DLog(@"%@", self.obj_marketingContentDescription.maketingText);
              
              [array_scrollerOneBanners addObject:obj];
          }];
         
         [self method_setupScroller];
         
     }
                            failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error.description);
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super initWithCoder:aDecoder])
    {
    }
    
    return self;
}


#pragma mark - setup scroller
-(void)method_setupScroller
{
    self.pagingEnabled = YES;
    
    self.delegate = self;
    
    float float_x = 0.0;
    
    for (unsigned i = 0; i<[array_scrollerOneBanners count]; i++)
    {
        UIView *view_temp = [[UIView alloc] initWithFrame:CGRectMake(float_x,
                                                                     0,
                                                                     self.frame.size.width,
                                                                     self.frame.size.height)];
        
        
        UIImageView *imageView_temp = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                                    0,
                                                                                    view_temp.frame.size.width,
                                                                                    view_temp.frame.size.height)];
        imageView_temp.contentMode = UIViewContentModeScaleAspectFit;
        
        NSString *string_temp = [NSString stringWithFormat:@"%@", k_placeHolder];

        self.obj_baseMarketingSpotActivityDatum = [array_scrollerOneBanners objectAtIndex:i];
        
        self.obj_marketingContentDescription = [self.obj_baseMarketingSpotActivityDatum.marketingContentDescription objectAtIndex:0];
        
        NSString *imageUrl = [[NSString stringWithFormat:@"%@%@", k_url, self.obj_marketingContentDescription.maketingText] stringByReplacingOccurrencesOfString:@" " withString:@""];
        imageUrl = [imageUrl stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        [imageView_temp sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                          placeholderImage:[UIImage imageNamed:string_temp]];
        
        DLog(@"%@", [NSString stringWithFormat:@"%@%@", k_url, self.obj_marketingContentDescription.maketingText]);
        
        [view_temp addSubview:imageView_temp];
        
        string_temp = nil;
        
        
        UIButton *button_ = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button_ setBackgroundImage:[UIImage imageNamed:@"front-btn"] forState:UIControlStateNormal];

        if (IS_IPHONE_4_OR_LESS)
        {
            [button_ setFrame:CGRectMake(20, 180, 150, 40)];
        }
        else if (IS_IPHONE_5)
        {
            [button_ setFrame:CGRectMake(20, 200, 150, 40)];
        }
        else if (IS_IPHONE_6P)
        {
            [button_ setFrame:CGRectMake(20, 250, 170, 40)];
        }
        
        
        
        [button_ setTintAdjustmentMode:UIViewTintAdjustmentModeDimmed];
        [button_ setReversesTitleShadowWhenHighlighted:YES];
        button_.titleLabel.adjustsFontSizeToFitWidth = YES;
        [button_ setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

        
        switch (i)
        {
            case 1:
                [button_ setTitle:@"QR Code" forState:UIControlStateNormal];
//                [button_ setBackgroundImage:[UIImage imageNamed:@"hmbarcode"] forState:UIControlStateNormal];
                [button_ addTarget:self action:@selector(method_barCode) forControlEvents:UIControlEventTouchUpInside];
                [view_temp addSubview:button_];
                break;
            case 2:
                [button_ setTitle:@"Store Locator" forState:UIControlStateNormal];
//                [button_ setBackgroundImage:[UIImage imageNamed:@"hmLocateStore"] forState:UIControlStateNormal];
                [button_ addTarget:self action:@selector(method_locateStore) forControlEvents:UIControlEventTouchUpInside];
                [view_temp addSubview:button_];
                break;
            case 3:
                [button_ setTitle:@"Loyalty Points" forState:UIControlStateNormal];
//                [button_ setBackgroundImage:[UIImage imageNamed:@"hmloypoints"] forState:UIControlStateNormal];
                [button_ addTarget:self action:@selector(method_loyalityPoints) forControlEvents:UIControlEventTouchUpInside];
                [view_temp addSubview:button_];
                break;
            case 4:
                [button_ setTitle:@"Catalogue" forState:UIControlStateNormal];
//                [button_ setBackgroundImage:[UIImage imageNamed:@"hmcatalog"] forState:UIControlStateNormal];
                [button_ addTarget:self action:@selector(method_catalog) forControlEvents:UIControlEventTouchUpInside];
                [view_temp addSubview:button_];
                break;
                
            default:
                break;
        }




        
        [self addSubview:view_temp];
        
        float_x += view_temp.frame.size.width;
        
        
    }
    
//    UIView *view_upperLayer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, float_x, self.frame.size.height)];
//    [view_upperLayer setBackgroundColor:[UIColor blackColor]];
//    view_upperLayer.alpha = 0.0;
//    view_upperLayer.tag = 111;
//    [self addSubview: view_upperLayer];
    
    
    [self setContentSize:CGSizeMake(float_x, 0)];

//    [self method_setupTimer];
}


#pragma mark - Timer
-(void)method_setupTimer
{
    if (![timer_ isValid])
    {
        timer_ = [NSTimer scheduledTimerWithTimeInterval:k_time
                                                  target:self
                                                selector:@selector(method_scrollToNext:)
                                                userInfo:nil
                                                 repeats:YES];
    }
}


-(void)method_cancelTimer
{
    if ([timer_ isValid])
    {
        [timer_ invalidate];
        timer_ = nil;
    }
}


#pragma mark - view alpha variation
-(void)method_setViewColor:(float)alpha_
{
    UIView *view_upperLayer = [self viewWithTag:111];
    view_upperLayer.alpha = alpha_;
    
    
//    view_upperLayer.backgroundColor = [color_ colorWithAlphaComponent:0.6];
}



#pragma mark - scroller delegates and methods
-(void)method_scrollToNext:(NSTimer *)timer
{
    [self method_setViewColor:0.6];
    
    CGFloat width = self.frame.size.width;
    NSInteger page = (self.contentOffset.x + (0.5f * width)) / width;

    
    CGRect frame = self.frame;
    frame.origin.x = frame.size.width * page+1;
    frame.origin.y = 0;

    
//    if (page == 0 || (page <= k_numberOfPage-2))
//    {
//        page = page + 1;
//    }
//    else if(page == k_numberOfPage-1)
//    {
//        page = 0;
//    }

    
    
    if (page == 0 || (page <= [array_scrollerOneBanners count]-2))
    {
        page = page + 1;
    }
    else if(page == [array_scrollerOneBanners count]-1)
    {
        page = 0;
    }
    
    
    
    [self setContentOffset:CGPointMake(self.frame.size.width*page, 0) animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView                                               // any offset changes
{
//    [self method_cancelTimer];
}



// called on start of dragging (may require some time and or distance to move)
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    DLog(@"scrollViewWillBeginDragging");
    
    
    [self method_cancelTimer];
    
    [self method_setViewColor:0.0];
}

// called on finger up if the user dragged. velocity is in points/millisecond. targetContentOffset may be changed to adjust where the scroll view comes to rest

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset NS_AVAILABLE_IOS(5_0)
{
    DLog(@"scrollViewWillEndDragging");
    
    [self method_setupTimer];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    DLog(@"scrollViewDidEndDragging");
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView   // called on finger up as we are moving
{
    DLog(@"scrollViewWillBeginDecelerating");
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView      // called when scroll view grinds to a halt
{
    DLog(@"scrollViewDidEndDecelerating");
}


- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView // called when setContentOffset/scrollRectVisible:animated: finishes. not called if not animating
{
    DLog(@"scrollViewDidEndScrollingAnimation");
    
    [self method_setViewColor:0.0];
}



#pragma mark - scroller start stop
-(void)method_startScroller
{
    self.delegate = self;
    [self method_setupTimer];
}

-(void)method_stopScroller
{
    self.delegate = nil;
    [self method_cancelTimer];
}



#pragma mark - buttons methods
-(void)method_catalog
{
    if ([self.delegate_ respondsToSelector:@selector(method_catalog)])
    {
        [self.delegate_ method_catalog];
    }
}


-(void)method_barCode
{
    if ([self.delegate_ respondsToSelector:@selector(method_barcode)])
    {
        [self.delegate_ method_barcode];
    }
}


-(void)method_locateStore
{
    if ([self.delegate_ respondsToSelector:@selector(method_locateStore)])
    {
        [self.delegate_ method_locateStore];
    }
}


-(void)method_loyalityPoints
{
    if ([self.delegate_ respondsToSelector:@selector(method_loyalityPoints)])
    {
        [self.delegate_ method_loyalityPoints];
    }
}


////////////////////////////////////////////////////////////////////////            Following methods call when user drag

//-[scroller_home scrollViewWillBeginDragging:]
//-[scroller_home scrollViewWillEndDragging:withVelocity:targetContentOffset:]


////////////////////////////////////////////////////////////////////////            Following methods call Automaticall drag

//scrollViewDidEndScrollingAnimation

//#pragma mark - delegate_responseData
//-(void)delegate_responseData:(id)data error:(NSError *)error servicename:(NSString *)servicename
//{
//    if (data)
//    {
//        NSArray *array_temp = [[NSArray alloc] initWithArray:data];
//        
//        [array_temp enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//            self.obj_homeScreenBannersOne = obj;
//            DLog(@"%@", self.obj_homeScreenBannersOne.baseMarketingSpotActivityName);
//            DLog(@"%@", self.obj_homeScreenBannersOne.contentName);
//            DLog(@"%@", self.obj_homeScreenBannersOne.contentId);
//            DLog(@"%@", self.obj_homeScreenBannersOne.activityIdentifierID);
//            DLog(@"%@", self.obj_homeScreenBannersOne.marketingContentDescription);
//            DLog(@"%@", self.obj_homeScreenBannersOne.properties);
//        }];
//
//    }
//    else
//    {
//        [[[UIAlertView alloc] initWithTitle:nil
//                                    message:error.description
//                                   delegate:nil
//                          cancelButtonTitle:@"OK"
//                          otherButtonTitles:nil, nil] show];
//    }
//}
//asdf

#pragma mark - web service delegate
-(void)delegate_responseData:(id)data error:(NSError *)error servicename:(NSString *)servicename{
    
    //ApplicationDelegate.single.delegate_ = nil;
    
    if (data)
    {
        if ([servicename isEqualToString:k_homeScreenBannerOne])
        {
            
            NSArray *array_temp = [[NSArray alloc] initWithArray:data];
            
            self.obj_homeScreenBannersOne = [array_temp objectAtIndex:0];
            
            
//            [array_temp enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//                self.obj_homeScreenBannersOne = obj;
//                DLog(@"%@", self.obj_homeScreenBannersOne.baseMarketingSpotActivityName);
//                DLog(@"%@", self.obj_homeScreenBannersOne.contentName);
//                DLog(@"%@", [self.obj_homeScreenBannersOne.contentId class]);
//                DLog(@"%@", self.obj_homeScreenBannersOne.activityIdentifierID);
//                DLog(@"%@", self.obj_homeScreenBannersOne.marketingContentDescription);
//                DLog(@"%@", self.obj_homeScreenBannersOne.properties);
//            }];
            
            
            if (array_scrollerOneBanners) {
                [array_scrollerOneBanners removeAllObjects];
            }
            else{
                array_scrollerOneBanners = [NSMutableArray new];
            }
            
            
//            for (unsigned i = 0; i<[self.obj_homeScreenBannersOne.contentName count]; i++){
//                
//                homeScreenBanners *obj = [[homeScreenBanners alloc] init];
//
//                
//                obj.activityIdentifierID = [self.obj_homeScreenBannersOne.activityIdentifierID objectAtIndex:i];
//                obj.contentId = [self.obj_homeScreenBannersOne.contentId objectAtIndex:i];
//                obj.contentName = [self.obj_homeScreenBannersOne.contentName objectAtIndex:i];
//                obj.baseMarketingSpotActivityName = [self.obj_homeScreenBannersOne.baseMarketingSpotActivityName objectAtIndex:i];
//                obj.marketingContentDescription = [self.obj_homeScreenBannersOne.marketingContentDescription objectAtIndex:i];
//                obj.properties = [self.obj_homeScreenBannersOne.properties objectAtIndex:i];
//                
//                
//
//                NSArray *array_temp = [[self.obj_homeScreenBannersOne.marketingContentDescription valueForKey:@"maketingText"] objectAtIndex:i];
//                NSMutableString *string_temp = [array_temp objectAtIndex:0];
//                string_temp = [[string_temp componentsSeparatedByString:@" alt"] objectAtIndex:0];
//                string_temp = [[string_temp componentsSeparatedByString:@"src=/"] objectAtIndex:1];
//                
//                obj.url_imageURL = string_temp;
//                
//                array_temp = nil;
//                string_temp = nil;
//                
//                [array_scrollerOneBanners addObject:obj];
//            }
//            
//            
////            DLog(@"%@", array_scrollerOneBanners);
//            
//            self.obj_homeScreenBannersOne = [array_scrollerOneBanners objectAtIndex:2];
//            
////            DLog(@"%@", self.obj_homeScreenBannersOne.baseMarketingSpotActivityName);
////            DLog(@"%@", self.obj_homeScreenBannersOne.contentName);
////            DLog(@"%@", [self.obj_homeScreenBannersOne.contentId class]);
////            DLog(@"%@", self.obj_homeScreenBannersOne.activityIdentifierID);
////            DLog(@"%@", self.obj_homeScreenBannersOne.marketingContentDescription);
////            DLog(@"%@", self.obj_homeScreenBannersOne.url_imageURL);
//            
//            NSMutableString *string_temp = [[self.obj_homeScreenBannersOne.marketingContentDescription valueForKey:@"maketingText"] objectAtIndex:0];
//            string_temp = [[string_temp componentsSeparatedByString:@" alt"] objectAtIndex:0];
//            string_temp = [[string_temp componentsSeparatedByString:@"src=/"] objectAtIndex:1];
            
            
            [self method_setupScroller];
            
//            DLog(@"%@", self.obj_homeScreenBannersOne.properties);
        }
        else{           // home screen banner 2
        }
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:error.description
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil] show];
    }
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:k_getShopingCartNotification object:nil];
}


@end