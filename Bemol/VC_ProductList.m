#import "VC_ProductList.h"
#import "cell_ProductList.h"
#import "VC_productDetail.h"
#import "ProductListCatalogEntryView.h"
#import "ProductListPrice.h"
#import "ProductList.h"

@interface VC_ProductList ()<UIPickerViewDataSource, UIPickerViewDelegate>
{
    int int_currentPage;
    int int_total;
    int int_orderBy;
}

@property(nonatomic, strong)NSMutableArray *array_products;
@property(nonatomic, strong)NSString *string_title;

@property (weak, nonatomic) IBOutlet UITableView *tableview_;

@property (weak, nonatomic) IBOutlet UILabel *label_ProductName;
@property (weak, nonatomic) IBOutlet UILabel *label_ProductCount;
@property (weak, nonatomic) IBOutlet UIView *view_descriptionView;
@property (weak, nonatomic) IBOutlet UITextField *textfield_sorting;
@property (weak, nonatomic) IBOutlet UITextField *textField_search;

@property(nonatomic, strong)ProductListCatalogEntryView *obj_productListCatalogEntryView;
@property(nonatomic, strong)ProductListPrice *obj_productListPrice;
@property(nonatomic, strong)NSArray *array_sorting;


- (IBAction)action_Back:(id)sender;


@end

@implementation VC_ProductList


@synthesize string_title = _string_title;

#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    int_currentPage = 1;
    int_orderBy = 1;

    
//    DLog(@"%@", [self.dic_data valueForKey:@"array"]);
//    DLog(@"%@", [self.dic_data valueForKey:@"total"]);
//    DLog(@"%@", [self.dic_data valueForKey:@"title"]);
    
    self.array_products = [NSMutableArray new];
    
    [self.array_products addObjectsFromArray:[self.dic_data valueForKey:@"array"]];
    self.string_title = [self.dic_data valueForKey:@"title"];
    int_total = [[self.dic_data valueForKey:@"total"] intValue];

    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ApplicationDelegate.single setFontFamily:k_fontName
                                          forView:self.view
                                      andSubViews:YES];
        
        if (self.string_title)
            
        {
            [self.view_descriptionView setHidden:NO];
            
            self.label_ProductName.text = self.string_title;
            self.label_ProductCount.text = [NSString stringWithFormat:@"Encontramos %lu resultado(s) para \"%@\".", (unsigned long)[self.array_products count], self.string_title];
            
            [self.tableview_ setFrame:CGRectMake(self.tableview_.frame.origin.x,
                                                 self.view_descriptionView.frame.size.height,
                                                 self.tableview_.frame.size.width,
                                                 self.view.frame.size.height - self.view_descriptionView.frame.size.height)];
            [self setTitle:@"Busca de produtos"];
        }
        else
        {
            [self.view_descriptionView setHidden:YES];
            [self.navigationItem setTitle:@"Lista de produto"];
            [self.tableview_ setFrame:self.view.frame];
        }
    });
    
//    Marcas = 1
//    Nome = 2
//    Menor ao maior (R$) = 3
//    Maior ao menor (R$) = 4
//    Disponibilidade = 5
//    Mais vendidos = 6
//    Lançamentos = 7
    
    
    
    
    
//    self.array_sorting = [[NSArray alloc] initWithObjects:@"Preço",@"Nome", nil];
    
    self.array_sorting = [[NSArray alloc] initWithObjects:
                          @"Marcas",
                          @"Nome",
                          @"Menor ao maior (R$)",
                          @"Maior ao menor (R$)",
                          @"Disponibilidade",
                          @"Mais vendidos",
                          @"Lançamentos", nil];
    
    UIPickerView *pickerView =[[UIPickerView alloc]init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    [pickerView selectRow:0 inComponent:0 animated:NO];
    
    self.textfield_sorting.text = [self.array_sorting objectAtIndex:0];
    
    self.textfield_sorting.inputView = pickerView;
    
}


-(void)viewWillAppear:(BOOL)animated
{
    DLog(@"check");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segue_productDetaiviaList"])
    {
        VC_productDetail *obj = segue.destinationViewController;
        obj.string_title = [sender valueForKey:@"name"];
        obj.uniqueID = [sender valueForKey:k_uniqueID];
    }
}

#pragma mark - tableview delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_products count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 119;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_ProductList *cell = (cell_ProductList *)[tableView dequeueReusableCellWithIdentifier:@"cell_ProductList"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_ProductList" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    self.obj_productListCatalogEntryView = [self.array_products objectAtIndex:indexPath.row];
    self.obj_productListPrice = [self.obj_productListCatalogEntryView.price objectAtIndex:0];
    
    [cell method_setValues:[NSDictionary dictionaryWithObjectsAndKeys:
                            self.obj_productListCatalogEntryView.name,@"name",
                            self.obj_productListCatalogEntryView.thumbnail,@"image",
                            self.obj_productListPrice.priceValue,@"price",nil]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    uniqueID = [[self.array_products objectAtIndex:indexPath.row] valueForKey:@"uniqueID"];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"segue_productDetaiviaList" sender:[self.array_products objectAtIndex:indexPath.row]];
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    DLog(@"indexPath.row: %ld", (long)indexPath.row);
//    DLog(@"int_total: %d", int_total);
//    DLog(@"[self.array_products count]-1: %lu", [self.array_products count]-1);
    
    if (indexPath.row != int_total-1 && indexPath.row == [self.array_products count] - 1 )
    {
        [self method_loadMoreData:++int_currentPage];
    }
}




#pragma mark - action_Back
- (IBAction)action_Back:(id)sender
{
    if ([self.navigationController.viewControllers count]<=1)
    {
        // it means this is root VC
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
//    if (self.navigationController != nil)
//    {
//        
//    }
}
- (IBAction)action_cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.array_sorting count];
}


#pragma mark - UIPickerView Delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.array_sorting objectAtIndex:row];
}


- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component
{
    self.textfield_sorting.text= [self.array_sorting objectAtIndex:row];
    
    
    int_currentPage = 1;
    
    switch (row)
    {
        case 0:
            int_orderBy = 1;
            break;
        case 1:
            int_orderBy = 2;
            break;
        case 2:
            int_orderBy = 3;
            break;
        case 3:
            int_orderBy = 4;
            break;
        case 4:
            int_orderBy = 5;
            break;
        case 5:
            int_orderBy = 6;
            break;
        case 6:
            int_orderBy = 7;
            break;
            
        default:
            break;
    }
    
    self.array_products = nil;
    [self.tableview_ reloadData];
    [self method_loadMoreData:int_currentPage];
    
    

//    if ([[self.array_sorting objectAtIndex:row]isEqualToString:@"Name"])
//    {
//        self.array_products = [[NSMutableArray alloc] initWithArray:[self.array_products sortedArrayUsingComparator:^NSComparisonResult(ProductListCatalogEntryView *p1, ProductListCatalogEntryView *p2)
//                                                                     {
//                                                                         return [p1.name compare:p2.name];
//                                                                     }]
//                               ];
//        [self.tableview_ reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationTop];
//    }
//    else
//    {
//        self.array_products = [[NSMutableArray alloc] initWithArray:[self.array_products sortedArrayUsingComparator:^NSComparisonResult(ProductListCatalogEntryView *p1, ProductListCatalogEntryView *p2)
//                                                                     {
//                                                                         ProductListPrice *px = p1.price[0];
//                                                                         ProductListPrice *py = p2.price[0];
//                                                                         NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
//                                                                         [f setNumberStyle:NSNumberFormatterDecimalStyle];
//                                                                         NSNumber * pricex = [f numberFromString:px.priceValue];
//                                                                         NSNumber * pricey = [f numberFromString:py.priceValue];
//                                                                         
//                                                                         DLog(@"%@", pricex);
//                                                                         DLog(@"%@", pricey);
//                                                                         
//                                                                         if (!pricey || !pricex)
//                                                                         {
//                                                                             return true;
//                                                                         }
//                                                                         
//                                                                         return [pricex compare:pricey];
//                                                                     }]
//                               ];
//        [self.tableview_ reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationTop];
//    }
}



#pragma mark - load more data
-(void)method_loadMoreData:(NSInteger)page
{
    NSString *string_url = nil;
    
    if ([self.dic_data valueForKey:@"id"])              /// it means from sub-category
    {
        string_url = [NSString stringWithFormat:@"%@%@?pageSize=%@&pageNumber=%@&orderBy=%@",
                      k_productListService,
                      [self.dic_data valueForKey:@"id"],
                      k_pageSize,
                      [NSString stringWithFormat:@"%d", int_currentPage],
                      [NSString stringWithFormat:@"%d", int_orderBy]];
    }
    else                                                /// it means from search term
    {
        string_url = [NSString stringWithFormat:@"%@%@?searchType=1002&pageSize=%@&pageNumber=%@&orderBy=%@",
                      k_productBysearch,
                      [self.dic_data valueForKey:@"title"],
                      k_pageSize,
                      [NSString stringWithFormat:@"%d", int_currentPage],
                      [NSString stringWithFormat:@"%d", int_orderBy]];
    }


    DLog(@"%@", string_url);

    [[ServiceModel sharedClient]GET:string_url
                         parameters:nil
                             onView:self.view
                            success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             if (!self.array_products)
                 self.array_products = [NSMutableArray new];
             
             [self.array_products addObjectsFromArray:[ProductList instanceFromDictionary:responseObject].catalogEntryView];
             
             self.label_ProductName.text = [self.dic_data valueForKey:@"title"];
             self.label_ProductCount.text = [NSString stringWithFormat:@"Encontramos %lu resultado(s) para \"%@\".", (unsigned long)[self.array_products count], [self.dic_data valueForKey:@"title"]];
             
             [self.tableview_ reloadData];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                            failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error.description);
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}



#pragma mark - textField search button clicked
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.textField_search.text)
    {
        [textField resignFirstResponder];
        
        int_currentPage = 1;
        int_orderBy = 1;
        self.array_products = nil;
        self.textfield_sorting.text = [self.array_sorting objectAtIndex:0];
        
        [self.tableview_ reloadData];
        
        DLog(@"%@", [self.dic_data valueForKey:@"title"]);
        [self.dic_data setObject:self.textField_search.text forKey:@"title"];
        [self method_loadMoreData:int_currentPage];
        DLog(@"%@", [self.dic_data valueForKey:@"title"]);
    }
    return NO;
}
@end