#import "ShippingAddress.h"

@implementation ShippingAddress

@synthesize addressId;
@synthesize bairo;
@synthesize cep;
@synthesize cidade;
@synthesize endereco;
@synthesize firstName;
@synthesize nickName;
@synthesize numero;
@synthesize uf;
@synthesize complemento;

+ (ShippingAddress *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ShippingAddress *instance = [[ShippingAddress alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.addressId = [aDictionary objectForKey:@"addressId"];
    self.bairo = [aDictionary objectForKey:@"bairo"];
    self.cep = [aDictionary objectForKey:@"cep"];
    self.cidade = [aDictionary objectForKey:@"cidade"];
    self.endereco = [aDictionary objectForKey:@"endereco"];
    self.firstName = [aDictionary objectForKey:@"firstName"];
    self.nickName = [aDictionary objectForKey:@"nickName"];
    self.numero = [aDictionary objectForKey:@"numero"];
    self.uf = [aDictionary objectForKey:@"uf"];
    self.complemento = [aDictionary objectForKey:@"complemento"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.addressId) {
        [dictionary setObject:self.addressId forKey:@"addressId"];
    }

    if (self.bairo) {
        [dictionary setObject:self.bairo forKey:@"bairo"];
    }

    if (self.cep) {
        [dictionary setObject:self.cep forKey:@"cep"];
    }

    if (self.cidade) {
        [dictionary setObject:self.cidade forKey:@"cidade"];
    }

    if (self.endereco) {
        [dictionary setObject:self.endereco forKey:@"endereco"];
    }

    if (self.firstName) {
        [dictionary setObject:self.firstName forKey:@"firstName"];
    }

    if (self.nickName) {
        [dictionary setObject:self.nickName forKey:@"nickName"];
    }

    if (self.numero) {
        [dictionary setObject:self.numero forKey:@"numero"];
    }

    if (self.uf) {
        [dictionary setObject:self.uf forKey:@"uf"];
    }
    if (self.complemento) {
        [dictionary setObject:self.complemento forKey:@"complemento"];
    }

    return dictionary;

}


@end
