//
//  MarketingSpotDatum.m
//  
//
//  Created by fahim on 07/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "MarketingSpotDatum.h"

#import "BaseMarketingSpotActivityDatum.h"

@implementation MarketingSpotDatum

@synthesize baseMarketingSpotActivityData;
@synthesize eSpotName;
@synthesize marketingSpotIdentifier;

+ (MarketingSpotDatum *)instanceFromDictionary:(NSDictionary *)aDictionary {

    MarketingSpotDatum *instance = [[MarketingSpotDatum alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }


    NSArray *receivedBaseMarketingSpotActivityData = [aDictionary objectForKey:@"baseMarketingSpotActivityData"];
    if ([receivedBaseMarketingSpotActivityData isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedBaseMarketingSpotActivityData = [NSMutableArray arrayWithCapacity:[receivedBaseMarketingSpotActivityData count]];
        for (NSDictionary *item in receivedBaseMarketingSpotActivityData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedBaseMarketingSpotActivityData addObject:[BaseMarketingSpotActivityDatum instanceFromDictionary:item]];
            }
        }

        self.baseMarketingSpotActivityData = populatedBaseMarketingSpotActivityData;

    }
    self.eSpotName = [aDictionary objectForKey:@"eSpotName"];
    self.marketingSpotIdentifier = [aDictionary objectForKey:@"marketingSpotIdentifier"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.baseMarketingSpotActivityData) {
        [dictionary setObject:self.baseMarketingSpotActivityData forKey:@"baseMarketingSpotActivityData"];
    }

    if (self.eSpotName) {
        [dictionary setObject:self.eSpotName forKey:@"eSpotName"];
    }

    if (self.marketingSpotIdentifier) {
        [dictionary setObject:self.marketingSpotIdentifier forKey:@"marketingSpotIdentifier"];
    }

    return dictionary;

}

@end
