#import "ProductListCatalogEntryView.h"

#import "ProductListPrice.h"
#import "XproductReviewList.h"

@implementation ProductListCatalogEntryView

@synthesize buyable;
@synthesize fullImage;
@synthesize keyword;
@synthesize name;
@synthesize parentCategoryID;
@synthesize partNumber;
@synthesize price;
@synthesize productType;
@synthesize resourceId;
@synthesize shortDescription;
@synthesize storeID;
@synthesize thumbnail;
@synthesize uniqueID;
@synthesize xproductReviewList;

+ (ProductListCatalogEntryView *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ProductListCatalogEntryView *instance = [[ProductListCatalogEntryView alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.buyable = [aDictionary objectForKey:@"buyable"];
    self.fullImage = [aDictionary objectForKey:@"fullImage"];
    self.keyword = [aDictionary objectForKey:@"keyword"];
    self.name = [aDictionary objectForKey:@"name"];
    self.parentCategoryID = [aDictionary objectForKey:@"parentCategoryID"];
    self.partNumber = [aDictionary objectForKey:@"partNumber"];

    NSArray *receivedPrice = [aDictionary objectForKey:@"Price"];
    if ([receivedPrice isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedPrice = [NSMutableArray arrayWithCapacity:[receivedPrice count]];
        for (NSDictionary *item in receivedPrice) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedPrice addObject:[ProductListPrice instanceFromDictionary:item]];
            }
        }

        self.price = populatedPrice;

    }
    self.productType = [aDictionary objectForKey:@"productType"];
    self.resourceId = [aDictionary objectForKey:@"resourceId"];
    self.shortDescription = [aDictionary objectForKey:@"shortDescription"];
    self.storeID = [aDictionary objectForKey:@"storeID"];
    self.thumbnail = [aDictionary objectForKey:@"thumbnail"];
    self.uniqueID = [aDictionary objectForKey:@"uniqueID"];

    NSArray *receivedXproductReviewList = [aDictionary objectForKey:@"xproductReviewList"];
    if ([receivedXproductReviewList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedXproductReviewList = [NSMutableArray arrayWithCapacity:[receivedXproductReviewList count]];
        for (NSDictionary *item in receivedXproductReviewList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedXproductReviewList addObject:[XproductReviewList instanceFromDictionary:item]];
            }
        }

        self.xproductReviewList = populatedXproductReviewList;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.buyable) {
        [dictionary setObject:self.buyable forKey:@"buyable"];
    }

    if (self.fullImage) {
        [dictionary setObject:self.fullImage forKey:@"fullImage"];
    }

    if (self.keyword) {
        [dictionary setObject:self.keyword forKey:@"keyword"];
    }

    if (self.name) {
        [dictionary setObject:self.name forKey:@"name"];
    }

    if (self.parentCategoryID) {
        [dictionary setObject:self.parentCategoryID forKey:@"parentCategoryID"];
    }

    if (self.partNumber) {
        [dictionary setObject:self.partNumber forKey:@"partNumber"];
    }

    if (self.price) {
        [dictionary setObject:self.price forKey:@"price"];
    }

    if (self.productType) {
        [dictionary setObject:self.productType forKey:@"productType"];
    }

    if (self.resourceId) {
        [dictionary setObject:self.resourceId forKey:@"resourceId"];
    }

    if (self.shortDescription) {
        [dictionary setObject:self.shortDescription forKey:@"shortDescription"];
    }

    if (self.storeID) {
        [dictionary setObject:self.storeID forKey:@"storeID"];
    }

    if (self.thumbnail) {
        [dictionary setObject:self.thumbnail forKey:@"thumbnail"];
    }

    if (self.uniqueID) {
        [dictionary setObject:self.uniqueID forKey:@"uniqueID"];
    }

    if (self.xproductReviewList) {
        [dictionary setObject:self.xproductReviewList forKey:@"xproductReviewList"];
    }

    return dictionary;

}


@end
