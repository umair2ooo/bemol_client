#import <Foundation/Foundation.h>

@class findSimulateSimulateMap;

@interface FindSimulate : NSObject {

    findSimulateSimulateMap *simulateMap;

}

@property (nonatomic, strong) findSimulateSimulateMap *simulateMap;

+ (FindSimulate *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
