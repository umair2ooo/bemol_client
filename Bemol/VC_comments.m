#import "VC_comments.h"
#import "Cell_comments.h"
#import "ReviewList.h"

@interface VC_comments ()

@property(nonatomic, strong)ReviewList *obj_reviewList;

@end

@implementation VC_comments

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.string_title;
    
    DLog(@"%@",self.array_reviewList);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - tableview delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_reviewList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Cell_comments *cell = (Cell_comments *)[tableView dequeueReusableCellWithIdentifier:@"Cell_comments"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Cell_comments" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    self.obj_reviewList = [self.array_reviewList objectAtIndex:indexPath.row];
    
    [cell method_setValues:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_reviewList.reviewRate,@"rating",self.obj_reviewList.nickname,@"name",self.obj_reviewList.lastUpdated,@"date",self.obj_reviewList.comments,@"comments",self.obj_reviewList.useful,@"useful",self.obj_reviewList.notUseful,@"notuseful", nil]];
    
    return cell;
}

- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_cancel:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end