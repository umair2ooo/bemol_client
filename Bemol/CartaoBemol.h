#import <Foundation/Foundation.h>

@class CartaoBemolBounusMap;
@class CartaoBemolCartaoBemolMap;
@class CartaoBemolCreditoMap;

@interface CartaoBemol : NSObject {

    CartaoBemolBounusMap *bounusMap;
    CartaoBemolCartaoBemolMap *cartaoBemolMap;
    CartaoBemolCreditoMap *creditoMap;

}

@property (nonatomic, strong) CartaoBemolBounusMap *bounusMap;
@property (nonatomic, strong) CartaoBemolCartaoBemolMap *cartaoBemolMap;
@property (nonatomic, strong) CartaoBemolCreditoMap *creditoMap;

+ (CartaoBemol *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
