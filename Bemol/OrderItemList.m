#import "OrderItemList.h"

@implementation OrderItemList

@synthesize cancelableProduct;
@synthesize itemStatus;
@synthesize productName;
@synthesize productURL;
@synthesize quantidade;
@synthesize sku;
@synthesize strSKUsEntregaId;
@synthesize strSKUsNotaFiscaisId;
@synthesize strSKUsStatus;
@synthesize totalPrice;
@synthesize unitPrice;

+ (OrderItemList *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OrderItemList *instance = [[OrderItemList alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.cancelableProduct = [(NSNumber *)[aDictionary objectForKey:@"cancelableProduct"] boolValue];
    self.itemStatus = [aDictionary objectForKey:@"itemStatus"];
    self.productName = [aDictionary objectForKey:@"productName"];
    self.productURL = [aDictionary objectForKey:@"productURL"];
    self.quantidade = [aDictionary objectForKey:@"quantidade"];
    self.sku = [aDictionary objectForKey:@"sku"];
    self.strSKUsEntregaId = [aDictionary objectForKey:@"strSKUsEntregaId"];
    self.strSKUsNotaFiscaisId = [aDictionary objectForKey:@"strSKUsNotaFiscaisId"];
    self.strSKUsStatus = [aDictionary objectForKey:@"strSKUsStatus"];
    self.totalPrice = [aDictionary objectForKey:@"totalPrice"];
    self.unitPrice = [aDictionary objectForKey:@"unitPrice"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    [dictionary setObject:[NSNumber numberWithBool:self.cancelableProduct] forKey:@"cancelableProduct"];

    if (self.itemStatus) {
        [dictionary setObject:self.itemStatus forKey:@"itemStatus"];
    }

    if (self.productName) {
        [dictionary setObject:self.productName forKey:@"productName"];
    }

    if (self.productURL) {
        [dictionary setObject:self.productURL forKey:@"productURL"];
    }

    if (self.quantidade) {
        [dictionary setObject:self.quantidade forKey:@"quantidade"];
    }

    if (self.sku) {
        [dictionary setObject:self.sku forKey:@"sku"];
    }

    if (self.strSKUsEntregaId) {
        [dictionary setObject:self.strSKUsEntregaId forKey:@"strSKUsEntregaId"];
    }

    if (self.strSKUsNotaFiscaisId) {
        [dictionary setObject:self.strSKUsNotaFiscaisId forKey:@"strSKUsNotaFiscaisId"];
    }

    if (self.strSKUsStatus) {
        [dictionary setObject:self.strSKUsStatus forKey:@"strSKUsStatus"];
    }

    if (self.totalPrice) {
        [dictionary setObject:self.totalPrice forKey:@"totalPrice"];
    }

    if (self.unitPrice) {
        [dictionary setObject:self.unitPrice forKey:@"unitPrice"];
    }

    return dictionary;

}


@end
