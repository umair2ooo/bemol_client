#import <Foundation/Foundation.h>

@interface AvailableCredits : NSObject {

    NSNumber *limite;
    NSNumber *limiteDiff;
    NSNumber *limiteTotal;

}

@property (nonatomic, copy) NSNumber *limite;
@property (nonatomic, copy) NSNumber *limiteDiff;
@property (nonatomic, copy) NSNumber *limiteTotal;

+ (AvailableCredits *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
