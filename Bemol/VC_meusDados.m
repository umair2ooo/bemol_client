//
//  VC_meusDados.m
//  Bemol
//
//  Created by Fahim Bilwani on 16/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "VC_meusDados.h"
#import "cell_meusDados.h"
#import "VC_atualizarDados.h"

@interface VC_meusDados ()

@property (weak, nonatomic) IBOutlet UITableView *tableview_;
@property (strong, nonatomic) NSArray *array_UserDetails;
@property (weak, nonatomic) IBOutlet UILabel *label_name;
@property (weak, nonatomic) IBOutlet UILabel *label_fullname;

- (IBAction)action_back:(id)sender;
- (IBAction)action_Atulaizar:(id)sender;
@end

@implementation VC_meusDados

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Meus dados"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.array_UserDetails = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_userDetails valueForKey:@"email1"],@"Email", nil],
                              [NSDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_userDetails valueForKey:@"mobPhone"],@"Telefone", nil],
                              [NSDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_userDetails valueForKey:@"dateOfBirth"],@"Nascimento", nil],
                              [NSDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_userDetails valueForKey:@"gender"],@"Sexo", nil],
                              [NSDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_userDetails valueForKey:@"cpf"],@"CPF", nil],
                              [NSDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_userDetails valueForKey:@"rg"],@"RG", nil],
                              [NSDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_userDetails valueForKey:@"zipCode"],@"CEP", nil],
                              [NSDictionary dictionaryWithObjectsAndKeys:[[ApplicationDelegate.single.dic_userDetails valueForKey:@"addressLine"] objectAtIndex:0],@"Endereço", nil],
                              [NSDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_userDetails valueForKey:@"country"],@"País/região", nil],
                              [NSDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_userDetails valueForKey:@"state"],@"Estado", nil],
                              [NSDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_userDetails valueForKey:@"city"],@"Cidade", nil],nil];
    
    [self.tableview_ reloadData];
    
    DLog(@"%@",self.array_UserDetails);
    
    if ([ApplicationDelegate.single.dic_userDetails valueForKey:@"firstName"])
    {
        self.label_name.text = [ApplicationDelegate.single.dic_userDetails valueForKey:@"firstName"];
        self.label_fullname.text = [ApplicationDelegate.single.dic_userDetails valueForKey:@"firstName"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - tableview dataSource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_UserDetails count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_meusDados *cell = (cell_meusDados *)[tableView dequeueReusableCellWithIdentifier:@"cell_meusDados"];
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_meusDados" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    DLog(@"%@",[[[self.array_UserDetails objectAtIndex:indexPath.row] allKeys] objectAtIndex:0]);
    cell.label_key.text = [[[self.array_UserDetails objectAtIndex:indexPath.row] allKeys] objectAtIndex:0];
    if (![[[self.array_UserDetails objectAtIndex:indexPath.row] valueForKey:[[[self.array_UserDetails objectAtIndex:indexPath.row] allKeys] objectAtIndex:0]] isKindOfClass:[NSNull class]])
    {
        cell.label_value.text = [[self.array_UserDetails objectAtIndex:indexPath.row] valueForKey:[[[self.array_UserDetails objectAtIndex:indexPath.row] allKeys] objectAtIndex:0]];
    }
    else
    {
        cell.label_value.text = @"";
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_Atulaizar:(id)sender
{
//    [ApplicationDelegate.single.dic_userDetails valueForKey:@"rg"]
    
    //[self.obj_shoppingCart.orderItem count]?[self.obj_shoppingCart.orderItem count]:0
    
    [self performSegueWithIdentifier:@"segue_atualizar" sender:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                [ApplicationDelegate.single.dic_userDetails valueForKey:@"email1"],@"Email",
                                                                [ApplicationDelegate.single.dic_userDetails valueForKey:@"mobPhone"],@"Telefone",
                                                                [ApplicationDelegate.single.dic_userDetails valueForKey:@"dateOfBirth"],@"Nascimento",
                                                                [ApplicationDelegate.single.dic_userDetails valueForKey:@"gender"],@"Sexo",
                                                                [ApplicationDelegate.single.dic_userDetails valueForKey:@"cpf"],@"CPF",
                                                                [ApplicationDelegate.single.dic_userDetails valueForKey:@"country"],@"Pais/regiao",
                                                                [ApplicationDelegate.single.dic_userDetails valueForKey:@"state"],@"Estado",
                                                                [ApplicationDelegate.single.dic_userDetails valueForKey:@"city"],@"Cidade",
                                                                [ApplicationDelegate.single.dic_userDetails valueForKey:@"zipCode"],@"CEP",
                                                                [[ApplicationDelegate.single.dic_userDetails valueForKey:@"addressLine"] objectAtIndex:0],@"Endereco",
                                                                [ApplicationDelegate.single.dic_userDetails valueForKey:@"rg"],@"RG",nil]];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_atualizar"])
    {
        VC_atualizarDados *obj = segue.destinationViewController;
        obj.dic_userDetials = sender;
    }
}

@end
