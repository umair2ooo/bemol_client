//
//  MarketingContentDescription.h
//  
//
//  Created by fahim on 07/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MarketingContentDescription : NSObject {
    NSString *language;
    NSString *maketingText;
}

@property (nonatomic, copy) NSString *language;
@property (nonatomic, copy) NSString *maketingText;

+ (MarketingContentDescription *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
