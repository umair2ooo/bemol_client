#import "Pagamento.h"

@implementation Pagamento

@synthesize paymentAmount;
@synthesize paymentName;

+ (Pagamento *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Pagamento *instance = [[Pagamento alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.paymentAmount = [aDictionary objectForKey:@"paymentAmount"];
    self.paymentName = [aDictionary objectForKey:@"paymentName"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.paymentAmount) {
        [dictionary setObject:self.paymentAmount forKey:@"paymentAmount"];
    }

    if (self.paymentName) {
        [dictionary setObject:self.paymentName forKey:@"paymentName"];
    }

    return dictionary;

}


@end
