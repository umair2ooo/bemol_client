#import <Foundation/Foundation.h>

@interface ContractsList : NSObject {

    BOOL debitoAutomatico;
    NSString *desconto;
    NSString *juros;
    NSString *multa;
    NSMutableArray *pagarEm;
    NSString *parcelaDataPagamento;
    NSString *parcelaDataVencim;
    NSString *parcelaNumber;
    NSString *parcelaStatus;
    NSString *parcelaValue;
    NSString *qtParcela;
    NSString *valor;
    NSString *valorDesconto;

}

@property (nonatomic, assign) BOOL debitoAutomatico;
@property (nonatomic, copy) NSString *desconto;
@property (nonatomic, copy) NSString *juros;
@property (nonatomic, copy) NSString *multa;
@property (nonatomic, copy) NSMutableArray *pagarEm;
@property (nonatomic, copy) NSString *parcelaDataPagamento;
@property (nonatomic, copy) NSString *parcelaDataVencim;
@property (nonatomic, copy) NSString *parcelaNumber;
@property (nonatomic, copy) NSString *parcelaStatus;
@property (nonatomic, copy) NSString *parcelaValue;
@property (nonatomic, copy) NSString *qtParcela;
@property (nonatomic, copy) NSString *valor;
@property (nonatomic, copy) NSString *valorDesconto;

+ (ContractsList *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
