#import <Foundation/Foundation.h>

@interface CheckoutShippingAddress : NSObject {

    NSMutableArray *shippingAddresses;

}

@property (nonatomic, copy) NSMutableArray *shippingAddresses;

+ (CheckoutShippingAddress *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
