#import <Foundation/Foundation.h>

@interface InstallmentCredFlexMap : NSObject {

    NSMutableArray *arrayList;

}

@property (nonatomic, copy) NSMutableArray *arrayList;

+ (InstallmentCredFlexMap *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
