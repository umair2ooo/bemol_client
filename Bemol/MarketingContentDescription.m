//
//  MarketingContentDescription.m
//  
//
//  Created by fahim on 07/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "MarketingContentDescription.h"

@implementation MarketingContentDescription

@synthesize language;
@synthesize maketingText;

+ (MarketingContentDescription *)instanceFromDictionary:(NSDictionary *)aDictionary {

    MarketingContentDescription *instance = [[MarketingContentDescription alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.language = [aDictionary objectForKey:@"language"];
    self.maketingText = [aDictionary objectForKey:@"maketingText"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.language) {
        [dictionary setObject:self.language forKey:@"language"];
    }

    if (self.maketingText) {
        [dictionary setObject:self.maketingText forKey:@"maketingText"];
    }

    return dictionary;

}

@end
