#import "YCameraViewController.h"

#import "AppDelegate.h"
#import <ImageIO/ImageIO.h>
#import "ZoomRotatePanImageView.h"
#import <Social/Social.h>


#define k_gap 10.0
#define DegreesToRadians(x) ((x) * M_PI / 180.0)

@interface YCameraViewController ()
{
    UIInterfaceOrientation orientationLast, orientationAfterProcess;
    CMMotionManager *motionManager;
    
    BOOL bool_isComingFromPhotoLib;
    NSInteger int_indexOfSelectedProduct;
    
    UIView *view_;
}

@property (weak, nonatomic) IBOutlet ZoomRotatePanImageView *imageView_zoomRotatePan;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_products;
@property (weak, nonatomic) IBOutlet UIButton *button_Save;
@property (weak, nonatomic) IBOutlet UIButton *button_Share;
@property (weak, nonatomic) IBOutlet UIButton *button_Detail;
@property (weak, nonatomic) IBOutlet UIButton *button_hideShowScroller;

- (IBAction)action_detail:(id)sender;
- (IBAction)action_save:(id)sender;
- (IBAction)action_share:(id)sender;
- (IBAction)action_hideScroller:(id)sender;


@end

@implementation YCameraViewController
@synthesize delegate;

#pragma mark - add rotating object
-(void)method_addRotatingObject
{
//    NSString *string_imageURL = [NSString stringWithFormat:@"%@%@", k_url, [ApplicationDelegate.single.dic_productDetail valueForKey:k_fullImage]];
//    
//    [self.imageView_zoomRotatePan setImageWithURL:[NSURL URLWithString:string_imageURL]
//                                 placeholderImage:nil
//                      usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.imageView_zoomRotatePan setImage:self.image_productimage];
}


#pragma mark - cycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.ImgViewGrid.alpha = 0.0f;

    [self.navigationController setNavigationBarHidden:YES];
    
	// Do any additional setup after loading the view.
    pickerDidShow = NO;
    
    FrontCamera = NO;
    self.captureImage.hidden = YES;
    
    // Setup UIImagePicker Controller
    imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imgPicker.delegate = self;
    imgPicker.allowsEditing = YES;
    croppedImageWithoutOrientation = [[UIImage alloc] init];
    
    initializeCamera = YES;
    photoFromCam = YES;
    
    // Initialize Motion Manager
    [self initializeMotionManager];
    
    self.button_hideShowScroller.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (initializeCamera){
        initializeCamera = NO;
        
        // Initialize camera
        [self initializeCamera];
    }
    
    [self method_addRotatingObject];
    
//    if (self.dic_productsList)
//    {
//        self.button_Detail.hidden = NO;
//        self.button_hideShowScroller.hidden = NO;
//        [self createImageScroller];
//    }
//    else
//    {
//        self.button_Detail.hidden = YES;
//        self.button_hideShowScroller.hidden = YES;
//        [self method_addRotatingObject];
//    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [session stopRunning];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dealloc
{
    [_imagePreview release];
    [_captureImage release];
    [imgPicker release];
    imgPicker = nil;
    self.imageView_zoomRotatePan = nil;
    
    if (session)
        [session release], session=nil;
    
    if (captureVideoPreviewLayer)
        [captureVideoPreviewLayer release], captureVideoPreviewLayer=nil;
    
    if (stillImageOutput)
        [stillImageOutput release], stillImageOutput=nil;
}

//#pragma mark - createImageScroller
//-(void)createImageScroller
//{
////    if ([[obj valueForKey:k_uniqueID] isEqualToString:[[[self.dic_productsList valueForKey:@"productList"] objectAtIndex:[[self.dic_productsList valueForKey:@"selectedIndex"] integerValue]] valueForKey:k_uniqueID]])
////    {
////        int_indexOfSelectedProduct = [array_temp count]-1;
////    }
//    //rotate hide/show scroller button
//    self.button_hideShowScroller.transform = CGAffineTransformMakeRotation(M_PI_2);
//    
//    
//    NSMutableArray *array_temp = [[NSMutableArray alloc] init];
//    
//    [(NSArray *)[self.dic_productsList valueForKey:k_productList] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
//    {
//        if (![obj isKindOfClass:[NSNull class]] && [[obj valueForKey:k_fullImage] rangeOfString:@".png"].location != NSNotFound)
//        {
//            [array_temp addObject:obj];
//        }
//        else
//        {
//            [array_temp addObject:[NSNull null]];
//        }
//    }];
//
//    
//    [self.dic_productsList setObject:array_temp forKey:k_productList];
//    
//    
//    int x = 0;
//    for (int i = 0; i < [[self.dic_productsList valueForKey:k_productList] count]; i++)
//    {
//        if (![[[self.dic_productsList valueForKey:k_productList] objectAtIndex:i] isKindOfClass:[NSNull class]])
//        {
//            
//            
//            UIImageView *imageView_temp = [[UIImageView alloc] initWithFrame:CGRectMake(x+k_gap,
//                                                                                        0,
//                                                                                        self.scroller_products.frame.size.height,
//                                                                                        self.scroller_products.frame.size.height)];
//            
//            [imageView_temp sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",
//                                                                     k_url,
//                                                                     [[[self.dic_productsList valueForKey:k_productList] objectAtIndex:i] valueForKey:k_thumbnail]]]
//                              placeholderImage:[UIImage imageNamed:k_placeHolder]];
//            
//            imageView_temp.clipsToBounds = YES;
//            imageView_temp.layer.borderWidth=2.0f;
//            imageView_temp.layer.borderColor=[UIColor clearColor].CGColor;
//            [imageView_temp setContentMode:UIViewContentModeScaleAspectFit];
//            
//            [imageView_temp setBackgroundColor:[UIColor clearColor]];
//            
//            
//            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(x+k_gap,
//                                                                          0,
//                                                                          self.scroller_products.frame.size.height,
//                                                                          self.scroller_products.frame.size.height)];
//            
//            DLog(@"%d", [[self.dic_productsList valueForKey:k_uniqueID] intValue]);
//            DLog(@"%d", [[[[self.dic_productsList valueForKey:k_productList] objectAtIndex:i] valueForKey:k_uniqueID] intValue]);
//            
//            
//            if ([[self.dic_productsList valueForKey:k_uniqueID] intValue] ==
//                [[[[self.dic_productsList valueForKey:k_productList] objectAtIndex:i] valueForKey:k_uniqueID] intValue])
//            {
//                int_indexOfSelectedProduct = i;
//                
//                DLog(@"%ld",(long)int_indexOfSelectedProduct);
//                [button setTag:int_indexOfSelectedProduct];
//                [self action_productSelected:button];
//            }
//            
//            
//            [button setTag:i];
//            
//            [button addTarget:self action:@selector(action_productSelected:) forControlEvents:UIControlEventTouchUpInside];
//            
//            
//            
//            //Clip/Clear the other pieces whichever outside the rounded corner
//            button.clipsToBounds = YES;
//            [button setBackgroundColor:[UIColor clearColor]];
//            button.layer.borderWidth=2.0f;
//            button.layer.borderColor=[UIColor clearColor].CGColor;
//            
//            [self.scroller_products addSubview:imageView_temp];
//            [self.scroller_products addSubview:button];
//            
//            
//            x += button.frame.size.width+k_gap;
//            
//        }
//    }
//    
//    self.scroller_products.contentSize = CGSizeMake(x, self.scroller_products.frame.size.height);
//    self.scroller_products.backgroundColor = [UIColor whiteColor];
//    array_temp = nil;
//}
//
//#pragma mark - action_colorSelected
//-(void)action_productSelected:(UIButton *)sender
//{
//    int_indexOfSelectedProduct = sender.tag;
//    
//    DLog(@"%ld",(long)int_indexOfSelectedProduct);
//    
//    NSString *string_imageURL = [NSString stringWithFormat:@"%@%@", k_imageURL, [[[self.dic_productsList valueForKey:k_productList] objectAtIndex:int_indexOfSelectedProduct] valueForKey:k_fullImage]];
//    
//    [self.imageView_zoomRotatePan setImageWithURL:[NSURL URLWithString:string_imageURL]
//                                 placeholderImage:nil
//                      usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    
//
//}
#pragma mark - CoreMotion Task
- (void)initializeMotionManager{
    motionManager = [[CMMotionManager alloc] init];
    motionManager.accelerometerUpdateInterval = .2;
    motionManager.gyroUpdateInterval = .2;
    
    [motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
                                        withHandler:^(CMAccelerometerData  *accelerometerData, NSError *error) {
                                            if (!error) {
                                                [self outputAccelertionData:accelerometerData.acceleration];
                                            }
                                            else{
                                                NSLog(@"%@", error);
                                            }
                                        }];
}

#pragma mark - UIAccelerometer callback

- (void)outputAccelertionData:(CMAcceleration)acceleration{
    UIInterfaceOrientation orientationNew;
    
    if (acceleration.x >= 0.75) {
        orientationNew = UIInterfaceOrientationLandscapeLeft;
    }
    else if (acceleration.x <= -0.75) {
        orientationNew = UIInterfaceOrientationLandscapeRight;
    }
    else if (acceleration.y <= -0.75) {
        orientationNew = UIInterfaceOrientationPortrait;
    }
    else if (acceleration.y >= 0.75) {
        orientationNew = UIInterfaceOrientationPortraitUpsideDown;
    }
    else {
        // Consider same as last time
        return;
    }
    
    if (orientationNew == orientationLast)
        return;
    
    //    NSLog(@"Going from %@ to %@!", [[self class] orientationToText:orientationLast], [[self class] orientationToText:orientationNew]);
    
    orientationLast = orientationNew;
}

#ifdef DEBUG
+(NSString*)orientationToText:(const UIInterfaceOrientation)ORIENTATION {
    switch (ORIENTATION) {
        case UIInterfaceOrientationPortrait:
            return @"UIInterfaceOrientationPortrait";
        case UIInterfaceOrientationPortraitUpsideDown:
            return @"UIInterfaceOrientationPortraitUpsideDown";
        case UIInterfaceOrientationLandscapeLeft:
            return @"UIInterfaceOrientationLandscapeLeft";
        case UIInterfaceOrientationLandscapeRight:
            return @"UIInterfaceOrientationLandscapeRight";
    }
    return @"Unknown orientation!";
}
#endif

#pragma mark - Camera Initialization

//AVCaptureSession to show live video feed in view
- (void) initializeCamera {
    if (session)
        [session release], session=nil;
    
    session = [[AVCaptureSession alloc] init];
	session.sessionPreset = AVCaptureSessionPresetPhoto;
	
    if (captureVideoPreviewLayer)
        [captureVideoPreviewLayer release], captureVideoPreviewLayer=nil;
    
	captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    [captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    
    self.imagePreview.frame = self.view.frame;
    self.ImgViewGrid.frame = self.view.frame;
    
	captureVideoPreviewLayer.frame = self.imagePreview.bounds;
	[self.imagePreview.layer addSublayer:captureVideoPreviewLayer];

    UIView *view = [self imagePreview];
    CALayer *viewLayer = [view layer];
    [viewLayer setMasksToBounds:YES];
    
    CGRect bounds = [view bounds];
    [captureVideoPreviewLayer setFrame:bounds];
    
    NSArray *devices = [AVCaptureDevice devices];
    AVCaptureDevice *frontCamera=nil;
    AVCaptureDevice *backCamera=nil;
    
    // check if device available
    if (devices.count==0) {
        NSLog(@"No Camera Available");
        [self disableCameraDeviceControls];
        return;
    }
    
    for (AVCaptureDevice *device in devices) {
        
        NSLog(@"Device name: %@", [device localizedName]);
        
        if ([device hasMediaType:AVMediaTypeVideo]) {
            
            if ([device position] == AVCaptureDevicePositionBack) {
                NSLog(@"Device position : back");
                backCamera = device;
            }
            else {
                NSLog(@"Device position : front");
                frontCamera = device;
            }
        }
    }
    
    if (!FrontCamera) {
        
        if ([backCamera hasFlash]){
            [backCamera lockForConfiguration:nil];
            if (self.flashToggleButton.selected)
                [backCamera setFlashMode:AVCaptureFlashModeOn];
            else
                [backCamera setFlashMode:AVCaptureFlashModeOff];
            [backCamera unlockForConfiguration];
            
            [self.flashToggleButton setEnabled:YES];
        }
        else{
            if ([backCamera isFlashModeSupported:AVCaptureFlashModeOff]) {
                [backCamera lockForConfiguration:nil];
                [backCamera setFlashMode:AVCaptureFlashModeOff];
                [backCamera unlockForConfiguration];
            }
            [self.flashToggleButton setEnabled:NO];
        }
        
        NSError *error = nil;
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:backCamera error:&error];
        if (!input) {
            NSLog(@"ERROR: trying to open camera: %@", error);
        }
        [session addInput:input];
    }
    
    if (FrontCamera) {
        [self.flashToggleButton setEnabled:NO];
        NSError *error = nil;
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
        if (!input) {
            NSLog(@"ERROR: trying to open camera: %@", error);
        }
        [session addInput:input];
    }
    
    if (stillImageOutput)
        [stillImageOutput release], stillImageOutput=nil;
    
    stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil] autorelease];
    [stillImageOutput setOutputSettings:outputSettings];
    
    [session addOutput:stillImageOutput];
    
	[session startRunning];
}

- (IBAction)snapImage:(id)sender {
    [self.photoCaptureButton setEnabled:NO];
    
    if (!haveImage) {
        self.captureImage.image = nil; //remove old image from view
        self.captureImage.hidden = NO; //show the captured image view
        self.imagePreview.hidden = YES; //hide the live video feed
        [self capImage];
    }
    else {
        self.captureImage.hidden = YES;
        self.imagePreview.hidden = NO;
        haveImage = NO;
    }
}

- (void) capImage { //method to capture image from AVCaptureSession video feed
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections) {
        
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            
            if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
                videoConnection = connection;
                break;
            }
        }
        
        if (videoConnection) {
            break;
        }
    }
    
    NSLog(@"about to request a capture from: %@", stillImageOutput);
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
        
        if (imageSampleBuffer != NULL) {
            
            NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
            [self processImage:[UIImage imageWithData:imageData]];
        }
    }];
}

- (UIImage*)imageWithImage:(UIImage *)sourceImage scaledToWidth:(float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void) processImage:(UIImage *)image { //process captured image, crop, resize and rotate
    haveImage = YES;
    photoFromCam = YES;
    
    // Resize image to 640x640
    // Resize image
    //    NSLog(@"Image size %@",NSStringFromCGSize(image.size));
    
    UIImage *smallImage = [self imageWithImage:image scaledToWidth:640.0f]; //UIGraphicsGetImageFromCurrentImageContext();
    
    CGRect cropRect = CGRectMake(0, 105, 640, 640);
    CGImageRef imageRef = CGImageCreateWithImageInRect([smallImage CGImage], cropRect);
    
    croppedImageWithoutOrientation = [[UIImage imageWithCGImage:imageRef] copy];
    
    UIImage *croppedImage = nil;
    //    assetOrientation = ALAssetOrientationUp;
    
    // adjust image orientation
    NSLog(@"orientation: %ld",orientationLast);
    orientationAfterProcess = orientationLast;
    switch (orientationLast) {
        case UIInterfaceOrientationPortrait:
            NSLog(@"UIInterfaceOrientationPortrait");
            croppedImage = [UIImage imageWithCGImage:imageRef];
            break;
            
        case UIInterfaceOrientationPortraitUpsideDown:
            NSLog(@"UIInterfaceOrientationPortraitUpsideDown");
            croppedImage = [[[UIImage alloc] initWithCGImage: imageRef
                                                       scale: 1.0
                                                 orientation: UIImageOrientationDown] autorelease];
            break;
            
        case UIInterfaceOrientationLandscapeLeft:
            NSLog(@"UIInterfaceOrientationLandscapeLeft");
            croppedImage = [[[UIImage alloc] initWithCGImage: imageRef
                                                       scale: 1.0
                                                 orientation: UIImageOrientationRight] autorelease];
            break;
            
        case UIInterfaceOrientationLandscapeRight:
            NSLog(@"UIInterfaceOrientationLandscapeRight");
            croppedImage = [[[UIImage alloc] initWithCGImage: imageRef
                                                       scale: 1.0
                                                 orientation: UIImageOrientationLeft] autorelease];
            break;
            
        default:
            croppedImage = [UIImage imageWithCGImage:imageRef];
            break;
    }
    
    CGImageRelease(imageRef);
    
    [self.captureImage setImage:croppedImage];
    
    [self setCapturedImage];
}

- (void)setCapturedImage{
    // Stop capturing image
    [session stopRunning];
    
    // Hide Top/Bottom controller after taking photo for editing
    [self hideControllers];
}

#pragma mark - Device Availability Controls
- (void)disableCameraDeviceControls{
    self.cameraToggleButton.enabled = NO;
    self.flashToggleButton.enabled = NO;
    self.photoCaptureButton.enabled = NO;
}

#pragma mark - UIImagePicker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    bool_isComingFromPhotoLib = false;
    
    if (info)
    {
        photoFromCam = NO;
        
        UIImage* outputImage = [info objectForKey:UIImagePickerControllerEditedImage];
        if (outputImage == nil)
        {
            outputImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        }
        
        if (outputImage)
        {
            self.captureImage.hidden = NO;
            self.captureImage.image=outputImage;
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
            // Hide Top/Bottom controller after taking photo for editing
            [self hideControllers];
        }
    }
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    bool_isComingFromPhotoLib = false;
    
    initializeCamera = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Button clicks
- (IBAction)gridToogle:(UIButton *)sender
{
    if (sender.selected)
    {
        sender.selected = NO;
        [UIView animateWithDuration:0.2 delay:0.0 options:0 animations:^{
            self.ImgViewGrid.alpha = 1.0f;
        }
                         completion:nil];
    }
    else
    {
        sender.selected = YES;
        [UIView animateWithDuration:0.2 delay:0.0 options:0 animations:^{
            self.ImgViewGrid.alpha = 0.0f;
        }
                         completion:nil];
    }
}


-(IBAction)switchToLibrary:(id)sender
{
    if (session)
    {
        [session stopRunning];
    }

    //    self.captureImage = nil;

    //    UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
    //    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //    imagePickerController.delegate = self;
    //    imagePickerController.allowsEditing = YES;

    bool_isComingFromPhotoLib = true;
    [self presentViewController:imgPicker animated:YES completion:NULL];
}



- (IBAction)skipped:(id)sender
{
    if ([delegate respondsToSelector:@selector(yCameraControllerdidSkipped)])
    {
        [delegate yCameraControllerdidSkipped];
    }

//  Dismiss self view controller
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}




-(IBAction) cancel:(id)sender
{
    if ([delegate respondsToSelector:@selector(yCameraControllerDidCancel)])
    {
        [delegate yCameraControllerDidCancel];
    }

    // Dismiss self view controller
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)donePhotoCapture:(id)sender
{
    if ([delegate respondsToSelector:@selector(didFinishPickingImage:)]) {
        [delegate didFinishPickingImage:self.captureImage.image];
    }
    // Dismiss self view controller
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)retakePhoto:(id)sender
{
    [self.photoCaptureButton setEnabled:YES];
    self.captureImage.image = nil;
    self.imagePreview.hidden = NO;
    // Show Camera device controls
    [self showControllers];
    
    haveImage=NO;
    FrontCamera = NO;
//    [self performSelector:@selector(initializeCamera) withObject:nil afterDelay:0.001];
    [session startRunning];
}



- (IBAction)switchCamera:(UIButton *)sender //switch cameras front and rear cameras
{
    
    // Stop current recording process
    [session stopRunning];
    
    if (sender.selected) {  // Switch to Back camera
        sender.selected = NO;
        FrontCamera = NO;
        [self performSelector:@selector(initializeCamera) withObject:nil afterDelay:0.001];
    }
    else {                  // Switch to Front camera
        sender.selected = YES;
        FrontCamera = YES;
        [self performSelector:@selector(initializeCamera) withObject:nil afterDelay:0.001];
    }
}

- (IBAction)toogleFlash:(UIButton *)sender
{
    if (!FrontCamera)
    {
        if (sender.selected) { // Set flash off
            [sender setSelected:NO];
            
            NSArray *devices = [AVCaptureDevice devices];
            for (AVCaptureDevice *device in devices) {
                
                NSLog(@"Device name: %@", [device localizedName]);
                
                if ([device hasMediaType:AVMediaTypeVideo]) {
                    
                    if ([device position] == AVCaptureDevicePositionBack) {
                        NSLog(@"Device position : back");
                        if ([device hasFlash]){
                            
                            [device lockForConfiguration:nil];
                            [device setFlashMode:AVCaptureFlashModeOff];
                            [device unlockForConfiguration];
                            
                            break;
                        }
                    }
                }
            }
            
        }
        else{                  // Set flash on
            [sender setSelected:YES];
            
            NSArray *devices = [AVCaptureDevice devices];
            for (AVCaptureDevice *device in devices) {
                
                NSLog(@"Device name: %@", [device localizedName]);
                
                if ([device hasMediaType:AVMediaTypeVideo]) {
                    
                    if ([device position] == AVCaptureDevicePositionBack) {
                        NSLog(@"Device position : back");
                        if ([device hasFlash]){
                            
                            [device lockForConfiguration:nil];
                            [device setFlashMode:AVCaptureFlashModeOn];
                            [device unlockForConfiguration];
                            
                            break;
                        }
                    }
                }
            }
            
        }
    }
}

#pragma mark - UI Control Helpers
- (void)hideControllers
{
    [UIView animateWithDuration:0.2 animations:^{
        //1)animate them out of screen
        self.photoBar.center = CGPointMake(self.photoBar.center.x, self.photoBar.center.y+116.0);
        self.topBar.center = CGPointMake(self.topBar.center.x, self.topBar.center.y-44.0);
        
        //2)actually hide them
        self.photoBar.alpha = 0.0;
        self.topBar.alpha = 0.0;
        
    } completion:nil];
}

- (void)showControllers
{
    [UIView animateWithDuration:0.2 animations:^{
        //1)animate them into screen
        self.photoBar.center = CGPointMake(self.photoBar.center.x, self.photoBar.center.y-116.0);
        self.topBar.center = CGPointMake(self.topBar.center.x, self.topBar.center.y+44.0);
        
        //2)actually show them
        self.photoBar.alpha = 1.0;
        self.topBar.alpha = 1.0;
        
    } completion:nil];
}

#pragma mark - ARSeletectImageDetail
- (IBAction)action_detail:(id)sender
{

    [self.dic_productsList setObject:[[[self.dic_productsList valueForKey:k_productList] objectAtIndex:int_indexOfSelectedProduct] valueForKey:k_uniqueID]
                              forKey:k_uniqueID];
    
    
    [self.delegate GetARSeletedImageDetail:[[[self.dic_productsList valueForKey:k_productList] objectAtIndex:int_indexOfSelectedProduct] valueForKey:k_uniqueID]];
    
    
        [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - SavetoPhotoLibraty
- (IBAction)action_save:(id)sender
{
    view_ = nil;
    
    view_ = [[UIView alloc] initWithFrame:self.view.frame];
    
    
    UIImageView *image_capture = [[UIImageView alloc] initWithImage:self.captureImage.image];
    [image_capture setFrame:self.captureImage.frame];
    [image_capture setContentMode:UIViewContentModeScaleAspectFill];
    image_capture.image = self.captureImage.image;
    
    
    UIImageView *image_zoomRotatePan = [[UIImageView alloc] initWithImage:self.imageView_zoomRotatePan.image];
    [image_zoomRotatePan setFrame:self.imageView_zoomRotatePan.frame];
    [image_zoomRotatePan setContentMode:UIViewContentModeScaleAspectFit];
    image_zoomRotatePan.image = self.imageView_zoomRotatePan.image;
    
    [view_ addSubview:image_capture];
    [view_ addSubview:image_zoomRotatePan];
    
    
    UIImageWriteToSavedPhotosAlbum([self imageWithView:view_], nil, nil, nil);
    [[[UIAlertView alloc] initWithTitle:nil
                                message:@"Image saved successfully"
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil, nil]
     show];
    
    image_zoomRotatePan = nil;
    image_capture = nil;
}


#pragma mark - combine uiimages
- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}




#pragma mark - Share
- (IBAction)action_share:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Facebook", @"Twitter", nil];
    
    [actionSheet showInView:self.view];
}

#pragma mark - action_hideScroller
- (IBAction)action_hideScroller:(id)sender
{
    if (self.scroller_products.hidden)
    {
        self.button_hideShowScroller.transform = CGAffineTransformMakeRotation(M_PI_2);
        
        [UIView beginAnimations:@"LeftFlip" context:nil];
        [UIView setAnimationDuration:0.8];
        self.scroller_products.hidden=NO;
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown    forView:self.scroller_products cache:YES];
        [UIView commitAnimations];
    }
    else
    {
        self.button_hideShowScroller.transform = CGAffineTransformMakeRotation(-M_PI_2);
        
        [UIView transitionWithView:self.scroller_products
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCurlUp
                        animations:NULL
                        completion:NULL];
        
        [self.scroller_products  setHidden:YES];
    }
    
}


#pragma UIactionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0)
    {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [controller addImage:[self imageWithView:view_]];
            [self presentViewController:controller animated:YES completion:Nil];
        }
        else
        {
            
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Facebook account not found. Go to the Settings application to add your Facebook account to this device."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
    else if (buttonIndex == 1)
    {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet addImage:[self imageWithView:view_]];
            [self presentViewController:tweetSheet animated:YES completion:nil];
        }
        else
        {
            
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Twitter account not found. Go to the Settings application to add your Twitter account to this device."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
}



@end