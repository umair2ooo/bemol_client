//
//  VC_cancelprodutos.h
//  Bemol
//
//  Created by Fahim Bilwani on 29/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetails.h"

@interface VC_cancelprodutos : BaseViewController

@property(nonatomic) BOOL bool_ispedido;
@property(nonatomic, strong)OrderDetails *obj_orderDetails;
@end
