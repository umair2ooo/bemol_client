#import "CheckoutPhysicalStoreList.h"

@implementation CheckoutPhysicalStoreList

@synthesize addressLine;
@synthesize city;
@synthesize country;
@synthesize displayName;
@synthesize displayValue;
@synthesize fax1;
@synthesize geoCodeLatitude;
@synthesize geoCodeLongitude;
@synthesize name;
@synthesize physicalStoreIdentifier;
@synthesize postalcode;
@synthesize telephone1;
@synthesize uniqueID;
@synthesize value;
@synthesize distance;

+ (CheckoutPhysicalStoreList *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CheckoutPhysicalStoreList *instance = [[CheckoutPhysicalStoreList alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.addressLine = [aDictionary objectForKey:@"addressLine"];
    self.city = [aDictionary objectForKey:@"city"];
    self.country = [aDictionary objectForKey:@"country"];
    self.displayName = [aDictionary objectForKey:@"displayName"];
    self.displayValue = [aDictionary objectForKey:@"displayValue"];
    self.fax1 = [aDictionary objectForKey:@"Fax1"];
    self.geoCodeLatitude = [aDictionary objectForKey:@"geoCode_Latitude"];
    self.geoCodeLongitude = [aDictionary objectForKey:@"geoCode_Longitude"];
    self.name = [aDictionary objectForKey:@"Name"];
    self.physicalStoreIdentifier = [aDictionary objectForKey:@"physicalStoreIdentifier"];
    self.postalcode = [aDictionary objectForKey:@"postalcode"];
    self.telephone1 = [aDictionary objectForKey:@"Telephone1"];
    self.uniqueID = [aDictionary objectForKey:@"uniqueID"];
    self.value = [aDictionary objectForKey:@"value"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.addressLine) {
        [dictionary setObject:self.addressLine forKey:@"addressLine"];
    }

    if (self.city) {
        [dictionary setObject:self.city forKey:@"city"];
    }

    if (self.country) {
        [dictionary setObject:self.country forKey:@"country"];
    }

    if (self.displayName) {
        [dictionary setObject:self.displayName forKey:@"displayName"];
    }

    if (self.displayValue) {
        [dictionary setObject:self.displayValue forKey:@"displayValue"];
    }

    if (self.fax1) {
        [dictionary setObject:self.fax1 forKey:@"fax1"];
    }

    if (self.geoCodeLatitude) {
        [dictionary setObject:self.geoCodeLatitude forKey:@"geoCodeLatitude"];
    }

    if (self.geoCodeLongitude) {
        [dictionary setObject:self.geoCodeLongitude forKey:@"geoCodeLongitude"];
    }

    if (self.name) {
        [dictionary setObject:self.name forKey:@"name"];
    }

    if (self.physicalStoreIdentifier) {
        [dictionary setObject:self.physicalStoreIdentifier forKey:@"physicalStoreIdentifier"];
    }

    if (self.postalcode) {
        [dictionary setObject:self.postalcode forKey:@"postalcode"];
    }

    if (self.telephone1) {
        [dictionary setObject:self.telephone1 forKey:@"telephone1"];
    }

    if (self.uniqueID) {
        [dictionary setObject:self.uniqueID forKey:@"uniqueID"];
    }

    if (self.value) {
        [dictionary setObject:self.value forKey:@"value"];
    }

    return dictionary;

}


@end
