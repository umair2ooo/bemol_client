#import "Installment.h"

#import "InstallmentCredFlexMap.h"
#import "InstallmentInstallmentMap.h"

@implementation Installment

@synthesize credFlexMap;
@synthesize installmentMap;

+ (Installment *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Installment *instance = [[Installment alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.credFlexMap = [InstallmentCredFlexMap instanceFromDictionary:[aDictionary objectForKey:@"credFlexMap"]];
    self.installmentMap = [InstallmentInstallmentMap instanceFromDictionary:[aDictionary objectForKey:@"installmentMap"]];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.credFlexMap) {
        [dictionary setObject:self.credFlexMap forKey:@"credFlexMap"];
    }

    if (self.installmentMap) {
        [dictionary setObject:self.installmentMap forKey:@"installmentMap"];
    }

    return dictionary;

}


@end
