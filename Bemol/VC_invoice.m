//
//  VC_invoice.m
//  Bemol
//
//  Created by Fahim Bilwani on 01/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_invoice.h"

@interface VC_invoice ()

- (IBAction)action_back:(id)sender;
- (IBAction)action_volarparaPedido:(id)sender;
@end

@implementation VC_invoice

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Historico de compras"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_volarparaPedido:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
