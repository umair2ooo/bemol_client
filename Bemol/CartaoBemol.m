#import "CartaoBemol.h"

#import "CartaoBemolBounusMap.h"
#import "CartaoBemolCartaoBemolMap.h"
#import "CartaoBemolCreditoMap.h"

@implementation CartaoBemol

@synthesize bounusMap;
@synthesize cartaoBemolMap;
@synthesize creditoMap;

+ (CartaoBemol *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CartaoBemol *instance = [[CartaoBemol alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.bounusMap = [CartaoBemolBounusMap instanceFromDictionary:[aDictionary objectForKey:@"bounusMap"]];
    self.cartaoBemolMap = [CartaoBemolCartaoBemolMap instanceFromDictionary:[aDictionary objectForKey:@"CartaoBemolMap"]];
    self.creditoMap = [CartaoBemolCreditoMap instanceFromDictionary:[aDictionary objectForKey:@"creditoMap"]];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.bounusMap) {
        [dictionary setObject:self.bounusMap forKey:@"bounusMap"];
    }

    if (self.cartaoBemolMap) {
        [dictionary setObject:self.cartaoBemolMap forKey:@"cartaoBemolMap"];
    }

    if (self.creditoMap) {
        [dictionary setObject:self.creditoMap forKey:@"creditoMap"];
    }

    return dictionary;

}


@end
