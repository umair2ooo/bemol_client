//
//  MarketingSpotDatum.h
//  
//
//  Created by fahim on 07/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MarketingSpotDatum : NSObject {
    NSMutableArray *baseMarketingSpotActivityData;
    NSString *eSpotName;
    NSString *marketingSpotIdentifier;
}

@property (nonatomic, copy) NSMutableArray *baseMarketingSpotActivityData;
@property (nonatomic, copy) NSString *eSpotName;
@property (nonatomic, copy) NSString *marketingSpotIdentifier;

+ (MarketingSpotDatum *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
