#import <Foundation/Foundation.h>

@interface ReviewList : NSObject {

    NSString *comments;
    NSString *lastUpdated;
    NSString *name;
    NSString *nickname;
    NSNumber *notUseful;
    NSNumber *reviewRate;
    NSNumber *useful;

}

@property (nonatomic, copy) NSString *comments;
@property (nonatomic, copy) NSString *lastUpdated;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSNumber *notUseful;
@property (nonatomic, copy) NSNumber *reviewRate;
@property (nonatomic, copy) NSNumber *useful;

+ (ReviewList *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
