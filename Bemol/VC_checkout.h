//
//  VC_checkout.h
//  Bemol
//
//  Created by Fahim Bilwani on 26/08/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VC_checkout : BaseViewController

@property (strong,nonatomic) NSString *orderId;

@end
