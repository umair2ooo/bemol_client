#import <UIKit/UIKit.h>
#import "HomePageSecondESpot.h"
#import "BannerTwoMarketingSpotDatum.h"
#import "BannerTwoBaseMarketingSpotActivityDatum.h"
#import "BannerTwoDescriptionText.h"

@interface scrollerTwo_home : UIScrollView<UIScrollViewDelegate>
{
    NSTimer *timer_;
    NSMutableArray *array_scrollerOneBanners;
}
@property(nonatomic, strong)HomePageSecondESpot *obj_HomePageSecondESpot;
@property(nonatomic, strong)BannerTwoMarketingSpotDatum *obj_MarketingSpotDatum;
@property(nonatomic, strong)BannerTwoBaseMarketingSpotActivityDatum *obj_baseMarketingSpotActivityDatum;
@property(nonatomic, strong)BannerTwoDescriptionText *obj_DescriptionText;


-(void)method_startScroller;
-(void)method_stopScroller;
-(void) method_getDataFromServer;
@end