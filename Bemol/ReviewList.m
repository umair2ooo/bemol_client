#import "ReviewList.h"

@implementation ReviewList

@synthesize comments;
@synthesize lastUpdated;
@synthesize name;
@synthesize nickname;
@synthesize notUseful;
@synthesize reviewRate;
@synthesize useful;

+ (ReviewList *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ReviewList *instance = [[ReviewList alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.comments = [aDictionary objectForKey:@"Comments"];
    self.lastUpdated = [aDictionary objectForKey:@"Last_Updated"];
    self.name = [aDictionary objectForKey:@"Name"];
    self.nickname = [aDictionary objectForKey:@"Nickname"];
    self.notUseful = [aDictionary objectForKey:@"NotUseful"];
    self.reviewRate = [aDictionary objectForKey:@"ReviewRate"];
    self.useful = [aDictionary objectForKey:@"Useful"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.comments) {
        [dictionary setObject:self.comments forKey:@"comments"];
    }

    if (self.lastUpdated) {
        [dictionary setObject:self.lastUpdated forKey:@"lastUpdated"];
    }

    if (self.name) {
        [dictionary setObject:self.name forKey:@"name"];
    }

    if (self.nickname) {
        [dictionary setObject:self.nickname forKey:@"nickname"];
    }

    if (self.notUseful) {
        [dictionary setObject:self.notUseful forKey:@"notUseful"];
    }

    if (self.reviewRate) {
        [dictionary setObject:self.reviewRate forKey:@"reviewRate"];
    }

    if (self.useful) {
        [dictionary setObject:self.useful forKey:@"useful"];
    }

    return dictionary;

}


@end
