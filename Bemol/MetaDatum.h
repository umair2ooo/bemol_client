#import <Foundation/Foundation.h>

@interface MetaDatum : NSObject {

    NSString *metaData;
    NSString *metaKey;

}

@property (nonatomic, copy) NSString *metaData;
@property (nonatomic, copy) NSString *metaKey;

+ (MetaDatum *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
