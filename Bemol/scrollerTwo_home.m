#import "scrollerTwo_home.h"



#define k_time 5.0
#define k_imageName @"banner3"

@implementation scrollerTwo_home

-(void)method_getDataFromServer
{
    [[ServiceModel sharedClient]GET:k_homeBannerTwo parameters:nil onView:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             //                 if (array_scrollerOneBanners) {
             //                     [array_scrollerOneBanners removeAllObjects];
             //                 }
             //                 else
             //                 {
             //                     array_scrollerOneBanners = [NSMutableArray new];
             //                 }
             
             self.obj_HomePageSecondESpot = [HomePageSecondESpot instanceFromDictionary:responseObject];
             self.obj_MarketingSpotDatum = [self.obj_HomePageSecondESpot.marketingSpotData objectAtIndex:0];
             if (self.obj_MarketingSpotDatum.baseMarketingSpotActivityData.count)
             {
                 [self method_setupScroller];
             }
             
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
         
     }
                            failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error.description);
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}



- (id)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super initWithCoder:aDecoder])
    {
        //[self performSelector:@selector(method_setupScroller) withObject:nil afterDelay:0.2];
    }
    
    return self;
}


#pragma mark - setup scroller
-(void)method_setupScroller
{
    self.pagingEnabled = YES;
    
    self.delegate = self;
    
    float float_x = 0.0;
    
    for (unsigned i = 0; i<[self.obj_MarketingSpotDatum.baseMarketingSpotActivityData count]; i++)
    {
        UIView *view_temp = [[UIView alloc] initWithFrame:CGRectMake(float_x,
                                                                     0,
                                                                     self.frame.size.width,
                                                                     self.frame.size.height)];
        
        
        UIImageView *imageView_temp = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                                    0,
                                                                                    view_temp.frame.size.width,
                                                                                    view_temp.frame.size.height)];
        imageView_temp.contentMode = UIViewContentModeScaleAspectFit;
        NSString *string_temp = [NSString stringWithFormat:@"%@", k_placeHolder];
        
        self.obj_baseMarketingSpotActivityDatum = [self.obj_MarketingSpotDatum.baseMarketingSpotActivityData objectAtIndex:i];
        
        if ([self.obj_baseMarketingSpotActivityDatum.descriptionText count])
        {
            self.obj_DescriptionText = [self.obj_baseMarketingSpotActivityDatum.descriptionText objectAtIndex:0];
            
            
            [imageView_temp sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", k_imageurl_slider, self.obj_DescriptionText.fullImage]]
                              placeholderImage:[UIImage imageNamed:string_temp]];
            
        }
        else
        {
            [imageView_temp setImage:[UIImage imageNamed:string_temp]];
        }
        
        DLog(@"%@", [NSString stringWithFormat:@"%@%@", k_imageurl_slider, self.obj_DescriptionText.fullImage]);
        
        [view_temp addSubview:imageView_temp];
        
        string_temp = nil;
        
        
        
        UIButton *button_ = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button_ setBackgroundImage:[UIImage imageNamed:@"front-btn"] forState:UIControlStateNormal];
        
        
        
        if (IS_IPHONE_4_OR_LESS)
        {
            [button_ setFrame:CGRectMake(20, 180, 150, 40)];
        }
        else if (IS_IPHONE_5)
        {
            [button_ setFrame:CGRectMake(20, 200, 150, 40)];
        }
        else if (IS_IPHONE_6P)
        {
            [button_ setFrame:CGRectMake(20, 250, 170, 40)];
        }
        
        
        
        [button_ setTintAdjustmentMode:UIViewTintAdjustmentModeDimmed];
        [button_ setReversesTitleShadowWhenHighlighted:YES];
        button_.titleLabel.adjustsFontSizeToFitWidth = YES;
        [button_ setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        
        switch (i)
        {
            case 1:
                [button_ setTitle:@"QR Code" forState:UIControlStateNormal];
                //                [button_ setBackgroundImage:[UIImage imageNamed:@"hmbarcode"] forState:UIControlStateNormal];
//                [button_ addTarget:self action:@selector(method_barCode) forControlEvents:UIControlEventTouchUpInside];
                [view_temp addSubview:button_];
                break;
            case 2:
                [button_ setTitle:@"Store Locator" forState:UIControlStateNormal];
                //                [button_ setBackgroundImage:[UIImage imageNamed:@"hmLocateStore"] forState:UIControlStateNormal];
//                [button_ addTarget:self action:@selector(method_locateStore) forControlEvents:UIControlEventTouchUpInside];
                [view_temp addSubview:button_];
                break;
            case 3:
                [button_ setTitle:@"Loyalty Points" forState:UIControlStateNormal];
                //                [button_ setBackgroundImage:[UIImage imageNamed:@"hmloypoints"] forState:UIControlStateNormal];
//                [button_ addTarget:self action:@selector(method_loyalityPoints) forControlEvents:UIControlEventTouchUpInside];
                [view_temp addSubview:button_];
                break;
            case 4:
                [button_ setTitle:@"Catalogue" forState:UIControlStateNormal];
                //                [button_ setBackgroundImage:[UIImage imageNamed:@"hmcatalog"] forState:UIControlStateNormal];
//                [button_ addTarget:self action:@selector(method_catalog) forControlEvents:UIControlEventTouchUpInside];
                [view_temp addSubview:button_];
                break;
                
            default:
                break;
        }
        
        
        
        
        
        
        
        
        [self addSubview:view_temp];
        
        float_x += view_temp.frame.size.width;
        
        
    }
    
    
    [self setContentSize:CGSizeMake(float_x, 0)];
    
//    [self method_setupTimer];
}

#pragma mark - Timer
-(void)method_setupTimer
{
    if (![timer_ isValid])
    {
        timer_ = [NSTimer scheduledTimerWithTimeInterval:k_time
                                                  target:self
                                                selector:@selector(method_scrollToNext:)
                                                userInfo:nil
                                                 repeats:YES];
    }
}


-(void)method_cancelTimer
{
    if ([timer_ isValid])
    {
        [timer_ invalidate];
        timer_ = nil;
    }
}


#pragma mark - view alpha variation
-(void)method_setViewColor:(float)alpha_
{
    UIView *view_upperLayer = [self viewWithTag:111];
    view_upperLayer.alpha = alpha_;
    
    
    //    view_upperLayer.backgroundColor = [color_ colorWithAlphaComponent:0.6];
}



#pragma mark - scroller delegates and methods
-(void)method_scrollToNext:(NSTimer *)timer
{
    [self method_setViewColor:0.6];
    
    CGFloat width = self.frame.size.width;
    NSInteger page = (self.contentOffset.x + (0.5f * width)) / width;
    
    
    CGRect frame = self.frame;
    frame.origin.x = frame.size.width * page+1;
    frame.origin.y = 0;
    
    
    //    if (page == 0 || (page <= k_numberOfPage-2))
    //    {
    //        page = page + 1;
    //    }
    //    else if(page == k_numberOfPage-1)
    //    {
    //        page = 0;
    //    }
    
   // page ++;
    
    if (page == 0 || (page <= [self.obj_MarketingSpotDatum.baseMarketingSpotActivityData count]-2))
    {
        page = page + 1;
    }
    else if(page == [self.obj_MarketingSpotDatum.baseMarketingSpotActivityData count]-1)
    {
        page = 0;
    }
    
    
    
    [self setContentOffset:CGPointMake(self.frame.size.width*page, 0) animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView                                               // any offset changes
{
    //    [self method_cancelTimer];
}



// called on start of dragging (may require some time and or distance to move)
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    DLog(@"scrollViewWillBeginDragging");
    
    
    [self method_cancelTimer];
    
    [self method_setViewColor:0.0];
}

// called on finger up if the user dragged. velocity is in points/millisecond. targetContentOffset may be changed to adjust where the scroll view comes to rest

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset NS_AVAILABLE_IOS(5_0)
{
    DLog(@"scrollViewWillEndDragging");
    
    [self method_setupTimer];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    DLog(@"scrollViewDidEndDragging");
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView   // called on finger up as we are moving
{
    DLog(@"scrollViewWillBeginDecelerating");
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView      // called when scroll view grinds to a halt
{
    DLog(@"scrollViewDidEndDecelerating");
}


- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView // called when setContentOffset/scrollRectVisible:animated: finishes. not called if not animating
{
    DLog(@"scrollViewDidEndScrollingAnimation");
    
    [self method_setViewColor:0.0];
}



#pragma mark - scroller start stop
-(void)method_startScroller
{
    self.delegate = self;
    [self method_setupTimer];
}

-(void)method_stopScroller
{
    self.delegate = nil;
    [self method_cancelTimer];
}


@end