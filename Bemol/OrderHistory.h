//
//  OrderHistory.h
//  Bemol
//
//  Created by Fahim Bilwani on 04/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderHistory : NSObject

@property(nonatomic, copy)NSString *date;
@property(nonatomic, copy)NSString *lojaId;
@property(nonatomic, copy)NSString *orderId;
@property(nonatomic, copy)NSString *status;
@property(nonatomic, copy)NSString *total;




@end
