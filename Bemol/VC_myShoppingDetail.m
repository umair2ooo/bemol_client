//
//  VC_myShoppingDetail.m
//  Bemol
//
//  Created by Fahim Bilwani on 01/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_myShoppingDetail.h"
#import "VC_cancelPedido.h"
#import "OrderDetails.h"
#import "NotaFiscal.h"
#import "OrderItemList.h"
#import "Pagamento.h"
#import "VC_cancelprodutos.h"

@interface VC_myShoppingDetail ()

- (IBAction)action_back:(id)sender;
- (IBAction)action_cancelpedido:(id)sender;
- (IBAction)action_email:(id)sender;
- (IBAction)action_cancelproduto:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *button_cancelorProdutos;
@property (weak, nonatomic) IBOutlet UIButton *button_cancelarPedido;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_;
@property (weak, nonatomic) IBOutlet UILabel *label_numerodepedido;
@property (weak, nonatomic) IBOutlet UILabel *label_status;
@property (weak, nonatomic) IBOutlet UILabel *label_date;
@property (weak, nonatomic) IBOutlet UILabel *label_valor;
@property (weak, nonatomic) IBOutlet UILabel *label_loja;
@property (weak, nonatomic) IBOutlet UILabel *label_itemProductName;
@property (weak, nonatomic) IBOutlet UILabel *label_itemProductPrice;
@property (weak, nonatomic) IBOutlet UILabel *label_quantidade;
@property (weak, nonatomic) IBOutlet UILabel *label_itemProductValor;
@property (weak, nonatomic) IBOutlet UILabel *label_notafiscalDanfe;
@property (weak, nonatomic) IBOutlet UILabel *label_valordanotafiscal;
@property (weak, nonatomic) IBOutlet UILabel *lable_formadepagamento;
@property (weak, nonatomic) IBOutlet UILabel *labe_endereco;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_itemproductImage;
@property (weak, nonatomic) IBOutlet UILabel *label_bairro;
@property (weak, nonatomic) IBOutlet UILabel *label_cidade;
@property (weak, nonatomic) IBOutlet UILabel *label_cep;
@property (weak, nonatomic) IBOutlet UILabel *label_Entrega;
@property (weak, nonatomic) IBOutlet UILabel *label_Monatagem;

@property(nonatomic, strong)OrderDetails *obj_orderDetails;
@property(nonatomic, strong)NotaFiscal *obj_notaFiscal;
@property(nonatomic, strong)OrderItemList *obj_orderItemList;
@property(nonatomic, strong)Pagamento *obj_pagamento;
@end

@implementation VC_myShoppingDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Histórico de compras"];
    
    DLog(@"%@",self.obj_List.orderId);
    DLog(@"%d",self.bool_ispedido);
    
    [[ServiceModel sharedClient] POST:k_OrderDetail
                          parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_List.orderId,@"orderId",
                                      self.obj_List.lojaId,@"lojaId",
                                      self.obj_List.status,@"orderStatus",
                                      @"10",@"pageSize",
                                      @"1",@"beginIndex",nil]
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             DLog(@"%@",responseObject);
             self.obj_orderDetails = [OrderDetails instanceFromDictionary:responseObject];
             [self setUIContents];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error.description);
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setUIContents
-(void)setUIContents
{
    self.label_numerodepedido.text = self.obj_orderDetails.numeroDoPedido;
    if ([[self.obj_orderDetails.status substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"S"])
    {
        [self.label_status setTextColor:[UIColor greenColor]];
        self.label_status.text = @"Entregue";
    }
    else if ([[self.obj_orderDetails.status substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"R"])
    {
        [self.label_status setTextColor:[UIColor blueColor]];
        self.label_status.text = @"A caminho";
    }
    if ([[self.obj_orderDetails.status substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"X"])
    {
        [self.label_status setTextColor:[UIColor redColor]];
        self.label_status.text = @"Cancelado";
    }
    self.label_date.text = self.obj_orderDetails.compra;
    if (![self.obj_orderDetails.entrega isKindOfClass:[NSNull class]])
    {
        self.label_Entrega.text = self.obj_orderDetails.entrega;
    }
    else
    {
        self.label_Entrega.text = @"-";
    }
    if (![self.obj_orderDetails.montagem isKindOfClass:[NSNull class]])
    {
        self.label_Monatagem.text = self.obj_orderDetails.montagem;
    }
    else
    {
        self.label_Monatagem.text = @"-";
    }
    
    self.label_valor.text = [NSString stringWithFormat:@"R$ %@",self.obj_orderDetails.valor];
    self.label_loja.text = self.obj_orderDetails.loja;
    
    self.obj_notaFiscal = [self.obj_orderDetails.notaFiscal objectAtIndex:0];
    self.obj_orderItemList = [self.obj_notaFiscal.itemList objectAtIndex:0];
    self.label_itemProductName.text = self.obj_orderItemList.productName;
    self.label_itemProductPrice.text = [NSString stringWithFormat:@"R$ %@",self.obj_orderItemList.unitPrice];
    self.label_quantidade.text = [NSString stringWithFormat:@"%@",self.obj_orderItemList.quantidade];
    self.label_itemProductValor.text = [NSString stringWithFormat:@"R$ %@",self.obj_orderItemList.unitPrice];
    self.label_notafiscalDanfe.text = self.obj_notaFiscal.notaFiscalId;
    self.label_valordanotafiscal.text = [NSString stringWithFormat:@"R$ %@",self.obj_notaFiscal.valorDaNotaFiscal];
    [self.imageview_itemproductImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",k_imageurl,self.obj_orderItemList.productURL]] placeholderImage:[UIImage imageNamed:k_placeHolder]];
    self.labe_endereco.text = self.obj_notaFiscal.endereco;
    self.label_bairro.text = self.obj_notaFiscal.bairro;
    self.label_cep.text = self.obj_notaFiscal.cep;
    self.label_cidade.text = self.obj_notaFiscal.cidade;
    
    self.obj_pagamento = [self.obj_orderDetails.pagamento objectAtIndex:0];
    self.lable_formadepagamento.text = self.obj_pagamento.paymentName;
    
    if (self.obj_orderDetails.cancelableOrder)
        self.button_cancelarPedido.enabled = YES;
    else
        self.button_cancelarPedido.enabled = NO;
    
    if (self.obj_orderItemList.cancelableProduct)
        self.button_cancelorProdutos.enabled = YES;
    else
        self.button_cancelorProdutos.enabled = NO;
    
    [self.scroller_ setContentSize:CGSizeMake(0,self.label_cep.frame.size.height+self.label_cep.frame.origin.y)];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_cancelproduto"])
    {
        VC_cancelprodutos *obj = segue.destinationViewController;
        obj.obj_orderDetails = sender;
        obj.bool_ispedido = self.bool_ispedido;
    }
    else if([segue.identifier isEqualToString:@"segue_cancelpedido"])
    {
        VC_cancelPedido *obj = segue.destinationViewController;
        obj.obj_orderDetails = sender;
        obj.bool_ispedido = self.bool_ispedido;
    }
}

#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_cancelpedido:(id)sender
{
    [self performSegueWithIdentifier:@"segue_cancelpedido" sender:self.obj_orderDetails];
}

- (IBAction)action_email:(id)sender
{
    [[ServiceModel sharedClient] POST:k_sendDanfeEmail
                           parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_List.orderId,@"notaFiscalId",
                                       nil]
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             if ([[responseObject valueForKey:@"valor"] boolValue] == TRUE)
             {
                 [self performSegueWithIdentifier:@"segue_invoice" sender:nil];
             }
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
     }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error.description);
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}

- (IBAction)action_cancelproduto:(id)sender
{
    [self performSegueWithIdentifier:@"segue_cancelproduto" sender:self.obj_orderDetails];
}

@end
