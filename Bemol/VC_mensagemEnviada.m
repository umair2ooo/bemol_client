//
//  VC_mensagemEnviada.m
//  Bemol
//
//  Created by Fahim Bilwani on 27/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "VC_mensagemEnviada.h"

@interface VC_mensagemEnviada ()

- (IBAction)action_back:(id)sender;
- (IBAction)action_dismiss:(id)sender;
@end

@implementation VC_mensagemEnviada

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_dismiss:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
