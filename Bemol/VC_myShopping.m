//
//  VC_myShopping.m
//  Bemol
//
//  Created by Fahim Bilwani on 01/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_myShopping.h"
#import "cell_myShoppingList.h"
#import "MyShopping.h"
#import "ProdutoList.h"
#import "PedidoList.h"
#import "List.h"
#import "VC_myShoppingDetail.h"

@interface VC_myShopping ()
{
    BOOL bool_isPedidos;
}
@property (weak, nonatomic) IBOutlet UIButton *button_produtos;
@property (weak, nonatomic) IBOutlet UIButton *button_pedidos;
@property (weak, nonatomic) IBOutlet UITableView *tableview_;

@property(nonatomic, strong)MyShopping *obj_myShopping;
@property(nonatomic, strong)ProdutoList *obj_produtoList;
@property(nonatomic, strong)PedidoList *obj_pedidoList;
@property(nonatomic, strong)List *obj_List;

- (IBAction)action_back:(id)sender;
- (IBAction)action_selection:(id)sender;
@end

@implementation VC_myShopping

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Minhas compras"];
    [self.button_produtos setSelected:YES];
    [self action_selection:nil];
    
    [[ServiceModel sharedClient] GET:k_OrderHistory
                          parameters:nil
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        if (![responseObject valueForKey:@"errors"])
        {
            self.obj_myShopping = [MyShopping instanceFromDictionary:responseObject];
            if ([self.obj_myShopping.produtoList count])
            {
                self.obj_produtoList = [self.obj_myShopping.produtoList objectAtIndex:0];
            }
            if ([self.obj_myShopping.pedidoList count])
            {
                self.obj_pedidoList = [self.obj_myShopping.pedidoList objectAtIndex:0];
            }
            [self.tableview_ reloadData];
        }
        else
        {
            [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                        valueForKey:@"errorMessage"]];
        }
    }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        DLog(@"%@",error.description);
        //[UtilitiesHelper showAlert:error.description];
        [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_shoppingDetail"])
    {
        VC_myShoppingDetail *obj = segue.destinationViewController;
        obj.obj_List = sender;
        obj.bool_ispedido = bool_isPedidos;
    }
}


#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_selection:(id)sender
{
    if ([sender tag] == 1)
    {
        bool_isPedidos = NO;
        if ([sender isSelected])
        {
            [self.button_produtos setSelected:NO];
            [self.button_pedidos setSelected:YES];
        }
        else
        {
            [self.button_produtos setSelected:YES];
            [self.button_pedidos setSelected:NO];
        }
        [self.tableview_ reloadData];
    }
    else if ([sender tag] == 2)
    {
        bool_isPedidos = YES;
        if ([sender isSelected])
        {
            [self.button_pedidos setSelected:NO];
            [self.button_produtos setSelected:YES];
        }
        else
        {
            [self.button_pedidos setSelected:YES];
            [self.button_produtos setSelected:NO];
        }
        [self.tableview_ reloadData];
    }
}


#pragma mark - tableview delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (bool_isPedidos)
        return [self.obj_pedidoList.list count];
    else
        return [self.obj_produtoList.list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_myShoppingList *cell = (cell_myShoppingList *)[tableView dequeueReusableCellWithIdentifier:@"cell_myShoppingList"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_myShoppingList" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    [UtilitiesHelper setRoundedView:cell.imageview_circle toDiameter:20];
    
    if (bool_isPedidos)
    {
        self.obj_List = [self.obj_pedidoList.list objectAtIndex:indexPath.row];
        cell.label_description.text = [self.obj_List.orderId substringFromIndex:2];
        cell.label_SKU.text = self.obj_List.date;
        cell.label_price.text = [NSString stringWithFormat:@"R$ %@",self.obj_List.total];
    }
    else
    {
        self.obj_List = [self.obj_produtoList.list objectAtIndex:indexPath.row];
        cell.label_description.text = self.obj_List.descriptionText;
        cell.label_SKU.text = [NSString stringWithFormat:@"SKU: %@",self.obj_List.sku];
        cell.label_price.text = [NSString stringWithFormat:@"R$ %@",self.obj_List.total];
    }
    
    DLog(@"%@",[self.obj_List.status substringWithRange:NSMakeRange(0, 1)]);
    if ([[self.obj_List.status substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"S"])
    {
        [cell.label_status setTextColor:[UtilitiesHelper colorFromHexString:@"#298b0f"]];
        cell.label_status.text = @"Entregue";
        [cell.imageview_circle setBackgroundColor:[UtilitiesHelper colorFromHexString:@"#298b0f"]];
        //[cell.imageview_circle setImage:[UIImage imageNamed:@"dotcircle3"]];
    }
    else if ([[self.obj_List.status substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"R"])
    {
        [cell.label_status setTextColor:[UtilitiesHelper colorFromHexString:@"#165c95"]];
        cell.label_status.text = @"A caminho";
        [cell.imageview_circle setBackgroundColor:[UtilitiesHelper colorFromHexString:@"#165c95"]];
       // [cell.imageview_circle setImage:[UIImage imageNamed:@"dotcircle1"]];
    }
    if ([[self.obj_List.status substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"X"])
    {
        [cell.label_status setTextColor:[UtilitiesHelper colorFromHexString:@"#d82502"]];
        cell.label_status.text = @"Cancelado";
        [cell.imageview_circle setBackgroundColor:[UtilitiesHelper colorFromHexString:@"#d82502"]];
        //[cell.imageview_circle setImage:[UIImage imageNamed:@"dotcircle2"]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (bool_isPedidos)
    {
        self.obj_List = [self.obj_pedidoList.list objectAtIndex:indexPath.row];
    }
    else
    {
        self.obj_List = [self.obj_produtoList.list objectAtIndex:indexPath.row];
    }
    
    [self performSegueWithIdentifier:@"segue_shoppingDetail" sender:self.obj_List];
}

@end
