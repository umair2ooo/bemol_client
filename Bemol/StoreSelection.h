#import <Foundation/Foundation.h>

@interface StoreSelection : NSObject {

    NSNumber *physicalStoreCount;
    NSMutableArray *physicalStoreList;

}

@property (nonatomic, copy) NSNumber *physicalStoreCount;
@property (nonatomic, copy) NSMutableArray *physicalStoreList;

+ (StoreSelection *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
