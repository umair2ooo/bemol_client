//
//  cell_Search.h
//  Bemol
//
//  Created by Fahim Bilwani on 20/08/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cell_Search : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label_title;
@property (weak, nonatomic) IBOutlet UILabel *label_subtitle;
@property (weak, nonatomic) IBOutlet UIImageView *image_searchList;
@end
