//
//  VC_cartaoBemol.m
//  Bemol
//
//  Created by Fahim Bilwani on 14/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "VC_cartaoBemol.h"
#import "CartaoBemol.h"
#import "CartaoBemolCartaoBemolMap.h"

@interface VC_cartaoBemol ()

@property(nonatomic, strong) CartaoBemol *obj_CartaoBemol;
@property(nonatomic, strong) CartaoBemolCartaoBemolMap *obj_cartaoBemolCartaoBemolMapp;
@property (weak, nonatomic) IBOutlet UILabel *label_CEP;
@property (weak, nonatomic) IBOutlet UILabel *label_validDate;
@property (weak, nonatomic) IBOutlet UILabel *label_name;
@property (weak, nonatomic) IBOutlet UILabel *label_viaCartao;
@property (weak, nonatomic) IBOutlet UILabel *label_birthyear;
@property (weak, nonatomic) IBOutlet UILabel *label_numberState;

- (IBAction)action_cancel:(id)sender;
@end

@implementation VC_cartaoBemol

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    
    if (ApplicationDelegate.single.obj_CartaoBemol)
    {
        self.obj_cartaoBemolCartaoBemolMapp = ApplicationDelegate.single.obj_CartaoBemol.cartaoBemolMap;
        self.label_CEP.text = self.obj_cartaoBemolCartaoBemolMapp.cEP;
        self.label_name.text = self.obj_cartaoBemolCartaoBemolMapp.name;
        self.label_validDate.text = [NSString stringWithFormat:@"VÁLIDO ATÉ %@",self.obj_cartaoBemolCartaoBemolMapp.vALIDOATE];
        self.label_viaCartao.text = self.obj_cartaoBemolCartaoBemolMapp.viaCartao;
        self.label_birthyear.text = self.obj_cartaoBemolCartaoBemolMapp.birthYear;
        self.label_numberState.text = [NSString stringWithFormat:@"%@-%@",self.obj_cartaoBemolCartaoBemolMapp.number,self.obj_cartaoBemolCartaoBemolMapp.state];
    }
    else
    {
        //k_cartaoBemol
        [[ServiceModel sharedClient] GET:k_cartaoBemol
                              parameters:nil
                                  onView:self.view
                                 success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             if (![responseObject valueForKey:@"errors"])
             {
                 ApplicationDelegate.single.obj_CartaoBemol = [CartaoBemol instanceFromDictionary:responseObject];
                 self.obj_cartaoBemolCartaoBemolMapp = ApplicationDelegate.single.obj_CartaoBemol.cartaoBemolMap;
                 self.label_CEP.text = self.obj_cartaoBemolCartaoBemolMapp.cEP;
                 self.label_name.text = self.obj_cartaoBemolCartaoBemolMapp.name;
                 self.label_validDate.text = [NSString stringWithFormat:@"VÁLIDO ATÉ %@",self.obj_cartaoBemolCartaoBemolMapp.vALIDOATE];
                 self.label_viaCartao.text = self.obj_cartaoBemolCartaoBemolMapp.viaCartao;
                 self.label_birthyear.text = self.obj_cartaoBemolCartaoBemolMapp.birthYear;
                 self.label_numberState.text = [NSString stringWithFormat:@"%@-%@",self.obj_cartaoBemolCartaoBemolMapp.mobile,self.obj_cartaoBemolCartaoBemolMapp.state];
             }
             else
             {
                 [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                             valueForKey:@"errorMessage"]];
             }
         }
                                 failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             DLog(@"%@",error.description);
             //[UtilitiesHelper showAlert:error.description];
             [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
         }];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Navigation
- (IBAction)action_cancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
