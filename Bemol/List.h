#import <Foundation/Foundation.h>

@interface List : NSObject {

    NSString *date;
    NSString *descriptionText;
    NSString *lojaId;
    NSString *orderId;
    NSString *sku;
    NSString *status;
    NSNumber *total;

}

@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *descriptionText;
@property (nonatomic, copy) NSString *lojaId;
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *sku;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSNumber *total;

+ (List *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
