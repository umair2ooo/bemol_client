#import "AdditionalstoresAdditionalStoreMap.h"

#import "PhysicalStoreList.h"

@implementation AdditionalstoresAdditionalStoreMap

@synthesize physicalStoreList;

+ (AdditionalstoresAdditionalStoreMap *)instanceFromDictionary:(NSDictionary *)aDictionary {

    AdditionalstoresAdditionalStoreMap *instance = [[AdditionalstoresAdditionalStoreMap alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }


    NSArray *receivedPhysicalStoreList = [aDictionary objectForKey:@"physicalStoreList"];
    if ([receivedPhysicalStoreList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedPhysicalStoreList = [NSMutableArray arrayWithCapacity:[receivedPhysicalStoreList count]];
        for (NSDictionary *item in receivedPhysicalStoreList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedPhysicalStoreList addObject:[PhysicalStoreList instanceFromDictionary:item]];
            }
        }

        self.physicalStoreList = populatedPhysicalStoreList;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.physicalStoreList) {
        [dictionary setObject:self.physicalStoreList forKey:@"physicalStoreList"];
    }

    return dictionary;

}


@end
