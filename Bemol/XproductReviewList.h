#import <Foundation/Foundation.h>

@interface XproductReviewList : NSObject {

    NSNumber *Estrelas1;
    NSNumber *Estrelas2;
    NSNumber *Estrelas3;
    NSNumber *Estrelas4;
    NSNumber *Estrelas5;
    NSNumber *avgRate;
    NSMutableArray *reviewList;
    NSNumber *totalReviews;

}

@property (nonatomic, copy) NSNumber *Estrelas1;
@property (nonatomic, copy) NSNumber *Estrelas2;
@property (nonatomic, copy) NSNumber *Estrelas3;
@property (nonatomic, copy) NSNumber *Estrelas4;
@property (nonatomic, copy) NSNumber *Estrelas5;
@property (nonatomic, copy) NSNumber *avgRate;
@property (nonatomic, copy) NSMutableArray *reviewList;
@property (nonatomic, copy) NSNumber *totalReviews;

+ (XproductReviewList *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
