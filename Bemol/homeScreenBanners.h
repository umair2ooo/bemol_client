#import <Foundation/Foundation.h>

@interface HomeScreenBanners : NSObject {
    NSMutableArray *marketingSpotData;
    NSString *resourceId;
    NSString *resourceName;
}

@property (nonatomic, copy) NSMutableArray *marketingSpotData;
@property (nonatomic, copy) NSString *resourceId;
@property (nonatomic, copy) NSString *resourceName;

+ (HomeScreenBanners *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end