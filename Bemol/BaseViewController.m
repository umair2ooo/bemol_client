#import "BaseViewController.h"

#import "VC_login.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}


-(void)viewWillAppear:(BOOL)animated
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:k_showLoginNotification
//                                                  object:nil];
//    
//    DLog(@"Notification removed");
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(receiveTestNotification:)
//                                                 name:k_showLoginNotification
//                                               object:nil];
//    
//    DLog(@"Notification added");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Back
-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - dissmiss
-(IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:k_showLoginNotification])
    {
        NSLog (@"Successfully received the test notification!");
        
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_WCToken];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_WCTrustedToken];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_personalizationID];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_userId];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:k_isLogin];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:k_count];
        
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        self.navigationController.navigationItem.rightBarButtonItem.title = @"Faça seu login aqui";
        
        ApplicationDelegate.single.dic_userDetails = nil;
        
        
//        UINavigationController *obj_login = [self.storyboard instantiateViewControllerWithIdentifier:@"nav_logIn"];
//        ApplicationDelegate.single.bool_isComingfromCart = NO;
//        [self presentViewController:obj_login animated:YES completion:nil];
        
        [self method_guestLogin];
    }
}



#pragma mark - guestLogin
-(void)method_guestLogin
{
    [[ServiceModel sharedClient] POST:k_guestLogin
                           parameters:nil
                               onView:nil
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (responseObject && ![responseObject valueForKey:@"errors"])
         {
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_WCToken] forKey:k_WCToken];
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_WCTrustedToken] forKey:k_WCTrustedToken];
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_personalizationID
                                                               ] forKey:k_personalizationID];
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_userId] forKey:k_userId];
             

             [[NSUserDefaults standardUserDefaults] synchronize];
             
             DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_userId]);
             DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken]);
             DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken]);
             DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_personalizationID]);
             
             if (![[responseObject valueForKey:k_userId] isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:k_userId]])
             {
                 [[NSUserDefaults standardUserDefaults] setValue:nil forKey:k_count];
             }
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
         }
     }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
     }];
}

@end