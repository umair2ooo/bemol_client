#import <Foundation/Foundation.h>

@interface Pagamento : NSObject {

    NSNumber *paymentAmount;
    NSString *paymentName;

}

@property (nonatomic, copy) NSNumber *paymentAmount;
@property (nonatomic, copy) NSString *paymentName;

+ (Pagamento *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
