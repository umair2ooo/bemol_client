#import "ContractsContractsMap.h"

#import "ContractsList.h"

@implementation ContractsContractsMap

@synthesize list;

+ (ContractsContractsMap *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ContractsContractsMap *instance = [[ContractsContractsMap alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }


    NSArray *receivedList = [aDictionary objectForKey:@"list"];
    if ([receivedList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedList = [NSMutableArray arrayWithCapacity:[receivedList count]];
        for (NSDictionary *item in receivedList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedList addObject:[ContractsList instanceFromDictionary:item]];
            }
        }

        self.list = populatedList;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.list) {
        [dictionary setObject:self.list forKey:@"list"];
    }

    return dictionary;

}


@end
