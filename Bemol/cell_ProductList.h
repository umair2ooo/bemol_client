//
//  cell_ProductList.h
//  CaptureLife
//
//  Created by Fahim Bilwani on 4/27/15.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cell_ProductList : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label_name;
@property (weak, nonatomic) IBOutlet UILabel *lable_Price;
@property (weak, nonatomic) IBOutlet UIImageView *image_product;

-(void)method_setValues:(NSDictionary *)dic;
@end
