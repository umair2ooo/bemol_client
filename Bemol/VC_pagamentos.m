//
//  VC_pagamentos.m
//  Bemol
//
//  Created by Fahim Bilwani on 27/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "VC_pagamentos.h"
#import "cell_pagamentos.h"
#import "Pagamentos.h"
#import "PagamentosList.h"
#import "VC_contracts.h"

@interface VC_pagamentos ()

@property (weak, nonatomic) IBOutlet UITableView *tableview_;

@property (nonatomic,strong) Pagamentos *obj_Pagamentos;
@property (nonatomic,strong) PagamentosList *obj_PagamentosList;
@property (nonatomic,strong) NSString *string_Total;

- (IBAction)action_back:(id)sender;
@end

@implementation VC_pagamentos

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self.navigationItem setTitle:@"Pagamentos"];
    
    [[ServiceModel sharedClient] GET:k_pagamentos
                          parameters:nil
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                 if (![responseObject valueForKey:@"errors"])
                                 {
                                     self.obj_Pagamentos = [Pagamentos instanceFromDictionary:responseObject];
                                     [self method_calculateTotal];
                                     [self.tableview_ reloadData];
                                 }
                                 else
                                 {
                                     [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                                                 valueForKey:@"errorMessage"]];
                                 }
                             }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                 [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
                             }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_contracts"])
    {
        VC_contracts *obj = segue.destinationViewController;
        obj.dic_details = sender;
    }
}


#pragma mark - tableview delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.obj_Pagamentos.list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_pagamentos *cell = (cell_pagamentos *)[tableView dequeueReusableCellWithIdentifier:@"cell_pagamentos"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_pagamentos" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    self.obj_PagamentosList = [self.obj_Pagamentos.list objectAtIndex:indexPath.row];
    
    [cell method_setValues:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_PagamentosList.dateVencimiento,@"date", self.obj_PagamentosList.valor,@"amount",self.obj_PagamentosList.status,@"Status",nil]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.obj_PagamentosList = [self.obj_Pagamentos.list objectAtIndex:indexPath.row];
    DLog(@"%@",self.string_Total);
    [self performSegueWithIdentifier:@"segue_contracts" sender:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_PagamentosList.contractNumber,@"contractNumber",self.string_Total,@"total", nil]];
}


#pragma mark - calculate total price
-(void)method_calculateTotal
{
    if ([self.obj_Pagamentos.list count])
    {
        __block float float_totalPrice = 0.0;
        
        [self.obj_Pagamentos.list enumerateObjectsUsingBlock:^(PagamentosList *obj, NSUInteger idx, BOOL *stop)
         {
             float_totalPrice = float_totalPrice + [obj.valor floatValue];
             
             if (idx == [self.obj_Pagamentos.list count]-1)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     self.string_Total = [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%.2f",float_totalPrice]];
                 });
             }
         }];
    }
    else
    {
        self.string_Total = @"R$0";
    }
}
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
