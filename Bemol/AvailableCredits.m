#import "AvailableCredits.h"

@implementation AvailableCredits

@synthesize limite;
@synthesize limiteDiff;
@synthesize limiteTotal;

+ (AvailableCredits *)instanceFromDictionary:(NSDictionary *)aDictionary {

    AvailableCredits *instance = [[AvailableCredits alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.limite = [aDictionary objectForKey:@"limite"];
    self.limiteDiff = [aDictionary objectForKey:@"limiteDiff"];
    self.limiteTotal = [aDictionary objectForKey:@"limiteTotal"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.limite) {
        [dictionary setObject:self.limite forKey:@"limite"];
    }

    if (self.limiteDiff) {
        [dictionary setObject:self.limiteDiff forKey:@"limiteDiff"];
    }

    if (self.limiteTotal) {
        [dictionary setObject:self.limiteTotal forKey:@"limiteTotal"];
    }

    return dictionary;

}


@end
