//
//  VC_myShoppingDetail.h
//  Bemol
//
//  Created by Fahim Bilwani on 01/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "List.h"

@interface VC_myShoppingDetail : BaseViewController
@property(nonatomic, strong)List *obj_List;
@property(nonatomic) BOOL bool_ispedido;
@end
