
//  UtilitiesHelper.m
//
//  Created by Muhammad Waqas Khalid on 8/29/13.
//
//

#import "UtilitiesHelper.h"
#import "Reachability.h"
//#import "BlockAlertView.h"
#import <CoreLocation/CLGeocoder.h>
#import <CoreLocation/CLPlacemark.h>
#import <Mapkit/MKDirectionsRequest.h>
#import <MapKit/MKDirections.h>

@implementation UtilitiesHelper



+ (id)shareUtitlities
{
    static dispatch_once_t once;
    static UtilitiesHelper *shareUtilities;
    dispatch_once(&once, ^ { shareUtilities = [[self alloc] init];  });
    return shareUtilities;
}

+ (id)getUserDefaultForKey:(NSString*)key
{
    
    return [[NSUserDefaults standardUserDefaults] valueForKey:key];
    
}


+ (void)setUserDefaultForKey:(NSString*)key  value:(NSString*)value
{
   NSUserDefaults *usrDef =    [NSUserDefaults standardUserDefaults];
   [usrDef setObject:value forKey:key];
   [usrDef synchronize];
}


+ (id)returnUserDefaultForKey:(NSString*)key
{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
}


+(void)showLoader:(NSString *)title forView:(UIView *)view  setMode:(MBProgressHUDMode)mode delegate:(id)vwDelegate
{
    // If view is nil dont show loader.
    
    if (view) {
        MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
        //progressHUD.mode = MBProgressHUDModeAnnularDeterminate;
        
        UITapGestureRecognizer *HUDSingleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTap:)];
        [progressHUD addGestureRecognizer:HUDSingleTap];
        
        [progressHUD setMode:mode];
        [progressHUD setDimBackground:YES];
        [progressHUD setLabelText:title];
        [progressHUD setMinShowTime:1.0];
    }
    
}

+ (void)singleTap:(UITapGestureRecognizer*)sender
{
    DLog(@"loader clicked");
    [[ServiceModel sharedClient] cancelOperation:sender.view];
}

+(void)hideLoader:(UIView *)forView
{
    [MBProgressHUD hideAllHUDsForView:forView animated:YES];

//    dispatch_async(dispatch_get_main_queue(), ^{
//        [MBProgressHUD hideAllHUDsForView:forView animated:YES];
//    });
    
}


+(NSString *) addSuffixToNumber:(NSInteger) number
{
    NSString *suffix;
    int ones = number % 10;
    int temp = floor(number/10.0);
    int tens = temp%10;
    
    if (tens ==1) {
        suffix = @"th";
    } else if (ones ==1){
        suffix = @"st";
    } else if (ones ==2){
        suffix = @"nd";
    } else if (ones ==3){
        suffix = @"rd";
    } else {
        suffix = @"th";
    }
    
    NSString *completeAsString = [NSString stringWithFormat:@"%ld%@",(long)number,suffix];
    return completeAsString;
}




//^\+?\d+(-\d+)*$

+(BOOL) checkForEmptySpaces : (UITextField *)textField  {
 
    
    NSString *rawString = textField.text;
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    if ([trimmed length] == 0) {
//        textField.text = textField.placeholder;
//        textField.textColor = [UIColor redColor];
        // Text was empty or only whitespace.
    
        textField.text = @"";
        return NO;
    }
    
    return YES;
    
}

+(BOOL) checkForEmptySpacesInString : (NSString *)rawString  {
    
    
       NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    if ([trimmed length] == 0) {
        return NO;
    }
    return YES;
    
}
+(BOOL) checkForEmptySpacesInTextView : (UITextView *)textView  {
    
    
    NSString *rawString = textView.text;
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    if ([trimmed length] == 0) {
        // Text was empty or only whitespace.
        
        textView.text = @"";
        return NO;
    }
    
    return YES;
    
}

+ (BOOL)checkLocationCoordinate:(CLLocationCoordinate2D)coordinate
{
    if (CLLocationCoordinate2DIsValid(coordinate) && (coordinate.latitude != 0 || coordinate.longitude !=0)) {
        return YES;
    }
    
    return NO;
}

+(void)showPromptAlertwithMessage:(NSString *)message forDelegate:(id)deleg {
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Message" message:message delegate:deleg cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];

    [alertView show];
}



+(void)showPromptAlertforTitle:(NSString *)title withMessage:(NSString *)message forDelegate:(id)deleg
{
    
    [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];

//    BlockAlertView  *alertBlock = [[BlockAlertView alloc]initWithTitle:title message:message];
//    [alertBlock setCancelButtonWithTitle:@"Ok" block:^{
//        
//    }];
//    [alertBlock show];
    
    
}
//
//+(void)showYesNoPromptAlertforTitle:(NSString *)title withMessage:(NSString *)message block:(void(^)(BOOL))block {
//    
//    
//    BlockAlertView  *alertBlock = [[BlockAlertView alloc]initWithTitle:title message:message];
//    [alertBlock setCancelButtonWithTitle:@"No" block:^{
//        
//        block(NO);
//        
//    }];
//    
//    [alertBlock addButtonWithTitle:@"YES" block:^{
//       
//        block(YES);
//        
//    }];
//    
//    [alertBlock show];
//    
//    
//}


+(BOOL) validateUsername:(NSString*)checkUsername {
    
    NSString *regex = @"^[a-z0-9_-]{5,15}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicate evaluateWithObject:checkUsername];
    
}


+(BOOL) validateEmail:(NSString *)checkEmail
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkEmail];
}

+(BOOL) validateFullName:(NSString *)checkFullName
{
    NSString *regex = @"[a-zA-Z]{2,}+(\\s{1}[a-zA-Z]{2,}+)+";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:checkFullName];
}
+(BOOL) checkAlphabets:(NSString *)text
{
    NSString *regex = @"[a-zA-Z][a-zA-Z ]+";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:text];
}
+(BOOL) checkFaxNumber:(NSString *)text
{
    NSString *regex = @"^[0-9\\-\\+]{6,12}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:text];
}

///[\+? *[1-9]+]?[0-9 ]+/

+(BOOL) checkPhoneNumber:(NSString *)text
{
    NSString *regex = @"^[0-9\\-\\+]{9,15}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:text];
}
+(BOOL) checkPassword:(NSString *)text
{
//    NSString *regex = @"^\\w*(?=\\w*\\d)(?=\\w*[a-z])(?=\\w*[A-Z])\\w*$";
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
//    return [predicate evaluateWithObject:text];
    
    
//    NSCharacterSet *upperCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
//    NSCharacterSet *lowerCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyz"];
    
    //NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    if ( [text length]<6 || [text length]>20 )
        return NO;  // too long or too short
    NSRange rang;
    rang = [text rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
    if ( !rang.length )
        return NO;  // no letter
    rang = [text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
    if ( !rang.length )
        return NO;  // no number;
//    rang = [text rangeOfCharacterFromSet:upperCaseChars];
//    if ( !rang.length )
//        return NO;  // no uppercase letter;
//    rang = [text rangeOfCharacterFromSet:lowerCaseChars];
//    if ( !rang.length )
//        return NO;  // no lowerCase Chars;
    return YES;
}

//+(NSURL *)imageURLMaker :(NSString *)imgUrl
//{
//    
//  return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebBaseUrl,imgUrl]];
//    
//}

+(BOOL)isReachable{

        Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
        if (networkStatus == NotReachable) {
            
             [UtilitiesHelper showPromptAlertforTitle:@"Message" withMessage:@"Please check your network connection and try again." forDelegate:nil];
            return NO;
        } else {
            
           
            return YES;
            
        }
    
}

+ (id)loadNibNamed:(NSString *)nibName ofClass:(Class)objClass {
    if (nibName && objClass) {
        NSArray *objects = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
        
        for (id currentObject in objects ){
            if ([currentObject isKindOfClass:objClass])
                return currentObject;
        }
    }
    
    return nil;
}

+(void)writeJsonToFile:(id)responseString withFileName:(NSString*)fileName
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, fileName];
    NSLog(@"filePath %@", filePath);

    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) { // if file is not exist, create it.
         NSError *error;
        [responseString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
        
    }
    else
    {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath: filePath error: &error];

        [responseString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    }
    
}
+(id)readJsonFromFile:(NSString*)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
    {
//        NSString *myPathInfo = [[NSBundle mainBundle] pathForResource:@"myfile" ofType:@"txt"];
//        NSFileManager *fileManager = [NSFileManager defaultManager];
//        [fileManager copyItemAtPath:myPathInfo toPath:myPathDocs error:NULL];
        return NULL;
    }
    
    //Load from File
    NSString *myString = [[NSString alloc] initWithContentsOfFile:myPathDocs encoding:NSUTF8StringEncoding error:NULL];
    NSLog(@"string ===> %@",myString);
    return myString;
}

+ (UIImage*) maskImage:(UIImage *)image withOverlayMask:(UIImage *) maskImage
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    //    UIImage *maskImage = [UIImage imageNamed:@"mask.png"];
    CGImageRef maskImageRef = [maskImage CGImage];
    
    // create a bitmap graphics context the size of the image
    CGContextRef mainViewContentContext = CGBitmapContextCreate (NULL, maskImage.size.width, maskImage.size.height, 8, 0, colorSpace, kCGImageAlphaPremultipliedLast);
    
    
    if (mainViewContentContext==NULL)
        return NULL;
    
    CGFloat ratio = 0;
    
    ratio = maskImage.size.width/ image.size.width;
    
    if(ratio * image.size.height < maskImage.size.height) {
        ratio = maskImage.size.height/ image.size.height;
    }
    
    CGRect rect1  = {{0, 0}, {maskImage.size.width, maskImage.size.height}};
    CGRect rect2  = {{-((image.size.width*ratio)-maskImage.size.width)/2 , -((image.size.height*ratio)-maskImage.size.height)/2}, {image.size.width*ratio, image.size.height*ratio}};
    
    
    CGContextClipToMask(mainViewContentContext, rect1, maskImageRef);
    CGContextDrawImage(mainViewContentContext, rect2, image.CGImage);
    
    
    // Create CGImageRef of the main view bitmap content, and then
    // release that bitmap context
    CGImageRef newImage = CGBitmapContextCreateImage(mainViewContentContext);
    CGContextRelease(mainViewContentContext);
    
    UIImage *theImage = [UIImage imageWithCGImage:newImage];
    
    CGImageRelease(newImage);
    
    // return the image
    return theImage;
}

+ (void)getAddressFromLocation:(CLLocation *)location completionHandler:(void(^)(NSString *locationAddress))completionHandler
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {

        NSString *locString;
        if (!error) {
            if (placemarks && [placemarks count]) {
                CLPlacemark *placemark = [placemarks lastObject];
                
                if (placemark.name) {
                    locString = placemark.name;
                } else if (placemark.thoroughfare) {
                    locString = placemark.thoroughfare;
                } else if (placemark.locality) {
                    locString = placemark.locality;
                } else if (placemark.administrativeArea) {
                    locString = placemark.administrativeArea;
                }
            }
        }
        
        completionHandler(locString);
    }];
}

+ (void)getRouteFrom:(CLLocationCoordinate2D)source To:(CLLocationCoordinate2D)destination CompletionHandler:(void(^)(MKDirectionsResponse *response, NSError *error))completionHandler
{
    MKDirectionsRequest *directionsRequest = [[MKDirectionsRequest alloc] init];
    
    MKPlacemark *srcPlacemark = [[MKPlacemark alloc] initWithCoordinate:source addressDictionary:nil];
    MKPlacemark *dstPlacemark = [[MKPlacemark alloc] initWithCoordinate:destination addressDictionary:nil];
    
    [directionsRequest setSource:[[MKMapItem alloc] initWithPlacemark:srcPlacemark]];
    [directionsRequest setDestination:[[MKMapItem alloc] initWithPlacemark:dstPlacemark]];
    directionsRequest.transportType = MKDirectionsTransportTypeAutomobile;
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:directionsRequest];
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        
        completionHandler(response, error);
    }];
}


+(void) makeViewWithRoundedCornersModified:(UIView *) view radius:(double)newSize {
    
    [view.layer setMasksToBounds:YES];
    [view setClipsToBounds:YES];
    
    CGPoint saveCenter = view.center;
    CGRect newFrame = CGRectMake(view.frame.origin.x, view.frame.origin.y, newSize, newSize);
    view.frame = newFrame;
    view.layer.cornerRadius = newSize / 2.0;
    view.center = saveCenter;
    
}

+ (UIImage *)scaleAndRotateImage:(UIImage *)image {
    int kMaxResolution = 2000;
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

+(void)showAlert:(NSString *)message {
    
    [UtilitiesHelper showPromptAlertforTitle:nil withMessage:message forDelegate:nil];
    
}

+(NSString *)method_currencyFormatter:(NSString *)amount
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSLocale *localizableIdentifier = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    [formatter setLocale:localizableIdentifier];
    float money = [amount floatValue];
    NSNumber *meuDinheiro = [NSNumber numberWithFloat:money];
    
    return [formatter stringFromNumber:meuDinheiro];
}

+ (NSString *)getIPAddress
{
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+ (void)setRoundedView:(UIImageView *)roundedView toDiameter:(float)newSize
{
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
}

+ (NSString *)method_RemoveLeadingZeros:(NSString *)stringValues
{
    NSRange range = [stringValues rangeOfString:@"^0*" options:NSRegularExpressionSearch];
    stringValues= [stringValues stringByReplacingCharactersInRange:range withString:@""];
    NSLog(@"str %@",stringValues);
    return stringValues;
}

@end

