#import "Contracts.h"

#import "ContractsContractsMap.h"

@implementation Contracts

@synthesize contractsMap;

+ (Contracts *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Contracts *instance = [[Contracts alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.contractsMap = [ContractsContractsMap instanceFromDictionary:[aDictionary objectForKey:@"contractsMap"]];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.contractsMap) {
        [dictionary setObject:self.contractsMap forKey:@"contractsMap"];
    }

    return dictionary;

}


@end
