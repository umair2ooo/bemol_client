#import "UsableShippingChargePolicy.h"

@implementation UsableShippingChargePolicy

@synthesize name;
@synthesize type;
@synthesize uniqueID;

+ (UsableShippingChargePolicy *)instanceFromDictionary:(NSDictionary *)aDictionary {

    UsableShippingChargePolicy *instance = [[UsableShippingChargePolicy alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.name = [aDictionary objectForKey:@"name"];
    self.type = [aDictionary objectForKey:@"type"];
    self.uniqueID = [aDictionary objectForKey:@"uniqueID"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.name) {
        [dictionary setObject:self.name forKey:@"name"];
    }

    if (self.type) {
        [dictionary setObject:self.type forKey:@"type"];
    }

    if (self.uniqueID) {
        [dictionary setObject:self.uniqueID forKey:@"uniqueID"];
    }

    return dictionary;

}

@end
