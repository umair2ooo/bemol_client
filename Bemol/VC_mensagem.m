//
//  VC_mensagem.m
//  Bemol
//
//  Created by Fahim Bilwani on 27/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "VC_mensagem.h"

@interface VC_mensagem ()

- (IBAction)action_back:(id)sender;
- (IBAction)action_message:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *textfield_message;
@end

@implementation VC_mensagem

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Fale Conosco"];
    
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_message:(id)sender
{
    [[ServiceModel sharedClient] POST:k_faleConosco
                           parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.textfield_message.text,@"message", nil]
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             if ([responseObject valueForKey:@"dataMap"])
             {
                 [UtilitiesHelper showAlert:[[responseObject valueForKey:@"dataMap"] valueForKey:@"StatusMessage"]
                                             ];
                 [self performSegueWithIdentifier:@"segue_faleconscoMessage" sender:nil];
             }
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}
@end
