#import <Foundation/Foundation.h>

@interface UsableShippingChargePolicy : NSObject {
    NSString *name;
    NSString *type;
    NSString *uniqueID;
}

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *uniqueID;

+ (UsableShippingChargePolicy *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end