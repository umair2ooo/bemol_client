#import <Foundation/Foundation.h>

@interface OrderItem : NSObject {
    NSString *contractId;
    NSString *correlationGroup;
    NSString *createDate;
    NSString *currency;
    NSString *freeGift;
    NSString *fulfillmentCenterId;
    NSString *fulfillmentCenterName;
    NSString *lastUpdateDate;
    NSString *offerID;
    NSString *orderItemFulfillmentStatus;
    NSString *orderItemId;
    NSString *orderItemInventoryStatus;
    NSString *orderItemPrice;
    NSString *orderItemStatus;
    NSString *partNumber;
    NSString *productId;
    NSString *productUrl;
    NSString *quantity;
    NSString *salesTax;
    NSString *salesTaxCurrency;
    NSString *shippingCharge;
    NSString *shippingChargeCurrency;
    NSString *shippingTax;
    NSString *shippingTaxCurrency;
    NSString *unitPrice;
    NSString *productShortDescription;
    NSString *productThumbnail;
    NSString *unitQuantity;
    NSString *unitUom;
    NSString *uOM;
    NSArray *usableShippingChargePolicy;
}

@property (nonatomic, copy) NSString *contractId;
@property (nonatomic, copy) NSString *correlationGroup;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSString *freeGift;
@property (nonatomic, copy) NSString *fulfillmentCenterId;
@property (nonatomic, copy) NSString *fulfillmentCenterName;
@property (nonatomic, copy) NSString *lastUpdateDate;
@property (nonatomic, copy) NSString *offerID;
@property (nonatomic, copy) NSString *orderItemFulfillmentStatus;
@property (nonatomic, copy) NSString *orderItemId;
@property (nonatomic, copy) NSString *orderItemInventoryStatus;
@property (nonatomic, copy) NSString *orderItemPrice;
@property (nonatomic, copy) NSString *orderItemStatus;
@property (nonatomic, copy) NSString *partNumber;
@property (nonatomic, copy) NSString *productId;
@property (nonatomic, copy) NSString *productUrl;
@property (nonatomic, copy) NSString *quantity;
@property (nonatomic, copy) NSString *salesTax;
@property (nonatomic, copy) NSString *salesTaxCurrency;
@property (nonatomic, copy) NSString *shippingCharge;
@property (nonatomic, copy) NSString *shippingChargeCurrency;
@property (nonatomic, copy) NSString *shippingTax;
@property (nonatomic, copy) NSString *shippingTaxCurrency;
@property (nonatomic, copy) NSString *unitPrice;
@property (nonatomic, copy) NSString *productShortDescription;
@property (nonatomic, copy) NSString *productThumbnail;
@property (nonatomic, copy) NSString *unitQuantity;
@property (nonatomic, copy) NSString *unitUom;
@property (nonatomic, copy) NSString *uOM;
@property (nonatomic, copy) NSArray *usableShippingChargePolicy;

+ (OrderItem *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
