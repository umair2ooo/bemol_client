#import <Foundation/Foundation.h>

@interface PedidoList : NSObject {

    NSNumber *countTotal;
    NSMutableArray *list;

}

@property (nonatomic, copy) NSNumber *countTotal;
@property (nonatomic, copy) NSMutableArray *list;

+ (PedidoList *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
