#import <Foundation/Foundation.h>

@interface DiscoverRegistradoCheckout : NSObject {

    BOOL featured;
    NSNumber *installmentAmount;
    NSNumber *interestRate;
    NSNumber *numberOfInstallment;
    NSNumber *totalAmout;

}

@property (nonatomic, assign) BOOL featured;
@property (nonatomic, copy) NSNumber *installmentAmount;
@property (nonatomic, copy) NSNumber *interestRate;
@property (nonatomic, copy) NSNumber *numberOfInstallment;
@property (nonatomic, copy) NSNumber *totalAmout;

+ (DiscoverRegistradoCheckout *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
