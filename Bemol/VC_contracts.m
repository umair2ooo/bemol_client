//
//  VC_contracts.m
//  Bemol
//
//  Created by Fahim Bilwani on 13/11/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "VC_contracts.h"
#import "cell_contracts.h"
#import "Contracts.h"
#import "ContractsList.h"
#import "ContractsContractsMap.h"


@interface VC_contracts ()<UIPickerViewDataSource,UIPickerViewDelegate>
{
    NSInteger int_selectedIndex;
    UIPickerView *pickerView;
}

@property (nonatomic,strong) Contracts *obj_Contracts;
@property (nonatomic,strong) ContractsList *obj_ContractsList;

@property (weak, nonatomic) IBOutlet UILabel *label_Contrato;
@property (weak, nonatomic) IBOutlet UITableView *tableview_;
@property (weak, nonatomic) IBOutlet UILabel *label_Total;
@property (weak, nonatomic) IBOutlet UITextField *textfield_pagarem;

- (IBAction)action_pagarComBoleta:(id)sender;


@end

@implementation VC_contracts

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *imgdropdown=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 28, 28)];
    [imgdropdown setImage:[UIImage imageNamed:@"downArrow"]];
    [imgdropdown setContentMode:UIViewContentModeBottomRight];
    
    self.textfield_pagarem.rightView = imgdropdown;
    self.textfield_pagarem.rightViewMode = UITextFieldViewModeAlways;
    
    
    self.label_Contrato.text = [self.dic_details valueForKey:@"contractNumber"];
    self.label_Total.text = [self.dic_details valueForKey:@"total"] ;
    
     [self.navigationItem setTitle:@"Pagamentos"];
    
    [[ServiceModel sharedClient] POST:k_contracts
                           parameters:[NSDictionary dictionaryWithObjectsAndKeys:[self.dic_details valueForKey:@"contractNumber"],@"contractNumber", nil]
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
            self.obj_Contracts  = [Contracts instanceFromDictionary:responseObject];
            [self.tableview_ reloadData];
             
             if ([self.obj_Contracts.contractsMap.list count])
             {
                 self.obj_ContractsList = [self.obj_Contracts.contractsMap.list lastObject];
                  DLog(@"%@",self.obj_ContractsList.pagarEm);
                 if([self.obj_ContractsList.pagarEm count])
                 {
                     self.textfield_pagarem.text = [self.obj_ContractsList.pagarEm objectAtIndex:0];
                     [self addPickerviewtoPagaremTextfield];
                 }
             }
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

 // In a @property (weak, nonatomic) IBOutlet UITableView *tableview_;
storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - tableview delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.obj_Contracts.contractsMap.list count]-1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_contracts *cell = (cell_contracts *)[tableView dequeueReusableCellWithIdentifier:@"cell_contracts"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_pagamentos" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    if (indexPath.row == int_selectedIndex)
    {
        cell.button_checkBox.selected = YES;
    }
    else
    {
        cell.button_checkBox.selected = NO;
    }
    
    self.obj_ContractsList = [self.obj_Contracts.contractsMap.list objectAtIndex:indexPath.row];
    
    [cell method_setValues:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_ContractsList.parcelaNumber,@"ParcelaNumber", self.obj_ContractsList.valor,@"ParcelaValue",self.obj_ContractsList.parcelaStatus,@"Status",self.obj_ContractsList.parcelaDataVencim,@"date",self.obj_ContractsList.parcelaDataPagamento,@"date1",nil]];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    cell_contracts *cell = (cell_contracts *)[tableView dequeueReusableCellWithIdentifier:@"cell_contracts"];
    [cell.button_checkBox setSelected:YES];
    int_selectedIndex = indexPath.row;
    [self.tableview_ reloadData];
}


#pragma mark - addPickerviewtoPacelementoTextfield
-(void)addPickerviewtoPagaremTextfield
{
    pickerView =[[UIPickerView alloc]init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    self.textfield_pagarem.inputView = pickerView;
}


- (IBAction)action_pagarComBoleta:(id)sender
{
    self.obj_ContractsList = [self.obj_Contracts.contractsMap.list objectAtIndex:int_selectedIndex];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *dateFromString_ = [[NSDate alloc] init];
    dateFromString_ = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",self.obj_ContractsList.parcelaDataVencim]];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    DLog(@"%@",self.obj_ContractsList);
    
    [[ServiceModel sharedClient] POST:k_ValidarParcela
                           parameters:[NSDictionary dictionaryWithObjectsAndKeys:[self.dic_details valueForKey:@"contractNumber"],@"contrato",
                                       self.obj_ContractsList.parcelaValue,@"valoresParcelas",
                                       [dateFormatter stringFromDate:dateFromString_],@"datasDaVencimiento",
                                       self.textfield_pagarem.text,@"dataDaPagamento",
                                       self.obj_ContractsList.parcelaNumber,@"parcela",
                                       self.obj_ContractsList.debitoAutomatico,@"debitoParcela",
                                       self.obj_ContractsList.parcelaStatus,@"statusParcela",
                                       self.obj_ContractsList.qtParcela,@"qtParcela",nil]
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             if ([responseObject valueForKey:@"contractsMap"])
             {
                // [UtilitiesHelper showAlert:[[responseObject valueForKey:@"contractsMap"]
                  //                           valueForKey:@"SUCCESS_MESSAGE_KEY"]];
                 [self performSegueWithIdentifier:@"segue_contractsMessage" sender:nil];
             }
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}


#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    self.obj_ContractsList = [self.obj_Contracts.contractsMap.list lastObject];
     DLog(@"%@",self.obj_ContractsList.pagarEm);
    return [self.obj_ContractsList.pagarEm count];
}


#pragma mark - UIPickerView Delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.obj_ContractsList.pagarEm objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component
{
   self.textfield_pagarem.text= [self.obj_ContractsList.pagarEm objectAtIndex:row];
}


@end
