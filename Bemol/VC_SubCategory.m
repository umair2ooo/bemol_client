#import "VC_SubCategory.h"

#import "cell_Category.h"
#import "VC_ProductList.h"
#import "VC_SubofSubCategory.h"


@interface VC_SubCategory ()

@property(nonatomic, strong)Categories *category;

@property (weak, nonatomic) IBOutlet UITableView *tableview_;

- (IBAction)action_back:(id)sender;
@end

@implementation VC_SubCategory


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:self.string_categoryName];
    [self.tableview_ reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // ApplicationDelegate.single.delegate_ = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
   // ApplicationDelegate.single.delegate_ = nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview dataSource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_Category *cell = (cell_Category *)[tableView dequeueReusableCellWithIdentifier:@"cell_Category"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_Category" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    self.category = [self.array_categories objectAtIndex:indexPath.row];
    [cell.label_categoryName setFrame:CGRectMake(25, 16, 300, 21)];
    [cell method_setValues:[NSDictionary dictionaryWithObjectsAndKeys:self.category.name,@"name",
                            self.category.thumbnail,@"thumbnail",nil]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.category = [self.array_categories objectAtIndex:indexPath.row];
//    //[SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
//    [ApplicationDelegate.single method_getSubCategories:self.category.uniqueID];
    
    
    VC_SubCategory *objSubCategory = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_SubCategory"];
    objSubCategory.array_categories = [[NSMutableArray alloc] init];
    
    [[ServiceModel sharedClient]GET:[NSString stringWithFormat:@"%@%@",k_subCategory,self.category.uniqueID] parameters:nil onView:self.view success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [[responseObject valueForKey:@"CatalogGroupView"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
          {
              Categories *obj_ = [[Categories alloc] init];
              obj_.identifier = [obj valueForKey:@"identifier"];
              obj_.name = [obj valueForKey:@"name"];
              obj_.productsURL = [obj valueForKey:@"productsURL"];
              obj_.resourceId = [obj valueForKey:@"resourceId"];
              obj_.uniqueID = [obj valueForKey:@"uniqueID"];
              obj_.shortDescription = [obj valueForKey:@"shortDescription"];
              
              [objSubCategory.array_categories addObject:obj_];
          }];
         [self performSegueWithIdentifier:@"segue_SubofSubCategory" sender:objSubCategory.array_categories];
     }
                            failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error.description);
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_SubCatProductList"])
    {
        VC_ProductList *obj = segue.destinationViewController;
        
        obj.dic_data = [NSMutableDictionary dictionaryWithObjectsAndKeys:(NSMutableArray *)sender, @"array", nil];
//        obj.array_products = [NSMutableArray arrayWithArray:];
    }
    else if ([segue.identifier isEqualToString:@"segue_SubofSubCategory"])
    {
        VC_SubofSubCategory *obj = segue.destinationViewController;
        obj.array_categories = [NSMutableArray arrayWithArray:(NSMutableArray *)sender];
        obj.string_categoryName = self.category.name;
    }
}

- (IBAction)action_cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}




@end