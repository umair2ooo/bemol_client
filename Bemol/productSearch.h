#import <Foundation/Foundation.h>

@interface productSearch : NSObject

@property(nonatomic, strong)NSString *name;
@property(nonatomic, strong)NSString *uniqueID;
@property(nonatomic, strong)NSString *thumbnail;
@property(nonatomic, strong)NSString *shortDescription;
@property(nonatomic, strong)NSArray *Price;
@end
