//
//  BaseMarketingSpotActivityDatum.h
//  
//
//  Created by fahim on 07/09/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseMarketingSpotActivityDatum : NSObject {
    NSString *activityFormat;
    NSString *activityIdentifierID;
    NSString *activityIdentifierName;
    NSString *activityPriority;
    NSString *baseMarketingSpotActivityID;
    NSString *baseMarketingSpotActivityName;
    NSString *baseMarketingSpotDataType;
    NSString *contentFormatId;
    NSString *contentFormatName;
    NSString *contentId;
    NSString *contentName;
    NSString *contentStoreId;
    NSString *contentUrl;
    NSMutableArray *marketingContentDescription;
    NSMutableArray *properties;
}

@property (nonatomic, copy) NSString *activityFormat;
@property (nonatomic, copy) NSString *activityIdentifierID;
@property (nonatomic, copy) NSString *activityIdentifierName;
@property (nonatomic, copy) NSString *activityPriority;
@property (nonatomic, copy) NSString *baseMarketingSpotActivityID;
@property (nonatomic, copy) NSString *baseMarketingSpotActivityName;
@property (nonatomic, copy) NSString *baseMarketingSpotDataType;
@property (nonatomic, copy) NSString *contentFormatId;
@property (nonatomic, copy) NSString *contentFormatName;
@property (nonatomic, copy) NSString *contentId;
@property (nonatomic, copy) NSString *contentName;
@property (nonatomic, copy) NSString *contentStoreId;
@property (nonatomic, copy) NSString *contentUrl;
@property (nonatomic, copy) NSMutableArray *marketingContentDescription;
@property (nonatomic, copy) NSMutableArray *properties;

+ (BaseMarketingSpotActivityDatum *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
