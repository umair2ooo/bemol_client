#import "ShoppingCart.h"

#import "OrderItem.h"

@implementation ShoppingCart

@synthesize buyerDistinguishedName;
@synthesize buyerId;
@synthesize checkoutUrl;
@synthesize grandTotal;
@synthesize grandTotalCurrency;
@synthesize lastUpdateDate;
@synthesize orderId;
@synthesize orderItem;
@synthesize orderStatus;
@synthesize paymentInstructionUrl;
@synthesize precheckoutUrl;
@synthesize prepareIndicator;
@synthesize recordSetComplete;
@synthesize recordSetCount;
@synthesize recordSetStartNumber;
@synthesize recordSetTotal;
@synthesize resourceId;
@synthesize resourceName;
@synthesize shipAsComplete;
@synthesize shippingInfoUrl;
@synthesize storeNameIdentifier;
@synthesize storeUniqueID;
@synthesize totalAdjustment;
@synthesize totalAdjustmentCurrency;
@synthesize totalProductPrice;
@synthesize totalProductPriceCurrency;
@synthesize totalSalesTax;
@synthesize totalSalesTaxCurrency;
@synthesize totalShippingCharge;
@synthesize totalShippingChargeCurrency;
@synthesize totalShippingTax;
@synthesize totalShippingTaxCurrency;
@synthesize usablePaymentInfoUrl;
@synthesize usableShippingInfoUrl;

+ (ShoppingCart *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ShoppingCart *instance = [[ShoppingCart alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.buyerDistinguishedName = [aDictionary objectForKey:@"buyerDistinguishedName"];
    self.buyerId = [aDictionary objectForKey:@"buyerId"];
    self.checkoutUrl = [aDictionary objectForKey:@"checkoutUrl"];
    self.grandTotal = [aDictionary objectForKey:@"grandTotal"];
    self.grandTotalCurrency = [aDictionary objectForKey:@"grandTotalCurrency"];
    self.lastUpdateDate = [aDictionary objectForKey:@"lastUpdateDate"];
    self.orderId = [aDictionary objectForKey:@"orderId"];

    NSMutableArray *receivedOrderItem = [aDictionary objectForKey:@"orderItem"];
    if ([receivedOrderItem isKindOfClass:[NSMutableArray class]]) {

        NSMutableArray *populatedOrderItem = [NSMutableArray arrayWithCapacity:[receivedOrderItem count]];
        for (NSDictionary *item in receivedOrderItem) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedOrderItem addObject:[OrderItem instanceFromDictionary:item]];
            }
        }

        self.orderItem = populatedOrderItem;

    }
    self.orderStatus = [aDictionary objectForKey:@"orderStatus"];
    self.paymentInstructionUrl = [aDictionary objectForKey:@"paymentInstructionUrl"];
    self.precheckoutUrl = [aDictionary objectForKey:@"precheckoutUrl"];
    self.prepareIndicator = [aDictionary objectForKey:@"prepareIndicator"];
    self.recordSetComplete = [aDictionary objectForKey:@"recordSetComplete"];
    self.recordSetCount = [aDictionary objectForKey:@"recordSetCount"];
    self.recordSetStartNumber = [aDictionary objectForKey:@"recordSetStartNumber"];
    self.recordSetTotal = [aDictionary objectForKey:@"recordSetTotal"];
    self.resourceId = [aDictionary objectForKey:@"resourceId"];
    self.resourceName = [aDictionary objectForKey:@"resourceName"];
    self.shipAsComplete = [aDictionary objectForKey:@"shipAsComplete"];
    self.shippingInfoUrl = [aDictionary objectForKey:@"shippingInfoUrl"];
    self.storeNameIdentifier = [aDictionary objectForKey:@"storeNameIdentifier"];
    self.storeUniqueID = [aDictionary objectForKey:@"storeUniqueID"];
    self.totalAdjustment = [aDictionary objectForKey:@"totalAdjustment"];
    self.totalAdjustmentCurrency = [aDictionary objectForKey:@"totalAdjustmentCurrency"];
    self.totalProductPrice = [aDictionary objectForKey:@"totalProductPrice"];
    self.totalProductPriceCurrency = [aDictionary objectForKey:@"totalProductPriceCurrency"];
    self.totalSalesTax = [aDictionary objectForKey:@"totalSalesTax"];
    self.totalSalesTaxCurrency = [aDictionary objectForKey:@"totalSalesTaxCurrency"];
    self.totalShippingCharge = [aDictionary objectForKey:@"totalShippingCharge"];
    self.totalShippingChargeCurrency = [aDictionary objectForKey:@"totalShippingChargeCurrency"];
    self.totalShippingTax = [aDictionary objectForKey:@"totalShippingTax"];
    self.totalShippingTaxCurrency = [aDictionary objectForKey:@"totalShippingTaxCurrency"];
    self.usablePaymentInfoUrl = [aDictionary objectForKey:@"usablePaymentInfoUrl"];
    self.usableShippingInfoUrl = [aDictionary objectForKey:@"usableShippingInfoUrl"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.buyerDistinguishedName) {
        [dictionary setObject:self.buyerDistinguishedName forKey:@"buyerDistinguishedName"];
    }

    if (self.buyerId) {
        [dictionary setObject:self.buyerId forKey:@"buyerId"];
    }

    if (self.checkoutUrl) {
        [dictionary setObject:self.checkoutUrl forKey:@"checkoutUrl"];
    }

    if (self.grandTotal) {
        [dictionary setObject:self.grandTotal forKey:@"grandTotal"];
    }

    if (self.grandTotalCurrency) {
        [dictionary setObject:self.grandTotalCurrency forKey:@"grandTotalCurrency"];
    }

    if (self.lastUpdateDate) {
        [dictionary setObject:self.lastUpdateDate forKey:@"lastUpdateDate"];
    }

    if (self.orderId) {
        [dictionary setObject:self.orderId forKey:@"orderId"];
    }

    if (self.orderItem) {
        [dictionary setObject:self.orderItem forKey:@"orderItem"];
    }

    if (self.orderStatus) {
        [dictionary setObject:self.orderStatus forKey:@"orderStatus"];
    }

    if (self.paymentInstructionUrl) {
        [dictionary setObject:self.paymentInstructionUrl forKey:@"paymentInstructionUrl"];
    }

    if (self.precheckoutUrl) {
        [dictionary setObject:self.precheckoutUrl forKey:@"precheckoutUrl"];
    }

    if (self.prepareIndicator) {
        [dictionary setObject:self.prepareIndicator forKey:@"prepareIndicator"];
    }

    if (self.recordSetComplete) {
        [dictionary setObject:self.recordSetComplete forKey:@"recordSetComplete"];
    }

    if (self.recordSetCount) {
        [dictionary setObject:self.recordSetCount forKey:@"recordSetCount"];
    }

    if (self.recordSetStartNumber) {
        [dictionary setObject:self.recordSetStartNumber forKey:@"recordSetStartNumber"];
    }

    if (self.recordSetTotal) {
        [dictionary setObject:self.recordSetTotal forKey:@"recordSetTotal"];
    }

    if (self.resourceId) {
        [dictionary setObject:self.resourceId forKey:@"resourceId"];
    }

    if (self.resourceName) {
        [dictionary setObject:self.resourceName forKey:@"resourceName"];
    }

    if (self.shipAsComplete) {
        [dictionary setObject:self.shipAsComplete forKey:@"shipAsComplete"];
    }

    if (self.shippingInfoUrl) {
        [dictionary setObject:self.shippingInfoUrl forKey:@"shippingInfoUrl"];
    }

    if (self.storeNameIdentifier) {
        [dictionary setObject:self.storeNameIdentifier forKey:@"storeNameIdentifier"];
    }

    if (self.storeUniqueID) {
        [dictionary setObject:self.storeUniqueID forKey:@"storeUniqueID"];
    }

    if (self.totalAdjustment) {
        [dictionary setObject:self.totalAdjustment forKey:@"totalAdjustment"];
    }

    if (self.totalAdjustmentCurrency) {
        [dictionary setObject:self.totalAdjustmentCurrency forKey:@"totalAdjustmentCurrency"];
    }

    if (self.totalProductPrice) {
        [dictionary setObject:self.totalProductPrice forKey:@"totalProductPrice"];
    }

    if (self.totalProductPriceCurrency) {
        [dictionary setObject:self.totalProductPriceCurrency forKey:@"totalProductPriceCurrency"];
    }

    if (self.totalSalesTax) {
        [dictionary setObject:self.totalSalesTax forKey:@"totalSalesTax"];
    }

    if (self.totalSalesTaxCurrency) {
        [dictionary setObject:self.totalSalesTaxCurrency forKey:@"totalSalesTaxCurrency"];
    }

    if (self.totalShippingCharge) {
        [dictionary setObject:self.totalShippingCharge forKey:@"totalShippingCharge"];
    }

    if (self.totalShippingChargeCurrency) {
        [dictionary setObject:self.totalShippingChargeCurrency forKey:@"totalShippingChargeCurrency"];
    }

    if (self.totalShippingTax) {
        [dictionary setObject:self.totalShippingTax forKey:@"totalShippingTax"];
    }

    if (self.totalShippingTaxCurrency) {
        [dictionary setObject:self.totalShippingTaxCurrency forKey:@"totalShippingTaxCurrency"];
    }

    if (self.usablePaymentInfoUrl) {
        [dictionary setObject:self.usablePaymentInfoUrl forKey:@"usablePaymentInfoUrl"];
    }

    if (self.usableShippingInfoUrl) {
        [dictionary setObject:self.usableShippingInfoUrl forKey:@"usableShippingInfoUrl"];
    }

    return dictionary;

}

@end
