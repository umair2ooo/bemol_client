//
//  VC_atualizarDados.m
//  Bemol
//
//  Created by Fahim Bilwani on 16/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "VC_atualizarDados.h"

@interface VC_atualizarDados ()<UIPickerViewDelegate,UIPickerViewDataSource>
{
    UIPickerView *pickerView;
    UIDatePicker *datePicker;
    NSArray *array_gender;
}

@property (weak, nonatomic) IBOutlet UITextField *textfield_Email;
@property (weak, nonatomic) IBOutlet UITextField *textfield_Telefone;
@property (weak, nonatomic) IBOutlet UITextField *textfield_Senha;
@property (weak, nonatomic) IBOutlet UITextField *textfield_CPF;
@property (weak, nonatomic) IBOutlet UITextField *textfield_Sexo;
@property (weak, nonatomic) IBOutlet UITextField *textfield_RG;
@property (weak, nonatomic) IBOutlet UITextField *textfield_CEP;
@property (weak, nonatomic) IBOutlet UITextField *textfield_Nascimento;
@property (weak, nonatomic) IBOutlet UITextField *textfield_Complemento;
@property (weak, nonatomic) IBOutlet UILabel *label_Endereco;
@property (weak, nonatomic) IBOutlet UILabel *label_Numero;
@property (weak, nonatomic) IBOutlet UILabel *label_Bairrio;
@property (weak, nonatomic) IBOutlet UILabel *label_Estado;
@property (weak, nonatomic) IBOutlet UILabel *label_Pais;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UIButton *button_salvor;

- (IBAction)action_Salvor:(id)sender;
- (IBAction)action_back:(id)sender;

@end

@implementation VC_atualizarDados

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Meus dados"];
    [self.scroller setContentSize:CGSizeMake(0,self.button_salvor.frame.size.height+self.button_salvor.frame.origin.y+10)];
    [self.navigationItem setTitle:@"Meus dados"];
    array_gender = [[NSArray alloc]initWithObjects:@"Masculino",@"Feminino", nil];
    [self addPickerViewAndDatepickerview];
    [self setUiContents];
    DLog(@"%@",self.dic_userDetials);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setUiContents
-(void)setUiContents
{
    self.textfield_Email.text = [self.dic_userDetials valueForKey:@"Email"];
    if (![[self.dic_userDetials valueForKey:@"CPF"] isKindOfClass:[NSNull class]])
    {
        self.textfield_CPF.text = [self.dic_userDetials valueForKey:@"CPF"];
    }
    else
    {
        self.textfield_CPF.text = @"";
    }
    self.textfield_Nascimento.text = [self.dic_userDetials valueForKey:@"Nascimento"];
    self.textfield_Telefone.text = [self.dic_userDetials valueForKey:@"Telefone"];
    self.textfield_Sexo.text = [self.dic_userDetials valueForKey:@"Sexo"];
    if (![[self.dic_userDetials valueForKey:@"RG"] isKindOfClass:[NSNull class]])
    {
        self.textfield_RG.text = [self.dic_userDetials valueForKey:@"RG"];
    }
    else
    {
        self.textfield_RG.text = @"";
    }
    self.textfield_CEP.text = [self.dic_userDetials valueForKey:@"CEP"];
    self.label_Endereco.text = [self.dic_userDetials valueForKey:@"Endereco"];
    self.label_Numero.text = @"";
    self.label_Bairrio.text = @"";
    self.label_Estado.text = [self.dic_userDetials valueForKey:@"Estado"];
    self.label_Pais.text = [self.dic_userDetials valueForKey:@"Pais/regiao"];
}

#pragma mark - setCartoBemolViewPickerView
-(void)addPickerViewAndDatepickerview
{
    pickerView =[[UIPickerView alloc]init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    
    self.textfield_Sexo.inputView = pickerView;
    
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(updatedateTextField:)
         forControlEvents:UIControlEventValueChanged];
    [self.textfield_Nascimento setInputView:datePicker];
}

#pragma mark - updatedateTextField
-(void)updatedateTextField:(UIDatePicker *)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    self.textfield_Nascimento.text = [dateFormat stringFromDate:sender.date];
    dateFormat = nil;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_Salvor:(id)sender
{
    [ApplicationDelegate.single.dic_userDetails setValue:self.textfield_Email.text forKey:@"email1"];
    [ApplicationDelegate.single.dic_userDetails setValue:self.textfield_Nascimento.text forKey:@"dateOfBirth"];
    [ApplicationDelegate.single.dic_userDetails setValue:self.textfield_Telefone.text forKey:@"mobPhone"];
    [ApplicationDelegate.single.dic_userDetails setValue:self.textfield_CPF.text forKey:@"cpf"];
    [ApplicationDelegate.single.dic_userDetails setValue:self.textfield_Sexo.text forKey:@"gender"];
    [ApplicationDelegate.single.dic_userDetails setValue:self.textfield_RG.text forKey:@"rg"];
    [ApplicationDelegate.single.dic_userDetails setValue:self.textfield_CEP.text forKey:@"zipCode"];
    [ApplicationDelegate.single.dic_userDetails setValue:@"ShippingAndBilling" forKey:@"addressType"];
    
    DLog(@"%@",ApplicationDelegate.single.dic_userDetails);
    
    
    [[ServiceModel sharedClient] PUT:k_updateUserdDetail
                          parameters:ApplicationDelegate.single.dic_userDetails
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        if (![responseObject valueForKey:@"errors"])
        {
            [UtilitiesHelper showAlert:@"Atualizado com sucesso"];
            [self.navigationController popViewControllerAnimated:YES];
            DLog(@"%@",responseObject);
        }
        else
        {
            [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                        valueForKey:@"errorMessage"]];
        }
                                 
    }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
                                 
                             }];
}

- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
        return 2;
}


#pragma mark - UIPickerView Delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
        return [array_gender objectAtIndex:row];
}


- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component
{
    self.textfield_Sexo.text = [array_gender objectAtIndex:row];
}
#pragma mark - textField delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField isEqual:self.textfield_CPF])
    {
        NSString *text = textField.text;
        if (range.location == 14)
        {
            return NO;
        }
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"." withString:@""];
        NSMutableString *mutableText = [text mutableCopy];
        if (mutableText.length > 3)
        {
            [mutableText insertString:@"." atIndex:3];
        }
        if (mutableText.length > 7)
        {
            [mutableText insertString:@"." atIndex:7];
        }
        if (mutableText.length > 11)
        {
            [mutableText insertString:@"-" atIndex:11];
        }
        text = mutableText;
        textField.text = text;
        return NO;
    }
    else if([textField isEqual:self.textfield_CEP])
    {
        NSString *text = textField.text;
        if (range.location == 9)
        {
            return NO;
        }
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSMutableString *mutableText = [text mutableCopy];
        if (mutableText.length > 5)
        {
            [mutableText insertString:@"-" atIndex:5];
        }
        text = mutableText;
        textField.text = text;
        return NO;
    }
    else if([textField isEqual:self.textfield_Telefone])
    {
        if (range.location == 11)
        {
            return NO;
        }
    }
    else if([textField isEqual:self.textfield_RG])
    {
        //NSString *text = textField.text;
        if (range.location == 8)
        {
            return NO;
        }
//        text = [text stringByReplacingCharactersInRange:range withString:string];
//        text = [text stringByReplacingOccurrencesOfString:@"-" withString:@""];
//        NSMutableString *mutableText = [text mutableCopy];
//        if (mutableText.length > 6)
//        {
//            [mutableText insertString:@"-" atIndex:6];
//        }
//        text = mutableText;
//        textField.text = text;
//        return NO;
    }
    return YES;
}
@end
