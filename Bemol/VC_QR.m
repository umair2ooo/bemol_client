#import "VC_QR.h"
#import <AVFoundation/AVFoundation.h>
#import "VC_productDetail.h"

@interface VC_QR ()<AVCaptureMetadataOutputObjectsDelegate,UIAlertViewDelegate>
{
    AVCaptureMetadataOutput *captureMetadataOutput;
}


@property (weak, nonatomic) IBOutlet UIView *viewPreview;


@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic) BOOL isReading;


- (IBAction)action_back:(id)sender;

-(BOOL)startReading;
-(void)stopReading;
-(void)loadBeepSound;


@end

@implementation VC_QR



#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Initially make the captureSession object nil.
    self.captureSession = nil;
    [self setTitle:@"QR CODE"];
    // Begin loading the sound effect so to have it ready for playback when it's needed.
    [self loadBeepSound];
}

-(void)viewWillAppear:(BOOL)animated
{
//    [self method_setAddToCartCounterLabel];
    // Set the initial value of the flag to NO.
    self.isReading = NO;
    [self startStopReading:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - method_setAddToCartCounterLabel
//-(void)method_setAddToCartCounterLabel
//{
//    UIView *view_counter;
//    if (ApplicationDelegate.single.array_cartObjects)
//    {
//        if ([ApplicationDelegate.single.array_cartObjects count])
//        {
//            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
//            UILabel *label_counter = (UILabel *)[self.navigationController.view viewWithTag:888];
//            [view_counter setHidden:NO];
//            label_counter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[ApplicationDelegate.single.array_cartObjects count]];
//        }
//        else
//        {
//            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
//            [view_counter setHidden:YES];
//        }
//        
//    }
//    else
//    {
//        view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
//        [view_counter setHidden:YES];
//    }
//}


#pragma mark - IBAction startStopReading
- (IBAction)startStopReading:(id)sender
{
    if (!self.isReading)
    {
        // This is the case where the app should read a QR code when the start button is tapped.
        if ([self startReading]) {
            // If the startReading methods returns YES and the capture session is successfully
            // running, then change the start button title and the status message.
        }
    }
    else{
        // In this case the app is currently reading a QR code and it should stop doing so.
        [self stopReading];
        // The bar button item's title should change again.
    }
    // Set to the flag the exact opposite value of the one that currently has.
    self.isReading = !self.isReading;
}

#pragma mark - action_Cancel
- (IBAction)action_Cancel:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - startReading
- (BOOL)startReading {
    NSError *error;
    
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize the captureSession object.
    self.captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [self.captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [self.captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    self.videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [self.videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.videoPreviewLayer setFrame:_viewPreview.layer.bounds];
    [self.viewPreview.layer addSublayer:self.videoPreviewLayer];
    
    
    // Start video capture.
    [_captureSession startRunning];
    
    return YES;
}

#pragma mark - stopReading
-(void)stopReading{
    // Stop video capture and make the capture session object nil.
    [self.captureSession stopRunning];
    self.captureSession = nil;
    
    // Remove the video preview layer from the viewPreview view's layer.
    [self.videoPreviewLayer removeFromSuperlayer];
}

#pragma mark - loadBeepSound
-(void)loadBeepSound{
    // Get the path to the beep.mp3 file and convert it to a NSURL object.
    NSString *beepFilePath = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    NSURL *beepURL = [NSURL URLWithString:beepFilePath];
    
    NSError *error;
    
    // Initialize the audio player object using the NSURL object previously set.
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:beepURL error:&error];
    if (error) {
        // If the audio player cannot be initialized then log a message.
        NSLog(@"Could not play beep file.");
        NSLog(@"%@", [error localizedDescription]);
    }
    else{
        // If the audio player was successfully initialized then load it in memory.
        [self.audioPlayer prepareToPlay];
    }
}


#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode])
        {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
            
            [self performSelectorOnMainThread:@selector(method_goToPRoductDetail_QRDetail:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            // If the audio player is not nil, then play the sound effect.
            if (self.audioPlayer)
            {
                [self.audioPlayer play];
            }
        }
    }
}
#pragma mark - method_goToPRoductDetail
-(void)method_goToPRoductDetail_QRDetail:(NSString *)qrDetail
{
    [captureMetadataOutput setMetadataObjectsDelegate:nil queue:nil];
    DLog(@"%@",qrDetail);
    if ([self isANumber:qrDetail])
    {
        [self performSegueWithIdentifier:@"segue_productDetailViaQR" sender:qrDetail];
    }
    else
    {
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Product not found" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
        alert.tag = 102;
    }
}
#pragma mark - checkStringisANumber
-(BOOL)isANumber:(NSString *)string
{
    NSPredicate *numberPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES '^[0-9]+$'"];
    return [numberPredicate evaluateWithObject:string];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_productDetailViaQR"])
    {
        VC_productDetail *obj = segue.destinationViewController;
        obj.uniqueID = sender;
    }
}

- (IBAction)action_back:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - alertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        self.isReading = NO;
        [self startStopReading:nil];
    }
}
@end