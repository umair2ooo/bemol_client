#import "VC_home.h"

#import "scroller_home.h"
#import "cell_Search.h"
#import "VC_ProductList.h"
#import "RKNotificationHub.h"
#import "ProductList.h"
#import "scrollerTwo_home.h"

@interface VC_home ()<UITextFieldDelegate,CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    RKNotificationHub* hub;
}

@property(nonatomic, strong)NSMutableArray *array_searcItem;
@property(nonatomic, strong)productSearch *productsearch;
@property(nonatomic, strong)ProductList *obj_productList;

@property (weak, nonatomic) IBOutlet scrollerTwo_home *scroller_two;
@property (weak, nonatomic) IBOutlet scroller_home *scroller_one;
@property (weak, nonatomic) IBOutlet UIView *container_menu;
@property (weak, nonatomic) IBOutlet UIView *container_shoppingCart;
@property (weak, nonatomic) IBOutlet UITableView *tableview_search;
@property (weak, nonatomic) IBOutlet UITextField *textField_search;
@property (weak, nonatomic) IBOutlet UIButton *button_cart;

- (IBAction)action_menu:(id)sender;
- (IBAction)action_shoppingCart:(id)sender;
- (IBAction)action_goToLogin:(id)sender;


@end

@implementation VC_home

#pragma mark - set Cart counter
-(void)method_setCounter
{
    NSUserDefaults *obj = [NSUserDefaults standardUserDefaults];
    
    DLog(@"%@",[obj valueForKey:k_count]);
    
    if ([obj valueForKey:k_count])
    {
        [hub showCount];
        hub.count = [[obj valueForKey:k_count] intValue];
       // hub.count = 1;
    }
    else
    {
        hub.count = 0;
        //[hub showCount];
        [hub hideCount];
    }
}



#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[self getUserdetails];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:k_isLogin] isEqualToString:@"no"] || ![[NSUserDefaults standardUserDefaults] objectForKey:k_isLogin])
    {
        [self method_guestLogin];
    }
    
    
    if (!hub)
    {
        hub = [[RKNotificationHub alloc]initWithView:self.button_cart]; // sets the count to 0
         [hub moveCircleByX:-15 Y:15];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ApplicationDelegate.single setFontFamily:k_fontName forView:self.view andSubViews:YES];
    });
    
    self.textField_search.rightViewMode = UITextFieldViewModeUnlessEditing;
    self.textField_search.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search-icon"]];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationManager requestWhenInUseAuthorization];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    [self.scroller_one method_startScroller];
    [self.scroller_two method_startScroller];
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:k_isLogin] isEqualToString:@"yes"])
    {
        if (!ApplicationDelegate.single.dic_userDetails)
        {
            [self getUserdetails];
        }
    }
    else
    {
        self.navigationController.navigationItem.rightBarButtonItem.title = @"Faça seu login aqui";
        VC_Menu *obj = [self.childViewControllers objectAtIndex:0];
        obj.label_menu.text = @"Faça seu login aqui";
    }
    
    [self method_setCounter];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideMenuDelegate:)
                                                 name:k_menuNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(logoutAndHideMenuDelegate:)
                                                 name:k_logoutNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideShopingCartDelegate:)
                                                 name:k_hideShopingCartNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showShoppingCartCounterDelegate:)
                                                 name:k_showShopingCartCounter
                                               object:nil];
    
    [self.tableview_search setHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.scroller_one method_stopScroller];
    [self.scroller_two method_stopScroller];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

#pragma mark - guestLogin
-(void)method_guestLogin
{
    [[ServiceModel sharedClient] POST:k_guestLogin
                           parameters:nil
                               onView:nil
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (responseObject && ![responseObject valueForKey:@"errors"])
         {
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_WCToken] forKey:k_WCToken];
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_WCTrustedToken] forKey:k_WCTrustedToken];
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_personalizationID
                                                               ] forKey:k_personalizationID];
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_userId] forKey:k_userId];
             [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:k_isLogin];
             [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:k_count];
             [self method_setCounter];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_userId]);
             DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken]);
             DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken]);
             DLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:k_personalizationID]);
             
             if (![[responseObject valueForKey:k_userId] isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:k_userId]])
             {
                 [[NSUserDefaults standardUserDefaults] setValue:nil forKey:k_count];
             }
             
             [self.scroller_one method_getDataFromServer];
             [self.scroller_two method_getDataFromServer];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
         }
     }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
     }];
}

#pragma mark - getUserdetails
-(void)getUserdetails
{
    //    if ([[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken])
    //    {
    [[ServiceModel sharedClient] GET:k_getUserdDetail
                          parameters:nil
                              onView:nil
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             ApplicationDelegate.single.dic_userDetails = [NSMutableDictionary dictionaryWithDictionary:responseObject];
             DLog(@"%@",ApplicationDelegate.single.dic_userDetails);
             
             if ([ApplicationDelegate.single.dic_userDetails valueForKey:@"firstName"] && [[ApplicationDelegate.single.dic_userDetails valueForKey:@"firstName"] length])
             {
                 self.navigationController.navigationItem.rightBarButtonItem.title = [NSString stringWithFormat:@"Olá, %@!",[[[ApplicationDelegate.single.dic_userDetails valueForKey:@"firstName"] componentsSeparatedByString:@" "] objectAtIndex:0]];
                 
                 VC_Menu *obj = [self.childViewControllers objectAtIndex:0];
                 obj.label_menu.text = [NSString stringWithFormat:@"Olá, %@!",[[[ApplicationDelegate.single.dic_userDetails valueForKey:@"firstName"] componentsSeparatedByString:@" "] objectAtIndex:0]];
             }
             else
             {
                 self.navigationController.navigationItem.rightBarButtonItem.title = @"Olá !";
                 
                 VC_Menu *obj = [self.childViewControllers objectAtIndex:0];
                 obj.label_menu.text = @"Olá !";
             }
             
             [self.scroller_one method_getDataFromServer];
             [self.scroller_two method_getDataFromServer];
             
             
             
             
             
             //             if ([[ApplicationDelegate.single.dic_userDetails valueForKey:@"firstName"] length] > 15)
             //             {
             //                 NSUInteger size;
             //                 if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
             //                     size = 11;
             //                 else
             //                     size = 14;
             //
             //                 UIFont * font = [UIFont fontWithName:k_fontName size:size];
             //                 NSDictionary * attributes = @{NSFontAttributeName: font};
             //                 [self.navigationController.navigationItem.rightBarButtonItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
             //
             //                // [obj.label_menu setFont: [obj.label_menu.font fontWithSize: 14]];
             //             }
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
             [self.navigationController popViewControllerAnimated:YES];
         }
         
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
         [self.navigationController popViewControllerAnimated:YES];
     }];
    // }
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_productlistViaHome"])
    {
        UINavigationController *nav = segue.destinationViewController;
        VC_ProductList *obj =  (VC_ProductList *)[nav topViewController];
        
        obj.dic_data = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                        (NSMutableArray *)sender, @"array",
                        self.obj_productList.recordSetTotal, @"total",
                        self.textField_search.text, @"title", nil];
        
//        obj.array_products = [NSMutableArray arrayWithArray:(NSMutableArray *)sender];
//        obj.string_title = self.textField_search.text;
    }
}


#pragma mark - action_menu
- (IBAction)action_menu:(id)sender
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:k_isLogin] isEqualToString:@"yes"])
    {
        [self.container_menu setHidden:NO];
//        [self.container_menu setFrame:CGRectMake(self.container_menu.frame.origin.x,
//                                                 20,
//                                                 self.container_menu.frame.size.width,
//                                                 self.container_menu.frame.size.height)];
//         self.edgesForExtendedLayout = UIRectEdgeNone;
//        [self.navigationController.view addSubview:self.container_menu];
        [self.view bringSubviewToFront:self.container_menu];
    }
    else
    {
        UINavigationController *obj_login = [self.storyboard instantiateViewControllerWithIdentifier:@"nav_logIn"];
        ApplicationDelegate.single.bool_isComingfromCart = NO;
        [self presentViewController:obj_login animated:YES completion:nil];
    }
}

#pragma mark - action_shoppingCart
- (IBAction)action_shoppingCart:(id)sender
{
    NSUserDefaults * defaults =  [NSUserDefaults standardUserDefaults];
    DLog(@"%@",[defaults stringForKey:k_count]);
//    if (![[defaults stringForKey:k_count] isEqualToString:@"0"] && ![[defaults stringForKey:k_count] isKindOfClass:[NSNull class]])
//    {
        [self.container_shoppingCart setHidden:NO];
//        [self.container_shoppingCart setFrame:CGRectMake(self.container_shoppingCart.frame.origin.x,
//                                             20,
//                                             self.container_shoppingCart.frame.size.width,
//                                             self.container_shoppingCart.frame.size.height)];
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//        [self.navigationController.view addSubview:self.container_shoppingCart];
        [self.view bringSubviewToFront:self.container_shoppingCart];
        [[NSNotificationCenter defaultCenter] postNotificationName:k_getShopingCartNotification object:nil];
//    }
}


#pragma mark - action_goToLogin
- (IBAction)action_goToLogin:(id)sender
{
//    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:k_userName];
//    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:k_password];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_userName];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_password];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Menu delegates
-(void)hideMenuDelegate:(NSNotification *) notification
{
    [self.container_menu setHidden:YES];
}

-(void)hideShopingCartDelegate:(NSNotification *) notification
{
    [self.container_shoppingCart setHidden:YES];
    
    [self method_setCounter];
}

-(void)logoutAndHideMenuDelegate:(NSNotification *) notification
{
    [self method_guestLogin];
    [self method_setCounter];
    [self.container_menu setHidden:YES];
    self.navigationController.navigationItem.rightBarButtonItem.title = @"Faça seu login aqui";
    
    VC_Menu *obj = [self.childViewControllers objectAtIndex:0];
    obj.label_menu.text = @"Faça seu login aqui";
}

-(void)showShoppingCartCounterDelegate:(NSNotification *) notification
{
    [self method_setCounter];
}


#pragma mark - tableview dataSource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_Search *cell = (cell_Search *)[tableView dequeueReusableCellWithIdentifier:@"cell_Search"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_Search" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    if (indexPath.row == 0)
    {
        cell.label_title.text = @"Ler código de barras ou QR CODE";
        cell.label_subtitle.text = @"Use essa funcionalidade em uma Loja Bemol";
        [cell.image_searchList setImage:[UIImage imageNamed:@"icon-barcode"]];
    }
    else
    {
        [cell.label_title setFrame:CGRectMake(cell.label_title.frame.origin.x,
                                              cell.label_title.frame.origin.y+8,
                                              cell.label_title.frame.size.width,
                                              cell.label_title.frame.size.height)];
        
        cell.label_title.text = @"Ver departamentos da Bemol";
        cell.label_subtitle.text = @"";
        [cell.image_searchList setImage:[UIImage imageNamed:@"icon-list"]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableview_search setHidden:YES];
    [self.view endEditing:YES];
    
    if (indexPath.row == 0)
    {
        [self performSegueWithIdentifier:@"segue_QR" sender:nil];
    }
    else
    {
        [self performSegueWithIdentifier:@"Segue_category" sender:nil];
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - method_GetProductsBySearch
-(void)method_GetProductsBySearch:(NSString *)searchTerm
{
    searchTerm = [searchTerm stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

//    @"%@%@?pageSize=%@&pageNumber=%@&orderBy=5"
    
    [[ServiceModel sharedClient]GET:[NSString stringWithFormat:@"%@%@?searchType=1002&pageSize=%@&pageNumber=1&orderBy=1", k_productBysearch,searchTerm, k_pageSize]
                         parameters:nil
                             onView:self.view
                            success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             self.obj_productList = [ProductList instanceFromDictionary:responseObject];
             
             [self performSegueWithIdentifier:@"segue_productlistViaHome" sender:self.obj_productList.catalogEntryView];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
         }
     }
        failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error.description);
         //[UtilitiesHelper showAlert:error.description];
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}

#pragma mark - textField delegates
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.tableview_search setHidden:NO];
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.tableview_search setHidden:YES];
}

#pragma mark - textField search button clicked
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.textField_search.text)
    {
        [textField resignFirstResponder];
        [self method_GetProductsBySearch:textField.text];
    }
    return NO;
}
@end