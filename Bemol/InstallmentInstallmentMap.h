#import <Foundation/Foundation.h>

@interface InstallmentInstallmentMap : NSObject {

    NSMutableArray *aMEXRegistradoCheckout;
    NSMutableArray *discoverRegistradoCheckout;
    NSMutableArray *masterCardRegistradoCheckout;
    NSMutableArray *vISARegistradoCheckout;

}

@property (nonatomic, copy) NSMutableArray *aMEXRegistradoCheckout;
@property (nonatomic, copy) NSMutableArray *discoverRegistradoCheckout;
@property (nonatomic, copy) NSMutableArray *masterCardRegistradoCheckout;
@property (nonatomic, copy) NSMutableArray *vISARegistradoCheckout;

+ (InstallmentInstallmentMap *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
