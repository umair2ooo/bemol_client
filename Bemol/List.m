#import "List.h"

@implementation List

@synthesize date;
@synthesize descriptionText;
@synthesize lojaId;
@synthesize orderId;
@synthesize sku;
@synthesize status;
@synthesize total;

+ (List *)instanceFromDictionary:(NSDictionary *)aDictionary {

    List *instance = [[List alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.date = [aDictionary objectForKey:@"date"];
    self.descriptionText = [aDictionary objectForKey:@"description"];
    self.lojaId = [aDictionary objectForKey:@"lojaId"];
    self.orderId = [aDictionary objectForKey:@"orderId"];
    self.sku = [aDictionary objectForKey:@"sku"];
    self.status = [aDictionary objectForKey:@"status"];
    self.total = [aDictionary objectForKey:@"total"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.date) {
        [dictionary setObject:self.date forKey:@"date"];
    }

    if (self.descriptionText) {
        [dictionary setObject:self.descriptionText forKey:@"descriptionText"];
    }

    if (self.lojaId) {
        [dictionary setObject:self.lojaId forKey:@"lojaId"];
    }

    if (self.orderId) {
        [dictionary setObject:self.orderId forKey:@"orderId"];
    }

    if (self.sku) {
        [dictionary setObject:self.sku forKey:@"sku"];
    }

    if (self.status) {
        [dictionary setObject:self.status forKey:@"status"];
    }

    if (self.total) {
        [dictionary setObject:self.total forKey:@"total"];
    }

    return dictionary;

}


@end
