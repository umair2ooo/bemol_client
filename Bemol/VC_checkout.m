#import "VC_checkout.h"
#import "VC_address.h"
#import "VC_addNewAddress.h"
#import "OrderSummary.h"
#import "cell_checkOut.h"
#import "OrderSummaryItem.h"
#import "VC_selectLocation.h"
#import "NSString+HTML.h"
#import "XShippingMode.h"
#import "XCredFlexMap.h"
#import "XBonusBemol.h"
#import "VISARegistradoCheckout.h"
#import "OrderSummaryXInstallmentMap.h"
#import "AMEXRegistradoCheckout.h"
#import "VISARegistradoCheckout.h"
#import "MasterCardRegistradoCheckout.h"
#import "DiscoverRegistradoCheckout.h"
#import "AMEXRegistradoCheckout.h"
#import "UsablePaymentInformation.h"
#import "UsableBillingAddres.h"
#import "FindSimulate.h"
#import "findSimulateSimulateMap.h"

@interface VC_checkout ()<UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate>
{
    NSArray *array_EscolhaOTipo;
    NSArray *array_selecione;
    UIPickerView *pickerView;
    NSString *string_physicalStoreId;
    UIDatePicker *datePicker;
}

@property (weak, nonatomic) IBOutlet UILabel *label_valoraPagar;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UITextField *textField_selectDate;
@property (weak, nonatomic) IBOutlet UITextField *textField_EscolhaOTipo;
@property (weak, nonatomic) IBOutlet UITextField *textfield_selecione;
@property (weak, nonatomic) IBOutlet UITableView *tableView_;
@property (weak, nonatomic) IBOutlet UILabel *label_subtotal;
@property (weak, nonatomic) IBOutlet UIView *view_coupon;
@property (weak, nonatomic) IBOutlet UIButton *button_finalizar;
@property (weak, nonatomic) IBOutlet UILabel *label_address;
@property (weak, nonatomic) IBOutlet UILabel *label_cep;
@property (weak, nonatomic) IBOutlet UILabel *label_endereco;
@property (weak, nonatomic) IBOutlet UILabel *label_bairro;
@property (weak, nonatomic) IBOutlet UILabel *label_cidad;
@property (weak, nonatomic) IBOutlet UILabel *label_nome;
@property (weak, nonatomic) IBOutlet UIView *view_payment;
@property (weak, nonatomic) IBOutlet UITextField *textfield_coupon;
@property (weak, nonatomic) IBOutlet UILabel *label_name;
@property (weak, nonatomic) IBOutlet UILabel *label_addressline;
@property (weak, nonatomic) IBOutlet UILabel *label_displayvalue;
@property (weak, nonatomic) IBOutlet UILabel *label_number;
@property (weak, nonatomic) IBOutlet UILabel *label_distance;
@property (weak, nonatomic) IBOutlet UILabel *label_Frete;
@property (weak, nonatomic) IBOutlet UILabel *label_prazo;
@property (weak, nonatomic) IBOutlet UITextField *textfield_plandepagamento;
@property (weak, nonatomic) IBOutlet UITextField *textfield_bonusbemol;
@property (weak, nonatomic) IBOutlet UITextField *textfield_valecredito;
@property (weak, nonatomic) IBOutlet UITextField *textfield_boletobonusbemol;
@property (weak, nonatomic) IBOutlet UITextField *textfield_boletovalecredito;
@property (weak, nonatomic) IBOutlet UIButton *button_selectedaddress;
@property (weak, nonatomic) IBOutlet UIButton *button_selectedstore;
@property (weak, nonatomic) IBOutlet UIButton *checkboxButton_bonusBemol;
@property (weak, nonatomic) IBOutlet UIButton *checkboxButton_valeCredito;
@property (weak, nonatomic) IBOutlet UIButton *checkboxButton_CreditobonusBemol;
@property (weak, nonatomic) IBOutlet UITextField *textfield_cartobemoldateField;
@property (weak, nonatomic) IBOutlet UITextField *textfield_creditCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *textfield_codigoVerificador;
@property (weak, nonatomic) IBOutlet UITextField *textfield_parcelamento;
@property (weak, nonatomic) IBOutlet UITextField *textfield_Nome;
@property (weak, nonatomic) IBOutlet UITextField *textfield_validade;
@property (weak, nonatomic) IBOutlet UITextField *textfield_cartoCreditCard;
@property (weak, nonatomic) IBOutlet UILabel *label_parcelo;
@property (weak, nonatomic) IBOutlet UILabel *label_juros;
@property (weak, nonatomic) IBOutlet UILabel *label_cartaobemototals;
@property (weak, nonatomic) IBOutlet UITextField *textfield_creditoBonusBemol;

@property(nonatomic, strong) OrderSummary *obj_orderSummary;
@property(nonatomic, strong) OrderSummaryItem *obj_orderItem;
@property(nonatomic, strong) XShippingMode *obj_xShippingMode;
@property(nonatomic, strong) XCredFlexMap *obj_xCredFlexMap;
@property(nonatomic, strong) XBonusBemol *obj_xBonusBemol;
@property(nonatomic, strong) OrderSummaryXInstallmentMap *obj_XInstallmentMap;
@property(nonatomic, strong) VISARegistradoCheckout *obj_VISARegistradoCheckout;
@property(nonatomic, strong) MasterCardRegistradoCheckout *obj_MasterCardRegistradoCheckout;
@property(nonatomic, strong) DiscoverRegistradoCheckout *obj_DiscoverRegistradoCheckout;
@property(nonatomic, strong) AMEXRegistradoCheckout *obj_AMEXRegistradoCheckout;
@property(nonatomic, strong) UsablePaymentInformation *obj_UsablePaymentInformation;
@property(nonatomic, strong) UsableBillingAddres *obj_UsableBillingAddres;
@property(nonatomic, strong)FindSimulate *obj_simulate;
@property(nonatomic, strong)findSimulateSimulateMap *obj_findSimulateSimulateMap;

@property(nonatomic, strong) NSString *string_addressId;
@property(nonatomic, strong) NSString *string_cpfNumber;
@property(nonatomic, strong) NSString *string_cardType;


- (IBAction)action_back:(id)sender;
- (IBAction)action_removePromoCode:(id)sender;
- (IBAction)action_selectedAddressOrStore:(id)sender;
- (IBAction)action_finalizarCompra:(id)sender;
- (IBAction)action_bonusBemol:(id)sender;
- (IBAction)action_Creditobonusbemol:(id)sender;

@end

@implementation VC_checkout


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Carrinho"];
    
    array_EscolhaOTipo = [NSArray arrayWithObjects:@"Normal",@"High",@"Low", nil];
    array_selecione = [NSArray arrayWithObjects:@"Pagar na loja",@"Vale Crédito",@"Cartão Bemol",@"Cartão de Crédito",@"Boleto Bancário", nil];
    
//    [self.tableView_ setContentInset:UIEdgeInsetsMake(50,0,0,0)];
    
    //[self method_addScrollerToView];
    
     [self method_addPickerViews];
    
//    [self.scroller setContentSize:CGSizeMake(0,
//                                             self.button_finalizar.frame.size.height+self.button_finalizar.frame.origin.y)];
    
    
    [self.view_coupon setHidden:YES];
    
    [self method_addViewsFromNibs:@"View_cartaoBemol"];
    
    [self getOrderSummary];
    //[self getUserdetails];
    
    [self.button_selectedaddress setSelected:YES];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    DLog(@"%@", NSStringFromCGRect(self.label_subtotal.frame));
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_selectLocation"])
    {
        VC_selectLocation *obj = segue.destinationViewController;
        obj.string_orderId = sender;
        obj.bool_comingFromCheckinnaLoja = NO;
        obj.bool_comingFromCartList = NO;
    }
}

#pragma mark - getUserdetails
-(void)getUserdetails
{
    [[ServiceModel sharedClient] GET:k_getUserdDetail
                          parameters:nil
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             if ([responseObject valueForKey:@"cpf"])
             {
                 self.string_cpfNumber = [responseObject valueForKey:@"cpf"];
                 DLog(@"%@",self.string_cpfNumber);
                 [self getOrderSummary];
             }
             else
             {
                 [self.navigationController popViewControllerAnimated:YES];
             }
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
             [self.navigationController popViewControllerAnimated:YES];
         }
         
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
         [self.navigationController popViewControllerAnimated:YES];
     }];
}

#pragma mark - getOrderSummary
-(void)shippinginfo
{
    self.obj_orderItem = [self.obj_orderSummary.orderItem objectAtIndex:0];
    [[ServiceModel sharedClient] PUT:k_shippinginfo
                          parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_orderSummary.orderId,@"orderId",self.obj_orderItem.addressId,@"addressId",@"-1,-5,-6",@"x_calculationUsage", nil]
                              onView:nil
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}

#pragma mark - getOrderSummary
-(void)getOrderSummary
{
    [[ServiceModel sharedClient] GET:k_ordersummary
                          parameters:nil
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        if (![responseObject valueForKey:@"errors"])
        {
            self.obj_orderSummary = [OrderSummary instanceFromDictionary:responseObject];
            [self setUIContents];
            //[self shippinginfo];
            
        }
        else
        {
            [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                        valueForKey:@"errorMessage"]];
        }
        
    }
        failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
    }];
}

#pragma mark - setUIContents
-(void)setUIContents
{
    self.label_subtotal.text = [NSString stringWithFormat:@"Subtotal: %@",[UtilitiesHelper method_currencyFormatter:self.obj_orderSummary.grandTotal]];
    
    self.label_valoraPagar.text = [NSString stringWithFormat:@"Valor a pagar: %@",[UtilitiesHelper method_currencyFormatter:self.obj_orderSummary.grandTotal]];
    
    if (self.obj_orderSummary.xShippingModes)
    {
        self.obj_xShippingMode = [self.obj_orderSummary.xShippingModes objectAtIndex:0];
        self.label_Frete.text = [UtilitiesHelper method_currencyFormatter:[NSString stringWithFormat:@"%@",self.obj_xShippingMode.shipModePrice]] ;
        self.label_prazo.text = [NSString stringWithFormat:@"%@ dias",self.obj_xShippingMode.shipModePrazo];
    }
    else
    {
        self.label_Frete.text = @"";
        self.label_prazo.text = @"";
    }
    
    
    self.obj_orderItem = [self.obj_orderSummary.orderItem objectAtIndex:0];
    self.label_address.text = self.obj_orderItem.firstName;
    self.label_cep.text = self.obj_orderItem.postalCode;
    self.label_endereco.text = @"-";
    self.label_bairro.text = self.obj_orderItem.bairo;
    self.label_cidad.text = [NSString stringWithFormat:@"%@",self.obj_orderItem.city];
    self.label_nome.text = self.obj_orderItem.numero;
    
    self.label_name.text = @"-";
    self.label_addressline.text = @"-";
    self.label_displayvalue.text = @"-";
    self.label_number.text = @"-";
    self.label_distance.text = @"-";
    
    [self.tableView_ reloadData];
    
    [self setCartoBemolViewPickerView];
}

#pragma mark - addPromoCode
-(void)addPromoCode
{
    [[ServiceModel sharedClient] POST:k_addPromoCode
                          parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_orderSummary.orderId,@"orderId",self.textfield_coupon.text,@"promoCode", nil]
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         DLog(@"%@",responseObject);
         if (![responseObject valueForKey:@"errors"])
         {
             [self.view_coupon setHidden:NO];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
             [self.view_coupon setHidden:YES];
         }
         
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"%@",error.description);
         
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
         [self.view_coupon setHidden:YES];
     }];
}

#pragma mark - add pickers in text fields
-(void)method_addPickerViews
{
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(updatedateTextField:)
         forControlEvents:UIControlEventValueChanged];
    [self.textField_selectDate setInputView:datePicker];
    
    
    pickerView =[[UIPickerView alloc]init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    //self.textField_EscolhaOTipo.inputView = pickerView;
    self.textfield_selecione.inputView = pickerView;
    
    //self.textField_EscolhaOTipo.text = [array_EscolhaOTipo objectAtIndex:0];
    self.textfield_selecione.text = [array_selecione objectAtIndex:2];
}


#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.textField_EscolhaOTipo.isFirstResponder)
    {
        return [array_EscolhaOTipo count];
    }
    else if (self.textfield_selecione.isFirstResponder)
    {
        return [array_selecione count];
    }
    else if (self.textfield_plandepagamento.isFirstResponder)
    {
        return [self.obj_orderSummary.xCredFlexMap count];
    }
    else if (self.textfield_parcelamento.isFirstResponder)
    {
        if ([self.string_cardType isEqualToString:@"VISA"])
        {
            return [self.obj_orderSummary.xInstallmentMap.vISARegistradoCheckout count];
        }
        else if ([self.string_cardType isEqualToString:@"MasterCard"])
        {
            return [self.obj_orderSummary.xInstallmentMap.masterCardRegistradoCheckout count];
        }
        else if ([self.string_cardType isEqualToString:@"Dinners"])
        {
            return [self.obj_orderSummary.xInstallmentMap.discoverRegistradoCheckout count];
        }
        else
        {
            return [self.obj_orderSummary.xInstallmentMap.aMEXRegistradoCheckout count];
        }
    }
    return 0;
}


#pragma mark - UIPickerView Delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (self.textField_EscolhaOTipo.isFirstResponder)
    {
        return [array_EscolhaOTipo objectAtIndex:row];
    }
    else if (self.textfield_selecione.isFirstResponder)
    {
        return [array_selecione objectAtIndex:row];
    }
    else if (self.textfield_plandepagamento.isFirstResponder)
    {
        self.obj_xCredFlexMap = [self.obj_orderSummary.xCredFlexMap objectAtIndex:row];
        return self.obj_xCredFlexMap.dataPedido;
    }
    else if (self.textfield_parcelamento.isFirstResponder)
    {
        if ([self.string_cardType isEqualToString:@"VISA"])
        {
            self.obj_VISARegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.vISARegistradoCheckout objectAtIndex:row];
            return [NSString stringWithFormat:@"%@ X R$ %@",self.obj_VISARegistradoCheckout.numberOfInstallment,self.obj_VISARegistradoCheckout.installmentAmount];
        }
        else if ([self.string_cardType isEqualToString:@"MasterCard"])
        {
            self.obj_MasterCardRegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.masterCardRegistradoCheckout objectAtIndex:row];
            return [NSString stringWithFormat:@"%@ X R$ %@",self.obj_MasterCardRegistradoCheckout.numberOfInstallment,self.obj_MasterCardRegistradoCheckout.installmentAmount];
        }
        else if ([self.string_cardType isEqualToString:@"Dinners"])
        {
            self.obj_DiscoverRegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.discoverRegistradoCheckout objectAtIndex:row];
            return [NSString stringWithFormat:@"%@ X R$ %@",self.obj_DiscoverRegistradoCheckout.numberOfInstallment,self.obj_DiscoverRegistradoCheckout.installmentAmount];
        }
        else
        {
            self.obj_AMEXRegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.aMEXRegistradoCheckout objectAtIndex:row];
            return [NSString stringWithFormat:@"%@ X R$ %@",self.obj_AMEXRegistradoCheckout.numberOfInstallment,self.obj_AMEXRegistradoCheckout.installmentAmount];
        }
    }
    return nil;
}


- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component
{
    if (self.textField_EscolhaOTipo.isFirstResponder)
    {
        self.textField_EscolhaOTipo.text= [array_EscolhaOTipo objectAtIndex:row];
    }
    else if (self.textfield_selecione.isFirstResponder)
    {
        self.textfield_selecione.text= [array_selecione objectAtIndex:row];
        if (row == 0)
        {
            [self method_addViewsFromNibs:@"View_pagarNaLoja"];
        }
        else if(row == 1)
        {
            [self method_addViewsFromNibs:@"View_valeCredito"];
        }
        else if (row == 2)
        {
            [self method_addViewsFromNibs:@"View_cartaoBemol"];
            [self setCartoBemolViewPickerView];
        }
        else if (row == 3)
        {
            [self method_addViewsFromNibs:@"View_cartoderedito"];
            [self setCartodeValiditoPickerView];
            self.obj_xBonusBemol = [self.obj_orderSummary.xBonusBemol objectAtIndex:0];
            self.textfield_bonusbemol.text = [NSString stringWithFormat:@"R$ %@",self.obj_xBonusBemol.bonusBemol];
            self.textfield_valecredito.text = [NSString stringWithFormat:@"R$ %@",self.obj_xBonusBemol.valeCredito];
        }
        else if (row == 4)
        {
            [self method_addViewsFromNibs:@"View_boleto"];
            self.obj_xBonusBemol = [self.obj_orderSummary.xBonusBemol objectAtIndex:0];
            self.textfield_boletobonusbemol.text = [NSString stringWithFormat:@"R$ %@",self.obj_xBonusBemol.bonusBemol];
            self.textfield_boletovalecredito.text = [NSString stringWithFormat:@"R$ %@",self.obj_xBonusBemol.valeCredito];
        }
    }
    else if (self.textfield_plandepagamento.isFirstResponder)
    {
        self.obj_xCredFlexMap = [self.obj_orderSummary.xCredFlexMap objectAtIndex:row];
        self.textfield_plandepagamento.text = self.obj_xCredFlexMap.dataPedido;
    }
    else if (self.textfield_parcelamento.isFirstResponder)
    {
        if ([self.string_cardType isEqualToString:@"VISA"])
        {
            self.obj_VISARegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.vISARegistradoCheckout objectAtIndex:row];
            self.textfield_parcelamento.text =  [NSString stringWithFormat:@"%@ X R$ %@",self.obj_VISARegistradoCheckout.numberOfInstallment,self.obj_VISARegistradoCheckout.installmentAmount];
        }
        else if ([self.string_cardType isEqualToString:@"MasterCard"])
        {
            self.obj_MasterCardRegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.masterCardRegistradoCheckout objectAtIndex:row];
            self.textfield_parcelamento.text =  [NSString stringWithFormat:@"%@ X R$ %@",self.obj_MasterCardRegistradoCheckout.numberOfInstallment,self.obj_MasterCardRegistradoCheckout.installmentAmount];
        }
        else if ([self.string_cardType isEqualToString:@"Dinners"])
        {
            self.obj_DiscoverRegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.discoverRegistradoCheckout objectAtIndex:row];
            self.textfield_parcelamento.text =  [NSString stringWithFormat:@"%@ X R$ %@",self.obj_DiscoverRegistradoCheckout.numberOfInstallment,self.obj_DiscoverRegistradoCheckout.installmentAmount];
        }
        else
        {
            self.obj_AMEXRegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.aMEXRegistradoCheckout objectAtIndex:row];
            self.textfield_parcelamento.text =  [NSString stringWithFormat:@"%@ X R$ %@",self.obj_AMEXRegistradoCheckout.numberOfInstallment,self.obj_AMEXRegistradoCheckout.installmentAmount];
        }
    }
}

#pragma mark - setCartoBemolViewPickerView
-(void)setCartoBemolViewPickerView
{
    pickerView =[[UIPickerView alloc]init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    
    self.textfield_plandepagamento.inputView = pickerView;
    
    if ([self.obj_orderSummary.xCredFlexMap count])
    {
       self.obj_xCredFlexMap = [self.obj_orderSummary.xCredFlexMap objectAtIndex:0];
    }
    self.textfield_plandepagamento.text = self.obj_xCredFlexMap.dataPedido;
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setMinimumDate: [NSDate date]];
    [datePicker addTarget:self action:@selector(updatedateTextField:)
         forControlEvents:UIControlEventValueChanged];
    [self.textfield_cartobemoldateField setInputView:datePicker];
}

#pragma mark - setCartodeValiditoPickerView
-(void)setCartodeValiditoPickerView
{
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(updatedateTextField:)
         forControlEvents:UIControlEventValueChanged];
    [self.textfield_validade setInputView:datePicker];
}

#pragma mark - updatedateTextField
-(void)updatedateTextField:(UIDatePicker *)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    if ([self.textField_selectDate isFirstResponder])
    {
        self.textField_selectDate.text = [dateFormat stringFromDate:sender.date];
    }
    else if ([self.textfield_cartobemoldateField isFirstResponder])
    {
        //get tommorow date
        NSDateComponents *deltaComps = [[NSDateComponents alloc] init];
        [deltaComps setDay:1];
        NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
        DLog(@"%@",[dateFormat stringFromDate:tomorrow]);
        
        if ([[dateFormat stringFromDate:sender.date] isEqualToString:[dateFormat stringFromDate:[NSDate date]]])
        {
            [UtilitiesHelper showAlert:@"A data deve ser futuro"];
        }
        else if ([[dateFormat stringFromDate:sender.date] isEqualToString:[dateFormat stringFromDate:tomorrow]])
        {
            [UtilitiesHelper showAlert:@"A data deve ser futuro"];
        }
        else
        {
            self.textfield_cartobemoldateField.text = [dateFormat stringFromDate:sender.date];
            UIPickerView *picker_temp = (UIPickerView *)self.textfield_plandepagamento.inputView;
            NSUInteger selectedRowOfPlanoDePagemento = [picker_temp selectedRowInComponent:0];
            
            self.obj_xCredFlexMap = [self.obj_orderSummary.xCredFlexMap objectAtIndex:selectedRowOfPlanoDePagemento];
            
            if ([self.textfield_plandepagamento.text length])
            {
                [self findSimulate:[NSString stringWithFormat:@"%@",self.obj_xCredFlexMap.comEntrada]
                           SapCode:self.obj_xCredFlexMap.sapCode
                       Index_plana:[NSString stringWithFormat:@"%@",self.obj_xCredFlexMap.indexPlana]];
            }
            else
            {
                [UtilitiesHelper showAlert:@"Selecionar entrada primeiro"];
            }
            
        }
    }
    else
    {
        self.textfield_validade.text = [dateFormat stringFromDate:sender.date];
    }
    
    dateFormat = nil;
}


#pragma mark - findSimulate
-(void)findSimulate:(NSString *)com_entrada SapCode:(NSString *)sapcode Index_plana:(NSString *)index_plana
{
    [[ServiceModel sharedClient]
     POST:k_findSimulate
     parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_orderSummary.grandTotal,@"total",
                 com_entrada,@"com_entrada",
                 index_plana,@"index_plana",
                 sapcode,@"sapCode",
                 self.textfield_cartobemoldateField.text,@"dataVencimento", nil]
     onView:self.view
     success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             DLog(@"%@",responseObject);
             if (![responseObject valueForKey:@"errors"])
             {
                 self.obj_simulate = [FindSimulate instanceFromDictionary:responseObject];
                 self.obj_findSimulateSimulateMap = self.obj_simulate.simulateMap;
    
                 self.label_parcelo.text = [UtilitiesHelper method_currencyFormatter:self.obj_findSimulateSimulateMap.prestacao];
                 self.label_juros.text = [UtilitiesHelper method_currencyFormatter:self.obj_findSimulateSimulateMap.financiamento];
                 self.label_cartaobemototals.text = [UtilitiesHelper method_currencyFormatter:self.obj_findSimulateSimulateMap.prazo];
             }
             else
             {
                 [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                             valueForKey:@"errorMessage"]];
             }
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:error.description];
         //         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}


#pragma mark - method_addScrollerToView
-(void)method_addScrollerToView{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CheckOutView"
                                                         owner:self
                                                       options:nil];
    UIView *view_obj = nibContents[0];
    
    CGRect rect_new;
    rect_new = CGRectMake(0,
                          0,
                          self.scroller.frame.size.width,
                          view_obj.frame.size.height);
    view_obj.frame = rect_new;
    [self.scroller addSubview:view_obj];
    [self.scroller setContentSize:CGSizeMake(0, 1200)];
    
    
    [self method_addPickerViews];
}



#pragma mark - uitable view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.obj_orderSummary.orderItem count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_checkOut *cell = (cell_checkOut *)[tableView dequeueReusableCellWithIdentifier:@"cell_checkOut"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_checkOut" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    self.obj_orderItem = [self.obj_orderSummary.orderItem objectAtIndex:indexPath.row];
    cell.label_description.text = self.obj_orderItem.productShortDescription;
    cell.label_price.text = [UtilitiesHelper method_currencyFormatter:self.obj_orderItem.unitPrice];
    cell.label_quantity.text = [NSString stringWithFormat:@"Quantidade: %@",self.obj_orderItem.quantity];
    [cell.imageview_product
     sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/wcsstore/Bemol%@",k_imageurl,self.obj_orderItem.productThumbnail]]
                              placeholderImage:[UIImage imageNamed:k_placeHolder]];
    
    return cell;
}


#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - action_removePromoCode
- (IBAction)action_removePromoCode:(id)sender
{
    if (!self.view_coupon.hidden)
    {
        [[ServiceModel sharedClient] POST:k_removePromoCode
                               parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_orderSummary.orderId,@"orderId",@"1234",@"promoCode", nil]
                                   onView:self.view
                                  success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             if (![responseObject valueForKey:@"errors"])
             {
                 self.obj_orderSummary = [OrderSummary instanceFromDictionary:responseObject];
                 [self setUIContents];
                 [self.view_coupon setHidden:YES];
             }
             else
             {
                 [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                             valueForKey:@"errorMessage"]];
                 [self.view_coupon setHidden:NO];
             }
             
         }
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             DLog(@"%@",error.description);
             
             [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
             [self.view_coupon setHidden:NO];
         }];
    }
}

#pragma mark - action_selectedAddressOrStore
- (IBAction)action_selectedAddressOrStore:(id)sender
{
    UIButton *button_ = (UIButton *)sender;
    if (button_.selected)
        return;
    
    if ([sender tag] == 100)
    {
        [self.button_selectedstore setSelected:NO];
        [self.button_selectedaddress setSelected:YES];
        self.obj_orderItem = [self.obj_orderSummary.orderItem objectAtIndex:0];
        if (self.string_addressId == nil || [self.string_addressId isEqualToString:@""])
        {
            [self selectShippingOnline:self.obj_orderItem.addressId];
        }
        else
        {
            [self selectShippingOnline:self.string_addressId];
        }
    }
    else
    {
        [self.button_selectedstore setSelected:YES];
        [self.button_selectedaddress setSelected:NO];
        
        if (string_physicalStoreId == nil || [string_physicalStoreId isEqualToString:@""])
        {
            [self performSegueWithIdentifier:@"segue_selectLocation" sender:self.obj_orderSummary.orderId];
        }
        else
        {
            [self selectShippingPickupStore];
        }
    }
}

#pragma mark - selectShippingPickupStore
- (void)selectShippingPickupStore
{
    self.obj_orderItem = [self.obj_orderSummary.orderItem objectAtIndex:0];
    [[ServiceModel sharedClient] PUT:k_selectShippingOnline
                          parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_orderSummary.orderId,@"orderId",@"-1,-2,-3,-4,-5,-6,-7",@"x_calculationUsage",self.obj_orderItem.shipModeId,@"shipModeId",self.obj_orderItem.addressId,@"addressId",string_physicalStoreId,@"physicalStoreId", nil]
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         DLog(@"%@",responseObject);
         if (![responseObject valueForKey:@"errors"])
         {
             
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                 [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
                             }];
}

#pragma mark - selectShippingOnline
-(void)selectShippingOnline:(NSString *)addressId
{
     self.obj_orderItem = [self.obj_orderSummary.orderItem objectAtIndex:0];
    [[ServiceModel sharedClient] PUT:k_selectShippingOnline
                          parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_orderSummary.orderId,@"orderId",@"-1,-2,-3,-4,-5,-6,-7",@"x_calculationUsage",self.obj_orderItem.shipModeId,@"shipModeId",addressId,@"addressId", nil]
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         DLog(@"%@",responseObject);
         if (![responseObject valueForKey:@"errors"])
         {
             
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                 [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
                             }];
}

#pragma mark - selectShippingAddress
-(void)selectShippingAddress:(NSString *)addressId
{
    [[ServiceModel sharedClient] PUT:k_selectShippingOnline
                          parameters:[NSDictionary dictionaryWithObjectsAndKeys:@"-1,-5,-6",@"x_calculationUsage",addressId,@"addressId",self.obj_orderSummary.orderId,@"orderId", nil]
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         DLog(@"%@",responseObject);
         if (![responseObject valueForKey:@"errors"])
         {
             
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                 [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
                             }];
}



#pragma mark - textfield delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([self.textfield_coupon.text length])
    {
        [self addPromoCode];
    }
    else if ([textField isEqual:self.textfield_creditCardNumber])
    {
        if ([self.textfield_creditCardNumber.text length])
        {
                        DLog(@"%@",[self.textfield_creditCardNumber.text substringToIndex:2]);
            if ([[self.textfield_creditCardNumber.text substringToIndex:1] isEqualToString:@"4"])
            {
                [self addPickerviewtoPacelementoTextfield:@"VISA"];
            }
            else if ([[self.textfield_creditCardNumber.text substringToIndex:1] isEqualToString:@"5"])
            {
                [self addPickerviewtoPacelementoTextfield:@"MasterCard"];
            }
            else if ([[self.textfield_creditCardNumber.text substringToIndex:1] isEqualToString:@"3"] && (![[self.textfield_creditCardNumber.text substringToIndex:2] isEqualToString:@"34"] && ![[self.textfield_creditCardNumber.text substringToIndex:2] isEqualToString:@"37"]))
            {
                [self addPickerviewtoPacelementoTextfield:@"Dinners"];
            }
            else if ([[self.textfield_creditCardNumber.text substringToIndex:2] isEqualToString:@"34"] || [[self.textfield_creditCardNumber.text substringToIndex:2] isEqualToString:@"37"])
            {
                [self addPickerviewtoPacelementoTextfield:@"Amex"];
            }
        }
    }
}

#pragma mark - textField delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField isEqual:self.textfield_creditCardNumber])
    {
        if (range.location == 19)
        {
            return NO;
        }
    }
    else if([textField isEqual:self.textfield_codigoVerificador])
    {
        if (range.location == 4)
        {
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:self.textfield_selecione])
    {
        //[pickerView reloadAllComponents];
    }
    else if ([textField isEqual:self.textField_EscolhaOTipo])
    {
        [pickerView reloadAllComponents];
        [pickerView selectRow:2 inComponent:0 animated:YES];
    }
    else if ([textField isEqual:self.textfield_plandepagamento])
    {
        [pickerView reloadAllComponents];
    }
    return YES;
}

#pragma mark - unwindToVC_checkout
- (IBAction)unwindToVC_checkout:(UIStoryboardSegue *)unwindSegue
{
    VC_address *objVC_address = (VC_address *)unwindSegue.sourceViewController;
    self.label_address.text = [objVC_address.dic_selectedAddress valueForKey:@"Destinatario"];
    self.label_cep.text = [objVC_address.dic_selectedAddress valueForKey:@"CEP"];
    self.label_endereco.text = [objVC_address.dic_selectedAddress valueForKey:@"Endereco"];
    self.label_bairro.text = [objVC_address.dic_selectedAddress valueForKey:@"Bairro"];
    self.label_cidad.text = [objVC_address.dic_selectedAddress valueForKey:@"Cidad/UF"];
    self.label_nome.text = [objVC_address.dic_selectedAddress valueForKey:@"nome"];
    self.string_addressId = [objVC_address.dic_selectedAddress valueForKey:@"addressId"];
    [self selectShippingAddress:self.string_addressId];
}
#pragma mark - unwindToVC_checkoutViaNewaddress
- (IBAction)unwindToVC_checkoutViaNewaddress:(UIStoryboardSegue *)unwindSegue
{
    VC_address *objVC_address = (VC_address *)unwindSegue.sourceViewController;
    self.label_address.text = [objVC_address.dic_selectedAddress valueForKey:@"Destinatario"];
    self.label_cep.text = [objVC_address.dic_selectedAddress valueForKey:@"CEP"];
    self.label_endereco.text = [objVC_address.dic_selectedAddress valueForKey:@"Endereco"];
    self.label_bairro.text = [objVC_address.dic_selectedAddress valueForKey:@"Bairro"];
    self.label_cidad.text = [objVC_address.dic_selectedAddress valueForKey:@"Cidad/UF"];
    self.label_nome.text = [objVC_address.dic_selectedAddress valueForKey:@"nome"];
}
#pragma mark - unwindToVC_checkoutviaLocation
- (IBAction)unwindToVC_checkoutviaLocation:(UIStoryboardSegue *)unwindSegue
{
    VC_selectLocation *objVC_address = (VC_selectLocation *)unwindSegue.sourceViewController;
    self.label_name.text = [objVC_address.dic_selectedstoreSelection valueForKey:@"name"];
    self.label_addressline.text = [objVC_address.dic_selectedstoreSelection valueForKey:@"address"];
    self.label_displayvalue.text = [[objVC_address.dic_selectedstoreSelection valueForKey:@"displayValue"] kv_stringByStrippingHTML:[objVC_address.dic_selectedstoreSelection valueForKey:@"displayValue"]];
    self.label_number.text = [objVC_address.dic_selectedstoreSelection valueForKey:@"telephone1"];
    self.label_distance.text = [objVC_address.dic_selectedstoreSelection valueForKey:@"distance"];
    string_physicalStoreId = [objVC_address.dic_selectedstoreSelection valueForKey:@"uniqueID"];
    
    [self selectShippingPickupStore];
}


#pragma mark - add subviews
-(void)method_addViewsFromNibs:(NSString *)nibName
{
    if ([self.scroller viewWithTag:999])
    {
        [[self.scroller viewWithTag:999] removeFromSuperview];
    }
    
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:nibName
                                                         owner:self
                                                       options:nil];
    UIView *view_obj = nibContents[0];
    
    view_obj.tag = 999;
    
    CGRect rect_new = CGRectMake(0,
                                 self.view_payment.frame.origin.y+self.view_payment.frame.size.height,
                                 self.view.frame.size.width,
                                 view_obj.frame.size.height);
    
    view_obj.frame = rect_new;
    
    [self.scroller addSubview:view_obj];
    
    [self.button_finalizar setFrame:CGRectMake(self.button_finalizar.frame.origin.x,
                                               view_obj.frame.size.height+view_obj.frame.origin.y,
                                               self.button_finalizar.frame.size.width,
                                               self.button_finalizar.frame.size.height)];
    
    [self.scroller setContentSize:CGSizeMake(0,
                                             self.button_finalizar.frame.size.height+self.button_finalizar.frame.origin.y)];
    
    nibContents = nil;
    view_obj = nil;
}

#pragma mark - action_selectLocation
- (IBAction)action_selectLocation:(id)sender
{
    [self performSegueWithIdentifier:@"segue_selectLocation" sender:self.obj_orderSummary.orderId];
}

#pragma mark - action_finalizarCompra
- (IBAction)action_finalizarCompra:(id)sender
{
    if ([self.textfield_selecione.text isEqualToString:@"Pagar na loja"])
    {
        [self CheckoutPaymentPagarnaLojaAndBoletoBancaria:@"PayLaterRegistradoCheckout"];
    }
    else if ([self.textfield_selecione.text isEqualToString:@"Boleto Bancário"])
    {
        if (![self.checkboxButton_bonusBemol isSelected])
            [self CheckoutPaymentPagarnaLojaAndBoletoBancaria:@"BoletoBraspagRegistradoCheckout"];
        else
            [self CheckoutPaymentBoletoBancariaAndBonus];
    }
    else if ([self.textfield_selecione.text isEqualToString:@"Cartão Bemol"])
    {
        [self CheckoutPaymentCartaoBemol];
    }
    else if ([self.textfield_selecione.text isEqualToString:@"Cartão de Crédito"])
    {
        if (![self.textfield_Nome.text length])
        {
            [UtilitiesHelper showAlert:@"Por favor, insira o nome"];
        }
        else if (![self.textfield_creditCardNumber.text length])
        {
            [UtilitiesHelper showAlert:@"Por favor, insira o número do cartão de crédito"];
        }
        else if (![self.textfield_parcelamento.text length])
        {
            [UtilitiesHelper showAlert:@"Por favor, indique parcela"];
        }
        else if (![self.textfield_codigoVerificador.text length])
        {
            [UtilitiesHelper showAlert:@"Por favor, indique CVC"];
        }
        else
        {
            [self CheckoutPaymentCartaodeCredito];
        }

        
    }
}

#pragma mark - CheckoutPaymentPagarnaLojaAndBoletoBancaria
-(void)CheckoutPaymentPagarnaLojaAndBoletoBancaria:(NSString *)payMethodId
{
    self.obj_orderItem = [self.obj_orderSummary.orderItem objectAtIndex:0];
    self.obj_UsablePaymentInformation = [self.obj_orderSummary.usablePaymentInformation objectAtIndex:0];
    self.obj_UsableBillingAddres = [self.obj_UsablePaymentInformation.usableBillingAddress objectAtIndex:0];
    
    
    [[ServiceModel sharedClient] POST:k_checkout
                           parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_orderSummary.orderId,@"orderId",
                                       [ApplicationDelegate.single.dic_userDetails valueForKey:@"cpf"],@"CPFNumber",
                                       @"",@"account",
                                       [[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken],@"authToken",
                                       self.string_addressId?self.string_addressId:self.obj_orderItem.addressId,@"billing_address_id",@"",@"cc_brand",
                                       @"",@"cc_cvc",
                                       @"",@"cc_nameoncard",
                                       @"",@"expire_month",
                                       @"",@"expire_year",
                                       payMethodId,@"payMethodId",
                                       self.obj_orderSummary.grandTotal,@"piAmount",
                                       @"MI",@"sourceId",
                                       @"1",@"notifyMerchant",
                                       @"1",@"notifyShopper",
                                       @"1",@"notifyOrderSubmitted",
                                       [[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken],@"sessionId",
                                       [UtilitiesHelper getIPAddress],@"remoteAddress",
                                       @"",@"checkRoutingNumber",
                                       @"",@"check_routing_number",
                                       @"",@"checkingAccountNumber",
                                       @"",@"companyName",
                                       @"",@"dataVencimento",
                                       @"",@"entrada",
                                       @"",@"financiamento",
                                       @"",@"installmentAmount",
                                       @"",@"installmentOption",
                                       @"",@"jurosAno",
                                       @"",@"jurosMes",
                                       @"",@"numberOfInstallments",
                                       @"",@"numeroParcelas",
                                       @"",@"prazo",
                                       @"",@"prestacao",
                                       @"",@"sap_code",
                                       @"",@"valueFromPaymentTC",
                                       @"",@"valueFromProfileOrder",nil]
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  DLog(@"%@",responseObject);
                                  if (![responseObject valueForKey:@"errors"])
                                  {
                                      if ([responseObject valueForKey:@"orderId"])
                                      {
                                          [UtilitiesHelper showAlert:@"Seu pedido foi finalizado com sucesso!"];
                                          ApplicationDelegate.single.string_orderId = nil;
//                                          [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_orderid];
//                                          [[NSUserDefaults standardUserDefaults] synchronize];
                                          [self method_shoppingCartCounter:@"0"];
                                          [self.navigationController popToRootViewControllerAnimated:YES];
                                      }
                                  }
                                  else
                                  {
                                      [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                                                  valueForKey:@"errorMessage"]];
                                  }
                              }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                  [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
                              }];
}

#pragma mark - CheckoutPaymentBoletoBancariaAndBonus
-(void)CheckoutPaymentBoletoBancariaAndBonus
{
       self.obj_orderItem = [self.obj_orderSummary.orderItem objectAtIndex:0];
    
    NSDictionary *dic_request = [NSDictionary dictionaryWithObjectsAndKeys:self.obj_orderSummary.orderId,@"orderId",
     @"MI",@"sourceId",
     @"1",@"notifyMerchant",
     @"1",@"notifyShopper",
     @"1",@"notifyOrderSubmitted",
     [[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken],@"sessionId",
     [UtilitiesHelper getIPAddress],@"remoteAddress",[NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                [ApplicationDelegate.single.dic_userDetails valueForKey:@"cpf"],@"CPFNumber",
                                                                                @"",@"account",
                                                                                [[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken],@"authToken",
                                                                                self.string_addressId?self.string_addressId:self.obj_orderItem.addressId,@"billing_address_id",@"",@"cc_brand",
                                                                                @"",@"cc_cvc",
                                                                                @"",@"cc_nameoncard",
                                                                                @"",@"expire_month",
                                                                                @"",@"expire_year",
                                                                                @"BoletoBraspagRegistradoCheckout",@"payMethodId",
                                                                                self.obj_orderSummary.grandTotal,@"piAmount",
                                                                                @"",@"checkRoutingNumber",
                                                                                @"",@"check_routing_number",
                                                                                @"",@"checkingAccountNumber",
                                                                                @"",@"companyName",
                                                                                @"",@"dataVencimento",
                                                                                @"",@"entrada",
                                                                                @"",@"financiamento",
                                                                                @"",@"installmentAmount",
                                                                                @"",@"installmentOption",
                                                                                @"",@"jurosAno",
                                                                                @"",@"jurosMes",
                                                                                @"",@"numberOfInstallments",
                                                                                @"",@"numeroParcelas",
                                                                                @"",@"prazo",
                                                                                @"",@"prestacao",
                                                                                @"",@"sap_code",
                                                                                @"",@"valueFromPaymentTC",
                                                                                @"",@"valueFromProfileOrder",nil],
                                                      [NSDictionary dictionaryWithObjectsAndKeys:
                                                       [ApplicationDelegate.single.dic_userDetails valueForKey:@"cpf"],@"CPFNumber",
                                                       @"",@"account",
                                                       [[NSUserDefaults standardUserDefaults]objectForKey:k_WCTrustedToken],@"authToken",
                                                       self.string_addressId?self.string_addressId:self.obj_orderItem.addressId,@"billing_address_id",@"",@"cc_brand",
                                                       @"",@"cc_cvc",
                                                       @"",@"cc_nameoncard",
                                                       @"",@"expire_month",
                                                       @"",@"expire_year",
                                                       @"BonusRegistradoCheckout",@"payMethodId",
                                                       self.obj_orderSummary.grandTotal,@"piAmount",
                                                       @"",@"checkRoutingNumber",
                                                       @"",@"check_routing_number",
                                                       @"",@"checkingAccountNumber",
                                                       @"",@"companyName",
                                                       @"",@"dataVencimento",
                                                       @"",@"entrada",
                                                       @"",@"financiamento",
                                                       @"",@"installmentAmount",
                                                       @"",@"installmentOption",
                                                       @"",@"jurosAno",
                                                       @"",@"jurosMes",
                                                       @"",@"numberOfInstallments",
                                                       @"",@"numeroParcelas",
                                                       @"",@"prazo",
                                                       @"",@"prestacao",
                                                       @"",@"sap_code",
                                                       @"",@"valueFromPaymentTC",
                                                       @"",@"valueFromProfileOrder",nil], nil],@"paymentInstruction",nil];
    
    DLog(@"%@",dic_request);
    
    [[ServiceModel sharedClient] POST:k_checkout
                           parameters:dic_request
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
                                  DLog(@"%@",responseObject);
                                  if (![responseObject valueForKey:@"errors"])
                                  {
                                      if ([responseObject valueForKey:@"orderId"])
                                      {
                                          [UtilitiesHelper showAlert:@"Seu pedido foi finalizado com sucesso!"];
                                          ApplicationDelegate.single.string_orderId = nil;
//                                          [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_orderid];
//                                          [[NSUserDefaults standardUserDefaults] synchronize];
                                          [self method_shoppingCartCounter:@"0"];
                                          [self.navigationController popToRootViewControllerAnimated:YES];
                                      }
                                  }
                                  else
                                  {
                                      [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                                                  valueForKey:@"errorMessage"]];
                                  }
                              }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                  [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
                              }];
}

#pragma mark - CheckoutPaymentCartaoBemol
-(void)CheckoutPaymentCartaoBemol
{
    UIPickerView *picker_temp = (UIPickerView *)self.textfield_plandepagamento.inputView;
    
    NSUInteger selectedRowOfPlanoDePagemento = [picker_temp selectedRowInComponent:0];
    
    DLog(@"%lu", (unsigned long)selectedRowOfPlanoDePagemento);
    self.obj_xCredFlexMap = [self.obj_orderSummary.xCredFlexMap objectAtIndex:selectedRowOfPlanoDePagemento];
    DLog(@"%@",self.obj_xCredFlexMap.sapCode);
    
    NSDate *date = [NSDate date];
    NSUInteger componentFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:componentFlags fromDate:date];
    
    DLog(@"%ld,%ld",(long)[components year],(long)[components month]);

    
    self.obj_orderItem = [self.obj_orderSummary.orderItem objectAtIndex:0];
    [[ServiceModel sharedClient] POST:k_checkout
                           parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.obj_orderSummary.orderId,@"orderId",
                                       [ApplicationDelegate.single.dic_userDetails valueForKey:@"cpf"],@"CPFNumber",
                                       self.textfield_cartoCreditCard.text,@"account",
                                       [[NSUserDefaults standardUserDefaults]objectForKey:k_WCTrustedToken],@"authToken",
                                       self.string_addressId?self.string_addressId:self.obj_orderItem.addressId,@"billing_address_id",@"CartaoBemol",@"cc_brand",
                                       @"62425463372",@"cc_cvc",
                                       @"",@"cc_nameoncard",
                                       [NSString stringWithFormat:@"%ld",(long)[components month]],@"expire_month",
                                       [NSString stringWithFormat:@"%ld",(long)[components year]],@"expire_year",
                                       @"CartaoBemolRegistradoCheckout",@"payMethodId",
                                       self.obj_orderSummary.grandTotal,@"piAmount",
                                       @"MI",@"sourceId",
                                       @"1",@"notifyMerchant",
                                       @"1",@"notifyShopper",
                                       @"1",@"notifyOrderSubmitted",
                                       [[NSUserDefaults standardUserDefaults]objectForKey:k_WCToken],@"sessionId",
                                       [UtilitiesHelper getIPAddress],@"remoteAddress",
                                       @"",@"checkRoutingNumber",
                                       @"",@"check_routing_number",
                                       @"",@"checkingAccountNumber",
                                       @"",@"companyName",
                                       self.textfield_cartobemoldateField.text,@"dataVencimento",
                                       [NSString stringWithFormat:@"%@",self.obj_xCredFlexMap.comEntrada],@"entrada",
                                       @"0",@"financiamento",
                                       @"",@"installmentAmount",
                                       @"",@"installmentOption",
                                       @"0",@"jurosAno",
                                       @"0",@"jurosMes",
                                       @"",@"numberOfInstallments",
                                       @"1",@"numeroParcelas",
                                       @"0",@"prazo",
                                       @"0",@"prestacao",
                                       self.obj_xCredFlexMap.sapCode,@"sap_code",
                                       @"",@"valueFromPaymentTC",
                                       @"",@"valueFromProfileOrder",nil]
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  DLog(@"%@",responseObject);
                                  if (![responseObject valueForKey:@"errors"])
                                  {
                                      if ([responseObject valueForKey:@"orderId"])
                                      {
                                          [UtilitiesHelper showAlert:@"Seu pedido foi finalizado com sucesso!"];
                                          ApplicationDelegate.single.string_orderId = nil;
//                                          [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_orderid];
//                                          [[NSUserDefaults standardUserDefaults] synchronize];
                                          [self method_shoppingCartCounter:@"0"];
                                          [self.navigationController popToRootViewControllerAnimated:YES];
                                      }
                                  }
                                  else
                                  {
                                      [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                                                  valueForKey:@"errorMessage"]];
                                  }
                              }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
                                  [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
                              }];
}

#pragma mark - CheckoutPaymentCartaodeCredito
-(void)CheckoutPaymentCartaodeCredito
{
    //self.textfield_creditCardNumber.text
    DLog(@"%@",[self.textfield_creditCardNumber.text substringToIndex:1]);
    if ([[self.textfield_creditCardNumber.text substringToIndex:1] isEqualToString:@"4"])
    {
        [self CheckoutPaymentVisaAndMaster:@"VISARegistradoCheckout"];
    }
    else if ([[self.textfield_creditCardNumber.text substringToIndex:1] isEqualToString:@"5"])
    {
        [self CheckoutPaymentVisaAndMaster:@"Master CardRegistradoCheckout"];
    }
    else if ([[self.textfield_creditCardNumber.text substringToIndex:1] isEqualToString:@"3"])
    {
        [self CheckoutPaymentVisaAndMaster:@"DiscoverRegistradoCheckout"];
    }
    else if ([[self.textfield_creditCardNumber.text substringToIndex:2] isEqualToString:@"34"] || [[self.textfield_creditCardNumber.text substringToIndex:2] isEqualToString:@"37"])
    {
        //Amex
        [self CheckoutPaymentVisaAndMaster:@"AMEXRegistradoCheckout"];
    }
}
#pragma mark - addPickerviewtoPacelementoTextfield
-(void)addPickerviewtoPacelementoTextfield:(NSString *)cardType
{
    pickerView =[[UIPickerView alloc]init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    
    self.textfield_parcelamento.inputView = pickerView;
    
    if ([cardType isEqualToString:@"VISA"])
    {
        self.string_cardType = @"VISA";
        self.obj_VISARegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.vISARegistradoCheckout objectAtIndex:0];
        self.textfield_parcelamento.text = [NSString stringWithFormat:@"%@ X R$ %@",self.obj_VISARegistradoCheckout.numberOfInstallment,self.obj_VISARegistradoCheckout.installmentAmount];
    }
    else if ([cardType isEqualToString:@"MasterCard"])
    {
        self.string_cardType = @"MasterCard";
        self.obj_MasterCardRegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.masterCardRegistradoCheckout objectAtIndex:0];
        self.textfield_parcelamento.text = [NSString stringWithFormat:@"%@ X R$ %@",self.obj_MasterCardRegistradoCheckout.numberOfInstallment,self.obj_MasterCardRegistradoCheckout.installmentAmount];
    }
    else if ([cardType isEqualToString:@"Dinners"])
    {
        self.string_cardType = @"Dinners";
        self.obj_DiscoverRegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.discoverRegistradoCheckout objectAtIndex:0];
        self.textfield_parcelamento.text = [NSString stringWithFormat:@"%@ X R$ %@",self.obj_DiscoverRegistradoCheckout.numberOfInstallment,self.obj_DiscoverRegistradoCheckout.installmentAmount];
    }
    else
    {
        self.string_cardType = @"Amex";
        self.obj_AMEXRegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.aMEXRegistradoCheckout objectAtIndex:0];
        self.textfield_parcelamento.text = [NSString stringWithFormat:@"%@ X R$ %@",self.obj_AMEXRegistradoCheckout.numberOfInstallment,self.obj_AMEXRegistradoCheckout.installmentAmount];
    }

}
#pragma mark - CheckoutPaymentVisa
-(void)CheckoutPaymentVisaAndMaster:(NSString *)payMethodId
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormat dateFromString:self.textfield_validade.text];
    
    NSUInteger componentFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:componentFlags fromDate:dateFromString];
    
    DLog(@"%ld,%ld",(long)[components year],(long)[components month]);
    
    self.obj_orderItem = [self.obj_orderSummary.orderItem objectAtIndex:0];
    
    UIPickerView *picker_temp = (UIPickerView *)self.textfield_parcelamento.inputView;
    
    NSUInteger selectedRowOfparcelamento = [picker_temp selectedRowInComponent:0];
    
    DLog(@"%lu", (unsigned long)selectedRowOfparcelamento);
    
    
    if ([self.string_cardType isEqualToString:@"VISA"])
    {
        self.obj_VISARegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.vISARegistradoCheckout objectAtIndex:selectedRowOfparcelamento];
    }
    else if ([self.string_cardType isEqualToString:@"MasterCard"])
    {
        self.obj_VISARegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.masterCardRegistradoCheckout objectAtIndex:selectedRowOfparcelamento];
    }
    else if ([self.string_cardType isEqualToString:@"Dinners"])
    {
        self.obj_VISARegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.discoverRegistradoCheckout objectAtIndex:selectedRowOfparcelamento];
    }
    else
    {
        self.obj_VISARegistradoCheckout = [self.obj_orderSummary.xInstallmentMap.aMEXRegistradoCheckout objectAtIndex:selectedRowOfparcelamento];
    }
    
    
    
    
    DLog(@"%@",self.obj_VISARegistradoCheckout.installmentAmount);
    
    DLog(@"%@",self.obj_orderSummary.orderId);
    DLog(@"%@",self.obj_orderSummary.grandTotal);
    
    NSDictionary *dic_params;
    
    if (self.checkboxButton_CreditobonusBemol.selected)
    {
       dic_params = [NSDictionary dictionaryWithObjectsAndKeys:self.obj_orderSummary.orderId,@"orderId",
         @"MI",@"sourceId",
         @"1",@"notifyMerchant",
         @"1",@"notifyShopper",
         @"1",@"notifyOrderSubmitted",
         [[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken],@"sessionId",
         [UtilitiesHelper getIPAddress],@"remoteAddress",
         [NSArray arrayWithObjects:
          [NSDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_userDetails valueForKey:@"cpf"],@"CPFNumber",
                                    self.textfield_creditCardNumber.text,@"account",
                                    [[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken],@"authToken",
                                    self.string_addressId?self.string_addressId:self.obj_orderItem.addressId,@"billing_address_id",self.string_cardType,@"cc_brand",
                                    self.textfield_codigoVerificador.text,@"cc_cvc",
                                    self.textfield_Nome.text,@"cc_nameoncard",
                                    [NSString stringWithFormat:@"%ld",(long)[components month]],@"expire_month",
                                    [NSString stringWithFormat:@"%ld",(long)[components year]],@"expire_year",
                                    payMethodId,@"payMethodId",
                                    self.obj_orderSummary.grandTotal,@"piAmount",
                                    @"",@"checkRoutingNumber",
                                    @"",@"check_routing_number",
                                    @"",@"checkingAccountNumber",
                                    @"",@"companyName",
                                    @"",@"dataVencimento",
                                    @"",@"entrada",
                                    @"",@"financiamento",
                                    [NSString stringWithFormat:@"%@",self.obj_VISARegistradoCheckout.installmentAmount],@"installmentAmount",
                                    [NSString stringWithFormat:@"%@ x de R$ %@ sem juros",self.obj_VISARegistradoCheckout.numberOfInstallment,self.obj_VISARegistradoCheckout.installmentAmount],@"installmentOption",
                                    @"",@"jurosAno",
                                    @"",@"jurosMes",
                                    [NSString stringWithFormat:@"%@",self.obj_VISARegistradoCheckout.numberOfInstallment],@"numberOfInstallments",
                                    @"",@"numeroParcelas",
                                    @"",@"prazo",
                                    @"",@"prestacao",
                                    @"",@"sap_code",
                                    @"",@"valueFromPaymentTC",
                                    @"",@"valueFromProfileOrder",nil],
          [NSDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_userDetails valueForKey:@"cpf"],@"CPFNumber",
           @"",@"account",
           [[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken],@"authToken",
           self.string_addressId?self.string_addressId:self.obj_orderItem.addressId,@"billing_address_id",@"",@"cc_brand",
           @"",@"cc_cvc",
           self.textfield_Nome.text,@"cc_nameoncard",
           @"",@"expire_month",
           @"",@"expire_year",
           @"BonusRegistradoCheckout",@"payMethodId",
           [self.textfield_bonusbemol.text stringByReplacingOccurrencesOfString:@"R$ " withString:@""],@"piAmount",
           @"",@"checkRoutingNumber",
           @"",@"check_routing_number",
           @"",@"checkingAccountNumber",
           @"",@"companyName",
           @"",@"dataVencimento",
           @"",@"entrada",
           @"",@"financiamento",
           @"",@"installmentAmount",
           @"",@"installmentOption",
           @"",@"jurosAno",
           @"",@"jurosMes",
           @"",@"numberOfInstallments",
           @"",@"numeroParcelas",
           @"",@"prazo",
           @"",@"prestacao",
           @"",@"sap_code",
           @"",@"valueFromPaymentTC",
           @"",@"valueFromProfileOrder",nil], nil],@"paymentInstruction", nil];
    }
    else
    {
        dic_params = [NSDictionary dictionaryWithObjectsAndKeys:self.obj_orderSummary.orderId,@"orderId",
                      [ApplicationDelegate.single.dic_userDetails valueForKey:@"cpf"],@"CPFNumber",
                      self.textfield_creditCardNumber.text,@"account",
                      [[NSUserDefaults standardUserDefaults] objectForKey:k_WCTrustedToken],@"authToken",
                      self.string_addressId?self.string_addressId:self.obj_orderItem.addressId,@"billing_address_id",self.string_cardType,@"cc_brand",
                      self.textfield_codigoVerificador.text,@"cc_cvc",
                      self.textfield_Nome.text,@"cc_nameoncard",
                      [NSString stringWithFormat:@"%ld",(long)[components month]],@"expire_month",
                      [NSString stringWithFormat:@"%ld",(long)[components year]],@"expire_year",
                      payMethodId,@"payMethodId",
                      self.obj_orderSummary.grandTotal,@"piAmount",
                      @"MI",@"sourceId",
                      @"1",@"notifyMerchant",
                      @"1",@"notifyShopper",
                      @"1",@"notifyOrderSubmitted",
                      [[NSUserDefaults standardUserDefaults] objectForKey:k_WCToken],@"sessionId",
                      [UtilitiesHelper getIPAddress],@"remoteAddress",
                      @"",@"checkRoutingNumber",
                      @"",@"check_routing_number",
                      @"",@"checkingAccountNumber",
                      @"",@"companyName",
                      @"",@"dataVencimento",
                      @"",@"entrada",
                      @"",@"financiamento",
                      [NSString stringWithFormat:@"%@",self.obj_VISARegistradoCheckout.installmentAmount],@"installmentAmount",
                      [NSString stringWithFormat:@"%@ x de R$ %@ sem juros",self.obj_VISARegistradoCheckout.numberOfInstallment,self.obj_VISARegistradoCheckout.installmentAmount],@"installmentOption",
                      @"",@"jurosAno",
                      @"",@"jurosMes",
                      [NSString stringWithFormat:@"%@",self.obj_VISARegistradoCheckout.numberOfInstallment],@"numberOfInstallments",
                      @"",@"numeroParcelas",
                      @"",@"prazo",
                      @"",@"prestacao",
                      @"",@"sap_code",
                      @"",@"valueFromPaymentTC",
                      @"",@"valueFromProfileOrder",nil];
    }
    
    [[ServiceModel sharedClient] POST:k_checkout
                           parameters:dic_params
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  DLog(@"%@",responseObject);
                                  if (![responseObject valueForKey:@"errors"])
                                  {
                                      if ([responseObject valueForKey:@"orderId"])
                                      {
                                          [UtilitiesHelper showAlert:@"Seu pedido foi finalizado com sucesso!"];
                                          ApplicationDelegate.single.string_orderId = nil;
//                                          [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_orderid];
//                                          [[NSUserDefaults standardUserDefaults] synchronize];
                                          [self method_shoppingCartCounter:@"0"];
                                          ApplicationDelegate.single.string_orderId = nil;
//                                          [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_orderid];
//                                          [[NSUserDefaults standardUserDefaults] synchronize];
                                          [self.navigationController popToRootViewControllerAnimated:YES];
                                      }
                                  }
                                  else
                                  {
                                      [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                                                  valueForKey:@"errorMessage"]];
                                  }
                              }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                  [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
                              }];
}

#pragma mark - method_shopping Cart Counter
-(void)method_shoppingCartCounter:(NSString *)counter
{
    NSUserDefaults *obj = [NSUserDefaults standardUserDefaults];
    
    //[obj setValue:nil forKey:k_userId];
    [obj setValue:nil forKey:k_count];
    [obj synchronize];
    
    DLog(@"%@",[obj stringForKey:k_count]);

    
    [obj synchronize];
}

#pragma mark - action_bonusBemol
- (IBAction)action_bonusBemol:(id)sender
{
    if ([self.checkboxButton_bonusBemol isSelected])
        [self.checkboxButton_bonusBemol setSelected:NO];
    else
        [self.checkboxButton_bonusBemol setSelected:YES];
}
#pragma mark - action_Creditobonusbemol
- (IBAction)action_Creditobonusbemol:(id)sender
{
    if ([self.checkboxButton_CreditobonusBemol isSelected])
        [self.checkboxButton_CreditobonusBemol setSelected:NO];
    else
        [self.checkboxButton_CreditobonusBemol setSelected:YES];
}
@end
