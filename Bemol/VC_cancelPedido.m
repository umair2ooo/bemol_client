//
//  VC_cancelPedido.m
//  Bemol
//
//  Created by Fahim Bilwani on 27/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "VC_cancelPedido.h"
#import "NotaFiscal.h"
#import "OrderItemList.h"
#import "VC_pedidoMessage.h"

@interface VC_cancelPedido ()

- (IBAction)action_back:(id)sender;
- (IBAction)action_cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *label_pedido;
@property (weak, nonatomic) IBOutlet UILabel *label_compara;
@property (weak, nonatomic) IBOutlet UILabel *label_valor;
@property (weak, nonatomic) IBOutlet UITextView *textview_justification;

@property(nonatomic, strong)NotaFiscal *obj_notaFiscal;
@property(nonatomic, strong)OrderItemList *obj_orderItemList;
@end

@implementation VC_cancelPedido

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Historico de compras"];
    
    DLog(@"%@",self.obj_orderDetails.numeroDoPedido);
    self.label_pedido.text = [NSString stringWithFormat:@"Pedido: %@",self.obj_orderDetails.numeroDoPedido];
    self.label_valor.text = [NSString stringWithFormat:@"R$ %@",self.obj_orderDetails.valor];
    self.label_compara.text = [NSString stringWithFormat:@"Compra realizada em %@",self.obj_orderDetails.compra];
    
    DLog(@"%d",self.bool_ispedido);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segue_message"])
    {
        VC_pedidoMessage *obj = segue.destinationViewController;
        obj.bool_ispedido = self.bool_ispedido;
    }
}


- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)action_cancel:(id)sender
{
//    NSDictionary *dic_params;
//    if (self.bool_ispedido)
//    {
//        [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"isPedido",
//         self.obj_List.orderId,@"orderId",
//         self.obj_List.lojaId,@"lojaId",
//         self.textview_justification.text,@"cancelJustification",
//         @"",@"cancelStatus",
//         @"",@"cancelSKUs",
//         @"",@"cancelAction",
//         @"",@"strSKUsEntregaId",
//         @"",@"strSKUsNotaFiscaisId",
//         @"",@"quantidade",nil];
//    }
//    else
//    {
//        [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"isPedido",
//         self.obj_List.orderId,@"orderId",
//         self.obj_List.lojaId,@"lojaId",
//         self.textview_justification.text,@"cancelJustification",
//         @"N",@"cancelStatus",
//         @"",@"cancelSKUs",
//         @"",@"cancelAction",
//         @"",@"strSKUsEntregaId",
//         @"",@"strSKUsNotaFiscaisId",
//         @"",@"quantidade",nil];
//    }
        self.obj_notaFiscal = [self.obj_orderDetails.notaFiscal objectAtIndex:0];
        self.obj_orderItemList = [self.obj_notaFiscal.itemList objectAtIndex:0];

        [[ServiceModel sharedClient] POST:k_cancelPedido
                               parameters:[NSDictionary dictionaryWithObjectsAndKeys:@"0",@"isPedido",
                                           self.obj_orderDetails.orderId,@"orderId",
                                           self.obj_orderDetails.lojaId,@"lojaId",
                                           self.textview_justification.text,@"cancelJustification",
                                           self.obj_orderItemList.strSKUsStatus,@"cancelStatus",
                                           self.obj_orderItemList.sku,@"cancelSKUs",
                                           @"",@"cancelAction",
                                           @"",@"strSKUsEntregaId",
                                           @"",@"strSKUsNotaFiscaisId",
                                           @"",@"quantidade",nil]
    onView:self.view
    success:^(AFHTTPRequestOperation *operation, id responseObject)
        {
            if (![responseObject valueForKey:@"errors"])
            {
                
                    if ([[responseObject valueForKey:@"valor"] boolValue] == TRUE)
                    {
                        [self performSegueWithIdentifier:@"segue_message" sender:nil];
                    }
            }
            else
            {
                [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                            valueForKey:@"errorMessage"]];
            }
        }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
        {
            DLog(@"%@",error.description);
            [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
        }];
}
@end
