#import <Foundation/Foundation.h>

@interface Entry : NSObject {

    NSString *count;
    NSString *entryValue;
    NSString *label;

}

@property (nonatomic, copy) NSString *count;
@property (nonatomic, copy) NSString *entryValue;
@property (nonatomic, copy) NSString *label;

+ (Entry *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
