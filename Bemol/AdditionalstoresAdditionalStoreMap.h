#import <Foundation/Foundation.h>

@interface AdditionalstoresAdditionalStoreMap : NSObject {

    NSMutableArray *physicalStoreList;

}

@property (nonatomic, copy) NSMutableArray *physicalStoreList;

+ (AdditionalstoresAdditionalStoreMap *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
