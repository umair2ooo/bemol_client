#import "ProductList.h"

#import "ProductListCatalogEntryView.h"
#import "FacetView.h"
#import "ProductListMetaDatum.h"

@implementation ProductList

@synthesize catalogEntryView;
@synthesize facetView;
@synthesize metaData;
@synthesize recordSetComplete;
@synthesize recordSetCount;
@synthesize recordSetStartNumber;
@synthesize recordSetTotal;
@synthesize resourceId;
@synthesize resourceName;

+ (ProductList *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ProductList *instance = [[ProductList alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }


    NSArray *receivedCatalogEntryView = [aDictionary objectForKey:@"CatalogEntryView"];
    if ([receivedCatalogEntryView isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedCatalogEntryView = [NSMutableArray arrayWithCapacity:[receivedCatalogEntryView count]];
        for (NSDictionary *item in receivedCatalogEntryView) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedCatalogEntryView addObject:[ProductListCatalogEntryView instanceFromDictionary:item]];
            }
        }

        self.catalogEntryView = populatedCatalogEntryView;

    }

    NSArray *receivedFacetView = [aDictionary objectForKey:@"FacetView"];
    if ([receivedFacetView isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedFacetView = [NSMutableArray arrayWithCapacity:[receivedFacetView count]];
        for (NSDictionary *item in receivedFacetView) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedFacetView addObject:[FacetView instanceFromDictionary:item]];
            }
        }

        self.facetView = populatedFacetView;

    }

    NSArray *receivedMetaData = [aDictionary objectForKey:@"MetaData"];
    if ([receivedMetaData isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedMetaData = [NSMutableArray arrayWithCapacity:[receivedMetaData count]];
        for (NSDictionary *item in receivedMetaData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedMetaData addObject:[ProductListMetaDatum instanceFromDictionary:item]];
            }
        }

        self.metaData = populatedMetaData;

    }
    self.recordSetComplete = [aDictionary objectForKey:@"recordSetComplete"];
    self.recordSetCount = [aDictionary objectForKey:@"recordSetCount"];
    self.recordSetStartNumber = [aDictionary objectForKey:@"recordSetStartNumber"];
    self.recordSetTotal = [aDictionary objectForKey:@"recordSetTotal"];
    self.resourceId = [aDictionary objectForKey:@"resourceId"];
    self.resourceName = [aDictionary objectForKey:@"resourceName"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.catalogEntryView) {
        [dictionary setObject:self.catalogEntryView forKey:@"catalogEntryView"];
    }

    if (self.facetView) {
        [dictionary setObject:self.facetView forKey:@"facetView"];
    }

    if (self.metaData) {
        [dictionary setObject:self.metaData forKey:@"metaData"];
    }

    if (self.recordSetComplete) {
        [dictionary setObject:self.recordSetComplete forKey:@"recordSetComplete"];
    }

    if (self.recordSetCount) {
        [dictionary setObject:self.recordSetCount forKey:@"recordSetCount"];
    }

    if (self.recordSetStartNumber) {
        [dictionary setObject:self.recordSetStartNumber forKey:@"recordSetStartNumber"];
    }

    if (self.recordSetTotal) {
        [dictionary setObject:self.recordSetTotal forKey:@"recordSetTotal"];
    }

    if (self.resourceId) {
        [dictionary setObject:self.resourceId forKey:@"resourceId"];
    }

    if (self.resourceName) {
        [dictionary setObject:self.resourceName forKey:@"resourceName"];
    }

    return dictionary;

}


@end
