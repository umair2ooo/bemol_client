#import <Foundation/Foundation.h>

@interface ProductList : NSObject {

    NSMutableArray *catalogEntryView;
    NSMutableArray *facetView;
    NSMutableArray *metaData;
    NSString *recordSetComplete;
    NSString *recordSetCount;
    NSString *recordSetStartNumber;
    NSString *recordSetTotal;
    NSString *resourceId;
    NSString *resourceName;

}

@property (nonatomic, copy) NSMutableArray *catalogEntryView;
@property (nonatomic, copy) NSMutableArray *facetView;
@property (nonatomic, copy) NSMutableArray *metaData;
@property (nonatomic, copy) NSString *recordSetComplete;
@property (nonatomic, copy) NSString *recordSetCount;
@property (nonatomic, copy) NSString *recordSetStartNumber;
@property (nonatomic, copy) NSString *recordSetTotal;
@property (nonatomic, copy) NSString *resourceId;
@property (nonatomic, copy) NSString *resourceName;

+ (ProductList *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
