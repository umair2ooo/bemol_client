#import "FacetView.h"

#import "Entry.h"

@implementation FacetView

@synthesize entry;
@synthesize name;
@synthesize value;

+ (FacetView *)instanceFromDictionary:(NSDictionary *)aDictionary {

    FacetView *instance = [[FacetView alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }


    NSArray *receivedEntry = [aDictionary objectForKey:@"Entry"];
    if ([receivedEntry isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedEntry = [NSMutableArray arrayWithCapacity:[receivedEntry count]];
        for (NSDictionary *item in receivedEntry) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedEntry addObject:[Entry instanceFromDictionary:item]];
            }
        }

        self.entry = populatedEntry;

    }
    self.name = [aDictionary objectForKey:@"name"];
    self.value = [aDictionary objectForKey:@"value"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.entry) {
        [dictionary setObject:self.entry forKey:@"entry"];
    }

    if (self.name) {
        [dictionary setObject:self.name forKey:@"name"];
    }

    if (self.value) {
        [dictionary setObject:self.value forKey:@"value"];
    }

    return dictionary;

}


@end
