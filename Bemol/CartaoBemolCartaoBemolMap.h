#import <Foundation/Foundation.h>

@interface CartaoBemolCartaoBemolMap : NSObject {

    NSString *string_9;
    NSString *address;
    NSString *bairro;
    NSString *birthDay;
    NSString *birthMonth;
    NSString *birthYear;
    NSString *cEP;
    NSString *cLI;
    NSString *cPF;
    NSString *gender;
    NSString *image;
    NSString *mobile;
    NSString *name;
    NSString *number;
    NSString *state;
    NSString *t03;
    NSString *telephone;
    NSString *vALIDOATE;
    NSString *viaCartao;
}

@property (nonatomic, copy) NSString *string_9;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *bairro;
@property (nonatomic, copy) NSString *birthDay;
@property (nonatomic, copy) NSString *birthMonth;
@property (nonatomic, copy) NSString *birthYear;
@property (nonatomic, copy) NSString *cEP;
@property (nonatomic, copy) NSString *cLI;
@property (nonatomic, copy) NSString *cPF;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *t03;
@property (nonatomic, copy) NSString *telephone;
@property (nonatomic, copy) NSString *vALIDOATE;
@property (nonatomic, copy) NSString *viaCartao;

+ (CartaoBemolCartaoBemolMap *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
