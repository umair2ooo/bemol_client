//
//  cell_pagamentos.m
//  Bemol
//
//  Created by Fahim Bilwani on 27/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "cell_pagamentos.h"

@implementation cell_pagamentos

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)method_setValues:(NSDictionary *)dic
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date_ = [[NSDate alloc] init];
    date_ = [dateFormat dateFromString:[dic valueForKey:@"date"]];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    self.label_date.text = [dateFormat stringFromDate:date_];
    dateFormat = nil;
    date_ = nil;
    
    self.label_amount.text = [UtilitiesHelper method_currencyFormatter:[dic valueForKey:@"amount"]] ;
    [UtilitiesHelper setRoundedView:self.imagview_circle toDiameter:20];
    
    if ([[dic valueForKey:@"Status"] isEqualToString:@"P"])
    {
        [self.imagview_circle setBackgroundColor:[UtilitiesHelper colorFromHexString:@"#319C00"]];
        [self.label_date setTextColor:[UtilitiesHelper colorFromHexString:@"#319C00"]];
        [self.label_amount setTextColor:[UtilitiesHelper colorFromHexString:@"#319C00"]];
    }
    else if ([[dic valueForKey:@"Status"] isEqualToString:@"A"])
    {
        [self.imagview_circle setBackgroundColor:[UtilitiesHelper colorFromHexString:@"#000000"]];
        [self.label_date setTextColor:[UtilitiesHelper colorFromHexString:@"#000000"]];
        [self.label_amount setTextColor:[UtilitiesHelper colorFromHexString:@"#000000"]];
    }
    else if ([[dic valueForKey:@"Status"] isEqualToString:@"E"])
    {
        [self.imagview_circle setBackgroundColor:[UtilitiesHelper colorFromHexString:@"#c3003f"]];
        [self.label_date setTextColor:[UtilitiesHelper colorFromHexString:@"#c3003f"]];
        [self.label_amount setTextColor:[UtilitiesHelper colorFromHexString:@"#c3003f"]];
    }
    else if ([[dic valueForKey:@"Status"] isEqualToString:@"R"])
    {
        [self.imagview_circle setBackgroundColor:[UtilitiesHelper colorFromHexString:@"#6503b0"]];
        [self.label_date setTextColor:[UtilitiesHelper colorFromHexString:@"#6503b0"]];
        [self.label_amount setTextColor:[UtilitiesHelper colorFromHexString:@"#6503b0"]];
    }
    else if ([[dic valueForKey:@"Status"] isEqualToString:@"H"])
    {
        [self.imagview_circle setBackgroundColor:[UtilitiesHelper colorFromHexString:@"#0864b7"]];
        [self.label_date setTextColor:[UtilitiesHelper colorFromHexString:@"#0864b7"]];
        [self.label_amount setTextColor:[UtilitiesHelper colorFromHexString:@"#0864b7"]];
    }
    else if ([[dic valueForKey:@"Status"] isEqualToString:@"W"])
    {
        [self.imagview_circle setBackgroundColor:[UtilitiesHelper colorFromHexString:@"#FF3300"]];
        [self.label_date setTextColor:[UtilitiesHelper colorFromHexString:@"#FF3300"]];
        [self.label_amount setTextColor:[UtilitiesHelper colorFromHexString:@"#FF3300"]];
    }
}
@end
