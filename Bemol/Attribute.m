#import "Attribute.h"

#import "Value.h"

@implementation Attribute

@synthesize comparable;
@synthesize dataType;
@synthesize displayable;
@synthesize identifier;
@synthesize name;
@synthesize searchable;
@synthesize uniqueID;
@synthesize usage;
@synthesize values;

+ (Attribute *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Attribute *instance = [[Attribute alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.comparable = [aDictionary objectForKey:@"comparable"];
    self.dataType = [aDictionary objectForKey:@"dataType"];
    self.displayable = [aDictionary objectForKey:@"displayable"];
    self.identifier = [aDictionary objectForKey:@"identifier"];
    self.name = [aDictionary objectForKey:@"name"];
    self.searchable = [aDictionary objectForKey:@"searchable"];
    self.uniqueID = [aDictionary objectForKey:@"uniqueID"];
    self.usage = [aDictionary objectForKey:@"usage"];

    NSArray *receivedValues = [aDictionary objectForKey:@"Values"];
    if ([receivedValues isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedValues = [NSMutableArray arrayWithCapacity:[receivedValues count]];
        for (NSDictionary *item in receivedValues) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedValues addObject:[Value instanceFromDictionary:item]];
            }
        }

        self.values = populatedValues;

    }

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.comparable) {
        [dictionary setObject:self.comparable forKey:@"comparable"];
    }

    if (self.dataType) {
        [dictionary setObject:self.dataType forKey:@"dataType"];
    }

    if (self.displayable) {
        [dictionary setObject:self.displayable forKey:@"displayable"];
    }

    if (self.identifier) {
        [dictionary setObject:self.identifier forKey:@"identifier"];
    }

    if (self.name) {
        [dictionary setObject:self.name forKey:@"name"];
    }

    if (self.searchable) {
        [dictionary setObject:self.searchable forKey:@"searchable"];
    }

    if (self.uniqueID) {
        [dictionary setObject:self.uniqueID forKey:@"uniqueID"];
    }

    if (self.usage) {
        [dictionary setObject:self.usage forKey:@"usage"];
    }

    if (self.values) {
        [dictionary setObject:self.values forKey:@"values"];
    }

    return dictionary;

}


@end
