#import "XproductReviewList.h"

#import "ReviewList.h"

@implementation XproductReviewList

@synthesize Estrelas1;
@synthesize Estrelas2;
@synthesize Estrelas3;
@synthesize Estrelas4;
@synthesize Estrelas5;
@synthesize avgRate;
@synthesize reviewList;
@synthesize totalReviews;

+ (XproductReviewList *)instanceFromDictionary:(NSDictionary *)aDictionary {

    XproductReviewList *instance = [[XproductReviewList alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.Estrelas1 = [aDictionary objectForKey:@"1_estrelas"];
    self.Estrelas2 = [aDictionary objectForKey:@"2_estrelas"];
    self.Estrelas3 = [aDictionary objectForKey:@"3_estrelas"];
    self.Estrelas4 = [aDictionary objectForKey:@"4_estrelas"];
    self.Estrelas5 = [aDictionary objectForKey:@"5_estrelas"];
    self.avgRate = [aDictionary objectForKey:@"Avg_Rate"];

    NSArray *receivedReviewList = [aDictionary objectForKey:@"ReviewList"];
    if ([receivedReviewList isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedReviewList = [NSMutableArray arrayWithCapacity:[receivedReviewList count]];
        for (NSDictionary *item in receivedReviewList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedReviewList addObject:[ReviewList instanceFromDictionary:item]];
            }
        }

        self.reviewList = populatedReviewList;

    }
    self.totalReviews = [aDictionary objectForKey:@"Total_Reviews"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.Estrelas1) {
        [dictionary setObject:self.Estrelas1 forKey:@"1_estrelas"];
    }

    if (self.Estrelas2) {
        [dictionary setObject:self.Estrelas2 forKey:@"2_estrelas"];
    }

    if (self.Estrelas3) {
        [dictionary setObject:self.Estrelas3 forKey:@"3_estrelas"];
    }

    if (self.Estrelas4) {
        [dictionary setObject:self.Estrelas4 forKey:@"4_estrelas"];
    }

    if (self.Estrelas5) {
        [dictionary setObject:self.Estrelas5 forKey:@"5_estrelas"];
    }

    if (self.avgRate) {
        [dictionary setObject:self.avgRate forKey:@"avgRate"];
    }

    if (self.reviewList) {
        [dictionary setObject:self.reviewList forKey:@"reviewList"];
    }

    if (self.totalReviews) {
        [dictionary setObject:self.totalReviews forKey:@"totalReviews"];
    }

    return dictionary;

}


@end
