#import <Foundation/Foundation.h>

@interface CartaoBemolCreditoMap : NSObject {

    NSNumber *disponivel;
    NSNumber *limiteDeCredito;
    NSNumber *saldoDisponivel;
    NSNumber *saldoUtilizado;

}

@property (nonatomic, copy) NSNumber *disponivel;
@property (nonatomic, copy) NSNumber *limiteDeCredito;
@property (nonatomic, copy) NSNumber *saldoDisponivel;
@property (nonatomic, copy) NSNumber *saldoUtilizado;

+ (CartaoBemolCreditoMap *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
