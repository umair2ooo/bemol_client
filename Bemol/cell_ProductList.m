#import "cell_ProductList.h"
#import <UIKit/UIKit.h>

@implementation cell_ProductList

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)method_setValues:(NSDictionary *)dic
{
    DLog(@"image: %@", [NSString stringWithFormat:@"%@%@",k_imageurl,[dic valueForKey:@"image"]]);
    [self.image_product sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",k_imageurl,[dic valueForKey:@"image"]]]
                          placeholderImage:[UIImage imageNamed:k_placeHolder]];

    self.label_name.text = [dic valueForKey:@"name"];
    if ([dic valueForKey:@"price"] != nil)
    {
        self.lable_Price.text = [UtilitiesHelper method_currencyFormatter:[dic valueForKey:@"price"]];
        //self.lable_Price.text = [dic valueForKey:@"price"];
        //[NSString stringWithFormat:@"Br %@/each",[[dic valueForKey:@"price"] stringByReplacingOccurrencesOfString:@"." withString:@","]];
    }
    else
    {
        self.lable_Price.text = @"-";
    }
}

@end