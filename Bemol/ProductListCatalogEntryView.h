#import <Foundation/Foundation.h>

@interface ProductListCatalogEntryView : NSObject {

    NSString *buyable;
    NSString *fullImage;
    NSString *keyword;
    NSString *name;
    NSString *parentCategoryID;
    NSString *partNumber;
    NSMutableArray *price;
    NSString *productType;
    NSString *resourceId;
    NSString *shortDescription;
    NSString *storeID;
    NSString *thumbnail;
    NSString *uniqueID;
    NSMutableArray *xproductReviewList;

}

@property (nonatomic, copy) NSString *buyable;
@property (nonatomic, copy) NSString *fullImage;
@property (nonatomic, copy) NSString *keyword;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *parentCategoryID;
@property (nonatomic, copy) NSString *partNumber;
@property (nonatomic, copy) NSMutableArray *price;
@property (nonatomic, copy) NSString *productType;
@property (nonatomic, copy) NSString *resourceId;
@property (nonatomic, copy) NSString *shortDescription;
@property (nonatomic, copy) NSString *storeID;
@property (nonatomic, copy) NSString *thumbnail;
@property (nonatomic, copy) NSString *uniqueID;
@property (nonatomic, copy) NSMutableArray *xproductReviewList;

+ (ProductListCatalogEntryView *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
