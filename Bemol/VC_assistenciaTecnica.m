//
//  VC_assistenciaTecnica.m
//  Bemol
//
//  Created by Fahim Bilwani on 26/10/2015.
//  Copyright © 2015 Royal Cyber. All rights reserved.
//

#import "VC_assistenciaTecnica.h"

@interface VC_assistenciaTecnica ()<UIPickerViewDataSource,UIPickerViewDelegate>
{
    UIPickerView *picker_;
}

@property (weak, nonatomic) IBOutlet UITextField *textfield_city;
@property (weak, nonatomic) IBOutlet UITextField *textfield_brand;
@property (strong, nonatomic) NSArray *array_brands;
@property (strong, nonatomic) NSDictionary *dic_assistenciaTecnica;
@property (strong, nonatomic) NSDictionary *dic_branddetail;

@property (weak, nonatomic) IBOutlet UILabel *label_nome;
@property (weak, nonatomic) IBOutlet UILabel *label_Endereco;
@property (weak, nonatomic) IBOutlet UILabel *label_Telefone;
@property (weak, nonatomic) IBOutlet UILabel *label_Cidade;
@property (weak, nonatomic) IBOutlet UILabel *label_Estado;
@property (weak, nonatomic) IBOutlet UIView *view_brandDetails;

- (IBAction)action_back:(id)sender;
@end

@implementation VC_assistenciaTecnica

@synthesize array_brands;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Assistências Técnicas Autorizadas"];
    
    [[ServiceModel sharedClient] GET:k_technicalassistence
                          parameters:nil
                              onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (![responseObject valueForKey:@"errors"])
         {
             picker_ = [[UIPickerView alloc] init];
             [picker_ setDelegate:self];
             [picker_ setDataSource:self];
             self.textfield_city.inputView = picker_;
             self.textfield_brand.inputView = picker_;
             
             self.dic_assistenciaTecnica = [NSDictionary dictionaryWithDictionary:responseObject];
         }
         else
         {
             [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0]
                                         valueForKey:@"errorMessage"]];
         }
         
     }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - picker delegate

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.textfield_city.isFirstResponder)
        return [self.dic_assistenciaTecnica allKeys].count;
    else
        return self.array_brands.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return  1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (self.textfield_city.isFirstResponder)
        return [self.dic_assistenciaTecnica allKeys][row];
    else
    {
        if ([self.textfield_city.text isEqualToString:@"MANAUS"])
            return [self.array_brands[row] valueForKey:@"brand"];
        else
            return [self.array_brands[row] valueForKey:@"MARCA"];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (self.textfield_city.isFirstResponder)
    {
        self.textfield_city.text = [self.dic_assistenciaTecnica allKeys][row];
        DLog(@"%@",[self.dic_assistenciaTecnica valueForKey:[self.dic_assistenciaTecnica allKeys][row]]);
        self.array_brands = [NSArray arrayWithArray:[self.dic_assistenciaTecnica valueForKey:[self.dic_assistenciaTecnica allKeys][row]]];
        
        if ([self.textfield_city.text isEqualToString:@"MANAUS"])
            self.textfield_brand.text = [self.array_brands[0] valueForKey:@"brand"];
        else
        self.textfield_brand.text = [self.array_brands[0] valueForKey:@"MARCA"];
    }
    else
    {
        if ([self.textfield_city.text isEqualToString:@"MANAUS"])
            self.textfield_brand.text = [self.array_brands[row] valueForKey:@"brand"];
        else
            self.textfield_brand.text = [self.array_brands[row] valueForKey:@"MARCA"];
        
        [self.view_brandDetails setHidden:NO];
        self.label_nome.text = [self.array_brands[row] valueForKey:@"nome"];
        if (![[self.array_brands[row] valueForKey:@"Endereco"] isKindOfClass:[NSNull class]])
        {
            self.label_Endereco.text = [self.array_brands[row] valueForKey:@"Endereco"];
        }
        else
        {
            self.label_Endereco.text = @"-";
        }
        if (![[self.array_brands[row] valueForKey:@"Telefone"] isKindOfClass:[NSNull class]])
        {
            self.label_Telefone.text = [self.array_brands[row] valueForKey:@"Telefone"];
        }
        else
        {
            self.label_Telefone.text = @"-";
        }
        self.label_Cidade.text = [self.array_brands[row] valueForKey:@"Cidade"];
        
        if (![[self.array_brands[row] valueForKey:@"Estado"] isKindOfClass:[NSNull class]])
        {
            self.label_Estado.text = [self.array_brands[row] valueForKey:@"Estado"];
        }
        else
        {
            self.label_Estado.text = @"-";
        }

    }
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
        [picker_ reloadAllComponents];
        return YES;
}


- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
