//
//  cell_Cart.m
//  CaptureLife
//
//  Created by Fahim Bilwani on 4/27/15.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "cell_Cart.h"

@implementation cell_Cart

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)action_details:(id)sender
{
    DLog(@"%ld",[sender tag]);
    [self.delegate method_details:(int)[sender tag]];
}

- (IBAction)action_Edit:(id)sender
{
    [self.delegate method_editObject:(int)[sender tag]];
}

- (IBAction)action_Remove:(id)sender
{
    [self.delegate method_deleteObject:(int)[sender tag]];
}

-(void)method_setValues:(OrderItem *)orderItem
{
    DLog(@"%@", orderItem);
    
    self.label_name.text = orderItem.productShortDescription;
    
    self.label_TotalPrice.text = [UtilitiesHelper method_currencyFormatter:orderItem.unitPrice];
    
    self.textfield_edit.text = [NSString stringWithFormat:@"%d", [orderItem.quantity intValue]];
   // self.label_TotalPrice.text = [UtilitiesHelper method_currencyFormatter:[self method_calculateTotalPrice:orderItem]];
    [self.image_product sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/wcsstore/Bemol/%@",k_imageurl,orderItem.productThumbnail]] placeholderImage:[UIImage imageNamed:k_placeHolder]];
}



-(NSString *)method_calculateTotalPrice:(OrderItem *)orderItem
{
    return [NSString stringWithFormat:@"%.2f",[orderItem.unitPrice floatValue] * [orderItem.quantity floatValue]];
}



@end
