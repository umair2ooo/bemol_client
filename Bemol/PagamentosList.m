#import "PagamentosList.h"

@implementation PagamentosList

@synthesize contractNumber;
@synthesize datePagamento;
@synthesize dateVencimiento;
@synthesize parcela;
@synthesize qtyParcela;
@synthesize status;
@synthesize valor;
@synthesize valorDesconto;

+ (PagamentosList *)instanceFromDictionary:(NSDictionary *)aDictionary {

    PagamentosList *instance = [[PagamentosList alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.contractNumber = [aDictionary objectForKey:@"ContractNumber"];
    self.datePagamento = [aDictionary objectForKey:@"Date_Pagamento"];
    self.dateVencimiento = [aDictionary objectForKey:@"Date_Vencimiento"];
    self.parcela = [aDictionary objectForKey:@"Parcela"];
    self.qtyParcela = [aDictionary objectForKey:@"Qty_Parcela"];
    self.status = [aDictionary objectForKey:@"Status"];
    self.valor = [aDictionary objectForKey:@"Valor"];
    self.valorDesconto = [aDictionary objectForKey:@"Valor_Desconto"];

}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.contractNumber) {
        [dictionary setObject:self.contractNumber forKey:@"contractNumber"];
    }

    if (self.datePagamento) {
        [dictionary setObject:self.datePagamento forKey:@"datePagamento"];
    }

    if (self.dateVencimiento) {
        [dictionary setObject:self.dateVencimiento forKey:@"dateVencimiento"];
    }

    if (self.parcela) {
        [dictionary setObject:self.parcela forKey:@"parcela"];
    }

    if (self.qtyParcela) {
        [dictionary setObject:self.qtyParcela forKey:@"qtyParcela"];
    }

    if (self.status) {
        [dictionary setObject:self.status forKey:@"status"];
    }

    if (self.valor) {
        [dictionary setObject:self.valor forKey:@"valor"];
    }

    if (self.valorDesconto) {
        [dictionary setObject:self.valorDesconto forKey:@"valorDesconto"];
    }

    return dictionary;

}


@end
