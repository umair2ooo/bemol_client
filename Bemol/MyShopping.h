#import <Foundation/Foundation.h>

@interface MyShopping : NSObject {

    NSMutableArray *pedidoList;
    NSMutableArray *produtoList;

}

@property (nonatomic, copy) NSMutableArray *pedidoList;
@property (nonatomic, copy) NSMutableArray *produtoList;

+ (MyShopping *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
