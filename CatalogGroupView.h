#import <Foundation/Foundation.h>

@interface CatalogGroupView : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *productsURL;
@property (nonatomic, strong) NSString *resourceId;
@property (nonatomic, strong) NSString *thumbnail;
@property (nonatomic, strong) NSString *uniqueID;


@end
