#import "VC_signUp.h"



@interface VC_signUp ()<UIWebViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UIPickerView *picker_;
    NSArray *array_Gender;
}

@property(readwrite)BOOL bool_isCEP_verified;
@property(readwrite)BOOL bool_isCPF_verified;

@property (weak, nonatomic) IBOutlet UIWebView *webview_;


@property (weak, nonatomic) IBOutlet UIButton *button_submit;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UITextField *textField_nomeComplexto;
@property (weak, nonatomic) IBOutlet UITextField *textField_selectDate;
@property (weak, nonatomic) IBOutlet UITextField *textField_numeroDeCellular;
@property (weak, nonatomic) IBOutlet UITextField *textfield_CPF;
@property (weak, nonatomic) IBOutlet UITextField *textField_RG;
@property (weak, nonatomic) IBOutlet UITextField *textField_email;
@property (weak, nonatomic) IBOutlet UITextField *textField_repetirSenha;
@property (weak, nonatomic) IBOutlet UITextField *textfield_CEP;
@property (weak, nonatomic) IBOutlet UITextField *senha;
@property (weak, nonatomic) IBOutlet UITextField *textField_bairro;
@property (weak, nonatomic) IBOutlet UITextField *textField_cidade;
@property (weak, nonatomic) IBOutlet UITextField *textField_pontoDeRefer;
@property (weak, nonatomic) IBOutlet UIButton *button_checkBox_email;
@property (weak, nonatomic) IBOutlet UIButton *button_checkBox_SMS;
@property (weak, nonatomic) IBOutlet UITextField *textField_endereco;
@property (weak, nonatomic) IBOutlet UITextField *textField_numero;
@property (weak, nonatomic) IBOutlet UITextField *textField_repetirEmail;
@property (weak, nonatomic) IBOutlet UITextField *textfield_sexo;
@property (weak, nonatomic) IBOutlet UITextField *textfield_complemento;
@property (weak, nonatomic) IBOutlet UITextField *textfield_country;
@property (weak, nonatomic) IBOutlet UITextField *textfield_state;


- (IBAction)action_cancel:(id)sender;
- (IBAction)action_submit:(id)sender;
- (IBAction)action_email:(id)sender;
- (IBAction)action_SMS:(id)sender;

@end

@implementation VC_signUp


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.bool_isCEP_verified = false;
    self.bool_isCPF_verified = false;

//    [[ServiceModel sharedClient] POST:k_guestLogin
//                           parameters:nil
//                               onView:self.view
//                              success:^(AFHTTPRequestOperation *operation, id responseObject)
//    {
//        if (responseObject && ![responseObject valueForKey:@"errors"])
//        {
////            ApplicationDelegate.single.string_WCToken = [responseObject valueForKey:@"WCToken"];
////            ApplicationDelegate.single.string_WCTrustedToken = [responseObject valueForKey:@"WCTrustedToken"];
////            ApplicationDelegate.single.string_personalizationID = [responseObject valueForKey:@"personalizationID"];
////            ApplicationDelegate.single.string_userId = [responseObject valueForKey:@"userId"];
//        }
//        else
//        {
//            [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
//            
//            [self dismissViewControllerAnimated:YES completion:nil];
//        }
//    }
//                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
//    {
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }];

    [self.scroller setContentSize:CGSizeMake(0,
                                             self.button_submit.frame.size.height+self.button_submit.frame.origin.y)];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(updatedateTextField:)
         forControlEvents:UIControlEventValueChanged];
    [self.textField_selectDate setInputView:datePicker];
    
    picker_ = [[UIPickerView alloc] init];
    [picker_ setDelegate:self];
    [picker_ setDataSource:self];
    self.textfield_sexo.inputView = picker_;
    array_Gender= [[NSArray alloc]initWithObjects:@"Masculino",@"Feminino", nil];
    
    self.button_checkBox_email.selected = true;
    self.button_checkBox_SMS.selected = true;
}


- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

#pragma mark - updatedateTextField
-(void)updatedateTextField:(UIDatePicker *)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    self.textField_selectDate.text = [dateFormat stringFromDate:sender.date];
    dateFormat = nil;
}

#pragma mark - Actions
- (IBAction)action_cancel:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)action_submit:(id)sender
{
    if ([self.textField_nomeComplexto.text length])
    {
        if ([self.textfield_sexo.text length])
        {

            if ([self.textField_selectDate.text length])
            {
                if ([self.textField_numeroDeCellular.text length])
                {
                    if ([self.textfield_CPF.text length] && self.bool_isCPF_verified)
                    {
                        if ([self.textField_RG.text length])
                        {
                            if ([self.textField_email.text length] &&
                                [ApplicationDelegate.single method_NSStringIsValidEmail:self.textField_email.text] &&
                                [self.textField_email.text isEqualToString:self.textField_repetirEmail.text])
                            {
                                if ([self.senha.text length])
                                {
                                    if ([UtilitiesHelper checkPassword:self.senha.text])
                                    {
                                        if ([self.textField_repetirSenha.text length] && [self.textField_repetirSenha.text isEqualToString:self.senha.text])
                                        {
                                            if ([self.textfield_CEP.text length] && self.bool_isCEP_verified)
                                            {
                                                if ([self.textField_pontoDeRefer.text length])
                                                {
                                                    if ([self.textField_numero.text length])
                                                    {
                                                        [self method_hitWebServiceForRegistration];
                                                    }
                                                    else
                                                    {
                                                        [[[UIAlertView alloc] initWithTitle:nil
                                                                                    message:@"Enter número"
                                                                                   delegate:nil
                                                                          cancelButtonTitle:@"OK"
                                                                          otherButtonTitles:nil, nil]
                                                         show];
                                                    }
                                                }
                                                else
                                                {
                                                    [[[UIAlertView alloc] initWithTitle:nil
                                                                                message:@"Enter ponto De Referencia"
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil, nil]
                                                     show];
                                                }
                                            }
                                            else
                                            {
                                                [[[UIAlertView alloc] initWithTitle:nil
                                                                            message:@"o valor inserido no campo cep nao e vallido.o cep deve ter 8 a 9 caracteres"
                                                                           delegate:nil
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil, nil]
                                                 show];
                                            }
                                        }
                                        else
                                        {
                                            [[[UIAlertView alloc] initWithTitle:nil
                                                                        message:@"As senhas inseridas não combinam."
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil, nil]
                                             show];
                                        }
                                    }
                                    else
                                    {
                                        [[[UIAlertView alloc] initWithTitle:nil
                                                                    message:@"Digite sua senha De 6 a 15 caracteres, lembre-se deve conter ao menos uma letra e um dígito"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil, nil]
                                         show];
                                    }
                                }
                                else
                                {
                                    [[[UIAlertView alloc] initWithTitle:nil
                                                                message:@"Enter senha"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil, nil]
                                     show];
                                }
                            }
                            else
                            {
                                [[[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Os campos E-mail e Confirmação de e-mail não conferem."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil, nil]
                                 show];
                            }
                        }
                        else
                        {
                            [[[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Digite RG"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil]
                             show];
                        }
                    }
                    else{
                        [[[UIAlertView alloc] initWithTitle:nil
                                                    message:@"CPF inválido"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil, nil]
                         show];
                    }
                }
                else
                {
                    [[[UIAlertView alloc] initWithTitle:nil
                                                message:@"Enter Número De Cellular"
                                               delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil, nil]
                     show];
                }
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:nil
                                        message:@"Selecionar Data"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
                 show];
            }
    }
    else{
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Selecione o sexo"
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil]
         show];
    }
}
    else{
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Enter Nome Complexto"
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil]
         show];
    }
}

- (IBAction)action_email:(id)sender
{
    if (self.button_checkBox_email.selected == true){
        self.button_checkBox_email.selected = false;
    }
    else{
        self.button_checkBox_email.selected = true;
    }
}

- (IBAction)action_SMS:(id)sender
{
    if (self.button_checkBox_SMS.selected == true)
    {
        self.button_checkBox_SMS.selected = false;
    }
    else
    {
        self.button_checkBox_SMS.selected = true;
    }
}

#pragma mark - method_hitWebServiceForRegistration
-(void)method_hitWebServiceForRegistration
{
    NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:
                              self.textField_email.text,@"logonId",
                              self.senha.text, @"logonPassword",
                              self.senha.text, @"logonPasswordVerify",
                              self.textField_nomeComplexto.text, @"firstName",
                              @"-", @"lastName",
                              [NSArray arrayWithObjects:self.textField_endereco.text, nil], @"addressLine",
                              self.textField_cidade.text, @"city",
                              self.textfield_state.text, @"state",
                              self.textfield_country.text, @"country",
                              @"69005-901", @"zipCode",
                              self.textfield_sexo.text, @"gender",
                              self.textField_email.text, @"email1",
                              self.textField_numeroDeCellular.text, @"mobilePhone1",
                              [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:k_storeId,@"storeID", self.button_checkBox_email.selected?@"true":@"false", @"value", nil], nil], @"receiveEmailPreference",
                              self.button_checkBox_SMS.selected? @"true":@"false", @"receiveSMSNotification",
                              [NSArray arrayWithObjects:k_storeId,@"storeID", self.button_checkBox_SMS.selected? @"true":@"false", @"value", nil], @"receiveSMSPreference",
                              @"Consumer", @"profileType",
                              @"-", @"challengeQuestion",
                              @"-", @"challengeAnswer",
                              self.textField_selectDate.text, @"dateOfBirth",
                              self.textfield_CPF.text, @"x_demographicField7",
                              self.textField_RG.text, @"x_demographicField5",
                              self.textField_bairro.text, @"x_addressField1",
                              self.textField_numero.text, @"x_addressField2",
                              self.textField_pontoDeRefer.text, @"x_addressField3",
                              
                              nil];
    
    DLog(@"%@", dic_temp);

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_WCToken];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_WCTrustedToken];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_personalizationID];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k_userId];
    
    
    [[ServiceModel sharedClient] POST:k_register
                           parameters:dic_temp
                               onView:self.view
                              success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        if (responseObject && ![responseObject valueForKey:@"errors"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_WCToken] forKey:k_WCToken];
            [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_WCTrustedToken] forKey:k_WCTrustedToken];
            [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_personalizationID
                                                              ] forKey:k_personalizationID];
            [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:k_userId] forKey:k_userId];
            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:k_isLogin];

            [UtilitiesHelper showAlert:@"Registro realizado com sucesso!"];

            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
        }
    }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        //[UtilitiesHelper showAlert:error.description];
        [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
    }];
    
}



#pragma mark - method_checkCPF
-(void)method_checkCPF{
    
    DLog(@"%@", [NSString stringWithFormat:@"%@%@%@", k_url, k_CPF, self.textfield_CPF.text]);
    
        [[ServiceModel sharedClient] GET:[NSString stringWithFormat:@"%@%@", k_CPF, self.textfield_CPF.text]
                              parameters:nil
                                  onView:self.view
                                 success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             if (responseObject)
             {
                 if ([[responseObject valueForKey:@"existsCpf"] boolValue] == true)
                 {
                     self.bool_isCPF_verified = false;
                     [UtilitiesHelper showAlert:@"O CPF informado não é válido!"];
                 }
                 else
                 {
                     self.bool_isCPF_verified = true;
                 }
             }
             else
             {
                 self.bool_isCPF_verified = false;
                 [UtilitiesHelper showAlert:@"O CPF informado não é válido!"];
             }
             
         }
                                 failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             //[UtilitiesHelper showAlert:error.description];
             [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
         }];
}


#pragma mark - method_checkCEP
-(void)method_checkCEP
{
    DLog(@"%@", [NSString stringWithFormat:@"%@%@%@", k_url, k_CEP, self.textfield_CEP.text]);
    
    [[ServiceModel sharedClient] GET:[NSString stringWithFormat:@"%@%@",k_CEP,self.textfield_CEP.text] parameters:nil onView:self.view
                             success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        
        if (responseObject && ![responseObject valueForKey:@"errors"])
        {
            self.bool_isCEP_verified = true;
            
            self.textField_endereco.text  = [responseObject valueForKey:@"endereco"];
            self.textField_bairro.text = [responseObject valueForKey:@"bairro"];
            self.textField_cidade.text  = [responseObject valueForKey:@"cidade"];
            self.textfield_state.text = [responseObject valueForKey:@"estado"];
        }
        else
        {
            if ([responseObject valueForKey:@"errors"])
            {
                [UtilitiesHelper showAlert:[[[responseObject valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]];
            }
            else
            {
                [UtilitiesHelper showAlert:@"CEP inválido"];
            }
        }
    }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        //[UtilitiesHelper showAlert:error.description];
        [UtilitiesHelper showAlert:@"Há algo de errado, do lado do servidor, por favor tente novamente"];
    }];
}



#pragma mark - textField delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField isEqual:self.textfield_CPF])
    {
        NSString *text = textField.text;
        if (range.location == 14)
        {
            return NO;
        }
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"." withString:@""];
        NSMutableString *mutableText = [text mutableCopy];
        if (mutableText.length > 3)
        {
            [mutableText insertString:@"." atIndex:3];
        }
        if (mutableText.length > 7)
        {
            [mutableText insertString:@"." atIndex:7];
        }
        if (mutableText.length > 11)
        {
            [mutableText insertString:@"-" atIndex:11];
        }
        text = mutableText;
        textField.text = text;
        return NO;
    }
    else if([textField isEqual:self.textfield_CEP])
    {
        NSString *text = textField.text;
        if (range.location == 9)
        {
            return NO;
        }
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSMutableString *mutableText = [text mutableCopy];
        if (mutableText.length > 5)
        {
            [mutableText insertString:@"-" atIndex:5];
        }
        text = mutableText;
        textField.text = text;
        return NO;
    }
     else if([textField isEqual:self.textField_numeroDeCellular])
     {
         if (range.location == 11)
         {
             return NO;
         }
     }
     else if([textField isEqual:self.textField_RG])
     {
         //NSString *text = textField.text;
         if (range.location == 8)
         {
             return NO;
         }
//         text = [text stringByReplacingCharactersInRange:range withString:string];
//         text = [text stringByReplacingOccurrencesOfString:@"-" withString:@""];
//         NSMutableString *mutableText = [text mutableCopy];
//         if (mutableText.length > 6)
//         {
//             [mutableText insertString:@"-" atIndex:6];
//         }
//         text = mutableText;
//         textField.text = text;
//         return NO;
     }
    else if ([textField isEqual:self.textField_numero])
    {
        if (range.location == 4)
        {
            return NO;
        }
    }
        return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:self.textfield_CEP])
    {
        DLog(@"hit CEP validation service here");
        if ([self.textfield_CEP.text length])
        {
            [self method_checkCEP];
        }
        else
        {
            [UtilitiesHelper showAlert:@"Por favor, indique CEP"];
        }
    }
    else if ([textField isEqual:self.textfield_CPF])
    {
        DLog(@"hit CPF validation service here");
        if ([self.textfield_CPF.text length])
        {
            [self method_checkCPF];
        }
        else
        {
            [UtilitiesHelper showAlert:@"Por favor, indique CPF"];
        }
    }
}

#pragma mark - picker delegate

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return array_Gender.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return  1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return array_Gender[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.textfield_sexo.text = array_Gender[row];
}
@end
